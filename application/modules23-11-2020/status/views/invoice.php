<!DOCTYPE html>
<html>
<head>
  <title>Invoice - Celerity Supply Chain </title>
</head>
<body style="margin:0 15px;">
  <table cellpadding="0" cellspacing="0" style="width:100%;margin:30px 0;">
    <tr>
      <td>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0"><strong>Celerity India Marketing Services</strong></p>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0">1505, Darshan Heights, Opp Deepak Cinema, Off Tulsipipe
          Road, Elphinstone West,</p>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0">Mumbai 400013 </p>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0">Maharashtra, India </p>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0">GSTIN: 27ABRPB4644F1ZN</p>
      </td>
      <td align="right">
        <img src="<?= base_url('images/logo_invoice.jpg')?>" alt="">
      </td>
    </tr>
  </table>
  <table cellpadding="0" cellspacing="0" style="width:100%;margin:30px 0;">
    <tr>
      <td>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0"><strong>Billing to</strong></p>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0"><?= $billing_detail[0]['first_name'].' '.$billing_detail[0]['last_name']?>,</p>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0"><?= $billing_detail[0]['billing_address']?>,</p>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0"><?= $billing_detail[0]['city_name']?> - <?= $billing_detail[0]['zipcode']?></p>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0"><?= $billing_detail[0]['state_name']?>, <?= $billing_detail[0]['country_name']?></p>
      </td>
      <td align="right" style="vertical-align: top;">
        <p style="font-size:16px;margin:0 0 5px 0;padding:0"><strong>Tax Invoice</strong></p>
        <p style="font-size:14px;margin:0 0 30px 0;padding:0"># <?= $invoice_no ?></p>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0"><strong>Invoice Date</strong></p>
        <p style="font-size:14px;margin:0 0 5px 0;padding:0"><?= date("d M, Y")?></p>
      </td>
    </tr>
  </table>
  <table cellpadding="0" cellspacing="0" style="width:100%;margin:30px 0;">
    <tr>
      <td style="background-color:#000000;color:#ffffff;padding:10px;text-align:left;">#</td>
      <td style="background:#000000;color:#ffffff;padding:10px;text-align:left;">Item & Description</td>
      <td style="background-color:#000000;color:#ffffff;padding:10px;text-align:left;">Qty</td>
      <td style="background-color:#000000;color:#ffffff;padding:10px;text-align:left;">Rate</td>
      <td style="background-color:#000000;color:#ffffff;padding:10px;text-align:left;">Discount</td>
      <?php  if($billing_detail[0]['state'] == '20'){?>
            <td style="background-color:#000000;color:#ffffff;padding:10px;text-align:left;">CGST</td>
            <td style="background-color:#000000;color:#ffffff;padding:10px;text-align:left;">SGST</td>
      <?php } else { ?>
            <td style="background-color:#000000;color:#ffffff;padding:10px;text-align:left;">IGST</td>
            <td style="background-color:#000000;color:#ffffff;padding:10px;text-align:left;"></td>
      <?php }?>
  
      <td style="background-color:#000000;color:#ffffff;padding:10px 30px 10px 10px;text-align:right;">Amount</td>
    </tr>
    <tr>
      <td style="font-size:14px;padding:10px;border-bottom:1px solid #cccccc">1</td>
      <td style="font-size:14px;padding:10px;border-bottom:1px solid #cccccc"><strong><?= $package_detail[0]['title']?>
      </strong><br/>Valid for <?= $package_detail[0]['duration']?> months</td>
      <td style="font-size:14px;padding:10px;border-bottom:1px solid #cccccc">1</td>
      <td style="font-size:14px;padding:10px;border-bottom:1px solid #cccccc"><?= $package_detail[0]['price']?></td>
      <td style="font-size:14px;padding:10px;border-bottom:1px solid #cccccc">0</td>
     <?php  if($billing_detail[0]['state'] == '20'){?>
          <td style="font-size:14px;padding:10px;border-bottom:1px solid #cccccc"><?= (($package_detail[0]['price']*$package_detail[0]['gst_cgst'])/100) ?> (<?= $package_detail[0]['gst_cgst']?> %)</td>
         <td style="font-size:14px;padding:10px;border-bottom:1px solid #cccccc"><?= (($package_detail[0]['price']*$package_detail[0]['gst_sgst'])/100) ?> (<?= $package_detail[0]['gst_sgst']?> %)</td>
     <?php }else{?>
           <td style="font-size:14px;padding:10px;border-bottom:1px solid #cccccc"><?= (($package_detail[0]['price']*$package_detail[0]['gst_igst'])/100) ?> (<?= $package_detail[0]['gst_igst']?> %)</td>
           <td style="font-size:14px;padding:10px;border-bottom:1px solid #cccccc"></td>
      <?php }?>

      <td style="font-size:14px;padding:10px 30px 10px 10px;border-bottom:1px solid #cccccc;text-align:right"><?=$package_detail[0]['price'] ?></td>
    </tr>
    <tr>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:20px 30px 20px 10px;border-bottom:1px solid #cccccc;text-align:right">Sub Total <?= $package_detail[0]['price']?></td>
    </tr>
    <tr>
      <td style="font-size:14px;padding:10px" colspan="7">
        <p style="margin:0;padding:20px 0;"><strong>Total In Words:</strong> <?= $total_amt_in_words?> </p>
      </td>      
      <td style="font-size:14px;padding:20px 30px 20px 10px;border-bottom:1px solid #cccccc;text-align:right">
      <?php 
        if($billing_detail[0]['state'] == '20'){?>
            
            <p style="margin:0 0 10px 0;">CGST ( <?= $package_detail[0]['gst_cgst']?>%) <?= (($package_detail[0]['price']*$package_detail[0]['gst_cgst'])/100)?></p>
            <p style="margin:0;">SGST (<?= $package_detail[0]['gst_sgst']?> %) <?= (($package_detail[0]['price']*$package_detail[0]['gst_sgst'])/100)?></p>
        <?php }else{ ?>
            <p style="margin:0;">IGST (<?= $package_detail[0]['gst_igst']?> %) <?= (($package_detail[0]['price']*$package_detail[0]['gst_igst'])/100)?></p>
            <p style="margin:0;"></p>
        <?php } ?>
        
      </td>
    </tr>
    <tr>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:10px"></td>
      <td style="font-size:14px;padding:20px 30px 20px 10px;text-align:right">
          <strong>Total &#x20B9; <?= $total_amt?></strong>
      </td>
    </tr>
  </table>
  <p style="font-size:14px;padding:30px 10px 0 10px;margin:40px 0 0 0;border-top:1px solid #cccccc;">Please Note: All payments are made in the name of Celerity India Marketing Services - PAN No.ABRPB4644F. This is a computer generated invoce and it does not require a signature.<br/> For any queries, please contact us at <a href="mailto:tech@celerityin.com">tech@celerityin.com</a></p>
</body>

</html>