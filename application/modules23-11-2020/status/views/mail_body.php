<table cellspacing="0" cellpadding="0" border="0" style="background:#fff;width:100%;">
	<tr>
		<td valign="top" align="center">
			<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
				<tr>
					<td valign="top" align="center">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
							<tr>
								<td valign="top" align="center" style="padding:20px 0;background:#0f3f93">
									<a href="#" target="_blank" style="outline:none;padding-bottom:15px;">
										<img src="<?= base_url('images/logo_mail_body.png')?>" border="0" alt="" title="" />
									</a>
								</td>
							</tr>
							<tr>
								<td valign="top" align="center" style="padding:20px 0;background:#363636">
									<p style="font:normal 22px calibri;font-weight:bold;margin:0 0 10px 0;color:#00aeef">Thank you!</p>
									<p style="font:normal 16px calibri;margin:0 0 10px 0;color:#fff;"><i>Your membership has been confirmed</i></p>
								</td>
							</tr>
							<tr>
								<td valign="top" align="left" style="padding:20px 30px;background:#ffffff">
									<p style="font:normal 16px calibri;margin:0 0 10px 0;color:#000;">
										Dear <strong><?= $billing_detail[0]['first_name'].' '.$billing_detail[0]['last_name']?></strong>,
									</p>
									<p style="font:normal 16px calibri;margin:0 0 10px 0;color:#000;">Welcome to the community!</p>
									<p style="font:normal 16px calibri;margin:0 0 10px 0;color:#000;">We're excited to have you with us. We look forward to seeing you on the website. Get started by interacting on the <a href="#" style="color:#00aeef;font-weight: bold;">Forum</a>, writing your own <a href="#" style="color:#00aeef;font-weight: bold;">Blogs</a> and reading our set of exclusive <a href="#" style="color:#00aeef;font-weight: bold;">Articles</a></p>
									<p style="font:normal 16px calibri;margin:0 0 10px 0;color:#000;">Thank you for subscribing to us. Your account is now <strong>active</strong>. We are enclosing a copy of your invoice for your reference</p>
								</td>
							</tr>
					
						</table>
						<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;">
							<tr>
								<td width="75%" valign="top" align="left" style="background:#efeeee;padding:20px;">
									<p style="font:normal 16px calibri;margin:0 0 10px 0;color:#000;"><strong>Bill to</strong></p>	
									<p style="font:normal 16px calibri;margin:0 0 10px 0;color:#000;"><?= $billing_detail[0]['first_name'].' '.$billing_detail[0]['last_name']?></p>
									<p style="font:normal 16px calibri;margin:0 0 10px 0;color:#000;"><?= $billing_detail[0]['billing_address']?>,<?= $billing_detail[0]['city_name']?> - <?= $billing_detail[0]['zipcode']?>,<?= $billing_detail[0]['state_name']?>, <?= $billing_detail[0]['country_name']?></p>
								</td>
								<td valign="top" align="left" style="background:#efeeee;padding:20px;">
									<p style="font:normal 16px calibri;margin:0 0 10px 0;color:#000;"><strong>Invoice #</strong></p>
									<p style="font:normal 16px calibri;margin:0 0 30px 0;color:#000;"><?= $invoice_no ?></p>	
									<p style="font:normal 16px calibri;margin:0 0 30px 0;color:#000;"><?= date("d M, Y")?></p>
								</td>
							</tr>
						</table>
						<table width="100%" cellspacing="0" cellpadding="0" border="0" style="background:#fff;max-width:800px;margin:30px 0">
							<tr>
								<td valign="top" align="left" colspan="2">
									<p style="font:normal 16px calibri;padding:0 0 10px 0;margin:0 0 10px 0;color:#000;border-bottom:1px solid #ccc;"><strong>Order Details</strong></p>
								</td>
							</tr>
							<tr>
								<td width="75%" valign="top" align="left" style="border-bottom:1px solid #ccc;">
									<p style="font:normal 16px calibri;padding:0 0 10px 0;margin:0 0 10px 0;color:#000;"><strong>Membership Subscription</strong> x 1<br/>Valid for <?= $package_detail[0]['duration']?> months</p>
								</td>
								<td valign="top" align="right" style="border-bottom:1px solid #ccc;">
									<p style="font:normal 16px calibri;padding:0 0 10px 0;margin:0 0 10px 0;color:#000;"><strong>Rs <?= $package_detail[0]['price']?></strong></p>
								</td>
							</tr>
							<tr>
								<td width="75%" valign="top" align="left">
									<p style="font:normal 16px calibri;padding:10px 0;margin:0;color:#000;"><strong>Sub Total</strong></p>
								</td>
								<td valign="top" align="right">
									<p style="font:normal 16px calibri;padding:10px 0;margin:0;color:#000;"><strong>Rs <?=$package_detail[0]['price']?></strong></p>
								</td>
                            </tr>

                            <?php  if($billing_detail[0]['state'] == '20'){?>
                                <tr>
                                    <td width="75%" valign="top" align="left" style="border-bottom:1px solid #ccc;">
                                        <p style="font:normal 16px calibri;padding:10px 0;margin:0;color:#000;"><strong>CGST</strong></p>
                                    </td>
                                    <td valign="top" align="right" style="border-bottom:1px solid #ccc;">
                                        <p style="font:normal 16px calibri;padding:10px 0;margin:0;color:#000;"><strong>Rs <?= (($package_detail[0]['price']*$package_detail[0]['gst_cgst'])/100) ?> </strong></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="75%" valign="top" align="left" style="border-bottom:1px solid #ccc;">
                                        <p style="font:normal 16px calibri;padding:10px 0;margin:0;color:#000;"><strong>SGST</strong></p>
                                    </td>
                                    <td valign="top" align="right" style="border-bottom:1px solid #ccc;">
                                        <p style="font:normal 16px calibri;padding:10px 0;margin:0;color:#000;"><strong>Rs <?= (($package_detail[0]['price']*$package_detail[0]['gst_sgst'])/100) ?> </strong></p>
                                    </td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td width="75%" valign="top" align="left" style="border-bottom:1px solid #ccc;">
                                        <p style="font:normal 16px calibri;padding:10px 0;margin:0;color:#000;"><strong>IGST</strong></p>
                                    </td>
                                    <td valign="top" align="right" style="border-bottom:1px solid #ccc;">
                                        <p style="font:normal 16px calibri;padding:10px 0;margin:0;color:#000;"><strong>Rs <?= (($package_detail[0]['price']*$package_detail[0]['gst_igst'])/100) ?></strong></p>
                                    </td>
                                </tr>
                            <?php }?>


						
							<tr>
								<td width="75%" valign="top" align="left">
									<p style="font:normal 16px calibri;padding:10px 0;margin:0;color:#000;"><strong>Total</strong></p>
								</td>
								<td valign="top" align="right">
									<p style="font:normal 16px calibri;padding:10px 0;margin:0;color:#000;"><strong>Rs <?= $total_amt?></strong></p>
								</td>
							</tr>
							<tr>
								<td valign="top" align="left" colspan="2">
									<p style="font:normal 16px calibri;padding:0 0 10px 0;margin:30px 0 10px 0;color:#000;"><strong>Please note</strong>: This is an electronically generated invoice and does not require signature. All terms and conditions are <a href="#" style="color:#00aeef;font-weight: bold;">defined on our website</a>.</p>
									<p style="font:normal 16px calibri;padding:0 0 10px 0;margin:0 0 30px 0;color:#000;">Please feel free to <a href="#" style="color:#00aeef;font-weight: bold;">contact us</a> if you have any queries</p>
									<p style="font:normal 16px calibri;padding:0;margin:0 0 5px 0;color:#000;">Best Regards</p>
									<p style="font:normal 16px calibri;padding:0;margin:0 0 5px 0;color:#000;"><strong>The Celerity Supply Chain Team</strong>></p>
									<p style="font:normal 16px calibri;padding:0;margin:0 0 30px 0;color:#000;"><a href="https://www.supplychaintribe.com/" style="color:#00aeef;" target="_blank">supplychain.celerityin.com</a></p>
								</td>
							</tr>
							<tr>
								<td valign="top" colspan="2" align="left" style="padding:20px;background:#363636">
									<p style="font:normal 16px calibri;margin:0;padding:0 0 20px 0;color:#fff;border-bottom:1px solid #fff;">This account notification was sent to you because you have signed up as a user on our platform. If you believe that you should not be receiving this notification, please contact us.</p>							
								</td>
							</tr>
							<tr>
								<td width="50%" valign="top" align="left" style="padding:0 20px 20px 20px;background:#363636">
									<p style="font:normal 16px calibri;padding:0;margin:0;color:#fff;"><strong>Celerity India Marketing Services</strong></p>
									<p style="font:normal 14px calibri;padding:0;margin:0;color:#fff;">Mumbai</p>
									<p style="font:normal 14px calibri;padding:0;margin:0;color:#fff;">400013 Maharashtra, India</p>
								</td>
								<td valign="top" align="right" style="padding:0 20px 20px 20px;background:#363636">
									<p style="font:normal 16px calibri;padding:10px 0;margin:0;color:#000;"><a href="#/" target="_blank"><img src="<?= base_url('images/linkedin.png')?>" alt=""></a></p>
								</td>
							</tr>
							<tr>
								<td valign="top" align="center" colspan="2" style="padding:20px;background:#000;">
									<p style="font:normal 14px calibri;padding:0;margin:0;color:#fff;">Copyright © 2020 Celerity India Marketing Services. All Rights Reserved</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>