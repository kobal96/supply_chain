<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login  extends MX_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('loginmodel','',TRUE);
		$this->load->library('form_validation'); 
		if($this->session->userdata('supply_chain_user'))
			redirect(base_url().'home/index');
		
	}

	public function index()
	{
		include_once APPPATH . "libraries/vendor/autoload.php";
	  $google_client = new Google_Client();
	  $google_client->setClientId('1073495410009-tja9b7egbaas2fd37n8g2jcis9gfvsk9.apps.googleusercontent.com'); //Define your ClientID
	  $google_client->setClientSecret('RVkpTEpXQhhlw5P_w21AM0b_'); //Define your Client Secret Key
	  $google_client->setRedirectUri('http://supplychain.webshowcase-india.com/google_login/login'); //Define your Redirect Uri
	  $google_client->addScope('email');
	  $google_client->addScope('profile');
	  if(isset($_GET["code"]))
	  {
	   $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
	   if(!isset($token["error"]))
	   {
	    $google_client->setAccessToken($token['access_token']);
	    $this->session->set_userdata('access_token', $token['access_token']);
	    $google_service = new Google_Service_Oauth2($google_client);
	   }
	  }
	  $data['login_button'] = '';
	  if(!$this->session->userdata('access_token'))
	  {
	   $data['login_button'] = $google_client->createAuthUrl();
	   
	  }
		$this->load->view('header');
		$this->load->view('login',$data);
		$this->load->view('footer');
	}

	public function form_validate(){
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if($this->form_validation->run()) {  
			//true  if validation property sectifice properly
			$email = $this->input->post('username');  
			$password = $this->input->post('password'); 
			$condition = "(username = '".$email."' OR email = '".$email."') AND password = md5('".$password."')";
			$result = $this->common->getData('tbl_users','*',$condition);
			
			if($result){
				if ($result[0]['verified']!='YES') {
					$this->session->set_flashdata('error', 'Please Verify your email account to login');  
                    redirect(base_url().'login/index');	
				}else if($result[0]['status']!='Active' && $result[0]['admin_status']='Pending'){
					$this->session->set_flashdata('error', 'You have been deactivated by admin please contact to  administration ');  
                    redirect(base_url().'login/index');	
				}else{
				   if (isset($_POST['remember'])) {
					setcookie('fusername', $email, time() + (86400 * 30 + 360), "/");
				    setcookie('fpassword', $password, time() + (86400 * 30 + 360), "/");
					}else{
						setcookie('fusername', '', time() + (86400 * 30 + 360), "/");
					    setcookie('fpassword', '', time() + (86400 * 30 + 360), "/");
					}
				   $session_data = array('supply_chain_user'=>$result);  
			       $this->session->set_userdata($session_data);

			       $this->session->set_flashdata('success_msg', 'login successfully');  
			       redirect(base_url('home'));
				}

				  
			}else{
			
				$this->session->set_flashdata('error', 'Invalid Username and Password');  
                redirect(base_url().'login/index');
			}
		}else{  
			// if validation is not proper
			$this->index();  
		}  

	}

}
?>