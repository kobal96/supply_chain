<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Billing extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('billingmodel','',TRUE);
	}

	public function index(){
		
		$result = array();
		$userid = $this->session->userdata('supply_chain_user')[0]['id'];
		$condition = "user_id = '".$userid."' ";
		$result['billing'] = $this->common->getData("tbl_billing_information",'*',$condition);
		$billig = $result['billing'][0];

		$where = "status = '1' ";
		$result['country_list'] = $this->common->getData("tbl_countries",'id, country_name',$where);

		// $where1 = "status = '1' ";
		// if (!empty($billig['country'])) {
		// 	$where1 .= " AND country_id = '".$billig['country']."' ";
		// }else{
		// 	$where1 .= " AND country_id = '103' ";
		// }
		// $result['state_list'] = $this->common->getData("tbl_states",'id, state_name',$where1);

		// $where2 = "city_status = '1' ";
		// if (!empty($billig['country']) && !empty($billig['state'])) {
		// 	$where2 .= " AND country_id = '".$billig['country']."' AND state_id = '".$billig['state']."' ";
		// }else{
		// 	$where2 .= " AND country_id = '103' ";
		// }
		// $result['city_list'] = $this->common->getData("tbl_city",'id, city_name',$where2);

		$condition = "1=1";
		$result['packages'] = $this->common->getData("tbl_packages",'*',$condition, "duration", "ASC");
		// echo "<pre>"; print_r($result);exit;

		$this->load->view('header');
		$this->load->view('index',$result);
		$this->load->view('footer');
	}


	function getCities(){
		$result = $this->common->getdata("tbl_city","*"," state_id = ".$_POST['state_id']."  ");
		$option = '';
			if(!empty($result)){
				foreach ($result as $key => $value) {
					$sel='';
					$sel = ($value['id'] == $_POST['city_id'])? 'selected="selected"' : '';
					$option .= '<option value='.$value['id'].' '.$sel.' >'.$value['city_name'].'</option>';
				}
				echo json_encode(array("status"=>"success","option"=>$option));
				exit;
			}
	}


	function getStates(){
		$result = $this->common->getdata("tbl_states","*"," country_id = ".$_POST['country_id']." && status ='1' ");
		$option = '';
			if(!empty($result)){
				foreach ($result as $key => $value) {
					$sel='';
					if(!empty($_POST['state_id'])){
						$sel = ($value['id'] == $_POST['state_id'])? 'selected="selected"' : '';
					}
					/* else{
						$sel = ($value['state_name'] == 'Maharashtra')? 'selected="selected"' : '';
					} */
					$option .= '<option value='.$value['id'].' '.$sel.' >'.$value['state_name'].'</option>';
				}
				echo json_encode(array("status"=>"success","option"=>$option));
				exit;
			}
	}

	public function submitForm(){
		// print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();

			$data['user_id']         = $this->session->userdata('supply_chain_user')[0]['id'];
			$data['first_name']      = $this->input->post('firstname');
			$data['last_name']       = $this->input->post('lastname');
			$data['billing_address'] = $this->input->post('address');
			$data['zipcode']         = $this->input->post('zipcode');
			$data['country']         = $this->input->post('country');
			$data['state']           = $this->input->post('state');
			$data['city']            = $this->input->post('city');
			$data['updated_at']      = date("Y-m-d H:i:s");
			
			if(!empty($this->input->post('user_id'))){
				$amount = $this->input->post('amount');
				$userid = $this->input->post('user_id');
				$this->session->set_userdata('amount', $amount);
				$this->session->set_userdata('bill', $userid);
				$condition = "id = '".$this->input->post('user_id')."' ";
				$result = $this->common->updateData("tbl_billing_information",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");

				$amount = $this->input->post('amount');
				
				$result = $this->common->insertData('tbl_billing_information',$data,'1');

				$this->session->set_userdata('amount', $amount);
				$this->session->set_userdata('bill', $result);

				if(!empty($result)){
					echo json_encode(array('success'=>true, 'id'=>$result, 'msg'=>'Create Billing successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	
}?>