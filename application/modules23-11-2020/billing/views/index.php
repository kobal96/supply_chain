<style type="text/css">
  .amount{
    display: none;
  }

  label.error {
    color: #F25961 !important;
    font-size: 80% !important;
    margin-top: .5rem;
  }
</style>
<section class="page-section">  
  <div class="billing-bg"></div>
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
        <div class="billing-wrapper mt-60 mb-60 mtmob-40 mtmob-40">
          <div class="billing-title">
            <h1>Select a package</h1>
          </div>
          <form class="billing-form" id="form-billing" method="post" enctype="multipart/form-data">
          <input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
          <div class="billing-plan-wrapper">
          <?php
          if (!empty($packages)) {
            foreach ($packages as $key => $value) {
              if ($value['duration']=='12') { ?>

              <div class="billing-plan-single checkamount most-popular active">
              <p class="most-popular-title">Most popular</p>
              <h3>₹<?=$value['price']?></h3>
              <input class="form-check-input amount" type="radio" name="amount" id="amount" value="<?=$value['id']?>" checked>
              <p class="billing-duration">for <?=$value['duration']?> months</p>
              </div>
               
            <?php }else{ ?>
               
            <div class="billing-plan-single checkamount">
              <h3>₹<?=$value['price']?></h3>
              <input class="form-check-input amount" type="radio" name="amount" id="amount" value="<?=$value['id']?>">
              <p class="billing-duration">for <?=$value['duration']?> months</p>
            </div>

          <?php } }
           } 
          ?>

          </div>
          <div class="billing-info-wrapper">
            <h3>Billing information</h3>

            <input name="user_id" id="user_id" value="<?= (!empty($billing[0]['id'])?$billing[0]['id']:"") ?>" type="hidden">
            <div class="row">
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">First Name</label>
                  <input type="text" class="form-control" id="firstname" name="firstname" placeholder="eg.John" value="<?= (!empty($billing[0]['first_name'])?$billing[0]['first_name']:"") ?>" required>                  
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">Last Name</label>
                  <input type="text" class="form-control" id="lastname" name="lastname" placeholder="eg. Raskin" value="<?= (!empty($billing[0]['last_name'])?$billing[0]['last_name']:"") ?>" required>                  
                </div>
              </div>
              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label for="">Billing Address</label>
                 <textarea name="address" id="address" cols="30" rows="4" class="form-control" placeholder="eg. 50  & , Hazari Baugh,  Th Rd, Jvpd Scheme, Opp Uco Bank, Juhu" id="billingaddress" name="billingaddress"  required> <?= (!empty($billing[0]['billing_address'])?$billing[0]['billing_address']:"") ?> </textarea>               
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">Country</label>
                  <select name="country" id="country" class="form-control selectclass"  onchange="getstate(this.value)" required>
                    <option value="" selected="selected" disabled>--- Select Country ---</option>
                    <?php
                    foreach ($country_list as $key => $value) {
                     ?>
                     <option value="<?=$value['id'] ?>" <?= (isset($billing[0]['country']) && $billing[0]['country'] == $value['id']) ? "selected" :""; ?> ><?=$value['country_name'] ?></option>
                    <?php } ?>                                       
                  </select>                   
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">State</label>
                  <select name="state" id="state" class=" form-control selectclass "  onchange="getcity(this.value)" required>
                  
                  </select>     
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">City</label>
                  <select name="city" id="city" class="form-control selectclass" required>
                    <option value="" selected="selected" disabled>--- Select City ---</option>
                    <?php
                    foreach ($city_list as $key => $value) {
                     ?>

                     <option value="<?=$value['id'] ?>" <?= (isset($billing[0]['city']) && $billing[0]['city'] == $value['id']) ? "selected" :""; ?> ><?=$value['city_name'] ?></option>

                    <?php } ?>                    
                  </select>                   
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">ZIP / PIN</label>
                  <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Enter ZIP/ PIN Code" value="<?= (!empty($billing[0]['zipcode'])?$billing[0]['zipcode']:"") ?>" required>                  
                </div>
              </div>
            </div>  
            <div class="form-check">
              <input type="checkbox" class="form-check-input" name="condition" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">I accept <a href="#/">Terms & Condition</a></label>
            </div>
            <button type="submit" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase">Proceed to payment</button>
            <button type="button" class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase">Cancel</button>
          </div>
           </form>

        </div>
      </div>      
    </div>
  </div>
</section>

<script>

<?php 
if($billing[0]){?>
  getstate(<?= $billing[0]['country'] ?>,<?= $billing[0]['state'] ?>);
  getcity(<?= $billing[0]['state'] ?>,<?= $billing[0]['city'] ?>);
<?php } ?>

$(document).ready(function(){
 
    $('.billing-form').validate({ignore: []});

    $( ".checkamount" ).click(function() {
     $(".amount").attr('checked', false);
     $(this).find(".amount").attr('checked', true);
    });

  });


  function getstate(country_id,state_id=null){
	if(country_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>billing/getStates",
			data:{country_id:country_id,state_id:state_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#state").html("<option value=''>Select State</option>"+res['option']);
						
					}else{
						$("#state").html("<option value=''>No state found</option>");
			
					}
				}
				/* else{	
					$("#state").html("<option value=''>Select</option>");
					$("#state").selectpicker("refresh");
				} */
			}
		});
	}
}


  var vRules = 
    {
      firstname:{required:true},
      lastname:{required:true},
      address:{required:true},
      country:{required:true},
      state:{required:true},
      city:{required:true},
      zipcode:{required:true},
      condition:{required:true},
      
    };
    var vMessages = 
    {
      firstname:{required:"Please Enter First Name."},
      lastname:{required:"Please Enter Last Name."},
      address:{required:"Please Enter Address."},
      country:{required:"Please Enter Country."},
      state:{required:"Please Enter State."},
      city:{required:"Please Enter City."},
      zipcode:{required:"Please Enter Zipcode."},
      condition:{required:"Please Check Terms & Condition."},

    };

    $("#form-billing").validate({
      ignore:[],
      rules: vRules,
      messages: vMessages,
      submitHandler: function(form) 
      { 
        var act = "<?= base_url('billing/submitForm')?>";
        $("#form-billing").ajaxSubmit({
          url: act, 
          type: 'post',
          dataType: 'json',
          cache: false,
          clearForm: false,
          async:false,
          beforeSubmit : function(arr, $form, options){
            $(".btn btn-black").hide();
          },
          success: function(response) 
          {
            if(response.success){
              swal("Done", response.msg, {
                icon : "success",
                buttons: {              
                  confirm: {
                    className : 'btn btn-success'
                  }
                },
              }).then(
              function() {
                window.location = "<?= base_url('payumoney')?>";
                
              });
            }else{  
              swal("Failed", response.msg, {
                icon : "error",
                buttons: {              
                  confirm: {
                    className : 'btn btn-danger'
                  }
                },
              });
            }
            
          }
        });

      }
    });

function getcity(state_id,city_id=null){
	if(state_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>billing/getCities",
			data:{state_id:state_id,city_id:city_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#city").html("<option value=''>Select City</option>"+res['option']);
					}else{
						$("#city").html("<option value=''>No state found</option>");

					}
				}else{	
					$("#city").html("<option value=''>Select</option>");
				}
			}
		});
	}
}
</script>