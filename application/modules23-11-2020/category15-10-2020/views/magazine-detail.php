
<?php 
if($magazine_detail){?>
<section class="page-section magazine-detail-bg">  
  <div class="magazine-detail-bg-mobile"></div>
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-6">
        <a href="javascript:void(0)"  onclick="return history.back()"  class="md-back md-mobile"><img src="<?= base_url('assets/')?>images/md-back.svg" alt=""> Back</a>
        <div class="magazine-detail-img">
          <img src="<?= base_url('images/')?>edition_image/<?=  (!empty($magazine_detail[0]['image'])?$magazine_detail[0]['image']:"default-img.jpg")?>" alt="" class="img-fluid">
        </div>        
      </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-6">
        <div class="magazine-detail-content">
            <a href="javascript:void(0)" onclick="return history.back()" class="md-back md-desktop"><img src="<?= base_url('assets/')?>images/md-back.svg" alt=""> Back</a>
            <p class="category-tag">Magazine Edition</p>
            <p class="magazine-publish-date"><?= $magazine_detail[0]['title']?></p>
            <div class="magazine-detail-description">
              <p><?= $magazine_detail[0]['magazine_description']?></p>
            </div> 

            <?php 
            if($magazine_detail[0]['edition_pdf']){?>
              <div class="magazine-detail-actions">
                <a href="<?= base_url('images/edition_pdf/'.$magazine_detail[0]['edition_pdf'])?>" target="_blank" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase">View</a>
                <a href="<?= base_url('images/edition_pdf/'.$magazine_detail[0]['edition_pdf'])?>" target="_blank"  class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase">Download</a>
                <a href="#/"><img src="<?= base_url('assets/')?>images/md-share.svg" alt=""></a>
              </div>
            <?php }
            ?>

        </div>
      </div>
    </div>
  </div>
</section>
<?php } ?>

<?php 

if($similar_magazine){?>
<section class="page-section similar-magazines mt-60 mtmob-40 mbmob-40">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section-title-medium ">You might also like</h3>
      </div>
      <?php 
        foreach ($similar_magazine as $key => $value) {?>
          <div class="col-6 col-sm-3">
            <div class="home-magazine-single magazine-list-single">
              <a href="<?= base_url('magazine/'.$value['slug'])?>">
                <img src="<?= base_url('images/')?>edition_image/<?= (!empty($value['image'])?$value['image']:"default-img.jpg")?>" alt="" class="img-fluid">
                <h4><?= $value['title']?></h4>
              </a>
            </div>
          </div>
        <?php } ?>
            
    </div>        
  </div>
</section>
<?php } ?>
