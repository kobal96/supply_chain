
<section class="page-section mt-60 mtmob-0 mttab-0">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-6 category-select-list-wrapper">        
        <div class="category-select-list">
          <select name="category_id" id="category_id"  onchange="clearSeachValue(),getArticleList()" class="category-select niceselect">
          <option value="">Select Category</option>
          <?php 
            if($all_categories){
              $sel='';
              foreach ($all_categories as $key => $value) {
                $sel = ($category_id[0]['id'] == $value['id']?"selected":"");?>
                <option value="<?= $value['id']?>" <?= $sel?>><?= $value['title']?></option>
              <?php } 
            }
          ?>
          </select>
        </div>
      </div>
      <!-- Mobile view categories --> 
      <div class="col-12 col-sm-12 d-md-block col-md-12 d-lg-none d-xl-none nopadding-mobile">
        <div class="profile-tabs-wrapper profile-tabs-wrapper-mobile category-tabs-wrapper mt-30">
          <a href="#/" class="profile-link-mobile active automotive-link-mobile">Automotive & Industrial <img src="<?= base_url('assets/')?>images/profile-mobile-arrow.svg" alt=""></a>
         </div>
        <div class="profile-tabs-menu">
          <a href="#/" class="close-submenu-mobile">
            <img src="<?= base_url('assets/')?>images/down-menu-mobile.svg" alt="">
          </a>          
          <div class="profile-links-wrapper category-link-wrapper">
          
            <?php 
            if($all_categories){
              $sel='';
              foreach ($all_categories as $key => $value) {
                $sel = ($category_id[0]['id'] == $value['id']?"selected":"");?>
                <a href="javascript:void(0)" onclick="getArticleList($(this).attr('val'))" val="<?=$value['id']?>" id="automotive<?=$value['id']?>" class=""><?= $value['title']?></a>
              <?php } 
            }
          ?>  
          </div>
        </div>
      </div>
      <!-- Mobile view categories -->
      <div class="col-12 col-sm-12 col-md-6">
        <div class="category-search">
          <form>
            <input type="text" name="search_value" id="search_value" onkeyup="getArticleList()"  class="category-search-text" placeholder="Search">
          </form>
        </div>
      </div>
      <div class="col-12">
        <div class="category-select-seperator"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="category-filter-wrapper">
          <div class="category-sortby">
            <span class="filter-label">Sort By</span>
            <select name="" id="" class="niceselect">
              <option value="Recent">Recent</option>
              <option value="Most Viewed">Most Viewed</option>
              <option value="Most Commented">Most Commented</option>
            </select>
          </div>
          <div class="category-type">
            <span class="filter-label">Type</span>
            <select name="" id="" class="niceselect">
              <option value="Recent">Recent</option>
              <option value="Most Viewed">Most Viewed</option>
              <option value="Most Commented">Most Commented</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-30 mb-60 mtmob-10">
  <div class="container">
    <div class="row" id="article-listing-block">
            
    </div>
  </div>
</section>
</body>

</html>

<script>
$( window ).on( "load", function() {
  getArticleList();
});

function clearSeachValue(){
  $('#search_value').val('');
}
function getArticleList(category_id=null){
  if(category_id){
    category_id = category_id;
  }else{
    category_id = $("#category_id").val();
  }
 
  var search_value = $("#search_value").val();
  var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('category/ArticleListing')?>",
			type: "POST",
			data:{ search_value,category_id,[csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
			  	$("#article-listing-block").html(response.html);	
				}else{
			  	$("#article-listing-block").html('');
				}
			}
		});
		

}
</script>