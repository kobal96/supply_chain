<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class aboutus extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('aboutusmodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$about_us = $this->common->getData("tbl_about_us",'*',$condition);

		foreach ($about_us as $key => $value) {
		  $condition = "about_us_id = ".$value['about_us_id'];
		  $total_reel = $this->common->getData("tbl_about_advisors",'*',$condition);
		  $about_us[$key]['advisors'] = $total_reel;
		}
		$result['about_us'] = $about_us;
		//echo "<pre>"; print_r($result['about_us']);exit;

		$this->load->view('header');
		$this->load->view('aboutus',$result);
		$this->load->view('footer');
	}

	
}?>
