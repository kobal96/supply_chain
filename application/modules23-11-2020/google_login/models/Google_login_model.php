<?PHP
class Google_login_model extends CI_Model
{

	public function Is_already_register($id)
	 {
	  $this->db->where('login_oauth_uid', $id);
	  $query = $this->db->get('tbl_users');
	  if($query->num_rows() > 0)
	  {
	   return true;
	  }
	  else
	  {
	   return false;
	  }
	 }

	public function Update_user_data($data, $id)
	 {
	  $this->db->where('login_oauth_uid', $id);
	  $this->db->update('tbl_users', $data);
	 }

	public function Insert_user_data($data)
	 {
	  $this->db->insert('tbl_users', $data);
	 }

}
?>
