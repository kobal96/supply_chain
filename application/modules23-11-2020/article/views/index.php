<?php  
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
         $url = "https://";   
    else  
         $url = "http://";   
    // Append the host(domain name, ip) to the URL.   
    $url.= $_SERVER['HTTP_HOST'];   
    
    // Append the requested resource location to the URL   
    $url.= $_SERVER['REQUEST_URI'];
    //print_r(urlencode($url));die;
  ?>  
<style>
@media (max-width:767px){
  #slider {
    top: -73px;  
  }
}
</style>
<div class="popup-msg">
	<span>Link copied to clipboard!</span> 
</div>
<div class="quicknav-header-wrapper">
  <div class="quicknav-header">
    <a href="javascript:void(0)" onclick="window.history.go(-1); return false;">
      <p class="quicknav-article-title">
        <img src="<?=base_url('assets/')?>images/back-header.svg" alt="">
        <span><?= (!empty($article_data[0]['title'])?$article_data[0]['title']:"") ?></span>
      </p>
    </a>
    <div class="quicknav-share">
      <a href="#/" id="bookmark" data-bookmark="<?= (!empty($article_data[0]['id'])?$article_data[0]['id']:"") ?>"><img src="<?=base_url('assets/')?>images/bookmark-header.svg" alt=""></a>
      <a href="#/" class="article-share-link"><img src="<?=base_url('assets/')?>images/share-header.svg" alt=""></a>
	  <div class="article-nav-share">
        <p>Share on</p>
        <div>
          <a href="https://www.facebook.com/sharer.php?u=<?=urlencode($url)?>" target="_blank"><img src="<?=base_url('assets/')?>images/fb-article.svg" alt=""></a>
          <a href="https://twitter.com/share?url=<?=$url?>&text=supply&via=supply&hashtags=supply" target="_blank"><img src="<?=base_url('assets/')?>images/twitter-article.svg" alt=""></a>
          <a href="https://www.linkedin.com/shareArticle?url=<?=$url?>&title=supply" target="_blank"><img src="<?=base_url('assets/')?>images/linkedin-article.svg" alt=""></a>
          <a href="mailto:?subject=I wanted you to see this site&body=Check out this site <?=$url?>" target="_blank"><img src="<?=base_url('assets/')?>images/email-article.svg" alt=""></a>
          <a href="javascript:void(0)" onclick="copyLink()" ><img src="<?=base_url('assets/')?>images/link-article.svg" alt=""></a>
        </div>
      </div>
        
      <!--<div class="toast">
        <div class="toast-body">
          Link Copied!
        </div>
      </div>-->
    </div>
   
  </div>
  <div class="quicknav-header-clear"></div>
</div>


<section class="page-section mt-60 mtmob-40">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5">
        <div class="article-detail-title">
          <p class="category-tag"><?= (!empty($article_data[0]['category_name'])?$article_data[0]['category_name']:"") ?></p>
          <h1><?= (!empty($article_data[0]['title'])?$article_data[0]['title']:"") ?></h1>
          <div class="article-author-info">
            <p>In the <span class="author-name"><?= (!empty($article_data[0]['edition_name'])?$article_data[0]['edition_name']:"") ?> Edition</span></p>
            <p>by <?= (!empty($article_data[0]['author_name'])?$article_data[0]['author_name']:"") ?></p>
          </div>
        </div>
        <div class="article-detail-share">
          <!-- <a href="javascript:void(0)" onclick="javascript:genericSocialShare('https://www.facebook.com/sharer.php?u=https%3A%2F%2Fwww.codexworld.com%2Fcreate-custom-social-share-links%2F')"><img src="<?=base_url('assets/')?>images/facebook-ad.svg" alt=""></a> -->
          <a href="https://www.facebook.com/sharer.php?u=<?=urlencode($url)?>" target="_blank"><img src="<?=base_url('assets/')?>images/facebook-ad.svg" alt=""></a>
          <a href="https://twitter.com/share?url=<?=$url?>&text=supply&via=supply&hashtags=supply" target="_blank"><img src="<?=base_url('assets/')?>images/twitter-ad.svg" alt=""></a>
          <a href="https://www.linkedin.com/shareArticle?url=<?=$url?>&title=supply" target="_blank"><img src="<?=base_url('assets/')?>images/linkedin-ad.svg" alt=""></a>
          <a href="mailto:?subject=I wanted you to see this site&body=Check out this site <?=$url?>" target="_blank"><img src="<?=base_url('assets/')?>images/email-ad.svg" alt=""></a>
          <a href="javascript:void(0)" onclick="copyLink()"><img src="<?=base_url('assets/')?>images/link-ad.svg" alt=""></a>
        </div>
        <!--<div class="toast">
          <div class="toast-body">
            Link Copied!
          </div>
        </div>-->
        
      </div>
      <div class="d-none d-lg-none d-xl-block col-xl-1"></div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <img src="<?=base_url().'images/article_image/'.$article_data[0]['image']?>" alt="" class="img-fluid article-detail-banner">
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="article-detail-banner-full text-center">
          <a href="#"><img src="<?=base_url('assets/')?>images/home-banner-2.png" alt="" class="img-fluid m-auto d-none d-sm-none d-md-block"></a>
          <a href="#"><img src="<?=base_url('assets/')?>images/home-banner-1.png" alt="" class="img-fluid m-auto d-block d-sm-block d-md-none"></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="d-none d-sm-none d-md-block col-md-3">
        <p class="category-tag">In Spotlight</p>
        <ul class="spotlight">

          <?php
            if (!empty($spotlight_data)) {
            foreach ($spotlight_data as $key => $value) { 
              ?>
              <li><a href="<?= base_url().'article/'.$value['slug'] ?>"> <?= $value['title'] ?> </a></li>
          <?php } } ?>

        </ul>
        <div class="article-detail-add-1">
          <img src="<?=base_url('assets/')?>images/article-detail-add-1.png" alt="" class="img-fluid">
        </div>
      </div>
      <!--<div class="d-sm-none d-md-block col-md-1"></div>-->
	  <!--<div class="col-12 col-sm-12 col-md-8 article-detail-content">-->
      <div class="col-12 col-sm-12 col-md-9 article-detail-content">
		<?= (!empty($article_data[0]['description'])?$article_data[0]['description']:"") ?>        
      </div>
    </div>
    <!--<div class="row">
      <div class="col-12 col-sm-12 offset-md-2 col-md-10 article-detail-content">
        <?= (!empty($article_data[0]['description'])?$article_data[0]['description']:"") ?>
      </div>
    </div>-->
  
    <div class="row">
		<div class="d-none d-sm-none d-md-block col-md-3">&nbsp;</div>
      <!--<div class="col-12 col-sm-12 offset-md-4 col-md-8">-->
	  <div class="col-12 col-sm-12 col-md-9">
        <div class="article-detail-tags article-tags">
        <?php 
        $tag_title = explode(',',$article_data[0]['tag_title']);
        $tag_slug  = explode(',',$article_data[0]['tag_slug']);      
        foreach ($tag_title as $key => $value) {?>
          <a href="<?= base_url('categories/'.$article_data[0]['category_slug'].'/'.$tag_slug[$key])?>"><?=$value?></a>
        <?php } ?>
        </div>
        <div class="article-detail-share-bottom article-detail-share">
          <a href="#/"><img src="<?=base_url('assets/')?>images/facebook-ad.svg" alt=""></a>
          <a href="#/"><img src="<?=base_url('assets/')?>images/twitter-ad.svg" alt=""></a>
          <a href="#/"><img src="<?=base_url('assets/')?>images/linkedin-ad.svg" alt=""></a>
          <a href="#/"><img src="<?=base_url('assets/')?>images/email-ad.svg" alt=""></a>
          <a href="#/"><img src="<?=base_url('assets/')?>images/link-ad.svg" alt=""></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="article-detail-newsletter-seperator"></div>
      </div>
      <div class="col-12 col-sm-12 col-sm-12 offset-lg-3 col-lg-9">
        <div class="article-detail-newsletter">
          <h4>Join our mailing list</h4>
          <div class="newsletter-form">
            <p>Sign up for weekly emails on latest supplychain trends.</p>
            <form class="article-subscribe" method="post" id="newsletter-article">
              <input type="text" class="newsletter-email" name="email" placeholder="Your Email">            
      				<input type="submit" value="Sign me up!" class="newsletter-submit">
      			</form>
          </div>
        </div>
      </div>
      <div class="col-12">
        <div class="article-detail-newsletter-seperator"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="article-detail-banner-full text-center">
          <a href="#"><img src="<?=base_url('assets/')?>images/home-banner-2.png" alt="" class="img-fluid m-auto d-none d-sm-none d-md-block"></a>
          <a href="#"><img src="<?=base_url('assets/')?>images/home-banner-1.png" alt="" class="img-fluid m-auto d-block d-sm-block d-md-none"></a>
        </div>
      </div>
    </div>
  </div>  
</section>
<section class="page-section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section-title-large related-article-section-title">
          More on <?= (!empty($article_data[0]['category_name'])?$article_data[0]['category_name']:"") ?>
        </h3>
      </div>
    </div>
    <div class="row">

      <?php
      if($article_data_more){
          foreach ($article_data_more as $key => $value) { ?>
             <div class="col-12 col-sm-6 col-md-4">
              <div class="hps-single">
                <a href="<?= base_url('article/'.$value['slug'])?>">
                  <img src="<?= base_url('images/article_image/'.$value['image'])?>" alt="" class="img-fluid">
                  <div class="hps-content">
                    <p class="category-tag"><?= $value['category_name']?></p>
                    <h4><?= $value['title']?></h4>
                    <div class="article-author-info">
                      <p>by <span class="author-name"><?=(!empty($value['author_name'])?$value['author_name']:$value['author'])?></span></p>
                      <p><span class="article-date"><?= date('d M Y',strtotime($value['updated_at']))?></span><span class="reading-time"></span> </p>
                    </div>
                  </div>
                </a>
              </div>
            </div>
      <?php } } ?>


      <div class="col-12">
        <div class="article-detail-newsletter-seperator"></div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-60 mb-60 mtmob-40 mbmob-40">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 offset-md-12 offset-lg-2 col-lg-8">
        <div class="comments-section-wrapper">
        <form class="comment-form" id="form-comment" method="post" enctype="multipart/form-data">
        <input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">    
          <input name="id" id="id" value="<?= (!empty($article_data[0]['id'])?$article_data[0]['id']:"") ?>" type="hidden">
          <div class="comments-input">
            <img src="<?= (!empty($article_data[0]['profile_photo'] )?base_url('assets/').'images/profile_image/'.$article_data[0]['profile_photo']:base_url('assets/').'images/comments-avatar.png')?>" alt="">
            <textarea name="comment" id="comment" placeholder="Write your comment"></textarea>
            <input type="submit" value="Publish" class="btn-celerity btn-large btn-blue text-uppercase">            
          </div>
          <input type="text" name="copyLink_url" id="copyLink_url" value="<?=$url?>" style="display:none">
        </form>
       
        <?php
        if ($this->session->userdata('supply_chain_user') && !empty($comment_data)) {
           foreach ($comment_data as $key => $value) {
            $date_convert = date('Y-M-d',strtotime($value['created_at']));
            $date_convert_array = explode('-',$date_convert);
            ?>

            <div class="comment-single">
            <img src="<?= (!empty($article_data[0]['profile_photo'] )?base_url('assets/').'images/profile_image/'.$article_data[0]['profile_photo']:base_url('assets/').'images/comments-avatar.png')?>" alt="" class="comment-avatar">
            <div class="comment-single-content">
              <p class="comment-name"><?= (!empty($value['author_name'])?$value['author_name']:"") ?></p>
              <p class="comment-date"><?= $date_convert_array[2].' '.$date_convert_array[1].', '.$date_convert_array[0]?></p>
              <p class="comment-content"><?= (!empty($value['comment'])?$value['comment']:"") ?>.</p>
              <div class="comment-like">
               
              </div>
            </div>
          </div>
             
         <?php }
         } 
        ?>

        </div>
      </div>
    </div>
  </div>
</section>
<script>
  $(document).ready(function(){
    $("body").prognroll();
	$('.article-share-link').click(function(){
      $('.article-nav-share').toggle();
    });
    $(document).mouseup(function(e) 
    {
        var container = $(".article-nav-share");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) 
        {
            container.hide();
        }
    });  
  });
</script>


<script type="text/javascript">
function genericSocialShare(url){
    window.open(url,'sharer','toolbar=0,status=0,width=648,height=395');
    return true;
}
</script>

<script>
 function copyLink(){
   /* Get the text field */
   var copyText = document.getElementById("copyLink_url");
       $('#copyLink_url').show();
      console.log(copyText);
        // alert(copyText)
      /* Select the text field */
      copyText.select();
      copyText.setSelectionRange(0, 99999); /*For mobile devices*/

      /* Copy the text inside the text field */
      document.execCommand("copy");
      $('#copyLink_url').hide();
      /* Alert the copied text */
      // alert("Copied the text: " + copyText.value);
      //$('.toast').toast('show');
	  $('.popup-msg').show().delay(2000).fadeOut();;
      // alert("Link Copied!")
    }


$(document).on("click","#bookmark",function(){
    <?php
      if(empty($this->session->userdata('supply_chain_user')[0])){?>
          alert("To bookmark the Article you need to login first");
          window.location.href = "<?= base_url('login')?>";
          return false;
      <?php }?>
    var bookmark = $(this).attr("data-bookmark");
    var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
    $.ajax({
      url: "<?= base_url('article/bookmark')?>",
      type: "POST",
      data:{ bookmark, [csrfName]: csrfHash },
      dataType: "json",
      success: function(response){
        if(response.success){
          swal("done", response.msg, {
            icon : "success",
            buttons: {              
              confirm: {
                className : 'btn btn-success'
              }
            },
          }).then(
          function() {
           location.reload();
            
          });
        }else{  
          swal("Failed", response.msg, {
            icon : "error",
            buttons: {              
              confirm: {
                className : 'btn btn-danger'
              }
            },
          });
        }
        
      }
    }); 

  });

  $(document).on("focusin","#comment",function(){
    <?php
      if(empty($this->session->userdata('supply_chain_user')[0])){?>
          alert("To comment the post you need to login first");
          window.location.href = "<?= base_url('login')?>";
      <?php }?>
  });


  var vRules = 
    {
      comment:{required:true},
    };
    var vMessages = 
    {
      comment:{required:"Please Enter Comment."},
    };

    $("#form-comment").validate({
      ignore:[],
      rules: vRules,
      messages: vMessages,
      submitHandler: function(form) 
      { 
        var act = "<?= base_url('article/submitForm')?>";
        $("#form-comment").ajaxSubmit({
          url: act, 
          type: 'post',
          dataType: 'json',
          cache: false,
          clearForm: false,
          async:false,
          beforeSubmit : function(arr, $form, options){
            $(".btn btn-black").hide();
          },
          success: function(response) 
          {
            if(response.success){
              swal("done", response.msg, {
                icon : "success",
                buttons: {              
                  confirm: {
                    className : 'btn btn-success'
                  }
                },
              }).then(
              function() {
                location.reload();
                //window.location = "<?= base_url('article')?>";
                
              });
            }else{  
              swal("Failed", response.msg, {
                icon : "error",
                buttons: {              
                  confirm: {
                    className : 'btn btn-danger'
                  }
                },
              });
            }
            
          }
        });

      }
    });

$("#newsletter-article").validate({
  ignore:[],
  rules: {
  email:{required:true},
    },
  messages: {
  email:{required:"Please Enter Email."},
    },
  submitHandler: function(form) 
  { 
    var act = "<?= base_url('newsletter/letter')?>";
    $("#newsletter-article").ajaxSubmit({
      url: act, 
      type: 'post',
      dataType: 'json',
      cache: false,
      clearForm: false,
      async:false,
      beforeSubmit : function(arr, $form, options){
        $(".btn btn-black").hide();
      },
      success: function(response) 
      {
        if(response.success){
          swal("done", response.msg, {
            icon : "success",
            buttons: {              
              confirm: {
                className : 'btn btn-success'
              }
            },
          }).then(
          function() {
            location.reload();
          });
        }else{  
          swal("Failed", response.msg, {
            icon : "error",
            buttons: {              
              confirm: {
                className : 'btn btn-danger'
              }
            },
          });
        }
      }
    });

  }
});
$(document).ready(function(){
	var lastScrollTop = 0;
	$(window).scroll(function(event){
	   var st = $(this).scrollTop();
	   if (st > lastScrollTop){
		   // downscroll code
		   $('.quicknav-header-wrapper').addClass('quicknav-header-wrapper-fixed');
		   $('.prognroll-bar').fadeIn();
	   } else {
		  // upscroll code
		  $('.quicknav-header-wrapper').removeClass('quicknav-header-wrapper-fixed');
		  $('.prognroll-bar').fadeOut();
	   }
	   lastScrollTop = st;
	});
});
</script>