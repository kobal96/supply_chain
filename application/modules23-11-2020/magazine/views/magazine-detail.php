<?php  
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
         $url = "https://";   
    else  
         $url = "http://";   
    // Append the host(domain name, ip) to the URL.   
    $url.= $_SERVER['HTTP_HOST'];   
    
    // Append the requested resource location to the URL   
    $url.= $_SERVER['REQUEST_URI'];
    //print_r(urlencode($url));die;
  ?>  

<?php 
if($magazine_detail){?>
<section class="page-section magazine-detail-bg">  
  <div class="magazine-detail-bg-mobile"></div>
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-6">
        <a href="javascript:void(0)"  onclick="return history.back()"  class="md-back md-mobile">Back</a>
        <div class="magazine-detail-img">
          <img src="<?= base_url('images/')?>edition_image/<?=  (!empty($magazine_detail[0]['image'])?$magazine_detail[0]['image']:"default-img.jpg")?>" alt="" class="img-fluid">
        </div>        
      </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-6">
        <div class="magazine-detail-content">
            <a href="javascript:void(0)" onclick="return history.back()" class="md-back md-desktop">Back</a>
            <p class="category-tag">Magazine Edition</p>
            <p class="magazine-publish-date"><?= $magazine_detail[0]['title']?></p>
            <div class="magazine-detail-description">
              <p><?= $magazine_detail[0]['magazine_description']?></p>
            </div> 

              <div class="magazine-detail-actions">
              <?php if($magazine_detail[0]['issue_link']){?>
                  <a href="<?= $magazine_detail[0]['issue_link'] ?>" target="_blank" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase">View</a>
              <?php }  ?>
              <?php if($magazine_detail[0]['edition_pdf']){?>
                <a href="<?= base_url('images/edition_pdf/'.$magazine_detail[0]['edition_pdf'])?>" target="_blank"  class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase">Download</a>
            <?php }  ?>
                <a href="#/" class="magazine-share-link"><img src="<?= base_url('assets/')?>images/md-share.svg" alt=""></a>
            </div>
                        
            <div class="magazine-nav-share" style="display:none">
              <p>Share on</p>
              <div>
                <a href="https://www.facebook.com/sharer.php?u=<?=urlencode($url)?>" target="_blank"><img src="<?=base_url('assets/')?>images/fb-article.svg" alt=""></a>
                <a href="https://twitter.com/share?url=<?=$url?>&text=supply&via=supply&hashtags=supply" target="_blank"><img src="<?=base_url('assets/')?>images/twitter-article.svg" alt=""></a>
                <a href="https://www.linkedin.com/shareArticle?url=<?=$url?>&title=supply" target="_blank"><img src="<?=base_url('assets/')?>images/linkedin-article.svg" alt=""></a>
                <a href="mailto:?subject=I wanted you to see this site&body=Check out this site <?=$url?>" target="_blank"><img src="<?=base_url('assets/')?>images/email-article.svg" alt=""></a>
                <a href="javascript:void(0)" onclick="copyLink()"><img src="<?=base_url('assets/')?>images/link-article.svg" alt=""></a>
                <!-- <input type="hidden" name="copyLink_url" id="copyLink_url" value="<?=$url?>" > -->
              </div>
              
            </div>

             <input type="text" name="copyLink_url" id="copyLink_url" value="<?=$url?>" style="display:none">
            <div class="toast">
              <div class="toast-body">
                Link Copied!
              </div>
            </div>

        </div>
      </div>
    </div>
  </div>
</section>
<?php } ?>

<?php 

if($similar_magazine){?>
<section class="page-section similar-magazines mt-60 mtmob-40 mbmob-40">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section-title-medium ">You might also like</h3>
      </div>
      <?php 
        foreach ($similar_magazine as $key => $value) {?>
          <div class="col-6 col-sm-3">
            <div class="home-magazine-single magazine-list-single">
              <a href="<?= base_url('magazine/'.$value['slug'])?>">
                <img src="<?= base_url('images/')?>edition_image/<?= (!empty($value['image'])?$value['image']:"default-img.jpg")?>" alt="" class="img-fluid home-magazine-single-img">
                <h4><?= $value['title']?></h4>
              </a>
            </div>
          </div>
        <?php } ?>
            
    </div>        
  </div>
</section>
<?php } ?>

<script>
	$('.magazine-share-link').click(function(){
      $('.magazine-nav-share').toggle();
    });
    function copyLink(){
        /* Get the text field */
        $('#copyLink_url').show();
        var copyText = document.getElementById("copyLink_url");

        // alert(copyText)
      /* Select the text field */
      copyText.select();
      copyText.setSelectionRange(0, 99999); /*For mobile devices*/

      /* Copy the text inside the text field */
      document.execCommand("copy");
      $('#copyLink_url').hide();

      /* Alert the copied text */
      // alert("Copied the text: " + copyText.value);
      $('.toast').toast('show');
    }

    
</script>