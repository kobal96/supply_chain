
<?php
if($magazine){
    foreach ($magazine as $key => $value) { ?>
        <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="<?= base_url('magazine/'.$value['slug'])?>">
            <img src="<?= base_url('images/')?>edition_image/<?= (!empty($value['image'])?$value['image']:"default-img.jpg")?>" alt="" class="img-fluid home-magazine-single-img">
            <h4><?= $value['title']?></h4>
          </a>
        </div>
      </div>
    <?php  }
    }?>


