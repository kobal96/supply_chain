
<section class="page-section mt-60 mtmob-30">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-6">
        <h1 class="page-title-large magazine-title">Magazine Editions</h1>
      </div>
      <div class="col-12 col-sm-12 col-md-6">
        <div class="category-search magazine-search">
          <form>
            <input type="text" id="search_value" name="search_value" onkeyup="getmagazineList()" class="category-search-text" placeholder="Search">
          </form>
        </div>
      </div>
      <div class="col-12">
        <div class="magazine-title-seperator"></div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-60 mb-60 mtmob-40 mbmob-40">
  <div class="container">
    <div class="row" id="magazine-listing">
    </div>
    <div class="row">
      <div class="col-12">
        <div>
          <a href="#"><img src="<?= base_url('assets/')?>images/home-banner-2.png" alt="" class="img-fluid m-auto d-none d-sm-none d-md-block"></a>
          <a href="#"><img src="<?= base_url('assets/')?>images/home-banner-1.png" alt="" class="img-fluid m-auto d-block d-sm-block d-md-none"></a>
        </div>
      </div>
    </div>    
  </div>
</section>
</body>

</html>

<script>
$( window ).on( "load", function() {
  getmagazineList();
});

function getmagazineList(){
  var search_value = $("#search_value").val();
  var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('magazine/magazineListing')?>",
			type: "POST",
			data:{ search_value,[csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
			  	$("#magazine-listing").html(response.html);	
				}else{
			  	$("#magazine-listing").html('');
				}
			}
		});
		

}
</script>