<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Newsletter extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('newslettermodel','',TRUE);
	}

	public function index(){
		$result = array();
		
	}

	public function letter(){
		//print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "email_id = '".$this->input->post('email')."' ";
			if($this->input->post('id') && $this->input->post('id') > 0){
				$condition .= " AND  id != ".$this->input->post('id')." ";
			}			
			$check_name = $this->common->getData("tbl_newsletter",'*',$condition);

			if(!empty($check_name[0]['id'])){
				echo json_encode(array("success"=>false, 'msg'=>'Newsletter Email Already Present!'));
				exit;
			}
			if ( $this->session->userdata('supply_chain_user') ) {
				$data['user_id'] = $this->session->userdata('supply_chain_user')[0]['id'];
			}else{
				$data['user_id'] = '';
			}
			
			$data['email_id'] = $this->input->post('email');
			$data['created_at'] = date("Y-m-d H:i:s");
			$data['updated_at'] = date("Y-m-d H:i:s");
			$result = $this->common->insertData('tbl_newsletter',$data,'1');
			if(!empty($result)){
				echo json_encode(array('success'=>true, 'msg'=>'Newsletter create successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
			
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
		
}
?>