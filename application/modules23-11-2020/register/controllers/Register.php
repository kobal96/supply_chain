<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('registermodel','',TRUE);
		$this->load->library('email');
	}

	public function index(){
		
		$result = array();
		$condition = "1=1";
		$result['users'] = $this->common->getData("tbl_users",'*',$condition);
		 echo "<pre>";print_r($result);exit;
	}

	public function cookies(){
		$result = array();
		$cookie_name = "celerity";
		$cookie_value = "celerity";
		setcookie($cookie_name, $cookie_value, time() + (86400 * 30 + 360), "/");
	}

	public function submitForm(){
		 //print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "email = '".$this->input->post('email')."' ";
			if($this->input->post('id') && $this->input->post('id') > 0){
				$condition .= " AND  id != ".$this->input->post('id')." ";
			}			
			$check_name = $this->common->getData("tbl_users",'*',$condition);

			if(!empty($check_name[0]['id'])){
				echo json_encode(array("success"=>false, 'msg'=>'User Email Already Present!'));
				exit;
			}

			$condition1 = "username = '".$this->input->post('username')."' ";
			$check = $this->common->getData("tbl_users",'*',$condition1);
			if(!empty($check[0]['id'])){
				echo json_encode(array("success"=>false, 'msg'=>'Username Already Present!'));
				exit;
			}

			$data['first_name'] = $this->input->post('fname');
			$data['last_name'] = $this->input->post('lname');
			$data['email'] = $this->input->post('email');
			$data['username'] = $this->input->post('username');
			$data['password'] = md5($this->input->post('password'));
			$data['verified'] = 'YES';
			$data['status'] = 'Active';
			$data['updated_at'] = date("Y-m-d H:i:s");
			
			if(!empty($this->input->post('id'))){
				$condition = "id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_users",$data,$condition);
				if($result){

					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");

				/*$this->email->set_mailtype('html');
		        
		        $this->email->from(FROM_EMAIL, 'Supply_chain');
		        
		        $this->email->to($data['email']);
		        $this->email->subject('Supply Chain Email Verified');
		        $msg = "Hello " . $data['username'] . ",<br><br>As per your request your registration has been create.<br>
		        <p>Please click this link to verify your account:<a href='". base_url(). "register/verify?email=".$data['email']."'>Click here</a></p>";
		        
		        $content = '<!DOCTYPE HTML>' . '<head>' . '<meta http-equiv="content-type" content="text/html">' . '<title>Email notification</title>' . '</head>' . '<body>' . '<div class="message">&nbsp;</div>' . '<div class="message">' . $msg . '</div>' . '<div class="message">&nbsp;</div>' . '<div class="message">&nbsp;</div>' . '<div class="message">&nbsp;</div>' . '<div>Thanks</div>' . '<div>Supply Chain Support Team</div>' . '<div>&nbsp;</div>' . '<div class="logo">&nbsp;</div>' . '</body>';
		        $this->email->message($content);
		        $this->email->send();*/
				
				$resultid = $this->common->insertData('tbl_users',$data,'1');

				
				// $param = array(
				// 	'user_id' => $resultid, 
				// 	'email_id' => $this->input->post('email'), 
				// 	'created_at' => date("Y-m-d H:i:s"), 
				// 	'updated_at' => date("Y-m-d H:i:s"), 
				// );
				// $newsletter = $this->common->insertData('tbl_newsletter',$param,'1');

				$condition = "id = '".$resultid."' ";
			    $result = $this->common->getData('tbl_users','*',$condition);
			    $session_data = array('supply_chain_user'=>$result);  
			    $this->session->set_userdata($session_data);
				if(!empty($result)){
					//echo json_encode(array('success'=>true, 'msg'=>'User Registration verification mail send Please verify your email.'));
					echo json_encode(array('success'=>true, 'msg'=>'User signup successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function verify() {
		
	    $data = array('verified' => 'YES');
	    $condition = "email = '".$_GET['email']."' ";
		$result = $this->common->updateData("tbl_users",$data,$condition);
		if($result){
			redirect(base_url());
				}
	}

	public function forgotpassword() {
        $username = isset($_POST['email']) ? $_POST['email'] : "";
        if ($username != "") {
            $data = array();
            $this->load->helper(array('url', 'form'));
            $this->load->library('user_agent');
            $this->load->library('email');
            $config['charset'] = 'utf-8';
	        $config['wordwrap'] = TRUE;
	        $config['mailtype'] = 'html';
	        $this->email->initialize($config);
            
            $user_email = $this->check_valid_email_('tbl_users', 'email', $username);
            if ($user_email) {
            	echo json_encode(array('success'=>true, 'msg'=>'Your password has been reset and emailed to you.'));
					exit;
            } else {
                echo json_encode(array('success'=>false, 'msg'=>'The email id for this username is not found in our database.'));
                exit;
            }
            
        } else {
        	echo json_encode(array('success'=>false, 'msg'=>'The email id for this username is not found in our database.'));
			exit;
        }
    }

    function check_valid_email_($table, $email_field, $email_value) {
        $result1 = $this->db->query("select id,first_name,email,username from tbl_users where email = '$email_value'");
        if ($result1->num_rows() > 0) {
            $res = $result1->result();
            $user = $res[0];
  			
            if ($user->email != '') {
                $this->resetpassword($user);
            }

            return $user->email;
        } else {

            return false;
        }
    }

    function resetpassword($user) {
        $this->load->helper('string');
        //$password = random_string('alnum', 6);
        $passname = $user->first_name;
        $password = substr($passname, 0, 4).rand(100, 999);

        $this->db->where('id', $user->id);
        $this->db->update('tbl_users', array('password' => md5($password)));

        //Send email using codeigniter
        $sendmail = FROM_EMAIL;
        $this->email->from('$sendmail', 'Supply_chain');

        $mails = explode(',', $user->email);

        $this->email->to($mails);
        $this->email->subject('Supply chain Password Reset');
        $msg = "Hello " . $user->first_name . ",<br><br>As per your request your password has been reset and your new temporary password is: <b>" . $password . "</b><br>Click  <a href='".base_url()."' target=new>here </a>to login with your temporary password then go to My Account and create your new password.";
        // Change image src to your site specific settings
        $content = '<!DOCTYPE HTML>' . '<head>' . '<meta http-equiv="content-type" content="text/html">' . '<title>Email notification</title>' . '</head>' . '<body>' . '<div class="message">&nbsp;</div>' . '<div class="message">' . $msg . '</div>' . '<div class="message">&nbsp;</div>' . '<div class="message">&nbsp;</div>' . '<div class="message">&nbsp;</div>' . '<div>Thanks</div>' . '<div>Supply chain Support Team</div>' . '<div>&nbsp;</div>' . '<div class="logo">&nbsp;</div>' . '</body>';
        $this->email->message($content);
        $this->email->send();
        //var_dump($this->email->print_debugger());
        //echo $this->email->print_debugger();
    }

	public function logout(){
		$this->session->unset_userdata('supply_chain_user');
		$this->session->unset_userdata('access_token');

		$this->session->unset_userdata('oauth_status');
        // Destroy entire session 
        $this->session->sess_destroy();
		redirect(base_url()); 
	}
		
}
?>