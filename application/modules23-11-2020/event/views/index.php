<section class="page-section mt-30 mb-30">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-9">
        <a href="<?= base_url('home') ?>" class="md-back">Back</a>
        <div class="event-single event-detail">
          <div class="event-day events-detail-day">
            <div class="event-date"><?= (!empty($event_data[0]['event_date'])?date('d',strtotime($event_data[0]['event_date'])):"") ?></div>
            <div class="event-month"><?= (!empty($event_data[0]['event_date'])?date('M, y',strtotime($event_data[0]['event_date'])):"") ?></div>
          </div>
          <div class="event-content-wrapper">
            <div class="event-content">              
              <div class="article-block-content">
                <p class="category-tag">Event</p>
                <h4><?= (!empty($event_data[0]['title'])?$event_data[0]['title']:"") ?></h4>
                <div class="event-detail-share">
                  <a href="<?= (!empty($event_data[0]['link'])?$event_data[0]['link']:"") ?>" class="btn-celerity btn-small btn-blue text-uppercase register-event">Register</a><br/><br/>
                  <a href="#/" class="event-share"><img src="images/facebook-ad.svg" alt=""></a>
                  <a href="#/" class="event-share"><img src="images/twitter-ad.svg" alt=""></a>
                  <a href="#/" class="event-share"><img src="images/linkedin-ad.svg" alt=""></a>
                  <a href="#/" class="event-share"><img src="images/email-ad.svg" alt=""></a>
                  <a href="#/" class="event-share"><img src="images/link-ad.svg" alt=""></a>
                </div>
                <div class="event-description-detail">
                  <p><?= (!empty($event_data[0]['description'])?$event_data[0]['description']:"") ?> </p>
                </div>                
              </div>              
            </div>
          </div>
        </div>
      </div>      
    </div>
  </div>
</section>
