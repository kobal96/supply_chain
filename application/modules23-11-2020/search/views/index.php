<section class="page-section mt-60 mtmob-0 mttab-0">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-6 col-lg-9">
        <div class="searched-in mtmob-20">
          <p>SEARCHED IN</p>
          <h2><?=$keywords;?></h2>
        </div>        
      </div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-3">
        <div class="search-category mtmob-20">
          <p class="pl-18">Category</p>
          <select name="category" id="category" class="niceselect category-search-list" onchange="getSearch()">
            <option value="all">All</option>
            <option value="articles">Articles</option>
            <option value="magazines">Magazines</option>
          </select>
        </div>      
      </div>
      <div class="col-12">
        <div class="category-select-seperator"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="category-filter-wrapper">
          <div class="category-sortby">
            <span class="filter-label">Sort By</span>
            <select name="sortby" id="sortby" class="niceselect" onchange="getSearch()">
              <option value="recent">Recent</option>
              <option value="old">Oldest</option>
            </select>
          </div>          
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-30 mb-60 mtmob-10">
  <div class="container" id="searchdata">

  </div>
</section>

<script type="text/javascript">
  $( window ).on( "load", function() {
    getSearch();
  });

  $(document).ready(function() {

  $(document).on("change",".theme_filter",function () {
    var key = "<?=$keywords;?>";
        var theme_id = $('#theme_id').val();
        var country  = $('#country_id').val();
        var year     = $('#year').val();
        var color    = $('#color').val();
        var sound    = $('#sound').val();
        
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
      var csrfHash = $('.txt_csrfname').val(); // CSRF hash
    $.ajax({
      url: "<?= base_url('search/searchdata')?>",
      type: "POST",
      data:{ key, theme_id, country, year, color, sound, [csrfName]: csrfHash },
      dataType: "json",
      success: function(response){
        if(response.success){
          
        $("#searchdata").html(response.data); 
          
        }else{
        $("#searchdata").html('');
        }
        
      }
    });
        
      });

            
  });

  function getSearch() {

    var key = "<?=$keywords;?>";
    var category = $("#category").val();
    var sortby = $("#sortby").val();
    
    var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
    $.ajax({
      url: "<?= base_url('search/searchdata')?>",
      type: "POST",
      data:{ category, sortby, key, [csrfName]: csrfHash },
      dataType: "json",
      success: function(response){
        if(response.success){
          $("#searchdata").html(response.data);   
        }else{
          $("#searchdata").html('');
        }

      }
    });
  }
</script>