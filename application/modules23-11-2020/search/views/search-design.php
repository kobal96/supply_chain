<?php if(!empty($search_data) || !empty($magazine)){ ?>
<div class="row">
<?php
if(!empty($search_data)){
	foreach ($search_data as $key => $value) { ?>

		<div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="<?= base_url('article/'.$value['slug'])?>">
            <img src="<?= base_url('images/article_image/'.$value['image'])?>" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag"><?=$value['category_name'];?></p>
              <h4><?=$value['title'];?></h4>
              <div class="article-author-info">
                <p>by <span class="author-name"><?=(!empty($value['author_name'])?$value['author_name']:$value['author'])?></span></p>
                <p><span class="article-date"><?= date('d M Y',strtotime($value['updated_at']))?></span> <!-- <span class="reading-time">12 min read</span> --> </p>
              </div>
            </div>
          </a>
        </div>
      </div>

	<?php } } ?>

      </div>
      <div class="row">
      <div class="col-12">
        <div class="category-listing-banner-full article-detail-banner-full text-center">
          <a href="#"><img src="images/home-banner-2.png" alt="" class="img-fluid m-auto d-none d-sm-none d-md-block"></a>
          <a href="#"><img src="images/home-banner-1.png" alt="" class="img-fluid m-auto d-block d-sm-block d-md-none"></a>
        </div>
      </div>
    </div>
    <div class="row">

      <?php
      if(!empty($magazine)){
      foreach ($magazine as $key => $value) { ?>

      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="<?= base_url('magazine/'.$value['slug'])?>">
            <img src="<?= base_url('images/')?>edition_image/<?= (!empty($value['image'])?$value['image']:"default-img.jpg")?>" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4><?= $value['title']?></h4>
              <!-- <div class="article-author-info">
                <p>by <span class="author-name">Joshua Mullins</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div> -->
            </div>
          </a>
        </div>
      </div>
      <?php }
      } ?>

    </div>
    <?php }else{ ?>
        <h4 style="text-align: center;">No result found</h4>
     <?php } ?>