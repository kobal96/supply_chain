 <?php
  if(!empty($notification) && isset($notification)) {
    foreach ($notification as $key => $value) {
       ?>
  <div class="notification-single">
    <div class="notification-icon">
      <img src="images/notification-article.svg" alt="">
    </div>
    <div class="notification-content">
      <div class="notification-icon-mobile">
        <img src="images/notification-article.svg" alt="">
      </div>
      <div class="notification-name-date">
        <p class="notification-name"><?=$value['title'];?> Article approved</p>
        <p class="notification-date"><?=date('M d, Y',strtotime($value['updated_at']));?></p>
      </div>
      <div class="notification-post-comment">
        <p>Your article on A Generational Opportunity - Redesign of Supply Chains is approved by celerity team and is live on the website. .</p>
        <a href="<?= base_url('article/'.$value['article_id'])?>" class="view-article">
          Click here to view
        </a>
      </div>
    </div>
  </div>

  <?php } } ?>