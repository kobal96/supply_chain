<?php
$data = $this->session->userdata('supply_chain_user')[0];
?>
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/css/medium-editor-insert-plugin-frontend.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/css/medium-editor-insert-plugin.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/css/medium-editor.min.css" />

<section class="page-section">
  <div class="container-fluid">
    <div class="row">
      <!-- Mobile Profile Links -->
      <div class="col-12 col-sm-12 d-md-block col-md-12 d-lg-none d-xl-none nopadding-mobile">
        <div class="profile-tabs-wrapper profile-tabs-wrapper-mobile mt-30">
          <a href="#/" class="profile-link-mobile active my-profile-link-mobile">My Profile <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile my-articles-link-mobile">My Articles <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile my-bookmarks-link-mobile">My Bookmarks <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile my-notifications-link-mobile">Notification <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile my-settings-link-mobile">Settings <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="index.php">Logout</a>
        </div>
        <div class="profile-tabs-menu">
          <a href="#/" class="close-submenu-mobile">
            <img src="images/down-menu-mobile.svg" alt="">
          </a>
          <div class="profile-name">
            <!-- <img src="images/comments-avatar.png" alt=""> -->
            <img src="<?= (!empty($this->session->userdata('supply_chain_user')[0]['profile_photo'])?FRONT_URL.'/images/profile_image/'.$this->session->userdata('supply_chain_user')[0]['profile_photo']:'images/profile_image/comments-avatar.png')?>" alt="">
			<!--<span class="user-profile-icon user-profile-icon-mobile">V</span>-->
            <div>
              <p class="user-name">Hi, <?= $this->session->userdata('supply_chain_user')[0]['first_name'] ?></p>
              <p class="user-email"><?= $this->session->userdata('supply_chain_user')[0]['email'] ?></p>
            </div>
          </div>
          <div class="profile-links-wrapper">
            <a href="#/" id="my-profile">My Profile</a>
            <a href="#/" id="my-articles">My Articles</a>
            <a href="#/" id="my-bookmarks">My Bookmarks</a>
            <a href="#/" id="my-notifications">Notifications</a>
            <a href="#/" id="my-settings">Settings</a>
            <span class="profile-logout-seperator"></span>
            <a href="index.php" id="profile-logout">Logout</a>
          </div>
        </div>
      </div>
      <!-- Mobile Profile Links -->
      <div class="d-none d-sm-none d-md-none d-lg-block col-lg-3 col-xl-2 pl-0 pr-0">
        <div class="profile-tabs-wrapper mt-30">
          <a href="#/" id="my-profile" class="active">My Profile</a>
          <a href="#/" id="my-articles">My Articles</a>
          <a href="#/" id="my-bookmarks">My Bookmarks</a>
          <a href="#/" id="my-notifications">Notification</a>
          <a href="#/" id="my-settings">Settings</a>
          <a href="<?= base_url('register/logout') ?>" id="profile-logout">Logout</a>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-9 profile-content-border">
        <div class="profile-content-tab" id="my-profile-content">
          <div class="profile-content-wrapper">
            <form class="billing-form" id="profile-form" method="post">
            <div class="row mb-20">
              <div class="col-12 col-sm-12">
              <div class="profile-pic-wrapper">
                <img id="profile-pic-avatar" src="<?php  if($this->session->userdata('supply_chain_user')[0]['login_oauth_uid']){
                    echo   $this->session->userdata('supply_chain_user')[0]['profile_photo'];
                }else if($this->session->userdata('supply_chain_user')[0]['profile_photo']){
                  echo base_url('images/profile_image/').$this->session->userdata('supply_chain_user')[0]['profile_photo'];
                }else{
                  echo  base_url('images/')."profile-pic-avatar.svg" ;
                } ?>" />
                <input type="file" id="imgupload" name="imgupload" accept=".jpg,.png,.jpeg,.svg,.gif"   onchange="readURL(this);" style="display:none" /> 
                <input type="hidden" id="pre_imgupload" name="pre_imgupload"  value="<?= $this->session->userdata('supply_chain_user')[0]['profile_photo']?>"/> 
                <?php if(empty($this->session->userdata('supply_chain_user')[0]['login_oauth_uid'])){?>
                  <a href="#/" class="profile-upload-btn" id="OpenImgUpload"><img src="<?= base_url('assets/') ?>images/profile-pic-button.svg"/></a>
                <?php }?>
              </div>
              </div>
            </div>
              <h2 class="profile-section-title">Personal Info</h2>
              <div class="row mb-20">				
                <div class="col-12 col-sm-12 col-md-6">
                  <input type="hidden" name="id" value="<?= (!empty($billing[0]['id'])?$billing[0]['id']:"") ?>">
                  <div class="form-group">
                    <label for="">First Name</label>
                    <input type="text" class="form-control" name="fname" id="fname" value="<?= (!empty($data['first_name'])?$data['first_name']:$this->session->userdata('supply_chain_user')[0]['first_name'])?>" placeholder="eg.John">
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">Last Name</label>
                    <input type="text" class="form-control" name="lname" id="lname" value="<?= (!empty($data['last_name'])?$data['last_name']:$this->session->userdata('supply_chain_user')[0]['last_name'])?>" placeholder="eg. Raskin">
                  </div>
                </div>
              </div>
              <h2 class="profile-section-title">Billing Info</h2>
              <div class="row mb-20">
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="">Billing Address</label>
                    <textarea name="address" id="" cols="30" rows="4" class="form-control" placeholder="eg. 50  & , Hazari Baugh,  Th Rd, Jvpd Scheme, Opp Uco Bank, Juhu"><?= (!empty($billing[0]['billing_address'])?$billing[0]['billing_address']:"") ?> </textarea>
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">Country</label>
                    <select name="country" id="country" onchange="getstate(this.value)" class="form-control selectclass">
                      <option value="" selected="selected" disabled>--- Select Country ---</option>
                    <?php
                    foreach ($country_list as $key => $value) {
                     ?>
                     <option value="<?=$value['id'] ?>" <?= (isset($billing[0]['country']) && $billing[0]['country'] == $value['id']) ? "selected" :""; ?> ><?=$value['country_name'] ?></option>
                    <?php } ?> 
                    </select>
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">State</label>
                    <select name="state" id="state" onchange="getcity(this.value)" class="form-control selectclass">
                
                    </select>
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">City</label>
                    <select name="city" id="city" class="form-control selectclass">
                    
                    </select>
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">ZIP / PIN</label>
                    <input type="text" class="form-control" name="zipcode" id="zipcode" value="<?= (!empty($billing[0]['zipcode'])?$billing[0]['zipcode']:"") ?>" placeholder="Enter ZIP/ PIN Code">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <button type="submit" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase">Save</button>
                  <button type="button" class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase">Cancel</button>
                </div>
              </div>
              <br>
              <h2 class="profile-section-title">My Subscription</h2>
              <div class="row mb-20">
                <div class="col-12">
                <?php
                  $condition = "user_id = '".$this->session->userdata('supply_chain_user')[0]['id']."' ";
                  $subscribe = $this->common->getData("tbl_payment",'duration,paid_date,amount',$condition);
                  if (!empty($subscribe)) {
                    $aa = end($subscribe);
                    $date = $aa['paid_date'];
                    $duration = $aa['duration'];
                    $expire = date("Y-m-d", strtotime("$date + $duration months"));
                    $a = date("Y-m-d");
                    if($expire>$a){ ?>
                      <div class="my-subscription-wrapper">
                        <div class="msw-text">
                          <p class="active-plan">Active Plan</p>
                          <p class="active-plan-amt">₹<?=round($aa['amount']) ?> <span>/ for <?=$duration ?> months</span></p>
                        </div>
                        <a href="<?=base_url('billing')?>" class="renew-btn">Renew</a>
                      </div>
                  <?php  }else{
                      echo '<a href="'.base_url('billing').'" class="btn-celerity btn-small btn-blue"> Subscribe Now </a>';
                    }
                  }else{
                    echo '<a href="'.base_url('billing').'" class="btn-celerity btn-small btn-blue"> Subscribe Now </a>';
                  } 
                ?>
              
                </div>
              </div>
              <!-- <div class="row">
                <div class="col-12">
                  <button type="submit" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase">Save</button>
                  <button type="button" class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase">Cancel</button>
                </div>
              </div> -->
            </form>
          </div>
        </div>
        <div class="profile-content-tab" id="my-articles-content">
          <h2 class="profile-section-title">My Articles</h2>
          <div class="articles-content-wrapper">
            <div class="articles-tab-wrapper">
              <div class="article-tab-links">
                <a href="#/" class="active" id="articles-all">All</a>
                <a href="#/" id="articles-published">Published</a>
                <a href="#/" id="articles-draft">Drafts</a>
				<a href="#/" id="articles-review">Review</a>
              </div>
            </div>
            <div class="articles-list-wrapper" id="articles-all-content">
              <a href="#/" class="btn-celerity btn-small btn-blue btn-new-post"><img src="images/add-new-post.svg" alt=""> New Article</a>
              <div class="article-list">

              <?php
                if(!empty($article_data) && isset($article_data)) {
                  $cnt = 0;
                  foreach ($article_data as $key => $value) {
                    $cnt= ++$cnt;
                    $date_convert = date('Y-M-j',strtotime($value['updated_at']));
                    $date_convert_array = explode('-',$date_convert);
                     ?>
                <div class="myarticle-single">
                  <a href="#/">
                    <div class="mas-image">
                      <img src="<?= base_url('images/article_image/'.$value['image']) ?>" alt="">
                    </div>
                    <div class="article-block-content">
                      <p class="category-tag"><?= $value['category_name']?></p>
                      <h4><?=$value['title']?></h4>
                      <p class="mas-intro">
                          <!-- <?=(!empty($value['description'])?$value['description']:"") ?> -->
                      </p>
                      <div class="article-author-info">
                        <p>
                          <span class="author-name">Last Updated On </span> <?= $date_convert_array[1].' '.$date_convert_array[2].', '.$date_convert_array[0] ?></p>
                      </div>
                    </div>
                  </a>
                </div>
              <?php } } ?>

              </div>
            </div>
            <div class="articles-list-wrapper" id="articles-published-content">
              <div class="article-list">
              <?php
                if(!empty($published_data) && isset($published_data)) {
                  foreach ($published_data as $key => $value) {
                    $date_convert = date('Y-M-j',strtotime($value['updated_at']));
                    $date_convert_array = explode('-',$date_convert);
                     ?>
                <div class="myarticle-single">
                  <a href="<?= base_url('article/'.$value['id'])?>">
                    <div class="mas-image">
                      <img src="<?= base_url('images/article_image/'.$value['image']) ?>" alt="">
                    </div>
                    <div class="article-block-content">
                      <p class="category-tag"><?= $value['category_name']?></p>
                      <h4><?=$value['title']?></h4>
                      <!-- <p class="mas-intro"><?=(!empty($value['description'])?$value['description']:"") ?></p> -->
                      <div class="article-author-info">
                        <p>
                          <span class="author-name">Last Updated On </span> <?= $date_convert_array[1].' '.$date_convert_array[2].', '.$date_convert_array[0] ?></p>
                      </div>
                    </div>
                  </a>
                </div>
                <?php } } ?>
                <!-- <div class="myarticle-single">
                  <a href="#/">
                    <div class="mas-image">
                      <img src="images/home-spotlight-single.png" alt="">
                    </div>
                    <div class="article-block-content">
                      <p class="category-tag">Supplychain</p>
                      <h4>A Generational Opportunity - Redesign of Supply Chains</h4>
                      <p class="mas-intro">A good supply chain is demand driven; right from provisioning the inputs, to support production, and to distribute the product to final buyer</p>
                      <div class="article-author-info">
                        <p>
                          <span class="author-name">Last Updated On</span> May 02, 2020</p>
                      </div>
                    </div>
                  </a>
                </div> -->

              </div>
            </div>
            <div class="articles-list-wrapper" id="articles-draft-content">
              <a href="#/" class="btn-celerity btn-small btn-red btn-delete-post delete-draft"><img src="images/delete-article.svg" alt=""> Delete</a>
              <a href="#/" class="btn-celerity btn-small btn-blue-inv btn-md-blue-inv btn-cancel-post">Cancel</a>
              <div class="article-list">
              <?php
                if(!empty($draft_data) && isset($draft_data)) {
                  foreach ($draft_data as $key => $value) {
                    $date_convert = date('Y-M-j',strtotime($value['updated_at']));
                    $date_convert_array = explode('-',$date_convert);
                     ?>
                <div class="myarticle-single">
                  <a href="#/">
                    <div class="draft-checkbox">
                      <input type="checkbox" value="<?=$value['id']?>">
                    </div>
                    <div class="mas-image">
                      <img src="<?= base_url('images/article_image/'.$value['image']) ?>" alt="">
                    </div>
                    <div class="article-block-content">
                      <p class="category-tag"><?=(!empty($value['category_name'])?$value['category_name']:"") ?></p>
                      <h4><?=$value['title']?></h4>
                      <!-- <p class="mas-intro"><?=(!empty($value['description'])?$value['description']:"") ?></p> -->
                      <div class="article-author-info">
                        <p>
                          <span class="author-name">Last Updated On </span> <?= $date_convert_array[1].' '.$date_convert_array[2].', '.$date_convert_array[0] ?></p>
                      </div>
                    </div>
                  </a>
                </div>
                 <?php } } ?>

                <!-- <div class="myarticle-single">
                  <a href="#/">
                    <div class="draft-checkbox">
                      <input type="checkbox">
                    </div>
                    <div class="mas-image">
                      <img src="images/home-spotlight-single.png" alt="">
                    </div>
                    <div class="article-block-content">
                      <p class="category-tag">Supplychain</p>
                      <h4>A Generational Opportunity - Redesign of Supply Chains</h4>
                      <p class="mas-intro">A good supply chain is demand driven; right from provisioning the inputs, to support production, and to distribute the product to final buyer</p>
                      <div class="article-author-info">
                        <p>
                          <span class="author-name">Last Updated On</span> May 02, 2020</p>
                      </div>
                    </div>
                  </a>
                </div> -->

              </div>
            </div>
			<div class="articles-list-wrapper" id="articles-review-content">
				<div class="article-list">
            <?php
                if(!empty($review_article) && isset($review_article)) {
                  foreach ($review_article as $key => $value) {
                    $date_convert = date('Y-M-j',strtotime($value['updated_at']));
                    $date_convert_array = explode('-',$date_convert);
                     ?>
                    <div class="myarticle-single">
                      <a href="#/">
                        <div class="mas-image">
                        <img src="<?= base_url('images/article_image/'.$value['image']) ?>" alt="">
                        </div>
                        <div class="article-block-content">
                          <p class="category-tag"><?=(!empty($value['category_name'])?$value['category_name']:"") ?></p>
                          <h4><?=$value['title']?></h4>
                          <!-- <p class="mas-intro"><?=(!empty($value['description'])?$value['description']:"") ?></p> -->
                          <div class="article-author-info">
                            <p>
                              <span class="author-name">Last Updated On </span> <?= $date_convert_array[1].' '.$date_convert_array[2].', '.$date_convert_array[0] ?></p>
                          </div>
                        </div>
                      </a>
                    </div>
                <?php } } ?>
              </div>
			</div>
          </div>
        </div>
        <div class="profile-content-tab" id="new-article-content">
          <form class="billing-form article-form" id="form-article">
            <div id="article-step-1">
              <p class="category-tag article-step">Step 1 of 2</p>
              <h2 class="profile-section-title">
                <a href="#/" id="new-article-back"><img src="images/md-back.svg" alt=""></a>
                New Article
              </h2>
              <div class="row mb-20">
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" id="title" placeholder="eg. Vaccination Preparedness And Food Supply">
                  </div>
                </div>
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="category">Select Category</label>
                    <select name="category[]" id="category" class="form-control selectclass">
                      <option value="">Select</option>
                      <?php $sel="";  
                      foreach ($categories as $key => $value) {                           
                        if(!empty($categories)){                            
                          // $sel = (in_array($value['id'],$article_category_data)?"selected":" ");                              
                        }?> 
                          <option value="<?= $value['id']?>" ><?=$value['title']?></option>                
                    <?php } ?>
                     
                    </select>

                  </div>
                </div>
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="tag">Tags</label>
                    <!-- <input type="text" class="form-control" id="" placeholder="Select Tags"> -->
                    <!-- <select class="newvideotags" name="tag[]" id="tag" multiple="multiple" required> -->
                    <select name="tag[]" id="tag" class="form-control newvideotags"   multiple="multiple" width="300px">
                    <option value="">Select</option>  
                      <?php $sel="";  
                      // echo "<pre>";
                      // print_r($tags);
                      // exit;
                        foreach ($tags as $key => $value) {  ?>
                            <option value="<?= $value['id']?>" ><?=$value['title']?></option>                
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="">Cover Image (1000px X 1000px)</label>
                    <input type="file" class="form-control-file" id="articlefile" name="articlefile" required >

                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <button type="button" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase proceed-article">Proceed</button>
                  <button type="button" class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase cancel-article">Cancel</button>
                </div>
              </div>
            </div>
            <div id="article-step-2">
              <p class="category-tag article-step">Step 2 of 2</p>
              <h2 class="profile-section-title">
                <div class="article-compose-title">
                  <a href="#/" id="new-article-back1"><img src="images/md-back.svg" alt=""></a>
                  <span id="data"> </span>
                </div>
                <div class="article-compose-actions">
                  <button type="submit" class="btn-celerity btn-small btn-blue btn-md-blue text-uppercase">Send for Approval</button>
                  <button type="button" class="btn-celerity btn-small btn-blue-inv btn-md-blue-inv text-uppercase cancel-article">Cancel</button>
                </div>
              </h2>
              <div class="row mt-40">
                <div class="col-12 col-sm-12">
                  <div class="form-group compose-article-content">
                    <!-- <img src="images/article-add.svg" alt="" class="article-add-icon"> -->
                    <textarea name="article_description" id="" cols="30" rows="15" class="compose-article article_description" placeholder="Tell your story…"></textarea>

                    <!-- <textarea class="form-control article_description" id="article_description" name="article_description" rows="15" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($article_data[0]['description'])?$article_data[0]['description']:"") ?></textarea> -->
                    
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="profile-content-tab" id="my-bookmarks-content">
          <h2 class="profile-section-title pst-border">My Bookmarks</h2>
          <div class="articles-list-wrapper bookmark-articles" id="articles-draft-content">
            <a href="#/" class="btn-celerity btn-small btn-red btn-delete-post delete-bookmark"><img src="images/delete-article.svg" alt=""> Delete</a>
            <a href="#/" class="btn-celerity btn-small btn-blue-inv btn-md-blue-inv btn-cancel-post cancel-bookmark">Cancel</a>
            <div class="article-list">

             <?php
                if(!empty($bookmark_data) && isset($bookmark_data)) {
                  foreach ($bookmark_data as $key => $value) {
                    $date_convert = date('Y-M-j',strtotime($value['updated_at']));
                    $date_convert_array = explode('-',$date_convert);
                     ?>
              <div class="myarticle-single">
                <a href="<?= base_url('article/'.$value['id'])?>">
                  <div class="bookmark-checkbox">
                    <input type="checkbox" value="<?=$value['id']?>">
                  </div>
                  <div class="mas-image">
                    <img src="<?= base_url('images/article_image/'.$value['image']) ?>" alt="">
                  </div>
                  <div class="article-block-content">
                    <p class="category-tag"><?= $value['category_name']?></p>
                    <h4><?=$value['title']?></h4>
                    <p class="mas-intro"><?= $value['title']?></p>
                    <div class="article-author-info">
                      <p>
                        <span class="author-name">Last Updated On </span> <?= $date_convert_array[1].' '.$date_convert_array[2].', '.$date_convert_array[0] ?></p>
                    </div>
                  </div>
                </a>
              </div>
            <?php } } ?>
            
            </div>
          </div>
        </div>
        <div class="profile-content-tab" id="my-notifications-content">
          <div class="row">
            <div class="col-12 col-sm-12 col-md-6">
              <h2 class="profile-section-title">Notifications</h2>
            </div>
            <!-- <div class="col-12 col-sm-12 col-md-6">
              <div class="category-search notifications-search">
                <form>
                  <input type="text" class="category-search-text" id="notifications-search" placeholder="Search">
                </form>
              </div>
            </div> -->
            <div class="col-12">
              <div class="magazine-title-seperator"></div>
            </div>
          </div>
          <!-- <div class="row">
            <div class="col-12">
              <div class="category-filter-wrapper notifications-filter-wrapper">
                <div class="category-sortby">
                  <span class="filter-label">Sort By</span>
                  <select name="" id="" class="niceselect">
                    <option value="Recent">Recent</option>
                    <option value="Most Viewed">Most Viewed</option>
                    <option value="Most Commented">Most Commented</option>
                  </select>
                </div>
              </div>
            </div>
          </div> -->
          <div class="row">
            <div class="col-12">
              <div class="notification-list-wrapper">

                <?php
                if(!empty($notification_user) && isset($notification_user)) {
                  foreach ($notification_user as $key => $value) {
                    // echo "<pre>";
                    // print_r($value);
                    if($value['notification_type'] == 'Articles'  && $value['article_status'] == 'Published' && $value['article_admin_status'] == 'Approved'){?>
                        <div class="notification-single">
                          <div class="notification-icon">
                            <img src="images/notification-article.svg" alt="">
                          </div>
                          <div class="notification-content">
                            <div class="notification-icon-mobile">
                              <img src="images/notification-article.svg" alt="">
                            </div>
                            <div class="notification-name-date">
                              <p class="notification-name"><?=$value['article_title'];?> Article <?= $value['article_admin_status'] ?> </p>
                              <p class="notification-date"><?=date('M d, Y',strtotime($value['article_updated_at']));?></p>
                            </div>
                            <div class="notification-post-comment">
                              <p> <?= $value['article_desc'];?> </p>
                              <a href="<?= base_url('article/'.$value['slug'])?>" class="view-article">
                                Click here to view
                              </a>
                            </div>
                          </div>
                        </div>

                <?php }else if ($value['notification_type'] == 'Comment'  && $value['status'] == 'Published' && $value['admin_status'] == 'Approved') {?>
                  <div class="notification-single">
                  <div class="notification-icon">
                    <img src="images/notification-comment.svg" alt="">
                  </div>
                  <div class="notification-content">
                    <div class="notification-icon-mobile">
                      <img src="images/notification-comment.svg" alt="">
                    </div>
                    <div class="notification-name-date">
                      <p class="notification-name"><?= $value['author_name']?></p>
                      <p class="notification-date"><?=date('M d, Y',strtotime($value['updated_at']));?></p>
                    </div>
                    <div class="notification-post-name">
                      <p>comment on <span><?= $value['commented_article_title']?></span></p>
                    </div>
                    <div class="notification-post-comment">
                      <p>"<?= $value['comment']?>"</p>
                      <a href="<?= base_url('article/'.$value['commented_slug'])?>" class="read-more">Read more</a>
                    </div>
                    <!-- <div class="comment-single">
                      <div class="comment-like">
                        <a href="#/"><img src="images/comment-like.svg" alt=""> Like</a>
                        <a href="#/"><img src="images/comment-reply.svg" alt=""> Comment</a>
                      </div>
                    </div> -->
                  </div>
                </div>
                  
                <?php } 
                    }
                  
                  } ?>

              

              </div>
            </div>
          </div>
        </div>
        <div class="profile-content-tab" id="my-settings-content">
          <div class="profile-content-wrapper">
            <form class="billing-form" id="form-password" method="post">
              <h2 class="profile-section-title">Settings</h2>
              <div class="row mb-20">
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" class="form-control" id="" name="email" value="<?=$data['email']?>" placeholder="samuel_massey@gmail.com">
                  </div>
                </div>
              </div>
              <h2 class="profile-section-title">Change Password</h2>
              <div class="row mb-20">
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">Current Password</label>
                    <input type="hidden" class="form-control" name="userid" value="<?=$data['id']?>" >
                    <input type="password" class="form-control" id="" name="currentpassword" placeholder="Enter current password">
                  </div>
                </div>
              </div>
              <div class="row mb-20">
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">New Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter New password">
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">Confirm Password</label>
                    <input type="password" class="form-control" id="" name="confirmpassword" placeholder="Enter New password">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <button type="submit" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase">Save</button>
                  <button type="submit" class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase">Cancel</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.12/handlebars.runtime.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sortable/0.9.13/jquery-sortable-min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery.ui.widget@1.10.3/jquery.ui.widget.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.iframe-transport/1.0.1/jquery.iframe-transport.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.28.0/js/jquery.fileupload.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/js/medium-editor.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/js/medium-editor-insert-plugin.min.js"></script>

<script>
        var editor = new MediumEditor('.article_description', {
            buttonLabels: 'fontawesome',
            toolbar: {
            allowMultiParagraphSelection: true,
            buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist', 'orderedlist'],
            diffLeft: 0,
            diffTop: -10,
            firstButtonClass: 'medium-editor-button-first',
            lastButtonClass: 'medium-editor-button-last',
            relativeContainer: null,
            standardizeSelectionStart: false,
            static: false,
            align: 'center',
            sticky: false,
            updateOnEmptySelection: false
        }
        });

        $(function () {
            $('.article_description').mediumInsert({
                editor: editor
            });
        });
    </script>

<script type="text/javascript">

  $(document).ready(function(){
    <?php if(!empty($billing[0])){ ?>
      getstate(<?= $billing[0]['country']?>,<?php $billing[0]['state'] ?>);
      getcity(<?= $billing[0]['state']?>,<?php $billing[0]['city'] ?>);
    <?php } ?> 
    

    $("#title").keyup(function () {
      var title = $(this).val();
      $("#data").text(title);
    });

    $('#new-article-back1').click(function(){
        $('#my-articles-content, #article-step-2').hide();   
        $('#new-article-content, #article-step-1').show();   
    });

    $('.cancel-article').click(function(){
        location.reload();   
    });
    
	$('#OpenImgUpload').click(function(){ $('#imgupload').trigger('click'); });
	
	/*Open relevant tab when selected from header menu*/
	//console.log(localStorage);
	if(localStorage.getItem('tabvalue') == 'my-articles' || localStorage.getItem('tabvalue') == 'my-bookmarks' ||  localStorage.getItem('tabvalue') == 'my-notifications' || localStorage.getItem('tabvalue') == 'my-settings'){
		var tabval = localStorage.getItem('tabvalue');		
		var tabvalue_content = '#'+tabval+'-content';
		$('.profile-content-tab').hide();
		$(tabvalue_content).show();
		$('.profile-tabs-wrapper a').removeClass('active');		
		$('.profile-tabs-wrapper a#'+tabval).addClass('active');
	}	
	/*Open relevant tab when selected from header menu*/	
  });
	/*Preview profile pic*/
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-pic-avatar')
                    .attr('src', e.target.result);                   
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
	/*Preview profile pic*/
  function getstate(country_id,state_id=null){
    if(country_id != "" ){
      $.ajax({
        url:"<?php echo base_url();?>billing/getStates",
        data:{country_id:country_id,state_id:state_id},
        dataType: 'json',
        method:'post',
        success: function(res){
          if(res['status']=="success"){
            if(res['option'] != ""){
              $("#state").html("<option value=''>Select State</option>"+res['option']);
              $("#state").val(<?= (!empty($billing[0]['state'])?$billing[0]['state']:"") ?>);
              
            }else{
              $("#state").html("<option value=''>No state found</option>");
        
            }
          }
          /* else{  
            $("#state").html("<option value=''>Select</option>");
            $("#state").selectpicker("refresh");
          } */
        }
      });
    }
  }

  function getcity(state_id,city_id=null){
  if(state_id != "" ){
    $.ajax({
      url:"<?php echo base_url();?>billing/getCities",
      data:{state_id:state_id,city_id:city_id},
      dataType: 'json',
      method:'post',
      success: function(res){
        if(res['status']=="success"){
          if(res['option'] != ""){
            $("#city").html("<option value=''>Select City</option>"+res['option']);
            $("#city").val(<?= (!empty($billing[0]['city'])?$billing[0]['city']:"") ?>);
          }else{
            $("#city").html("<option value=''>No state found</option>");
          }
        }else{  
          $("#city").html("<option value=''>Select</option>");
        }
      }
    });
  }
}

  $('.btn-new-post').click(function(){
      var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
      var csrfHash = $('.txt_csrfname').val(); // CSRF hash
      $.ajax({
        url: "<?= base_url('profile/subscribe')?>",
        type: "POST",
        data:{ [csrfName]: csrfHash },
        dataType: "json",
        success: function(response){
          if(response.success){
            $('#my-articles-content').hide();
            $('#new-article-content, #article-step-1').show();
            $('#article-step-2').hide();
            
          }else{  
            swal("", response.msg, {
              //icon : "success",
              buttons: {              
                confirm: {
                  className : 'btn btn-success'
                }
              },
            }).then(
            function() {
              window.location = "<?= base_url('billing')?>";
             //location.reload();
              
            });
          }
          
        }
      });
   
  });
  
  $(document).on('click', '.delete-draft', function () {
          var myCheckboxes = new Array();
              $("input:checked").each(function() {
                 myCheckboxes.push($(this).val());
              });
      if (myCheckboxes.length>0) {
        var r = confirm("Are you sure you want to delete this Draft Article?");
        if (r == true) {
        //var pagedata = { "keyword": myCheckboxes};
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
        var csrfHash = $('.txt_csrfname').val(); // CSRF hash
        $.ajax({
          url: "<?= base_url('profile/deleteDraft')?>",
          type: "POST",
          data:{ draft: myCheckboxes, [csrfName]: csrfHash },
          dataType: "json",
          success: function(response){
            if(response.success){
              swal("done", response.msg, {
                icon : "success",
                buttons: {              
                  confirm: {
                    className : 'btn btn-success'
                  }
                },
              }).then(
              function() {
               location.reload();
                
              });
            }else{  
              swal("Failed", response.msg, {
                icon : "error",
                buttons: {              
                  confirm: {
                    className : 'btn btn-danger'
                  }
                },
              });
            }
            
          }
        }); 

        }
      }else{
          alert("To delete the Draft you need to Draft checked first");
        }

  });

  $(document).on('click', '.delete-bookmark', function () {
          var myCheckboxes = new Array();
              $("input:checked").each(function() {
                 myCheckboxes.push($(this).val());
              });
      if (myCheckboxes.length>0) {
        var r = confirm("Are you sure you want to delete this Bookmark?");
        if (r == true) {
        //var pagedata = { "keyword": myCheckboxes};
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
        var csrfHash = $('.txt_csrfname').val(); // CSRF hash
        $.ajax({
          url: "<?= base_url('profile/deletebookmark')?>",
          type: "POST",
          data:{ bookmark: myCheckboxes, [csrfName]: csrfHash },
          dataType: "json",
          success: function(response){
            if(response.success){
              swal("done", response.msg, {
                icon : "success",
                buttons: {              
                  confirm: {
                    className : 'btn btn-success'
                  }
                },
              }).then(
              function() {
               location.reload();
                
              });
            }else{  
              swal("Failed", response.msg, {
                icon : "error",
                buttons: {              
                  confirm: {
                    className : 'btn btn-danger'
                  }
                },
              });
            }
            
          }
        }); 

        }
      }else{
          alert("To delete the bookmark you need to bookmark checked first");
        }

  });

  $(document).on('click', '.bookmark-checkbox input[type="checkbox"]', function () {
          var myCheckboxes = new Array();
              $("input:checked").each(function() {
                 myCheckboxes.push($(this).val());
              });
      if (myCheckboxes.length>0) {
        var pagedata = { "keyword": myCheckboxes};
          $('.delete-bookmark,.cancel-bookmark').show();
        }else{
          $('.delete-bookmark,.cancel-bookmark').hide();
        }

  });      
  /*Bookmark Button Toggle*/
  /*$('.bookmark-checkbox input[type="checkbox"]').click(function(){
    if($(this).prop("checked") == true){
        $('.delete-bookmark,.cancel-bookmark').show();
    }
    else if($(this).prop("checked") == false){
      $('.delete-bookmark,.cancel-bookmark').hide();
    }
  });*/
  $('.cancel-bookmark').click(function(){
    $('.bookmark-checkbox input[type="checkbox"]').prop("checked",false);
    $('.delete-bookmark,.cancel-bookmark').hide();
  });
  /*Bookmark Button Toggle*/ 

  $("#form-password").validate({
  ignore:[],
  rules: {
  currentpassword:{required:true},
 /* password:{required:true},
  confirmpassword:{required:true},*/
  password : {
      required:true,
      minlength : 6
                },
    confirmpassword : {
      required:true,
      minlength : 6,
      equalTo : "#password"
    }
    },
  messages: {
  currentpassword:{required:"Please Enter Current Password."},
  password:{required:"Please Enter New Password."},
  confirmpassword:{required:"Please Enter Confirm Password."},

    },
  submitHandler: function(form) 
  { 
    var act = "<?= base_url('profile/changepassword')?>";
    $("#form-password").ajaxSubmit({
      url: act, 
      type: 'post',
      dataType: 'json',
      cache: false,
      clearForm: false,
      async:false,
      beforeSubmit : function(arr, $form, options){
        $(".btn btn-black").hide();
      },
      success: function(response) 
      {
        if(response.success){
          swal("done", response.msg, {
            icon : "success",
            buttons: {              
              confirm: {
                className : 'btn btn-success'
              }
            },
          }).then(
          function() {
            window.location = "<?= base_url('profile/')?>";
          });
        }else{  
          swal("Failed", response.msg, {
            icon : "error",
            buttons: {              
              confirm: {
                className : 'btn btn-danger'
              }
            },
          });
        }
      }
    });

  }
});


  var vRules = 
{
  title:{required:true},
  "category[]":{required:true},
  "tag[]":{required:true},
};
var vMessages = 
{
  title:{required:"Please Enter Article Name."},
  "category[]":{required:"Please Select Categories ."},
  "tag[]":{required:"Please Select Tags ."},

};

$("#form-article").validate({
  ignore:[],
  rules: vRules,
  messages: vMessages,
  submitHandler: function(form) 
  { 
    var act = "<?= base_url('profile/submitform')?>";
    $("#form-article").ajaxSubmit({
      url: act, 
      type: 'post',
      dataType: 'json',
      cache: false,
      clearForm: false,
      async:false,
      beforeSubmit : function(arr, $form, options){
        $(".btn btn-black").hide();
      },
      success: function(response) 
      {
        if(response.success){
          swal("done", response.msg, {
            icon : "success",
            buttons: {              
              confirm: {
                className : 'btn btn-success'
              }
            },
          }).then(
          function() {
            window.location = "<?= base_url('profile')?>";
            // $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
          });
        }else{  
          swal("Failed", response.msg, {
            icon : "error",
            buttons: {              
              confirm: {
                className : 'btn btn-danger'
              }
            },
          });
        }
      }
    });
  }
});

$(document).on('click', '.proceed-article', function () {
  
      var form = $("#form-article");
      form.validate({
          rules: {
            title:{required:true},
            "category[]":{required:true},
            "tag[]":{required:true},
            // articlefile:{required:true},
          },
          messages: {

            title:{required:"Please Enter Article Name."},
            "category[]":{required:"Please Select Categories ."},
            "tag[]":{required:"Please Select Tags ."},
            // articlefile:{required:"Please Select Image."},
              
          }
          
      });
      if (form.valid() == true){
        $('#article-step-1').hide();
        $('#article-step-2').show(); 
          /*$('.descript').show();
          $('.basicinfo').hide();
          $('#next').hide();
          $('#save').show();   
          $('#previous').show();*/   
      }
  });

/*$('.proceed-article').click(function(){
    $('#article-step-1').hide();
    $('#article-step-2').show();  
  });*/


  $("#profile-form").validate({
    ignore:[],
    submitHandler: function(form) 
    { 
      var act = "<?= base_url('profile/profileupdate')?>";
      $("#profile-form").ajaxSubmit({
        url: act, 
        type: 'post',
        dataType: 'json',
        cache: false,
        clearForm: false,
        async:false,
        beforeSubmit : function(arr, $form, options){
          $(".btn btn-black").hide();
        },
        success: function(response) 
        {
          if(response.success){
            swal("done", response.msg, {
              icon : "success",
              buttons: {              
                confirm: {
                  className : 'btn btn-success'
                }
              },
            }).then(
            function() {
              window.location = "<?= base_url('profile')?>";
            });
          }else{  
            swal("Failed", response.msg, {
              icon : "error",
              buttons: {              
                confirm: {
                  className : 'btn btn-danger'
                }
              },
            });
          }
        }
      });

    }
  });


</script>
