<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Profile extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('profilemodel','',TRUE);
		// if(!$this->session->userdata('supply_chain_user'))
		// 	redirect(base_url().'home/index');

	}

	public function index(){
		$result = array();

		$userid = $this->session->userdata('supply_chain_user')[0]['id'];

		$condition = "user_id = '".$userid."' ";
		$result['billing'] = $this->common->getData("tbl_billing_information",'*',$condition);

		$where = "status = '1' ";
		$result['country_list'] = $this->common->getData("tbl_countries",'id, country_name',$where);
		$where1 = "status = '1' ";
		$result['state_list'] = $this->common->getData("tbl_states",'id, state_name',$where1);
		$where2 = "city_status = '1' ";
		$result['city_list'] = $this->common->getData("tbl_city",'id, city_name',$where2);
	
		// get all article with respective to user code on 30-10-2020 by shiv
		// $condition = " author_id ='".$userid."' ";
		// $result['article_data'] = $this->common->getData("tbl_magazine_articles",'*',$condition);

		$condition ="a.created_by ='".$userid."' ";
		$main_table = array("tbl_magazine_articles as a", array("a.*"));
		$join_tables =  array();
		$join_tables = array(
				array("", "tbl_magazine_article_category as  ac", "ac.article_id = a.id", array()),
				array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
				array("left", "tbl_users as  tu", "tu.id = a.created_by", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
				  );
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"a.id"); 
		$result['article_data'] = $this->common->MySqlFetchRow($rs, "array");

		
		// get all approved n  Published article with respective to user code on 30-10-2020 by shiv
		// $condition = " author_id ='".$userid."' AND status = 'Published' ";
		// $result['published_data'] = $this->common->getData("tbl_magazine_articles",'*',$condition,"id","desc");

		$condition = "a.status = 'Published' && a.admin_status ='Approved' && a.created_by ='".$userid."' ";
		$main_table = array("tbl_magazine_articles as a", array("a.*"));
		$join_tables =  array();
		$join_tables = array(
				array("", "tbl_magazine_article_category as  ac", "ac.article_id = a.id", array()),
				array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
				array("left", "tbl_users as  tu", "tu.id = a.created_by", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
				  );
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"a.id"); 
		$result['published_data'] = $this->common->MySqlFetchRow($rs, "array");




		// get all approved n  Published article with respective to user code on 30-10-2020 by shiv
		// $condition = " authr_oid ='".$userid."' AND status = 'Draft' ";
		// $result['draft_data'] = $this->common->getData("tbl_magazine_articles",'*',$condition,"id","desc");

		$condition = "a.status = 'Draft' && a.admin_status ='Pending' && a.created_by ='".$userid."' ";
		$main_table = array("tbl_magazine_articles as a", array("a.*"));
		$join_tables =  array();
		$join_tables = array(
				array("", "tbl_magazine_article_category as  ac", "ac.article_id = a.id", array()),
				array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
				array("left", "tbl_users as  tu", "tu.id = a.created_by", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
				  );
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"a.id"); 
		$result['draft_data'] = $this->common->MySqlFetchRow($rs, "array");

		$condition = "a.status = 'Published' && a.admin_status ='Pending' && a.created_by ='".$userid."' ";
		$main_table = array("tbl_magazine_articles as a", array("a.*"));
		$join_tables =  array();
		$join_tables = array(
				array("", "tbl_magazine_article_category as  ac", "ac.article_id = a.id", array()),
				array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
				array("left", "tbl_users as  tu", "tu.id = a.created_by", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
				  );
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"a.id"); 
		$result['review_article'] = $this->common->MySqlFetchRow($rs, "array");

		
		$condition = "b.user_id ='".$userid."' ";
		$main_table = array("tbl_magazine_articles as m", array("m.*"));
		$join_tables =  array();
		$join_tables = array(
				  array("", "tbl_bookmark as b", "b.article_id = m.id", array()),
				  array("", "tbl_magazine_article_category as  ac", "ac.article_id = m.id", array()),
				  array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
				  
		          );
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("m.id" => "DESC"),"m.id",null); 
		$result['bookmark_data'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

		// $condition = " b.user_id ='".$userid."' ";
		// $main_table = array("tbl_magazine_articles as m", array("m.title,m.meta_description,m.slug"));
		// $join_tables =  array();
		// $join_tables = array(
		//           array("", "tbl_notification as b", "b.article_id = m.id", array("b.*")),
		//           );
		// $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("b.id" => "DESC"),"b.id",null); 
		// $result['notification'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

		$condition = "sender_id = '".$this->session->userdata('supply_chain_user')[0]['id']."' AND tn.status = 'Unread'  ";
		$main_table = array("tbl_notifications as tn", array("tn.notification_type,tn.article_id,tn.comment_id,tn.id as notification_id,tn.updated_at as article_updated_at"));
		$join_tables =  array();
		$join_tables = array(
							array("left", "tbl_magazine_articles as  ta", "ta.id = tn.article_id", array("ta.title as article_title,ta.id as article_id,ta.description as article_desc,ta.slug,ta.status as article_status,ta.admin_status as article_admin_status")),
							array("left", "tbl_magazine_comments as  tmc", "tmc.id = tn.comment_id AND tmc.status = 'Published' AND tmc.admin_status = 'Approved' ", array("tmc.*")),
							array("left", "tbl_magazine_articles as  ta1", "ta1.id = tmc.article_id", array('ta1.title as commented_article_title,ta1.id as commented_article_id,ta1.slug as commented_slug')),
							array("left", "tbl_users as  tu", "tu.id = tmc.user_id", array("concat(tu.first_name,' ',tu.last_name) as author_name"))
						);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tn.id" => "DESC"),"",null); 
			// fetch query
		$result['notification_user'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
		

		$condition = " admin_status ='Approved' ";
		$result['tags'] = $this->common->getData("tbl_magazine_tags",'id,title',$condition);
		$result['categories'] = $this->common->getData("tbl_magazine_categories",'id,title',$condition);
		// echo '<pre>';print_r($result['notification_user']);die;
		$this->load->view('header.php');
		$this->load->view('profile',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$user_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$user_id = $url_prams['id'];
			$condition = " user_id ='".$user_id."' ";
			$result['theme_data'] = $this->common->getData("tbl_users",'*',$condition);
		}
		
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	public function submitForm(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "title = '".$this->input->post('title')."' ";
			if($this->input->post('id') && $this->input->post('id') > 0){
				$condition .= " AND  id != ".$this->input->post('id')." ";
			}			
			$check_name = $this->common->getData("tbl_magazine_articles",'*',$condition);

			if(!empty($check_name[0]['id'])){
				echo json_encode(array("success"=>false, 'msg'=>'Article Name Already Present!'));
				exit;
			}
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["articlefile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/article_image/";
				$config['max_size']    = '0';
				$config['allowed_types'] = '*';
                $config['min_width']            = 1000;
                $config['min_height']           = 1000;
				$config['file_name']     = $_FILES["articlefile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("articlefile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);

				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('id'))){	
					$condition_image = " id = ".$this->input->post('id');
					$image =$this->common->getData("tbl_magazine_articles",'*',$condition_image);
					if(is_array($image) && !empty($image[0]['image']) && file_exists(DOC_ROOT_FRONT."/images/article_image/".$image[0]['image']))
					{
						unlink(DOC_ROOT_FRONT."/images/article_image/".$image[0]['image']);
					}
				}
			}else{
				$thumnail_value = "";
			}
			$str = $this->input->post('title');
			$str1 = explode(" ",$str);
			$data['image'] = (!empty($thumnail_value)?$thumnail_value:"");
			$data['title'] = $this->input->post('title');
			$data['slug'] = implode("-",$str1);
			$data['description'] = $this->input->post('description');
			// $data['author_id'] = $this->session->userdata('supply_chain_user')[0]['id'];
			$data['author'] = $this->session->userdata('supply_chain_user')[0]['first_name'].' '.$this->session->userdata('supply_chain_user')[0]['last_name'] ;
			$data['article_date'] = date('Y-m-d');
			$data['description'] = $this->input->post('article_description');
			$data['status'] = 'Published';
			$data['admin_status'] = 'Pending';
			$data['updated_at'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('supply_chain_user')[0]['id'];
			if(!empty($this->input->post('id'))){
				$condition = "id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_magazine_articles",$data,$condition);
				if($result){

					$condition = "article_id = '".$this->input->post('id')."' ";
					$this->common->deleteRecord("tbl_magazine_article_category",$condition);
					$this->common->deleteRecord("tbl_magazine_article_tag",$condition);
					if(!empty($_POST['category']) && isset($_POST['category'])){
						foreach ($_POST['category'] as $key => $value) {
							$article_data_category = array();
							$article_data_category['article_id'] = $this->input->post('id');
							$article_data_category['category_id'] = $value;
							$this->common->insertData('tbl_magazine_article_category',$article_data_category,'1');
							// echo "inside category  update";
							// exit;
						}
					}

					if(!empty($_POST['tag']) && isset($_POST['tag'])){
						foreach ($_POST['tag'] as $key => $value) {
							$article_data_tag = array();
							$article_data_tag['article_id'] = $this->input->post('id');
							$article_data_tag['tag_id'] = $value;
							$this->common->insertData('tbl_magazine_article_tag',$article_data_tag,'1');
						}
					}
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('supply_chain_user')[0]['id'];
				$result = $this->common->insertData('tbl_magazine_articles',$data,'1');

			

				if(!empty($result)){
					if(!empty($_POST['category']) && isset($_POST['category'])){
						foreach ($_POST['category'] as $key => $value) {
							$article_data_category = array();
							$article_data_category['article_id'] = $result;
							$article_data_category['category_id'] = $value;
							$this->common->insertData('tbl_magazine_article_category',$article_data_category,'1');
						}
					}

					if(!empty($_POST['tag']) && isset($_POST['tag'])){
						foreach ($_POST['tag'] as $key => $value) {
							$article_data_tag = array();
							$article_data_tag['article_id'] = $result;
							$article_data_tag['tag_id'] = $value;
							$this->common->insertData('tbl_magazine_article_tag',$article_data_tag,'1');
						}
					}

					$notification_data = array();
					$notification_data['user_id'] = $this->session->userdata('supply_chain_user')[0]['id'];
					$notification_data['sender_id'] = '1';
					$notification_data['article_id'] = $result;
					$notification_data['notification_type'] = 'Articles';
					$notification_data['created_at'] = date("Y-m-d H:i:s");
					$this->common->insertData('tbl_notifications',$notification_data,'1');
					

					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
			
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function profileupdate(){
		// echo "<pre>";
		// print_r($_SESSION);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();

				$profile_image = "";
				if(isset($_FILES) && isset($_FILES["imgupload"]["name"])){
				 $config = array();
				 $config['upload_path'] = DOC_ROOT_FRONT."/images/profile_image/";
				 $config['max_size']    = '2000';
				 $config['allowed_types'] = '*';
                // $config['min_width']            = 1000;
                // $config['min_height']           = 1000;
				$config['file_name']     = $_FILES["imgupload"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("imgupload")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$profile_image = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);

				}	
				/* Unlink previous profile image */
				if(!empty($this->session->userdata('supply_chain_user')[0]['id'])){	
					$condition_image = " id = ".$this->session->userdata('supply_chain_user')[0]['id'];
					$image =$this->common->getData("tbl_users",'*',$condition_image);
					if(is_array($image) && !empty($image[0]['profile_photo']) && file_exists(DOC_ROOT_FRONT."/images/profile_image/".$image[0]['profile_photo']))
					{
						unlink(DOC_ROOT_FRONT."/images/profile_image/".$image[0]['profile_photo']);
					}
				}
			}else{
				$profile_image = $this->input->post('pre_imgupload');
			}

			$user_data =array();
			$condition = "id = '".$this->session->userdata('supply_chain_user')[0]['id']."' ";
			$user_data['first_name']      = (!empty($this->input->post('fname'))?$this->input->post('fname'):$this->session->userdata('supply_chain_user')[0]['first_name']);
			$user_data['last_name']       = (!empty($this->input->post('lname'))?$this->input->post('lname'):$this->session->userdata('supply_chain_user')[0]['last_name']);
			$user_data['profile_photo']       = $profile_image;
			$this->common->updateData("tbl_users",$user_data,$condition);

			$result = $this->common->getData('tbl_users','*',$condition);
				if($result){
					$session_data = array('supply_chain_user'=>$result);  
					$this->session->set_userdata($session_data);
				}

			$data['user_id'] = $this->session->userdata('supply_chain_user')[0]['id'];
			$data['first_name']      = $this->input->post('fname');
			$data['last_name']       = $this->input->post('lname');
			$data['billing_address'] = $this->input->post('address');
			$data['country']         = $this->input->post('country');
			$data['state']           = $this->input->post('state');
			$data['city']            = $this->input->post('city');
			$data['zipcode']            = $this->input->post('zipcode');
			
			if(!empty($this->input->post('id'))){
				$condition = "id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_billing_information",$data,$condition);
				if($result){
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");
				$result = $this->common->insertData('tbl_billing_information',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
			
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function changepassword(){
		//print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "password = '".md5($this->input->post('currentpassword'))."' ";
			if($this->input->post('userid') && $this->input->post('userid') > 0){
				$condition .= " AND  id = ".$this->input->post('userid')." ";
			}			
			$check_name = $this->common->getData("tbl_users",'*',$condition);
			if(empty($check_name[0]['id'])){
				echo json_encode(array("success"=>false, 'msg'=>'Current Password did not match'));
				exit;
			}

			if($this->input->post('password') !== $this->input->post('confirmpassword') ){
				echo json_encode(array("success"=>false, 'msg'=>'New Password and Confirm Password did not match'));
				exit;
			}

			$data['password'] = md5($this->input->post('password'));
			$data['updated_at'] = date("Y-m-d H:i:s");
			
			if(!empty($this->input->post('userid'))){

				$condition = "id = '".$this->input->post('userid')."' ";
				$result = $this->common->updateData("tbl_users",$data,$condition);
				if($result){ 
					echo json_encode(array('success'=>true, 'msg'=>'Password Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	

	function changeUserStatus(){

		$data = array();
		$condition =" role_id = ".$_POST['role_id']." ";
		$get_usertype = $this->common->getData("tbl_roles","*",$condition);
		$data['user_type'] = $get_usertype[0]['role_name'];
		$data['role_id'] = $this->input->post('role_id');
	
		$data['updated_on'] = date("Y-m-d H:i:s");
		$data['updated_by'] = $this->session->userdata('footage_farm_user')[0]['user_id'];
		$condition = "user_id = '".$this->input->post('user_id')."' ";
		$result = $this->common->updateData("tbl_users",$data,$condition);
		if($result){
			echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
			exit;
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
			exit;
		}
	}

	function deleteUser(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$condition="user_id = '".$_POST['user_id']."' ";
			$result = $this->common->deleteRecord('tbl_users',$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'User deleted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}
	}

	public function editprofile(){
		$result = array();
		$condition = "1=1 AND user_id = ".$this->session->userdata('footage_farm_user')[0]['user_id']." ";
		$result['profile_data'] = $this->common->getData("tbl_users",'*',$condition);
		//get  enum value
		$result['roles'] = $this->common->getData("tbl_roles","*");
		
		$this->load->view('header.php');
		$this->load->view('editprofile',$result);
		$this->load->view('footer.php');
	}

	public function delete_row(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

			$condition = "project_id = '".$_POST['id']."' ";
			$result = $this->common->deleteRecord('tbl_projects',$condition);
					  $this->common->deleteRecord('tbl_project_reel',$condition);
					  
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record deleted Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}
		}

	}


	public function deletebookmark(){
		//print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			
			if(!empty($this->input->post('bookmark'))){
				$bookmark = $this->input->post('bookmark');
				foreach ($bookmark as $key => $value) {
					$condition = "article_id = '".$value."' AND user_id = '".$this->session->userdata('supply_chain_user')[0]['id']."' ";
				    $result = $this->common->deleteRecord("tbl_bookmark",$condition);
				}
				if($result){ 
					echo json_encode(array('success'=>true, 'msg'=>'Bookmark Delete Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function deleteDraft(){
		//print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			
			if(!empty($this->input->post('draft'))){
				$draft = $this->input->post('draft');
				foreach ($draft as $key => $value) {
					$condition = "id = '".$value."'";
				    $result = $this->common->deleteRecord("tbl_magazine_articles",$condition);
				}
				if($result){ 
					echo json_encode(array('success'=>true, 'msg'=>'Draft Delete Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	function subscribe(){

		$condition = "user_id = '".$this->session->userdata('supply_chain_user')[0]['id']."' AND payment_status = 'Success' ";
		$subscribe = $this->common->getData("tbl_payment",'duration,paid_date',$condition);
		if (!empty($subscribe)) {
		$aa = end($subscribe);
		$date = $aa['paid_date'];
		$duration = $aa['duration'];
		$expire = date("Y-m-d", strtotime("$date + $duration months"));
		$a = date("Y-m-d");
		if($expire>$a){
			echo json_encode(array('success'=>true, 'msg'=>''));
			exit;
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Sorry you need to suscribe/renew the package'));
			exit;
		}
	}else{
		echo json_encode(array('success'=>false, 'msg'=>'Sorry you need to suscribe/renew the package'));
		exit;
	}
}

	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/article_image/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		$config1['width'] = 1000;
		$config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/article_image/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
		}
	
}?>