
	<script src="<?= base_url('assets/')?>js/bootstrap.min.js"></script>
	<script src="<?= base_url('assets/')?>js/owl.carousel.min.js"></script>
	<script src="<?= base_url('assets/')?>js/jquery.fancybox.min.js"></script>
	<script src="<?= base_url('assets/')?>js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="<?= base_url('assets/')?>js/jquery.nice-select.min.js"></script>
	<script src="<?= base_url('assets/')?>js/prognroll.min.js"></script>
	<script src="<?= base_url('assets/')?>js/script.js"></script>

	<script src='<?= base_url()?>assets/js/jquery.form.js'></script>
	<script src='<?= base_url()?>assets/js/jquery.validate.js'></script> 
	<script src='<?= base_url()?>assets/js/form-validation.js'></script>
	<script src="<?= base_url("assets/js/plugin/sweetalert/sweetalert.min.js"); ?>"></script>
	<script src="<?= base_url("assets/js/plugin/select2/select2.full.min.js"); ?>"></script>


    <footer>
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-3">
					<a href="index.php"><img src="<?= base_url('assets/') ?>images/logo.svg" alt="" class="img-fluid"></a>
				</div>
				<div class="col-12 col-sm-12 col-md-3">
					<!--<div class="footer-section footer-address">					
						<p>1505, Darshan Heights,</p>
						<p>Opp Deepak Cinema, Off Tulsipipe Road,</p>
						<p>Elphinstone West, Mumbai</p>
						<p>400013 Maharashtra, India</p>
					</div>-->
				</div>
				<div class="col-12 col-sm-12 col-md-3">
					<div class="footer-section get-in-touch">
						<h5 class="footer-title hidden-mobile">Get in touch</h5>
						<div class="contact-detail phone">
							<a href="tel:+917977105913">
								<img src="<?= base_url('assets/') ?>images/phone-call.svg" alt="">
								<span>+91 7977105913</span>
							</a>
						</div>
						<div class="contact-detail">
							<a href="mailto:tech@celerityin.com">
								<img src="<?= base_url('assets/') ?>images/mail.svg" alt="">
								<span>tech@celerityin.com</span>
							</a>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-3">
					<div class="footer-section newsletter">
						<h5 class="footer-title mb-10">Sign up for newsletter</h5>
						<p>Sign up for weekly emails on latest supplychain trends.</p>
						<form method="post" id="newsletter-form">
							<input type="text" id="newsletter-email" name="email" class="newsletter-email" placeholder="eg. john@gmail.com">
							<input type="submit" class="btn-celerity btn-small btn-blue" value="Sign Up">
						</form>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="footer-links">
						<a href="<?= base_url()?>">Home</a>
						<a href="<?= base_url('news_event')?>">News & Events</a>
						<a href="https://www.40under40.in" target="_blank">Awards</a>
						<a href="<?= base_url('aboutus')?>">About us</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
				<div class="copyright-wrapper">
					<div class="copyright-text">
						<span>Copyright © Celerity India Marketing Services</span>
						<a href="<?= base_url('terms_condition')?>" class="disclaimer">Disclaimer</a>
						<a href="<?= base_url('privacy_policy')?>" class="privacy">Privacy Policy</a>
					</div>
					<div class="social-footer">
						<a href="#/"><img src="<?= base_url('assets/') ?>images/facebook.svg" alt=""></a>
						<a href="#/"><img src="<?= base_url('assets/') ?>images/twitter.svg" alt=""></a>
						<a href="#/"><img src="<?= base_url('assets/') ?>images/linkedin.svg" alt=""></a>
					</div>
				</div>
				</div>
			</div>
		</div>

		<?php 
		if(!isset($_COOKIE['celerity'])) {
		?>
		<div class="cookie-container">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="cookie-content">
							<div class="cookie-text">
								<p>This site stores cookies to enhance the user experience, read more in our privacy policy.<br/>All good?</p>
							</div>
							<div class="cookie-link">
								<a href="#/" class="btn-celerity btn-blue btn-small accept-cookie">Accept</a>
								<a href="#/" class="btn-celerity btn-black btn-small reject-cookie">Decline</a>
							</div>
						</div>
					</div>
				</div>
			</div>		
		</div>

		<?php } ?>

	</footer>

	<script type="text/javascript">
	<?php 
	if($this->session->flashdata('success_msg')) {?>
		// alert("<?= $this->session->flashdata('success_msg')?>");
		// swal("Welcome <?= $this->session->userdata('supply_chain_user')[0]['username']?>","<?= $this->session->flashdata('success_msg')?>", {
		// 	icon : "success",
		// 	buttons: {        			
		// 		confirm: {
		// 			className : 'btn btn-success'
		// 		}
		// 	},
		// })
	<?php }
	?>
	</script>

<script>

$(document).ready(function(){
	$('.newvideotags').select2();

	  $(document).on("click",".accept-cookie",function () {
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
        var csrfHash = $('.txt_csrfname').val(); // CSRF hash
      $.ajax({
        url: "<?= base_url('register/cookies')?>",
        type: "POST",
        data:{ [csrfName]: csrfHash },
        dataType: "json",
        success: function(response){

        	// location.reload();
          
        }
      }); 
          
    });


	/*$(document).on("keyup","input[name='search']",function () {
		var key = $(this).val();
        
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	    if (key.length > 2) {
	    	window.location = "<?= base_url('search?key=')?>"+key;
	    }
		   
	    });

	$(document).on("change","input[name='search']",function () {
		var key = $(this).val();
        if(key != ''){
        	window.location = "<?= base_url('search?key=')?>"+key;
        }
	});*/

});

// use for form submit
var vRules = 
{
	fname:{required:true},
	lname:{required:true},
	username:{required:true},
	email:{required:true},
	password : {
			required:true,
            minlength : 6
                },
    cpassword : {
    	required:true,
        minlength : 6,
        equalTo : "#password"
    }
};
var vMessages = 
{
	fname:{required:"Please Enter First Name."},
	lname:{required:"Please Enter Last Name."},
	username:{required:"Please Enter Username."},
	email:{required:"Please Enter Email."},
	password:{required:"Please Enter Password."},
	cpassword:{required:"Please Enter Confirm Password."},

};

$("#celerity-signup").validate({
	ignore:[],
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('register/submitForm')?>";
		$("#celerity-signup").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('billing')?>";
						
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
				
			}
		});

	}
});

$("#celerity-forgotpassword").validate({
	ignore:[],
	rules: { email:{required:true} },	
	messages: { email:{required:"Please Enter Email."} },
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('register/forgotpassword')?>";
		$("#celerity-forgotpassword").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('login')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});

	}
});

$("#form-profile").validate({
	ignore:[],
	rules: {
	username:{required:true},
	useremail:{required:true},
	userphone:{required:true},
    },
	messages: {
	username:{required:"Please Enter User Name."},
	useremail:{required:"Please Enter Email."},
	userphone:{required:"Please Enter Phone No."},

    },
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('profile/submitForm')?>";
		$("#form-profile").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('profile/editprofile')?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});

	}
});

$("#newsletter-form").validate({
	ignore:[],
	rules: {
	email:{required:true},
    },
	messages: {
	email:{required:"Please Enter Email."},
    },
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('newsletter/letter')?>";
		$("#newsletter-form").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						location.reload();
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});

	}
});

if($(window).width() > 991){
if(typeof(Storage)!=="undefined")
  {	  
    $('.profile-links-wrapper-menu a, a.notification-menu').click(function(){
      //var tabvalue = $(this).attr('id');	 
	  var tabvalue = $(this).attr('data-val');	  
      localStorage.tabvalue=tabvalue; 	  
    });	
  }
else
  {  
  alert("Sorry, your browser does not support web storage...");
  }
}

</script>


	</body>
</html>