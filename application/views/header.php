<?php 


		$this->load->model('common_model/common_model','common',TRUE);
	// $this->load->model('home/home','',TRUE);
	$condition = "sender_id = '".$this->session->userdata('supply_chain_user')[0]['id']."' AND tn.status = 'Unread'  ";
	$main_table = array("tbl_notifications as tn", array("tn.notification_type,tn.article_id,tn.comment_id,tn.id as notification_id"));
	$join_tables =  array();
	$join_tables = array(
						array("left", "tbl_magazine_articles as  ta", "ta.id = tn.article_id", array("ta.title as article_title,ta.id as article_id,ta.description as article_desc,ta.slug,ta.status as article_status,ta.admin_status as article_admin_status")),
						array("left", "tbl_magazine_comments as  tmc", "tmc.id = tn.comment_id AND tmc.status = 'Published' AND tmc.admin_status = 'Approved' ", array("tmc.*")),
						array("left", "tbl_magazine_articles as  ta1", "ta1.id = tmc.article_id", array('ta1.title as commented_article_title,ta1.id as commented_article_id,ta1.slug as commented_slug')),
						array("left", "tbl_users as  tu", "tu.id = tmc.user_id", array("concat(tu.first_name,' ',tu.last_name) as author_name"))
					);

	$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tn.id" => "DESC"),"",null); 
		// fetch query
	$notification_user = $this->common->MySqlFetchRow($rs, "array"); // fetch result
	
	// echo "<pre>";
	// print_r($notification_user);
	// exit;
?>

<!DOCTYPE html>
<html id="celerity-page" class="show">

<head>
	<meta charset="UTF-8">
	<meta name="description" content="<?= (!empty($meta_description)?$meta_description:"")  ?>">
	<meta name="keywords" content="<?= (!empty($meta_keywords)?$meta_keywords:"")?>">
	<meta name="meta_title" content="<?= (!empty($meta_title)?$meta_title:"")?>">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Celerity - Supply Chain</title>
	<link rel="icon" href="<?= base_url('assets/') ?>images/favicon-32x32.png" type="<?= base_url('assets/') ?>images/png" sizes="32x32">
	<link rel="stylesheet" href="<?= base_url('assets/')?>css/bootstrap.css" />
	<link rel="stylesheet" href="<?= base_url('assets/')?>css/owl.carousel.min.css" />
	<link rel="stylesheet" href="<?= base_url('assets/')?>css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="<?= base_url('assets/')?>css/jquery.mCustomScrollbar.min.css" />
	<link rel="stylesheet" href="<?= base_url('assets/')?>css/style.css" />
	<link rel="stylesheet" href="<?= base_url('assets/')?>css/responsive.css" />
	<!--<link rel="stylesheet" href="<?php echo base_url("assets/css/custom.css"); ?>">-->
	<link rel="stylesheet" href="<?php echo base_url("assets/css/select2.css"); ?>">
	<script src="<?= base_url()?>assets/js/jquery-3.2.1.min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.validate.min.js"></script>
<script src='<?= base_url()?>assets/js/jquery.form.js'></script> 
<script src='<?= base_url()?>assets/js/form-validation.js'></script>
<script src="<?= base_url("assets/js/plugin/sweetalert/sweetalert.min.js"); ?>"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-102688214-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-102688214-1');
</script>

<style type="text/css">
	label.error {
	    display: block;
	    color: #D00000;
	    font-weight: normal !important;
	    font-size: 12px;
	    letter-spacing: 0;
	    line-height: 22px;
	}
</style>

</head>


<!-- Header -->
<div class="container-fluid">
	<div class="row">
		<div class="col-12 pl-0 pr-0 nopadding-mobile">
			<div class="celerity-header">
				<div class="search-menu-wrapper">
					<a href="#" id="menu-toggle-link">
						<img src="<?= base_url('assets/') ?>images/menu.svg" alt="">
						<span>Explore</span>
					</a>
					<a href="#/" id="search-link-mobile"><img src="<?= base_url('assets/') ?>images/search-mobile.jpg" alt=""></a>
					<div class="search-wrapper">
						<form action="<?= base_url('search')?>">
							<input type="text" class="nav-search" name="key" placeholder="Search" value="<?=isset($keywords)?$keywords:''?>">
							<datalist id="browsers">
				    
				            </datalist>
						</form>
					</div>
				</div>
				<a href="<?= base_url()?>" class="celerity-logo"><img src="<?= base_url('assets/') ?>images/logo.svg" alt=""></a>
				<div class="login-links">
					<a href="https://www.40under40.in" target="_blank" class="btn-celerity btn-small btn-black">Celerity Awards</a>					
					<?php
					if($this->session->userdata('supply_chain_user')){ ?>
						<a href="<?= base_url('profile')?>" class="notification-menu" data-val="my-notifications">
							<img src="<?= base_url('assets/') ?>images/notification-menu.svg" alt="Notifications">
							<span></span>
							<!-- <img src="<?= base_url('assets/') ?>images/notification-menu-green.svg" alt="Notifications"> -->
						</a>
				

						<!-- <a href="<?= base_url('register/logout') ?>" class="btn-celerity btn-small btn-blue">Logout</a> -->
						<?php 
							if(!empty($this->session->userdata('supply_chain_user')[0]['profile_photo'])){?>
								<a href="#/" id="dropdown-desktop">
								<img src="<?= (empty($this->session->userdata('supply_chain_user')[0]['login_oauth_uid'])?FRONT_URL.'/images/profile_image/'.$this->session->userdata('supply_chain_user')[0]['profile_photo']:$this->session->userdata('supply_chain_user')[0]['profile_photo']) ?>"  class="dropdown-desktop-img">
								<span><?= $this->session->userdata('supply_chain_user')[0]['first_name'] ?></span></a>
							<?php }else{?>
								<a href="#/" id="dropdown-desktop">
									<span class="user-profile-icon"><?php $string= $this->session->userdata('supply_chain_user')[0]['first_name'];$stringLength= strlen($string);
 									echo ucfirst(substr($string,-$stringLength-1, 1));?></span><span><?= $this->session->userdata('supply_chain_user')[0]['first_name'] ?></span>
							<?php }	?>
							<span>
								<span><img src="images/down-arrow.svg" alt="" class="dropdown-toggle-arrow"></a>
					
								<div class="login-flyout">
									<div class="profile-tabs-menu profile-tabs-menu-nav">							
										<div class="profile-name">
											
											<?php  
										
											if(!empty($this->session->userdata('supply_chain_user')[0]['profile_photo'])){?>
												<img src="<?= (empty($this->session->userdata('supply_chain_user')[0]['login_oauth_uid'])?FRONT_URL.'/images/profile_image/'.$this->session->userdata('supply_chain_user')[0]['profile_photo']:$this->session->userdata('supply_chain_user')[0]['profile_photo']) ?>" class="dropdown-desktop-img" alt="">
											<?php }else{?>
													<span class="user-profile-icon user-profile-icon-nav"><?php $string= $this->session->userdata('supply_chain_user')[0]['first_name'];$stringLength= strlen($string);	echo ucfirst(substr($string,-$stringLength-1, 1));?></span>
											<?php }	?>
											<div>
												<p class="user-name">Hi, <?= $this->session->userdata('supply_chain_user')[0]['first_name'] ?></p>
												<p class="user-email"><?= $this->session->userdata('supply_chain_user')[0]['email'] ?></p>
											</div>
										</div>
										<div class="profile-links-wrapper profile-links-wrapper-menu">
											<a href="<?= base_url('profile') ?>" id="my-profile" data-val="my-profile">My Profile</a>
											<a href="<?= base_url('profile') ?>" id="my-articles" data-val="my-articles">My Articles</a>
											<a href="<?= base_url('profile') ?>" id="my-bookmarks" data-val="my-bookmarks">My Bookmarks</a>
											<a href="<?= base_url('profile') ?>" id="my-notifications" data-val="my-notifications">Notifications</a>
											<a href="<?= base_url('profile') ?>" id="my-settings" data-val="my-settings">Settings</a>
											<span class="profile-logout-seperator profile-logout-seperator-menu"></span>
											<a href="<?= base_url('register/logout') ?>" id="profile-logout">Logout</a>
										</div>
									</div>
								</div>
				    <?php }else{ ?>
					<a href="<?= base_url('login') ?>" class="btn-celerity btn-small btn-blue">Login/ Subscribe</a>
					<?php } ?>
				</div>
				<a href="<?= base_url('profile') ?>" class="account-mobile">
					<?php
						if($this->session->userdata('supply_chain_user')[0]){
							if(!empty($this->session->userdata('supply_chain_user')[0]['profile_photo'])){?>
								<img src="<?= (empty($this->session->userdata('supply_chain_user')[0]['login_oauth_uid'])?FRONT_URL.'/images/profile_image/'.$this->session->userdata('supply_chain_user')[0]['profile_photo']:$this->session->userdata('supply_chain_user')[0]['profile_photo']) ?>"  class="dropdown-desktop-img">
							<?php }else{?>
									<span class="user-profile-icon">
									<?php $string= $this->session->userdata('supply_chain_user')[0]['first_name'];
										$stringLength= strlen($string);	
										echo ucfirst(substr($string,-$stringLength-1, 1));?>
									</span><span><?= $this->session->userdata('supply_chain_user')[0]['first_name'] ?></span>
							<?php }	
						}else{?>
								<img src="<?= base_url('assets/images/account-mobile.svg')?>" alt="">
						<?php }?>
						
					</a>
			</div>

		</div>
	</div>
</div>
<!-- Navigation Menu -->
<div class="menu-overlay" id="menu-overlay"></div>
<div class="menu-block-wrapper" id="slider">
	<div class="menu-block">
		<div class="main-menu-block">
			<div class="menu-header-mobile">
				<a href="#" class="mobile-header-close">
					<img src="<?= base_url('assets/') ?>images/close-mobile-menu.svg" alt="">
				</a>
				<a href="<?= base_url()?>" class="mobile-header-logo">
					<img src="<?= base_url('assets/') ?>images/logo.svg" alt="">
				</a>
				<a href="<?= base_url('profile') ?>" class="mobile-header-account">
						<?php if($this->session->userdata('supply_chain_user')[0]){
							if(!empty($this->session->userdata('supply_chain_user')[0]['profile_photo'])){?>
								<img src="<?= (empty($this->session->userdata('supply_chain_user')[0]['login_oauth_uid'])?FRONT_URL.'/images/profile_image/'.$this->session->userdata('supply_chain_user')[0]['profile_photo']:$this->session->userdata('supply_chain_user')[0]['profile_photo']) ?>"  class="dropdown-desktop-img">
							<?php }else{?>
									<span class="user-profile-icon">
									<?php $string= $this->session->userdata('supply_chain_user')[0]['first_name'];
										$stringLength= strlen($string);	
										echo ucfirst(substr($string,-$stringLength-1, 1));?>
									</span><span><?= $this->session->userdata('supply_chain_user')[0]['first_name'] ?></span>
							<?php }	
						}else{?>
								<img src="<?= base_url('assets/images/account-mobile.svg')?>" alt="">
						<?php }?>
					<!-- <img src="<?= base_url('assets/') ?>images/account-mobile.svg" alt=""> -->
				</a>
			</div>
			<div class="main-menu">
				<ul class="main-categories-list">
					<li class="main-category-dropdown">
						<!--<a href="<?= base_url('categories')?>" class=" main-category-item"><span class="dot categories"></span>Categories <img src="<?= base_url('assets/') ?>images/right-arrow-menu.png" alt=""></a>-->
						<a href="#/" class=" main-category-item"><span class="dot categories"></span>Categories <img src="<?= base_url('assets/') ?>images/right-arrow-menu.png" alt=""></a>
						<ul class="flyout-list">
							<li>
								<div class="sub-menu-block">
									<div class="submenu">
										<h3><img src="<?= base_url('assets/') ?>images/subcategory-back.svg" alt="" class="back-mobile"> Categories</h3>
										<?php 
											$condition = " status = 'Published' && admin_status ='Approved' && category_delete = 1 ";
											$categories = $this->common->getData("tbl_magazine_categories",'*',$condition,"id","desc");
											$condition = " status = 'Published' && admin_status ='Approved' ";
											$magzine = $this->common->getData("tbl_magazine_editions",'*',$condition,"id","desc");
											
											// echo "<pre>";
											// print_r($categories);
											// exit;

										?>
										<ul class="sub-categories-list">
											<?php 
												foreach ($categories as $key => $value) {?>
													<li><a href="<?= base_url('categories/'.$value['slug'])?>" class="sub-category-item"><?= $value['title']?></a></li>
												
												<?php }
											?>
										</ul>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="main-category-dropdown">
						<!--<a href="<?= base_url('magazine')?>" class="main-category-item"><span class="dot magazine"></span>Magazine Editions <img src="<?= base_url('assets/') ?>images/right-arrow-menu.png" alt=""></a>-->
						<a href="#/" class="main-category-item"><span class="dot magazine"></span>Magazine Editions <img src="<?= base_url('assets/') ?>images/right-arrow-menu.png" alt=""></a>
						<ul class="flyout-list">
							<li>
								<div class="sub-menu-block">
									<div class="submenu">
										<h3><img src="<?= base_url('assets/') ?>images/subcategory-back.svg" alt="" class="back-mobile"> Magazine Editions</h3>
										<ul class="sub-categories-list">
											<?php 
											foreach ($magzine as $key => $value) {?>
												<li><a href="<?= base_url('magazine/'.$value['slug'])?>" class="sub-category-item"><?= $value['title']?></a></li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li><a href="<?= base_url('news_event')?>" class="main-category-item"><span class="dot news"></span>News & Events</a></li>
					<!-- <li><a href="#/" class="main-category-item"><span class="dot awards"></span>Awards</a></li> -->
					<li><a href="<?= base_url('aboutus') ?>" class="main-category-item"><span class="dot about"></span> About us</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- Navigation Menu -->
<!-- Header -->
<div class="header-clear"></div>