<!-- Sidebar -->
<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					
					<ul class="nav nav-primary">
						<li class="nav-item active">
							<a href="#dashboard" aria-expanded="false">
								<i class="fas fa-plus"></i>
								<p>Add Videos</p>								
							</a>							
                        </li>	
                        <li class="nav-item">
							<a href="#/">
								<i class="fas fa-th-list"></i>
								<p>Dashboard</p>								
							</a>
                        </li>
                        <li class="nav-item">
							<a href="#/">
								<i class="fas fa-th-list"></i>
								<p>All Videos</p>								
							</a>
                        </li>
                        <li class="nav-item">
							<a href="#/">
								<i class="fas fa-th-list"></i>
								<p>All Enquiries</p>								
							</a>
						</li>					
						<li class="nav-item">
							<a data-toggle="collapse" href="#website">
								<i class="fas fa-layer-group"></i>
								<p>Website Pages</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="website">
								<ul class="nav nav-collapse">
									<li>
										<a href="#/">
											<span class="sub-item">Blog</span>
										</a>
									</li>
									<li>
										<a href="#/">
											<span class="sub-item">About Us</span>
										</a>
                                    </li>
                                    <li>
										<a href="#/">
											<span class="sub-item">FAQs</span>
										</a>
                                    </li>
                                    <li>
										<a href="#/">
											<span class="sub-item">Contact Us</span>
										</a>
                                    </li>
                                    <li>
										<a href="#/">
											<span class="sub-item">Homepage Block</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a href="#/">
								<i class="fas fa-th-list"></i>
								<p>Testimonials</p>								
							</a>
                        </li>
                        <li class="nav-item">
							<a href="#/">
								<i class="fas fa-th-list"></i>
								<p>All Users</p>								
							</a>
                        </li>
                        <li class="nav-item">
							<a data-toggle="collapse" href="#master">
								<i class="fas fa-layer-group"></i>
								<p>Master</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="master">
								<ul class="nav nav-collapse">
									<li>
										<a href="master-new-tag.php">
											<span class="sub-item">Tags</span>
										</a>
									</li>
									<li>
										<a href="master-new-theme.php">
											<span class="sub-item">Theme Master</span>
										</a>
                                    </li>
                                    <li>
										<a href="master-new-subtheme.php">
											<span class="sub-item">Sub Theme Master</span>
										</a>
                                    </li>
                                    <li>
										<a href="master-new-video.php">
											<span class="sub-item">Video Master</span>
										</a>
                                    </li>                                    
								</ul>
							</div>
						</li>
                        <li class="nav-item">
							<a href="#/">
								<i class="fas fa-th-list"></i>
								<p>Profile</p>								
							</a>
                        </li>
                        <li class="nav-item">
							<a href="#/">
								<i class="fas fa-th-list"></i>
								<p>Logout</p>								
							</a>
						</li>
						
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->