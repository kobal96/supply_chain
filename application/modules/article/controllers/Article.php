<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Article extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('articlemodel','',TRUE);
	}

	function _remap($method_name){
		if(!method_exists($this, $method_name)){
			$this->index();
		}else{
			$this->{$method_name}();
		}
	}

	public function index(){
		$slug = $this->uri->segment(2);
		$result = array();
		
		$condition ="a.slug = '".$slug."' && a.status = 'Published' && a.admin_status ='Approved' && a.article_delete ='1' ";
        $main_table = array("tbl_magazine_article_category as ac", array());
        $join_tables =  array();
        $join_tables = array(
                array("", "tbl_magazine_articles as  a", "ac.article_id = a.id", array("a.*,a.author as  author_name")),
                // array("left", "tbl_users as  tu", "tu.id = a.author_id", array("concat(tu.first_name,' ',tu.last_name) as author_name,tu.profile_photo")),
                array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name,c.slug as category_slug")),
				array("left", "tbl_magazine_editions as e", "e.id = a.edition_id", array("e.title as edition_name")),
				array("", "tbl_magazine_article_tag as at", " at.article_id = a.id", array("GROUP_CONCAT(at.tag_id) as tag_id")),
				array("", "tbl_magazine_tags as mt", "mt.id = at.tag_id", array("GROUP_CONCAT(distinct(mt.title)) as tag_title,GROUP_CONCAT(distinct(mt.slug)) as tag_slug ")),

				
                  );
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"a.id",null); 
        $result['article_data'] = $this->common->MySqlFetchRow($rs, "array");

        $condition  ="a.slug != '".$slug."' && c.slug = '".$result['article_data'][0]['category_slug']."' && a.status = 'Published' && a.admin_status ='Approved' && a.article_delete ='1'  ";
        $main_table = array("tbl_magazine_article_category as ac", array());
        $join_tables =  array();
        $join_tables = array(
                array("", "tbl_magazine_articles as  a", "ac.article_id = a.id", array("a.*,a.author as  author_name")),
                // array("left", "tbl_users as  tu", "tu.id = a.author_id", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
                array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
                array("", "tbl_magazine_article_tag as  mat", "mat.article_id = ac.article_id", array("mat.tag_id")),

                  );
        $limit=array("offset"=>"0","rows"=>"3");
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"a.id",$limit); 
        $result['article_data_more'] = $this->common->MySqlFetchRow($rs, "array");
		
	
        $condition ="a.status = 'Published' && a.admin_status ='Approved' AND article_id = '".$result['article_data'][0]['id']."'  ";
        $main_table = array("tbl_magazine_comments as a", array("a.*"));
        $join_tables =  array();
        $join_tables = array(
                array("left", "tbl_users as  tu", "tu.id = a.user_id", array("concat(tu.first_name,' ',tu.last_name) as author_name,tu.profile_photo")),
                  );
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),null,null); 
        $result['comment_data'] = $this->common->MySqlFetchRow($rs, "array");

        $condition ="1=1 && a.article_delete ='1' ";
        $main_table = array("tbl_magazine_articles as a", array("a.id,a.title,a.slug"));
        $join_tables =  array();
        $join_tables = array(
                array("", "tbl_spotlight as  tu", "tu.article_id = a.id", array("tu.type")),
                  );
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array(),"",null); 
        $result['spotlight_data'] = $this->common->MySqlFetchRow($rs, "array");

		// echo "<pre>"; print_r($result['article_data']);exit;

		$this->load->view('header');
		$this->load->view('index',$result);
		$this->load->view('footer');
	}

	public function bookmark(){
		// print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$data['article_id'] = $this->input->post('bookmark');
			$data['user_id']    = $this->session->userdata('supply_chain_user')[0]['id'];
			$data['updated_at'] = date("Y-m-d H:i:s");
			
			$condition = "user_id ='".$data['user_id']."' AND article_id ='".$data['article_id']."' ";
			$bookmarkdata = $this->common->getData("tbl_bookmark",'*',$condition);
			if(!empty($bookmarkdata)){
				
				$result = $this->common->updateData("tbl_bookmark",$data,$condition);
				if($result){

					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");
				$result = $this->common->insertData('tbl_bookmark',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Bookmark the article record successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			 }
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	public function submitForm(){
		// print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();

			if (!$this->session->userdata('supply_chain_user')) {
				echo json_encode(array('success'=>true, 'msg'=>'Please login your account.'));
					exit;
			}
			
			$data['article_id']      = $this->input->post('id');
			$data['user_id']         = $this->session->userdata('supply_chain_user')[0]['id'];
			$data['comment']       = $this->input->post('comment');
			$data['status']       = 'Draft';
			$data['user_type']       = 'frontend';
			$data['admin_status']    = 'Pending';
			$data['created_by']    = $this->session->userdata('supply_chain_user')[0]['id'];
			$data['updated_at']      = date("Y-m-d H:i:s");
			
			// if(!empty($this->input->post('user_id'))){
			// 	$condition = "user_id = '".$this->input->post('user_id')."' ";
			// 	$result = $this->common->updateData("tbl_magazine_comments",$data,$condition);
			// 	if($result){

			// 		echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
			// 		exit;
			// 	}
			// 	else{
			// 		echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
			// 		exit;
			// 	}
			// }else{
				$data['created_at'] = date("Y-m-d H:i:s");
				$result = $this->common->insertData('tbl_magazine_comments',$data,'1');
				if(!empty($result)){
					$notification_data = array();
					$notification_data['user_id'] = $this->session->userdata('supply_chain_user')[0]['id'];
					$notification_data['sender_id'] = '1';
					$notification_data['comment_id'] = $result;
					$notification_data['notification_type'] = 'Comment';
					$notification_data['created_at'] = date("Y-m-d H:i:s");
					$this->common->insertData('tbl_notifications',$notification_data,'1');
					echo json_encode(array('success'=>true, 'msg'=>'comment on article recorded successfully. once the admin approve the comment will shown below of the article '));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			// }
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	
}
?>