<?php
class Magazine  extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('magazinemodel','',TRUE);
		parent::__construct();
		
    }

    function _remap($method_name){
		if(!method_exists($this, $method_name)){
			$this->index();
		}else{
			$this->{$method_name}();
		}
    }
    
    
    public function index(){
        if($this->uri->segment(2)){
            $condition = " status = 'Published' && admin_status ='Approved' AND slug = '".$this->uri->segment(2)."'  ";
            $result = array();
            $result['magazine_detail'] = $this->common->getData("tbl_magazine_editions",'*',$condition);
          
            // get similar magazine 
            $condition = " magazine_id ='".$result['magazine_detail'][0]['id']."' && status = 'Published' && admin_status ='Approved'   ";
            $main_table = array("tbl_similar_magazine as sm", array());
            $join_tables =  array();
            $join_tables = array(
                      array("", "tbl_magazine_editions as  m", "sm.similar_magazine_id = m.id", array("m.*")),
                      );

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("m.id" => "DESC"),"m.id",null); 
            $result['similar_magazine'] = $this->common->MySqlFetchRow($rs, "array");

            $result['meta_description'] = $result['magazine_detail'][0]['meta_description'];
            $result['meta_keywords'] = $result['magazine_detail'][0]['meta_keyword'];
            $result['meta_title'] = $result['magazine_detail'][0]['meta_title'];
            $this->load->view('header',$result);
            $this->load->view('magazine-detail',$result);
            $this->load->view('footer');

        }else{
            $result['meta_description'] = "home page banner";
            $result['meta_keywords'] = " keywords,shiv,admin";
            $this->load->view('header',$result);
            $this->load->view('index',$result);
            $this->load->view('footer');
        }

        

        

		// $result = array();

		// $condition = "sb.sub_theme_id = $id ";
	
    }

    function magazineListing(){
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

            $condition = '1=1';
            if(!empty($_POST['search_value']) && isset($_POST['search_value'])){
                $condition .=" && title like '%".$_POST['search_value']."%' " ;
            }

            $condition .=" && status = 'Published' && admin_status ='Approved' ";
            $result['magazine'] = $this->common->getData("tbl_magazine_editions",'*',$condition,"id","DESC");

            $magazine_html = $this->load->view('magazine-listing',$result,true);
            if(!empty($magazine_html)){
                echo json_encode(array('success'=>true, 'msg'=>'magazine fetch Successfully.' , "html"=>$magazine_html));
                exit;
            }else{
                echo json_encode(array('success'=>false, 'msg'=>'Problem while fetching magazine data.'));
                exit;
            }
        }
    }
}

?>