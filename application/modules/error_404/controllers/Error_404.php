<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Error_404 extends MX_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('error_404model','',TRUE);
	}



	public function index(){
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');

	}
}
?>