<section class="page-section mt-60 mtmob-40">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-9">
        <div class="aboutus-intro">
          <h1><?= (!empty($about_us[0]['title'])?$about_us[0]['title']:"") ?></h1>
          <p class="about-intro-tagline">
            <?= (!empty($about_us[0]['header_content'])?$about_us[0]['header_content']:"") ?>
          </p>
        </div>      
      </div>
      <div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-5">
        <div class="vision-mission-wrapper">
          <h3><?= (!empty($about_us[0]['section1_heading'])?$about_us[0]['section1_heading']:"") ?></h3>
          <p><?= (!empty($about_us[0]['section1_paragraph'])?$about_us[0]['section1_paragraph']:"") ?></p>
        </div>
      </div>  
      <div class="col-12 col-sm-12 col-md-12 col-lg-5">
          <div class="vision-mission-wrapper">
          <h3><?= (!empty($about_us[0]['section2_heading'])?$about_us[0]['section2_heading']:"") ?></h3>
          <p><?= (!empty($about_us[0]['section2_paragraph'])?$about_us[0]['section2_paragraph']:"") ?></p>
        </div>
      </div>   
      <div class="col-12">
        <div class="vision-mission-seperator"></div>
      </div>
      <div class="col-12 col-sm-12 col-md-6">
        <div class="founders-wrapper">
          <img src="<?= FRONT_URL.'/images/aboutus/'.$about_us[0]['founder_image']?>" alt="" class="img-fluid">
          <div class="founder-content">
            <p class="founder-role">Our Founder</p>
            <h3><?= (!empty($about_us[0]['founder_name'])?$about_us[0]['founder_name']:"") ?> <a href="<?= (!empty($about_us[0]['founder_linkedin'])?$about_us[0]['founder_linkedin']:"") ?>" target="_blank"><img src="images/aboutus/linkedin-about.svg" alt=""></a></h3>
            <p class="founder-info"><?= (!empty($about_us[0]['founder_description'])?$about_us[0]['founder_description']:"") ?></p>
            <p class="founder-info">When she is not disrupting at work, she disrupts her work by going for high altitude treks in the Himalayas.</p>
          </div>
        </div>
      </div> 
      <div class="col-12 col-sm-12 col-md-6">
        <div class="founders-wrapper">
          <img src="<?= FRONT_URL.'/images/aboutus/'.$about_us[0]['head_image']?>" alt="" class="img-fluid">
          <div class="founder-content">
            <p class="founder-role">Content Head</p>
            <h3><?= (!empty($about_us[0]['head_name'])?$about_us[0]['head_name']:"") ?> <a href="<?= (!empty($about_us[0]['head_linkedin'])?$about_us[0]['head_linkedin']:"") ?>" target="_blank"><img src="images/aboutus/linkedin-about.svg" alt=""></a></h3>
            <p class="founder-info"><?= (!empty($about_us[0]['head_description'])?$about_us[0]['head_description']:"") ?>.</p>
            <p class="founder-info">For me, Celerity has been more than just a profession as it was born with a  lot of passion and zeal to create a niche in the supply chain world.</p>
          </div>
        </div>
      </div> 
      <div class="col-12">        
        <h3 class="our-advisors-title">Our Advisors </h3>
        <div class="advisors-wrapper">
        <?php
        foreach ($about_us[0]['advisors'] as $key => $value) { ?>
          <div class="advisor-single">
            <img src="<?= FRONT_URL.'/images/aboutus/'.$value['advisor_image']?>" alt="">
            <h4><?=$value['advisor_name'] ?></h4>
            <p class="advisor-designation"><?=$value['advisor_designation'] ?></p>
          </div>
          
        <?php } ?>
          

          <!-- <div class="advisor-single">
            <img src="images/rajat-sharma.png" alt="">
            <h4>Rajat Sharma</h4>
            <p class="advisor-designation">Head-SCM, Hamilton Housewares</p>
          </div>
          <div class="advisor-single">
            <img src="images/sanjay-desai.png" alt="">
            <h4>Sanjay Desai</h4>
            <p class="advisor-designation">Advisory Consulting</p>
          </div>
          <div class="advisor-single">
            <img src="images/umesh-madhyan.png" alt="">
            <h4>Umesh Madhyan</h4>
            <p class="advisor-designation">Associate Vice President-Logistics, Hindustan Coca-Cola Beverages</p>
          </div>
          <div class="advisor-single">
            <img src="images/vickram-srivastava.png" alt="">
            <h4>Vickram Srivastava</h4>
            <p class="advisor-designation">Head of Supply Chain Planning, Ipca Laboratories</p>
          </div> -->
        </div>
      </div>  
    </div>   
  </div>
</section>