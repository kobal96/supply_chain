
<section class="page-section mt-60 mb-60 mtmob-40 mbmob-20">  
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="login-wrapper">
          <h1 class="section-title-medium">Login to your account</h1>
          <div class="social-login">
            <a href="<?= $login_button; ?>"><img src="images/google-login.svg" alt=""> Continue with Google</a>
            <!-- <a href="#/"><img src="images/linkedin-login.svg" alt=""> Continue with Linkedin</a> -->
            <a href="<?=base_url().'linkedin_login?oauth_init=1'?>"><img src="images/linkedin-login.svg" alt=""> Continue with Linkedin</a>
          </div>
          <p class="or"><span>or</span></p>

          <?php echo '<label class="text-danger">'.$this->session->flashdata("error").'</label>';   ?>
          <!-- <form class="billing-form login-form" id="celerity-login"> -->
          <?php $attributes = array('class' => 'billing-form login-form', 'id' => 'celerity-login','name' => 'celerity-login');
            echo form_open(base_url("login/form_validate"),$attributes);?>
            <div class="form-group">
              <label for="">Username</label>
              <input type="text" class="form-control" id="" name="username" placeholder="Username" value="<?=isset($_COOKIE['fusername'])?$_COOKIE['fusername']:''?>" >
              <span class="text-danger"><?php echo form_error("username"); ?></span>
            </div>
            <div class="form-group">
              <label for="">Password</label>
              <input type="password" class="form-control" id="" name="password" placeholder="6+ Characters" value="<?=isset($_COOKIE['fpassword'])?$_COOKIE['fpassword']:''?>" >
              <span class="text-danger"><?php echo form_error("password"); ?></span>
            </div>            
            <div class="form-check text-left">
              <div>
                <input type="checkbox" class="form-check-input" id="exampleCheck1" name="remember" value="remember" >
                <label class="form-check-label" for="exampleCheck1">Remember me</label>
              </div>
              <div>
                <label class="form-check-label">
                  <a href="#/" class="fp-link">Forgot your password?</a>
                </label>                
              </div>
            </div>
            <div class="form-group">              
              <input type="submit" class="btn-celerity btn-large btn-blue btn-full text-uppercase" id=""value="Login">
            </div>
            <div class="form-group"> 
              <div class="create-account-link-wrapper">
                <p><span>Don’t have an account?</span> <a href="#/" class="signup-link">Create one</a></p>
              </div>              
            </div>
            <?php echo form_close(); ?>
          <!-- </form> -->

          <form class="billing-form login-form" id="celerity-signup" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="">First Name</label>
              <input type="text" class="form-control" id="" name="fname" placeholder="First Name">
            </div>

            <div class="form-group">
              <label for="">Last Name</label>
              <input type="text" class="form-control" id="" name="lname" placeholder="Last Name">
            </div>

            <div class="form-group">
              <label for="">Username</label>
              <input type="text" class="form-control" id="" name="username" placeholder="Username">
            </div>

            <div class="form-group">
              <label for="">Email address</label>
              <input type="email" class="form-control" id="" name="email" placeholder="eg. john-clack@gmail.com">
            </div>
            <div class="form-group">
              <label for="">Password</label>
              <input type="password" class="form-control" id="password" name="password" placeholder="eg. jo@cl@acH">
            </div>
            <div class="form-group">
              <label for="">Confirm Password</label>
              <input type="password" class="form-control" id="" name="cpassword" placeholder="6+ Characters">
            </div>
            <div class="form-group">              
              <input type="submit" class="btn-celerity btn-large btn-blue btn-full text-uppercase" id=""value="Sign Up">
            </div>
            <div class="form-group"> 
              <div class="create-account-link-wrapper">
                <p><span>Already have an account?</span> <a href="#/" class="login-link">Log In</a></p>
              </div>              
            </div>
          </form>
          <form class="billing-form login-form" id="celerity-forgotpassword" method="post">
            <div class="form-group">
              <label for="">Email address</label>
              <input type="email" class="form-control" id="" name="email" placeholder="eg. john-clack@gmail.com">
            </div> 
            <!-- <div class="form-group">
              <label for="">New Password</label>
              <input type="password" class="form-control" id="" placeholder="eg. jo@cl@acH">
            </div>
            <div class="form-group">
              <label for="">Confirm Password</label>
              <input type="password" class="form-control" id="" placeholder="6+ Characters">
            </div> -->           
            <div class="form-group">              
              <input type="submit" class="btn-celerity btn-large btn-blue btn-full text-uppercase" id=""value="Reset Password">
            </div>
            <div class="form-group"> 
              <div class="create-account-link-wrapper">
                <p><a href="#/" class="login-link">Back to Log In</a></p>
              </div>              
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>


</body>
</html>