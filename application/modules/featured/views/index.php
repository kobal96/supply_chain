
<section class="page-section mt-60 mtmob-0 mttab-0">
  <div class="container">
    <div class="row">
	<div class="col-12 col-sm-12 col-md-6 category-select-list-wrapper">
	<span class="current-featured">Featured</span>
	</div>
      <div class="col-12 col-sm-12 col-md-6">
        <div class="category-search">
          <form>
            <input type="text" name="search_value" id="search_value" onkeyup="getArticleList()"  class="category-search-text" placeholder="Search">
          </form>
        </div>
      </div>
      <div class="col-12">
        <div class="category-select-seperator"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="category-filter-wrapper">
          <div class="category-sortby">
            <span class="filter-label">Sort By</span>
            <select name="sortby" id="sortby" class="niceselect" onchange="getArticleList()">
              <option value="Asc">Oldest</option>
              <option value="Desc">Newest</option>
              <!-- <option value="Most Commented">Most Commented</option> -->
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-30 mb-60 mtmob-10">
  <div class="container">
    <div class="row" id="article-listing-block">
            
    </div>
  </div>
</section>
</body>

</html>

<script>
$( window ).on( "load", function() {
  getArticleList();
});


function getArticleList(category_id=null,tag_id=null){
  var sortby = $("#sortby").val();
  var search_value = $("#search_value").val();
  var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('featured/ArticleListing')?>",
			type: "POST",
			data:{ search_value,sortby,[csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
			  	$("#article-listing-block").html(response.html);	
				}else{
			  	$("#article-listing-block").html('');
				}
			}
		});
		

}
</script>