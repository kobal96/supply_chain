<?php if(!empty($event_data)){
  foreach ($event_data as $key => $value) {?>
<div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
  <div class="event-single events-tab-single">
    <div class="event-day">
      <div class="event-date"><?= (!empty($value['event_date'])?date('d',strtotime($value['event_date'])):"") ?></div>
      <div class="event-month"><?= (!empty($value['event_date'])?date('M, y',strtotime($value['event_date'])):"") ?></div>
    </div>
    <div class="event-content-wrapper">
      <div class="event-content eventview" data-eventid="<?= $value['id']?>">
        <a href="<?= base_url('event?text='.rtrim(strtr(base64_encode("id=".$value['id']), '+/', '-_'), '=').'')?>">
          <div class="article-block-content">
            <p class="category-tag">Event</p>
            <h4><?= (!empty($value['title'])?$value['title']:"") ?></h4>
            <p class="event-info"><?= (!empty($value['meta_description'])?$value['meta_description']:"") ?></p>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>
<?php } }  ?>

  <?php if(count($event_data) > 6) {?>
  <div class="col-12">
      <div class="text-center">
        <a href="" class="btn-celerity btn-large btn-blue text-uppercase">Load More</a>
      </div>
    </div>
    <?php }?>