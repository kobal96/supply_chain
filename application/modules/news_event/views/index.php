<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
<section class="page-section mt-60 mtmob-0">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-6">
        <h1 class="page-title-large news-events-title">News and Events</h1>
      </div>
      <div class="col-12 col-sm-12 col-md-6">
        <div class="category-search">
          <form>
            <input type="text" name="search" class="category-search-text" placeholder="Search" style="display: none;">
          </form>
        </div>
      </div>
      <div class="col-12">
        <div class="news-events-tabs">
          <a href="#/" id="news-link" class="active">News</a>
          <a href="#/" id="events-link">Events</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="category-filter-wrapper">
          <div class="category-sortby" style="display: none;">
            <span class="filter-label">Sort By</span>
            <select name="" id="" class="niceselect event_filter">
              <option value="Desc">Recent</option>
              <option value="Mostviewed">Most Viewed</option>
              <option value="Asc">Oldest</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-30 mb-60 mtmob-10" id="news-tab">
  <div class="container">
    <div class="row">
      <?php 
      if($news_heading){
        foreach ($news_heading as $key => $value) { 
          ?>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a  target="_blank" href="<?= $value->url?>">
            <!-- <img src="<?= $value->urlToImage ?>" alt="" class="img-fluid"> -->
						<img src="<?= (!empty($value->urlToImage)?$value->urlToImage:base_url('images/edition_image/default-img.jpg'))?>" alt="" class="img-fluid">

            <div class="hps-content">
              <p class="category-tag">News</p>
              <h4><?= $value->title ?></h4>
              <div class="article-author-info">
                <p>by <span class="author-name"><?= $value->author ?></span></p>
                <p><span class="article-date"><?= date_format(date_create($value->publishedAt),"d M Y ");?></span> <!-- <span class="reading-time">12 min read</span> --></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <?php }
            } ?>
      <!-- <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-2.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>The Driving Force </h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Eliza Miller</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div> -->
      <div class="col-12">
        <div class="text-center">
          <a href="<?= base_url('news_event')?>" target="_blank" class="btn-celerity btn-large btn-blue text-uppercase">Load More</a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-30 mb-60 mtmob-10" id="events-tab">
  <div class="container">
    <div class="row" id="eventshow">

      <!-- <div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
        <div class="event-single events-tab-single">
          <div class="event-day">
            <div class="event-date">21</div>
            <div class="event-month">Jul, 20</div>
          </div>
          <div class="event-content-wrapper">
            <div class="event-content">
              <a href="#/">
                <div class="article-block-content">
                  <p class="category-tag">Event</p>
                  <h4>Title of the event will be displayed here and it won’t be longer than two lines Title of the event will be displayed here and it won’t be longer than two lines</h4>
                  <p class="event-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pulvinar blandit rutrum. Quisque hendrerit ultrices tortor ac porta. Donec id ante eleifend, condimentum felis non, eleifend enim. Aliquam fringilla sem id risus porta congue.</p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div> -->

    </div>
  </div>
</section>

<script type="text/javascript">

    $(document).ready(function(){
       getEvent();

    $(document).on("click",".eventview",function () {
          var eventid  = $(this).attr("data-eventid");
          var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
        var csrfHash = $('.txt_csrfname').val(); // CSRF hash
      $.ajax({
        url: "<?= base_url('news_event/eventview')?>",
        type: "POST",
        data:{ eventid, [csrfName]: csrfHash },
        dataType: "json",
        success: function(response){
          
        }
      }); 
          
    });

    $(document).on("click","#events-link",function () {
          $('.category-search-text').show();
          $('.category-sortby').show();
          
    });

    $(document).on("change",".event_filter",function () {
        var filter = $(this).val();

        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
        var csrfHash = $('.txt_csrfname').val(); // CSRF hash
      $.ajax({
        url: "<?= base_url('news_event/eventdata')?>",
        type: "POST",
        data:{ filter, [csrfName]: csrfHash },
        dataType: "json",
        success: function(response){
          if(response.success){
            
          $("#eventshow").html(response.data); 
            
          }else{
          $("#eventshow").html('');
          }
        }
      });
          
    });

    $(document).on("keyup","input[name='search']",function () {
    var search = $(this).val();
        
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
      var csrfHash = $('.txt_csrfname').val(); // CSRF hash
      if (search.length > 2) {
        $.ajax({
      url: "<?= base_url('news_event/eventdata')?>",
      type: "POST",
      data:{ search, [csrfName]: csrfHash },
      dataType: "json",
      success: function(response){
        if(response.success){
            
          $("#eventshow").html(response.data); 
            
          }else{
          $("#eventshow").html('');
          }
        
      }
    });

      }else{
        getEvent();
      }
    
        
      });

  });


  function getEvent() {
    var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
    $.ajax({
      url: "<?= base_url('news_event/eventdata')?>",
      type: "POST",
      data:{ [csrfName]: csrfHash },
      dataType: "json",
      success: function(response){
        if(response.success){
          $("#eventshow").html(response.data);    
        }else{
          $("#eventshow").html('');
        }

      }
    });
  }
</script>