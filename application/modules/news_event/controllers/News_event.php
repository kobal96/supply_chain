<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class News_event extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('newseventmodel','',TRUE);
	}

	function _remap($method_name){
		if(!method_exists($this, $method_name)){
			$this->index();
		}else{
			$this->{$method_name}();
		}
	}

	public function index(){
		
		$result = array();

		// get news n events from api hit 
		// $url = "http://newsapi.org/v2/top-headlines?to=2020-10-16&sortBy=popularity&sources=bbc-news&apiKey=6452e0926c0348b59d9226643b365bac&pageSize=9";
		$url = "http://newsapi.org/v2/top-headlines?country=in&sortBy=popularity&apiKey=6452e0926c0348b59d9226643b365bac&pageSize=9";
		// Initialize a CURL session. 
		$ch = curl_init(); 
		// Return Page contents. 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		//grab URL and pass it to the variable. 
		curl_setopt($ch, CURLOPT_URL, $url); 
		$topheading = json_decode(curl_exec($ch));
		// echo "<pre>";
		// print_r($topheading);
		// exit;

		$result['news_heading'] = '';
		if($topheading->status == "ok"){
			$result['news_heading'] = $topheading->articles;
		}
		
		//echo "<pre>"; print_r($result['news_heading']);exit;

		$this->load->view('header');
		$this->load->view('index',$result);
		$this->load->view('footer');
	}

	public function eventdata(){
		//get blog data
		$condition ="status = 'Published' AND admin_status ='Approved' ";
		$oderfield = "id";
		$oderby = "DESC";
		if (isset($_POST['search']) && $_POST['search']!='') {
			$keyword = $this->input->post('search');
			$condition .= " AND title LIKE '%$keyword%' ";
			//$condition .= "(title LIKE '%$key%' OR slug LIKE '%$key%') ";
		}
		if (isset($_POST['filter']) && $_POST['filter']!='') {
			$view = $this->input->post('filter');
			if ($view=='Asc') {
				$oderby = "ASC";
			}
			if ($view=='Mostviewed') {
				$oderfield = "event_view";
				$oderby = "DESC";
			}
			
		}

		$result['event_data'] = $this->common->getData("tbl_events",'*',$condition, $oderfield, $oderby);
		
		//echo "<pre>";print_r($result['event_data']);exit;

		$blogresponse = $this->load->view('event-design.php',$result,true);
		if (!empty($blogresponse)) {
			print_r(json_encode(array('success'=>true, 'data'=> $blogresponse)));
			exit;
		}else{
			print_r(json_encode(array('success'=>false, 'data'=> $blogresponse)));
			exit;
			}

	}

	public function eventview(){
		$id = $this->input->post('eventid');
		$sql = "SELECT event_view FROM tbl_events WHERE id = $id";
		$res = $this->db->query($sql);
		$result = $res->row_array();
		$count = ($result['event_view']+1);

		$update = $this->common->updateData("tbl_events",array('event_view'=>$count), array('id'=>$id));
		if ($update) {
			echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
				exit;
		}
	}

	public function submitForm(){
		// print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();

			if (!$this->session->userdata('supply_chain_user')) {
				echo json_encode(array('success'=>true, 'msg'=>'Please login your account.'));
					exit;
			}
			
			$data['article_id']      = $this->input->post('id');
			$data['user_id']         = $this->session->userdata('supply_chain_user')[0]['id'];
			$data['comment']       = $this->input->post('comment');
			$data['status']       = 'Published';
			$data['user_type']       = 'frontend';
			$data['admin_status']    = 'Approved';
			$data['created_by']    = $this->session->userdata('supply_chain_user')[0]['id'];
			$data['updated_at']      = date("Y-m-d H:i:s");
			
			if(!empty($this->input->post('user_id'))){
				$condition = "user_id = '".$this->input->post('user_id')."' ";
				$result = $this->common->updateData("tbl_magazine_comments",$data,$condition);
				if($result){

					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");
				
				$result = $this->common->insertData('tbl_magazine_comments',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Create comment successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	
}
?>