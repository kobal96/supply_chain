<section class="page-section mt-30 mb-30">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12">  
        <div class="mt-60 mb-60">
          <div class="payment-status-wrapper">
            <div>
              <img src="<?=base_url('assets/')?>images/payment-success.svg" class="mb-40" alt="">
              <h2 class="page-title-large mb-10">Payment Successful</h2>
			  <p>Payment Invoice has been successfully sent to your registered email address.</p>
              <a href="<?=base_url('home');?>" class="btn-celerity btn-large btn-blue">Back to Homepage</a>
            </div>
          </div>
        </div>       
      </div>      
    </div>
  </div>
</section>