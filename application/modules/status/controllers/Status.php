<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Status extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
	}

  function _remap($method_name){
    if(!method_exists($this, $method_name)){
      $this->index();
    }else{
      $this->{$method_name}();
    }
  }


	public function index() {
    $pid = $this->uri->segment(2);
    $bid = $this->uri->segment(3);
    $json = json_encode($_POST);
       $status = $this->input->post('status');
      if (empty($status)) {
            redirect('payumoney');
        }
       

     
        $firstname = $this->input->post('firstname');
        $amount = $this->input->post('amount');
        $txnid = $this->input->post('txnid');
        $posted_hash = $this->input->post('hash');
        $key = $this->input->post('key');
        $productinfo = $this->input->post('productinfo');
        $email = $this->input->post('email');
        // $salt = "eCwWELxi"; //  Your salt  for test
        $salt = "U811I54W"; //  Your salt  for live
        $add = $this->input->post('additionalCharges');
        If (isset($add)) {
            $additionalCharges = $this->input->post('additionalCharges');
            $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        } else {

            $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
        }
         $data['hash'] = hash("sha512", $retHashSeq);
          $data['amount'] = $amount;
          $data['txnid'] = $txnid;
          $data['posted_hash'] = $posted_hash;
          $data['status'] = $status;

          $condition = "id = '".$pid."' ";
          $package = $this->common->getData("tbl_packages",'*',$condition);

          // $condition = "id = '".$bid."' ";
          // $billing = $this->common->getData("tbl_billing_information",'*',$condition);

          $condition ="b.id = '".$bid."'";
          $main_table = array("tbl_billing_information as b", array("b.*"));
          $join_tables =  array();
          $join_tables = array(
              array("", "tbl_countries as  c", "c.id = b.country", array("c.country_name")),
              array("", "tbl_states as  s", "s.id = b.state", array("s.state_name")),
              array("", "tbl_city as  ct", "ct.id = b.city", array("ct.city_name")),
              array("", "tbl_users as  u", "u.id = b.user_id", array("u.email")),

                );
          $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("b.id" => "DESC"),"b.id"); 
          $billing = $this->common->MySqlFetchRow($rs, "array");

          $last_invoice='';
          // get invoice number
          if($status == 'success'){
            $condition = "1=1 AND invoice_sequence_no !='0' ";
            $last_invoice = $this->common->getDataLimit("tbl_payment",'invoice_sequence_no',$condition,"invoice_sequence_no","desc",'1');
            $last_invoice = ++$last_invoice[0]['invoice_sequence_no'];
          }

          $res = array();
          $res['package_detail'] = $package;
          $res['billing_detail'] =  $billing;  
          $res['total_amt'] = $amount;
          $res['invoice_number'] = $amount;
          $res['total_amt_in_words'] = $this->AmountInWords($amount);
          $res['invoice_no'] =  (($status == 'success')?'CSC'.$last_invoice:" ");

          $cgst = (($package[0]['price']*$package[0]['gst_cgst'])/100);
          $sgst = (($package[0]['price']*$package[0]['gst_sgst'])/100);
          $igst = (($package[0]['price']*$package[0]['gst_igst'])/100);
          $param = array(
            'user_id' => $billing[0]['user_id'],
            'billing_id' => $billing[0]['id'],
            'invoice_no' =>  (($status == 'success')?'CSC'.$last_invoice:" "),
            'invoice_sequence_no' => (($status == 'success')?$last_invoice:0),
            'txn_id' => $txnid,
            'package_id' => $pid,
            'cgst' => (($billing[0]['state'] == '20')?$cgst:""),
            'sgst' => (($billing[0]['state'] == '20')?$sgst:""),
            'igst' => (($billing[0]['state'] == '20')?"":$igst),
            'duration' => $package[0]['duration'],
            'package_amount' => $package[0]['price'],
            'paid_date' => date("Y-m-d"),
            'package_status' => 'Active',
            'expire_on' => date("Y-m-d"),
            'amount' => $package[0]['price'],
            'total' => $amount,
            'payment_status' => $status,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
          );

          $param1 = array(
            'payable_id' => $billing[0]['id'],
            'payable_type' => 'App\Models\Order',
            'txnid' => $txnid,
            'mihpayid' => $this->input->post('mihpayid'),
            'firstname' => $firstname,
            'email' => $billing[0]['email'],
            'amount' => $amount,
            'data' => $json,
            'status' => $status,
            'unmappedstatus' => $this->input->post('unmappedstatus'),
            'mode' => $this->input->post('mode'),
            'bank_ref_num' => $this->input->post('bank_ref_num'),
            'bankcode' => $this->input->post('bankcode'),
            'cardnum' => $this->input->post('cardnum'),
            'name_on_card' => $this->input->post('name_on_card'),
            'issuing_bank' => $this->input->post('issuing_bank'),
            'card_type' => $this->input->post('card_type'),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
          );

          $this->common->insertData('tbl_payment',$param,'1');
          $this->common->insertData('tbl_payu_payments',$param1,'1');

          if($status == 'success'){
            $invoice_html = $this->load->view('invoice',$res,true);
            $this->load->library('m_pdf');         
            $mpdf=new mPDF('utf-8');
            $mpdf->WriteHTML($invoice_html);
            $PdfFilePath = DOC_ROOT_FRONT."/images/invoice/".$last_invoice.".pdf";
            $mpdf->Output($PdfFilePath,"F");
            $to_email_array = array($billing[0]['email'],'charulata.bansal@celerityin.com'); 
            // $to_email_array = array($billing[0]['email']); 
        
              //Load email library 
              $this->load->library('email'); 
              $this->email->from(FROM_EMAIL, 'Supply Chain'); 
              $this->email->to($to_email_array);
              $this->email->subject("Invoice receipt"); 
              $mail_body  = $this->load->view('mail_body',$res,true);
              // print_r($mail_body);
              // print_r($invoice_html);

              // exit;
              $this->email->message($mail_body); 
              $this->email->attach($PdfFilePath);
              $this->email->send();
              $this->load->view('header');
              $this->load->view('payment-success', $data);
              $this->load->view('footer');   
         }
         else{
              $this->load->view('header');
              $this->load->view('payment-failure', $data);
              $this->load->view('footer'); 
         }
     
    }

      // Create a function for converting the amount in words
    function AmountInWords(float $amount)
    {
      $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
      // Check if there is any number after decimal
      $amt_hundred = null;
      $count_length = strlen($num);
      $x = 0;
      $string = array();
      $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
        3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
        7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
        10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
        13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
        16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
        19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
        40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
        70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
        $here_digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
        while( $x < $count_length ) {
          $get_divider = ($x == 2) ? 10 : 100;
          $amount = floor($num % $get_divider);
          $num = floor($num / $get_divider);
          $x += $get_divider == 10 ? 1 : 2;
          if ($amount) {
          $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
          $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
          $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.' 
          '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. ' 
          '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
            }
      else $string[] = null;
      }
      $implode_to_Rupees = implode('', array_reverse($string));
      $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . " 
      " . $change_words[$amount_after_decimal % 10]) . ' Paise' : '';
      return ($implode_to_Rupees ? $implode_to_Rupees . 'Rupees ' : '') . $get_paise;
    }


	
}
?>