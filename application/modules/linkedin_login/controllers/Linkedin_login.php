<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once (APPPATH .'third_party/linkedin-oauth-client-php/http.php');
require_once (APPPATH .'third_party/linkedin-oauth-client-php/oauth_client.php'); 
 
class Linkedin_login extends MX_Controller 
{ 
    private $userTbl = ' tbl_users';
    function __construct() { 
        parent::__construct();
        //Load user model  
        $this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('linkedin_model','',TRUE);
    } 
     
    public function index(){
        //$userData = array();
        $authUrl = $output = ''; 
        if(isset($_SESSION['oauth_status']) && $_SESSION['oauth_status'] == 'verified' && !empty($_SESSION['supply_chain_user'])){
            $userData = $_SESSION['supply_chain_user'][0];
            if(!empty($userData)){
                $output  = '<h2>LinkedIn Profile Details</h2>';
                $output .= '<div class="ac-data">';
                $output .= '<img src="'.$userData['profile_photo'].'"/>';
                $output .= '<p><b>LinkedIn ID:</b> '.$userData['oauth_uid'].'</p>';
                $output .= '<p><b>Name:</b> '.$userData['first_name'].' '.$userData['last_name'].'</p>';
                $output .= '<p><b>Email:</b> '.$userData['email'].'</p>';
                $output .= '<p><b>Logout from</b> <a href="linkedin_login/logout">LinkedIn</a></p>';
                $output .= '</div>';
            }
        }elseif((isset($_GET["oauth_init"]) && $_GET["oauth_init"] == 1) || (isset($_GET['oauth_token']) && isset($_GET['oauth_verifier'])) || (isset($_GET['code']) && isset($_GET['state']))){
            $client = new oauth_client_class;
            
            $client->client_id = LIN_CLIENT_ID;
            $client->client_secret = LIN_CLIENT_SECRET;
            $client->redirect_uri = LIN_REDIRECT_URL;
            $client->scope = LIN_SCOPE;
            $client->debug = 1;
            $client->debug_http = 1;
            $application_line = __LINE__;
            
            if(strlen($client->client_id) == 0 || strlen($client->client_secret) == 0){
                echo "Problem";
                die();
            }
            
            // If authentication returns success
            if($success = $client->Initialize()){
                if(($success = $client->Process())){
                    if(strlen($client->authorization_error)){
                        $client->error = $client->authorization_error;
                        $success = false;
                    }elseif(strlen($client->access_token)){
                        $success = $client->CallAPI(
                            'https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))', 
                            'GET', array(
                                'format'=>'json'
                            ), array('FailOnAccessError'=>true), $userInfo);
                        $emailRes = $client->CallAPI(
                            'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))', 
                            'GET', array(
                                'format'=>'json'
                            ), array('FailOnAccessError'=>true), $userEmail);
                    }
                }
                $success = $client->Finalize($success);
            }
            
            if($client->exit) exit;
            
            if(strlen($client->authorization_error)){
                $client->error = $client->authorization_error;
                $success = false;
            }
            
            if($success){
                //$user = new User();
                $inUserData = array();
                $inUserData['oauth_uid']  = !empty($userInfo->id)?$userInfo->id:'';
                $inUserData['first_name'] = !empty($userInfo->firstName->localized->en_US)?$userInfo->firstName->localized->en_US:'';
                $inUserData['last_name']  = !empty($userInfo->lastName->localized->en_US)?$userInfo->lastName->localized->en_US:'';
                $inUserData['email']      = !empty($userEmail->elements[0]->{'handle~'}->emailAddress)?$userEmail->elements[0]->{'handle~'}->emailAddress:'';
                $inUserData['picture']    = !empty($userInfo->profilePicture->{'displayImage~'}->elements[0]->identifiers[0]->identifier)?$userInfo->profilePicture->{'displayImage~'}->elements[0]->identifiers[0]->identifier:'';
                $inUserData['link']       = 'https://www.linkedin.com/';

                $inUserData['oauth_provider'] = 'linkedin';
                //$userData = $user->checkUser($inUserData);
                $userData = $this->checkUser($inUserData);
                
                $this->session->set_userdata('oauth_status','verified'); 
                $this->session->set_userdata('supply_chain_user',$userData);
                redirect('/home');
                //$_SESSION['userData'] = $userData;
                //$_SESSION['oauth_status'] = 'verified'; 
                //header('Location: ./');
            }else{
                 $output = 'error'.HtmlSpecialChars($client->error);
            }
        }elseif(isset($_GET["oauth_problem"]) && $_GET["oauth_problem"] <> ""){
            $output = $_GET["oauth_problem"];
        }else{
            $authUrl = '?oauth_init=1';
            
            $output = '<a href="?oauth_init=1"><img src="images/linkedin-sign-in-btn.png"></a>';
        }
        $data['output'] = $output;
        $this->load->view('index',$data); 
    }

    function checkUser($userData = array()){
        if(!empty($userData)){
            // Check whether user data already exists in database
            $prevQuery = "SELECT * FROM ".$this->userTbl." WHERE oauth_provider = '".$userData['oauth_provider']."' AND oauth_uid = '".$userData['oauth_uid']."'";
            $prevResult = $this->db->query($prevQuery);
            if($prevResult->num_rows() > 0){
                // Update user data if already exists
                $query = "UPDATE ".$this->userTbl.
                " SET first_name = '".$userData['first_name'].
                "', last_name = '".$userData['last_name'].
                "', email = '".$userData['email'].
                "', profile_photo = '".$userData['picture'].
                "', link = '".$userData['link'].
                "', updated_at = NOW() WHERE oauth_provider = '".
                $userData['oauth_provider'].
                "' AND oauth_uid = '".$userData['oauth_uid']."'";
                $update = $this->db->query($query);
            }else{
                // Insert user data
                $query = "INSERT INTO ".$this->userTbl." SET oauth_provider = '".$userData['oauth_provider']."', oauth_uid = '".$userData['oauth_uid']."', first_name = '".$userData['first_name']."', last_name = '".$userData['last_name']."', email = '".$userData['email']."', profile_photo = '".$userData['picture']."', status = 'Active', link = '".$userData['link']."', created_at = NOW(), updated_at = NOW()";
                $insert = $this->db->query($query);
            }
            
            // Get user data from the database
            $result = $this->db->query($prevQuery);
            $userData = $result->result_array();
        }
        //echo '<pre>';print_r($userData);die;
        // Return user data
        return $userData;
    } 
 
    public function logout() { 
        // Unset token and user data from session 
        $this->session->unset_userdata('oauth_status'); 
        $this->session->unset_userdata('supply_chain_user'); 
         
        // Destroy entire session 
        $this->session->sess_destroy(); 
         
        // Redirect to login page 
        redirect('/linkedin_login'); 
    } 
}
?>
