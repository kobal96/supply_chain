<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Home  extends MX_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('homemodel','',TRUE);
		
	}

	public function index()
	{
		// // get banner image/video
		// $condition = "1=1 AND banner_id = 1 ";
		// $result['banner_data'] = $this->common->getData("tbl_banner",'*',$condition);

		// //get featured themes
		// $condition = "1=1 AND t.featured_theme = 'Yes' AND t.status= 'Active' ";
		// $main_table = array("tbl_themes as t", array("t.*"));
		// $join_tables =  array();
		// $join_tables = array(
		// 					array("", "tbl_sub_themes as  sb", "t.theme_id = sb.theme_id", array("count(sb.sub_theme_id) as totol_sub_themes")),
		// 					);
	
		// $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("t.theme_id" => "DESC"),"t.theme_id",null); 
		//  // fetch query
		// $featured_theme_data = $this->common->MySqlFetchRow($rs, "array"); // fetch result

		// foreach ($featured_theme_data as $key => $value) {
		// 	$condition = "1=1 AND theme_id = ".$value['theme_id'];
		// 	$total_reel = $this->common->getData("tbl_video_theme_subtheme_mapping",'count(video_id) as total_reel',$condition,"","","video_id");
		// 	$featured_theme_data[$key]['total_reals'] =count($total_reel) ;
		// }
	
		// $result['featured_theme_data']  = $featured_theme_data;
		
		// //get testimonial 
		// $condition = "1=1 AND status = 'Active'  ";
		// $result['testimonial_data'] = $this->common->getDataLimit("tbl_testimonials",'*',$condition,"testimonial_id","desc",'3');

		// // get about us 
		// $condition = "1=1";
		// $result['about_us'] = $this->common->getData("tbl_about_us",'*',$condition);

		// //get blog data
		// $condition = "1=1 AND b.status= 'Active' ";
		// $main_table = array("tbl_blogs as b", array("b.*"));
		// $join_tables =  array();
		// $join_tables = array(
		// 					array("", "tbl_blog_tag_mapping as  bt", "bt.blog_id = b.blog_id"),
		// 					array("", "tbl_tags as  t", "t.tag_id = bt.tag_id", array("group_concat(t.tag_name) as tag_name,group_concat(t.tag_id) as tag_id")),
		// 					array("", "tbl_users as  u", "u.user_id = b.author_id", array("user_name as author_name")));
	
		// $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("bt.tag_id" => "ASC"),"b.blog_id",null); 
		//  // fetch query
		//  $result['blog_data']= $this->common->MySqlFetchRow($rs, "array"); // fetch result
 
		//  //get how we operate
		// $condition = "1=1 AND status = 'Active'  ";
		// $result['operate_data'] = $this->common->getDataLimit("tbl_how_we_operate",'*',$condition,"how_we_operate_id","ASC",'3');

	

		$result =array();

		// get all Published n approved categories 
		$condition = "status = 'Published' && admin_status ='Approved' && category_delete = '1' ";
		$result['categories'] = $this->common->getData("tbl_magazine_categories",'*',$condition);
		$condition = "status = 'Published' && admin_status ='Approved' ";
		$result['magzine'] = $this->common->getDataLimit("tbl_magazine_editions",'*',$condition,"id","desc","4");

		// to get all recent article 
		$condition ="a.status = 'Published' && a.admin_status ='Approved'  &&  a.article_delete = '1' ";
		$main_table = array("tbl_magazine_articles as a", array("a.*,a.author as  author_name"));
		$join_tables =  array();
		$join_tables = array(
				array("", "tbl_magazine_article_category as  ac", "ac.article_id = a.id", array()),
				array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
				// array("left", "tbl_users as  tu", "tu.id = a.author_id", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
				  );
		$limit=array("offset"=>"0","rows"=>"6");
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"a.id",$limit); 
		$result['article_data_listing'] = $this->common->MySqlFetchRow($rs, "array");

		$condition ="1=1 AND a.article_delete = '1'";
        $main_table = array("tbl_magazine_articles as a", array("a.*,a.author as  author_name"));
        $join_tables =  array();
        $join_tables = array(
                array("", "tbl_spotlight as  tu", "tu.article_id = a.id", array("tu.type")),
                // array("left", "tbl_users as  u", "u.id = a.author_id", array("concat(u.first_name,' ',u.last_name) as author_name")),
                array("", "tbl_magazine_categories as  c", "c.id = tu.category_id", array("c.title as category_name")),
                  );
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array(),"",null); 
        $result['spotlight_data'] = $this->common->MySqlFetchRow($rs, "array");


        // $condition ="a.status = 'Published' && a.admin_status ='Approved' && c.title='Perspective' ";
		// $main_table = array("tbl_magazine_articles as a", array("a.*,a.author as  author_name"));
		// $join_tables =  array();
		// $join_tables = array(
		// 		array("", "tbl_magazine_article_category as  ac", "ac.article_id = a.id", array()),
		// 		array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
		// 		// array("left", "tbl_users as  tu", "tu.id = a.author_id", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
		// 		  );
		// $limit=array("offset"=>"0","rows"=>"2");
		// $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"a.id",$limit); 
		// $result['perspective'] = $this->common->MySqlFetchRow($rs, "array");

		$condition ="a.status = 'Published' && a.admin_status ='Approved' && a.featured ='Featured' &&  a.article_delete = '1' ";
		$main_table = array("tbl_magazine_articles as a", array("a.*,a.author as  author_name"));
		$join_tables =  array();
		$join_tables = array(
				array("", "tbl_magazine_article_category as  ac", "ac.article_id = a.id", array()),
				array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
				// array("left", "tbl_users as  tu", "tu.id = a.author_id", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
				  );
		$limit=array("offset"=>"0","rows"=>"2");
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"a.id",$limit); 
		$result['featured'] = $this->common->MySqlFetchRow($rs, "array");


		$condition ="a.status = 'Published' && a.admin_status ='Approved' && c.title='Focus' &&  a.article_delete = '1' ";
		$main_table = array("tbl_magazine_articles as a", array("a.*,a.author as  author_name"));
		$join_tables =  array();
		$join_tables = array(
				array("", "tbl_magazine_article_category as  ac", "ac.article_id = a.id", array()),
				array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
				// array("left", "tbl_users as  tu", "tu.id = a.author_id", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
				  );
		$limit=array("offset"=>"0","rows"=>"2");
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"a.id",$limit); 
		$result['focus'] = $this->common->MySqlFetchRow($rs, "array");

		$condition ="status = 'Published' && admin_status ='Approved' ";
		$result['event_data'] = $this->common->getDataLimit("tbl_events",'*',$condition,"id","desc","2");
  	

		$result_metatag = array();		  
		$result_metatag['meta_description'] = "home page banner";
		$result_metatag['meta_keywords'] = " keywords,shiv,admin";
		

		// get news n events from api hit 
		// $url = "http://newsapi.org/v2/top-headlines?to=2020-10-16&sortBy=popularity&sources=bbc-news&apiKey=6452e0926c0348b59d9226643b365bac&pageSize=4";
		$url = "http://newsapi.org/v2/top-headlines?country=in&category=general&sortBy=popularity&apiKey=6452e0926c0348b59d9226643b365bac&pageSize=4";
		// $url = "http://newsapi.org/v2/everything?sortBy=popularity&q=Freight";
		// Initialize a CURL session. 
		$ch = curl_init(); 
		// Return Page contents. 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		//grab URL and pass it to the variable. 
		curl_setopt($ch, CURLOPT_URL, $url); 
		$topheading = json_decode(curl_exec($ch));
		// echo "<pre>";
		// print_r($topheading);
		// exit;

		$result['news_heading'] = '';
		if($topheading->status == "ok"){
			$result['news_heading'] = $topheading->articles;
		}
		

		$this->load->view('header',$result_metatag);
		$this->load->view('index',$result);
		$this->load->view('footer');
	}

}?>