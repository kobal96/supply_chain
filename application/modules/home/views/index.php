<div class="categories-scroll-top">
	<div>
	<?php  foreach ($categories as $key => $value) {?>
		<a href="<?= base_url('categories/'.$value['slug'])?>" ><?= $value['title']?></a>
	<?php } ?> 
	</div>
	<a href="<?=  base_url('categories') ?>" class="show-all-categories">Show all</a>
</div>	

	<section class="page-section mb-60 mbmob-40">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-7">
					<div class="main-article-home">

						<a href="<?= base_url('article/').$spotlight_data[0]['slug'] ?>">
						<img src="<?= FRONT_URL.'/images/article_image/'.$spotlight_data[0]['image']?>" alt="" class="img-fluid main-article-home-banner">

							<div class="article-preview">
								<p class="category-tag"><?= (!empty($spotlight_data[0]['category_name'])?$spotlight_data[0]['category_name']:"") ?></p>
								<h2 class="article-title-large"><?= (!empty($spotlight_data[0]['title'])?$spotlight_data[0]['title']:"") ?></h2>
								<p class="article-preview-text"><?= (!empty($spotlight_data[0]['meta_description'])?$spotlight_data[0]['meta_description']:"") ?></p>
								<div class="article-author-info">
									<p>by <span class="author-name"><?= (!empty($spotlight_data[0]['author_name'])?$spotlight_data[0]['author_name']:"") ?></span></p>
									<p><span class="article-date"><?= date('d-M, Y',strtotime($spotlight_data[0]['article_date']))?></span> 
									<!-- <span class="reading-time">6 min read</span></p> -->
								</div>
							</div>
						</a>

					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-5">
					<div class="home-spotlight">
						<h3 class="section-title-medium">Spotlight</h3>
						<div class="home-spotlight-list">
							<?php
					        if (!empty($spotlight_data)) {
					           foreach ($spotlight_data as $key => $value) {
					           	if ($value['type']=='Spotlight') {
					            $date_convert = date('Y-M-d',strtotime($value['created_at']));
					            $date_convert_array = explode('-',$date_convert);
					            ?>

							<div class="home-spotlight-single">
								<a href="<?= base_url('article/').$value['slug'] ?>">
									<div class="hss-image">
										<img src="<?= FRONT_URL.'/images/article_image/'.$value['image']?>" alt="" class="img-fluid home-spotlight-single-img">
									</div>
									<div class="article-block-content">
										<p class="category-tag"><?= $value['category_name'] ?></p>
										<h4><?= $value['title'] ?></h4>
										<div class="article-author-info">
											<p>by <span class="author-name"><?= $value['author_name'] ?></span></p>
											<p><span class="article-date"><?= date('d-M, Y',strtotime($value['article_date']))?></span>
											 <!-- <span class="reading-time"></span></p> -->
										</div>
									</div>
								</a>
							</div>
							 <?php 	} } } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section mb-60 mbmob-40">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<a href="#/" class="home-banner-top"><img src="<?=  base_url('assets/')?>images/strellar-home-top.jpg" alt="" class="img-fluid"></a>
				</div>
			</div>
		</div>
	</section>

	<!-- recent article  -->
	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 com-md-12 col-lg-9">
					<div class="row">
						<?php  
						if($article_data_listing){
							foreach ($article_data_listing as $key => $value) {?>
									<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
										<div class="hps-single">
											<a href="<?= base_url('article/'.$value['slug'])?>">
											<!-- <a href="<?= base_url('article/'.$value['id'])?>"> -->
												<img src="<?= base_url('images/article_image/'.$value['image'])?>" alt="" class="img-fluid hps-single-img">
												<div class="hps-content">
													<p class="category-tag"><?= $value['category_name'] ?></p>
													<h4><?= $value['title']?></h4>
													<div class="article-author-info">
														<p>by <span class="author-name"><?=(!empty($value['author_name'])?$value['author_name']:$value['author'])?></span></p>
														<p><span class="article-date"><?= date('d-M, Y',strtotime($value['article_date']))?></span> 
														<!-- <span class="reading-time">12 min read</span></p> -->
													</div>
												</div>
											</a>
										</div>
									</div>
							<?php }
						}
						?>
						<a href="<?= base_url('categories')?>" class="btn-celerity btn-large btn-blue text-uppercase m-auto">Load More</a>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-3">
					<div class="home-perspective">
						<h3 class="section-title-small">Featured</h3>
						<div class="home-perspective-list">

						<?php  
						if($featured){
							foreach ($featured as $key => $value) { ?>
							<div class="hps-single hps-sidebar">
								<a href="<?= base_url('article/'.$value['slug'])?>">
									<div class="hps-content">
										<p class="category-tag"><?= $value['category_name'] ?></p>
										<h4><?= $value['title']?></h4>
										<div class="article-author-info">
											<p>by <span class="author-name"><?=(!empty($value['author_name'])?$value['author_name']:$value['author'])?></span></p>
											<p><span class="article-date"><?= date('d-M, Y',strtotime($value['article_date']))?></span> <!-- <span class="reading-time">12 min read</span> --></p>
										</div>
									</div>
								</a>
							</div>
						<?php } } ?>
							<a href="<?= base_url('featured')?>" class="view-more-small">View More</a>
						</div>

						<?php  
						if($focus){?>
							<h3 class="section-title-small">Focus</h3>
							<div class="home-focus-list">
								<?php foreach ($focus as $key => $value) { ?>
								<div class="hps-single hps-sidebar">
									<a href="<?= base_url('article/'.$value['slug'])?>">
										<div class="hps-content">
											<p class="category-tag"><?= $value['category_name'] ?></p>
											<h4><?= $value['title']?></h4>
											<div class="article-author-info">
												<p>by <span class="author-name"><?=(!empty($value['author_name'])?$value['author_name']:$value['author'])?></span></p>
												<p><span class="article-date"><?= date('d M Y',strtotime($value['article_date']))?></span> <!-- <span class="reading-time">12 min read</span> --></p>
											</div>
										</div>
									</a>
								</div>
								<?php } ?>
								<a href="<?= base_url('categories/focus')?>" class="view-more-small">View More</a>
							</div>
							
						<?php } ?>
						
					</div>
				</div>
				<div class="col-12">
					<div class="section-separator mt-60 mb-60 mtmob-40 mbmob-40"></div>
				</div>
			</div>
		</div>
	</section>
	<!-- magazine editions -->
	<section class="page-section home-magazine-section mb-60 mbmob-40">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-title-link">
						<h2 class="section-title-medium">Magazine Editions</h2>
						<a href="<?= base_url('magazine')?>" class="view-more-small viewmore-desktop">View More</a>
					</div>
				</div>
			</div>
			<div class="row">
			<?php if($magzine){
				foreach ($magzine as $key => $value) {?>
					<div class="col-6 col-sm-3">
						<div class="home-magazine-single">
							<a href="<?=  base_url('magazine/'.$value['slug'])?>">
								<img src="<?=  base_url('images/edition_image/'.(!empty($value['image'])?$value['image']:"default-img.jpg"))?>" alt="" class="img-fluid home-magazine-single-img">
								<h4> <?= $value['title']?></h4>
							</a>
						</div>
					</div>
				<?php }
				
			}?>
				
			<div class="col-12 text-center">
				<a href="<?= base_url('magazine')?>" class="view-more-small viewmore-mobile">View More</a>
			</div>
			</div>
		</div>
	</section>

	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<a href="#/" class="home-banner-bottom"><img src="<?=  base_url('assets/')?>images/proconnect-home-bottom.gif" alt="" class="img-fluid"></a>
				</div>
				<div class="col-12">
					<div class="section-separator mt-60 mb-60 mtmob-40 mbmob-40"></div>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section mb-60 mbmob-40">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8">
					<div class="section-title-link">
						<h2 class="section-title-medium home-news-desktop">News</h2>						
						<a href="<?= base_url('news_event')?>" class="view-more-small viewmore-desktop">See all</a>
					</div>
					<div class="news-list-home">
					<?php 
						if($news_heading){
							foreach ($news_heading as $key => $value) { 
								?>
								<div class="nlh-single">
									<a  target="_blank" href="<?= $value->url?>">
										<div class="article-block-content">
											<p class="category-tag">News</p>
											<h4><?= $value->title ?></h4>
											<div class="article-author-info">
												<p>by <span class="author-name"><?= $value->author ?></span></p>
												<p><span class="article-date"><?= date_format(date_create($value->publishedAt),"d M, Y ");?></span></p>
											</div>
										</div>
										<div class="nlh-image">
											<img src="<?= (!empty($value->urlToImage)?$value->urlToImage:base_url('images/edition_image/default-img.jpg'))?>" alt="" class="nlh-img">
										</div>
									</a>
								</div>
							<?php }
						} ?>

					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-4">
					<?php if($event_data){?>
						<div class="section-title-link">
							<h2 class="section-title-medium home-news-desktop">Events</h2>
							<h2 class="section-title-medium home-news-mobile">News & Events</h2>
							<a href="<?= base_url('news_event')?>" class="view-more-small viewmore-desktop">See all</a>
						</div>
					<?php }?>
					<div class="events-list-home">
						<?php 
						if($event_data){
							foreach ($event_data as $key => $value) { 
								?>
						<div class="event-single">
							<div class="event-day">
								<div class="event-date"><?= date('d',strtotime($value['event_date'])) ?></div>
								<div class="event-month"><?= date('M, y',strtotime($value['event_date'])) ?></div>
							</div>
							<div class="event-content-wrapper">
								
								<div class="event-content">
									<a href="<?= base_url('event?text='.rtrim(strtr(base64_encode("id=".$value['id']), '+/', '-_'), '=').'')?>">
										<div class="article-block-content">
											<p class="category-tag">Event</p>
											<h4><?= $value['title']?></h4>									
										</div>
									</a>
								</div>							
							</div>
						</div>
						<?php } } ?>

					</div>
					<a href="#/"><img src="<?=  base_url('assets/')?>images/house-ad-2.jpg" alt="" class="img-fluid events-banner-img"></a>
				</div>
			</div>
		</div>
	</section>

</body>
</html>


