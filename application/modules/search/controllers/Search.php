<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Search extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		//checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('searchmodel','',TRUE);
	}

	public function index(){
		$result = array();
		$result['keywords']  = $_GET['key'];
		//echo "<pre>";print_r($result['featured_theme_data']);exit;
		$this->load->view('header',$result);
		$this->load->view('index.php',$result);
		$this->load->view('footer');

	}

	public function searchdata(){
		$result = array();
		$key = $_POST['key'];

		//$condition = "(a.title LIKE '%$key%' OR st.sub_theme_name LIKE '%$key%' OR t.video_title LIKE '%$key%') ";

		$condition  =" (a.title LIKE '%".$key."%' OR c.title LIKE '%".$key."%'  OR mt.title LIKE '%".$key."%' )" ;
        $condition .=" && a.status = 'Published' && a.admin_status ='Approved' ";

        $orderby = array("a.id" => "DESC");
        if ($_POST['sortby']=='old') {
			$orderby = array("a.id" => "ASC");
		}

		$main_table = array("tbl_magazine_articles as a", array("a.*,a.author as author_name"));
		$join_tables =  array();
		$join_tables = array(
				array("", "tbl_magazine_article_category as  ac", "ac.article_id = a.id", array()),
				array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
				array("", "tbl_magazine_article_tag as  st", "st.article_id = a.id", array()),
				array("", "tbl_magazine_tags as  mt", "mt.id = st.tag_id", array()),
				// array("left", "tbl_users as  tu", "tu.id = a.author_id", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
				  );
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, $orderby,"a.id",null); 
		$result['search_data'] = $this->common->MySqlFetchRow($rs, "array");

        $order = "DESC";
        if ($_POST['sortby']=='old') {
			$order = "ASC";
		}
        $condition  =" title like '%".$key."%' " ;
        $condition .=" && status = 'Published' && admin_status ='Approved' ";
        $result['magazine'] = $this->common->getData("tbl_magazine_editions",'*',$condition, 'id', $order);

        if ($_POST['category']=='articles') {
			$result['magazine'] = array();	
		}
		if ($_POST['category']=='magazines') {
			$result['search_data'] = array();	
		}

		// echo "<pre>";
		// echo $this->db->last_query();
		// print_r($result);
		// exit;

		$themeresponse = $this->load->view('search-design.php',$result,true);	
		
		if (!empty($themeresponse)) {
			print_r(json_encode(array('success'=>true, 'data'=> $themeresponse)));
			exit;
		}else{
			print_r(json_encode(array('success'=>false, 'data'=> $themeresponse)));
			exit;
			}
		

	}

	public function listdata(){
		$result = array();
		$keyword = $_POST['key'];

		$condition = "theme_name LIKE '%$keyword%'";
		$theme = $this->common->getData("tbl_themes",'theme_name',$condition);
		$themedata = array();
		if ($theme) {
		foreach ($theme as $key => $value) {
			$themedata[] = $value['theme_name'];
		}	
		}
		

		$condition = "sub_theme_name LIKE '%$keyword%' ";
		$stheme = $this->common->getData("tbl_sub_themes",'sub_theme_name',$condition);
		$subtheme = array();
		if ($stheme) {
		foreach ($stheme as $key => $value) {
			$subtheme[] = $value['sub_theme_name'];
		}
		}
		
		$condition = "video_title LIKE '%$keyword%' ";
		$videodata = $this->common->getData("tbl_videos",'video_title',$condition);
		$video = array();
		if ($stheme) {
		foreach ($videodata as $key => $value) {
			$video[] = $value['video_title'];
		}
		}

		$condition = "nicename LIKE '%$keyword%' ";
		$countrydata = $this->common->getData("tbl_country",'country_name',$condition);
		
		$country = array();
		if ($countrydata) {
		foreach ($countrydata as $key => $value) {
			$country[] = $value['country_name'];
		}
		}

		$data = array_merge($themedata,$subtheme,$video,$country);
		
		if (!empty($data)) {
			print_r(json_encode($data));
			exit;
		}
		

	}

	

}?>