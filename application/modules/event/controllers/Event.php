<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Event extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('eventmodel','',TRUE);
	}

	function _remap($method_name){
		if(!method_exists($this, $method_name)){
			$this->index();
		}else{
			$this->{$method_name}();
		}
	}

	public function index(){
		$id = $this->uri->segment(2);
		$result = array();

		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];
			$condition = "id ='".$id."' ";
			$result['event_data'] = $this->common->getData("tbl_events",'*',$condition);
		
		}
		
		//echo "<pre>"; print_r($result['spotlight_data']);exit;

		$this->load->view('header');
		$this->load->view('index',$result);
		$this->load->view('footer');
	}

	public function submitForm(){
		// print_r($_POST);exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();

			if (!$this->session->userdata('supply_chain_user')) {
				echo json_encode(array('success'=>true, 'msg'=>'Please login your account.'));
					exit;
			}
			
			$data['article_id']      = $this->input->post('id');
			$data['user_id']         = $this->session->userdata('supply_chain_user')[0]['id'];
			$data['comment']       = $this->input->post('comment');
			$data['status']       = 'Published';
			$data['user_type']       = 'frontend';
			$data['admin_status']    = 'Approved';
			$data['created_by']    = $this->session->userdata('supply_chain_user')[0]['id'];
			$data['updated_at']      = date("Y-m-d H:i:s");
			
			if(!empty($this->input->post('user_id'))){
				$condition = "user_id = '".$this->input->post('user_id')."' ";
				$result = $this->common->updateData("tbl_magazine_comments",$data,$condition);
				if($result){

					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");
				
				$result = $this->common->insertData('tbl_magazine_comments',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Create comment successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

	
}?>
