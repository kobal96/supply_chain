$(document).ready(function () {	
	/*Menu toggle*/
	//var $html = document.getElementById('menu-overlay');
	var $slider = document.getElementById('slider');
	var $toggle = document.getElementById('menu-toggle-link');

	$toggle.addEventListener('click', function() {
			var isOpen = $slider.classList.contains('slide-in');			
			$slider.setAttribute('class', isOpen ? 'slide-out' : 'slide-in');
			//var ishtmlOpen = $html.classList.contains('show');			
			//$html.setAttribute('class', ishtmlOpen ? 'hide' : 'show');
			event.preventDefault();
	});	
	/*Close menu when clicked outside*/
	if ($(window).width() > 992) {		
	$(document).click(function() {			
		if(	$('#slider').hasClass('slide-in')){
			$('#slider').addClass('slide-out').removeClass('slide-in')
		}	
	});
	$("#slider,#menu-toggle-link").click(function(event) {			
			event.stopPropagation();
	});
	}
	/*Close menu when clicked outside*/
	if($(window).width() < 992){
		$('.mobile-header-close').click(function(){
			$('#slider').removeClass('slide-in').addClass('slide-out')
		});
		$('.main-category-item').click(function(){			
			$(this).siblings('.flyout-list').removeClass('hidemenu-mobile').toggleClass('slide-inmob');	
		})
		
		$('.back-mobile').click(function(){				
			$(this).parent('h3').parent('.submenu').parent('.sub-menu-block').parent('li').parent('.flyout-list').removeClass('slide-inmob').addClass('hidemenu-mobile');
		})
		
	}
	/*Menu toggle*/		
	if($(window).width() > 992){
		$('.main-category-dropdown').hover(function(){
			$('.flyout-list').hide();
			$(this).children('.flyout-list').fadeIn();	
		},function(){
			$('.flyout-list').fadeOut();		
		});
		$(window).on("load", function () {
			$(".sub-categories-list").mCustomScrollbar({
				theme: "dark"
			});
		});
	}
	$('.niceselect').niceSelect();
	$('#events-link').click(function(){
		$('#news-tab').hide();
		$('#events-tab').fadeIn();
		$('#news-link').removeClass('active');
		$(this).addClass('active')
	});
	$('#news-link').click(function(){
		$('#events-tab').hide();
		$('#news-tab').fadeIn();
		$('#events-link').removeClass('active');
		$(this).addClass('active')
	})
	/*Profile Tabs*/
	/*Mobile*/
	$('.profile-link-mobile').click(function(){
		$('.profile-tabs-menu').slideToggle();		
	})	
	$('.close-submenu-mobile').click(function(){	
		$('.profile-tabs-menu').slideToggle();
	});
	$('.profile-links-wrapper a').click(function(){
		var mob_tab_link = $(this).attr('id');
		var mob_tab_dropdown = '.'+mob_tab_link+'-link-mobile';
		$('.profile-tabs-wrapper a').hide();
		$(mob_tab_dropdown).css({'display':'block'});
		$('.profile-tabs-menu').slideToggle();
		$('.profile-links-wrapper a').removeClass('active');
		$(this).addClass('active')
	})
	/*Mobile*/
	if($(window).width()>991){
		$('.profile-tabs-wrapper a').click(function(){
			var tab_link = $(this).attr('id');
			var tab_content = '#'+tab_link+'-content';
			$('.profile-content-tab').hide();
			$(tab_content).show();
			$('.profile-tabs-wrapper a').removeClass('active');
			$(this).addClass('active');
		})
	}else{
		$('.profile-links-wrapper a').click(function(){
			var tab_link = $(this).attr('id');
			var tab_content = '#'+tab_link+'-content';
			$('.profile-content-tab').hide();
			$(tab_content).show();
			$('.profile-tabs-wrapper a').removeClass('active');
			$(this).addClass('active');
		})
	}
	
	/*Profile Tabs*/
	/*My Articles Tabs*/
	$('.article-tab-links a').click(function(){
		var article_tab = $(this).attr('id');
		var article_tab_content = '#'+article_tab+'-content';
		$('.articles-list-wrapper').hide();
		$(article_tab_content).show();
		$('.article-tab-links a').removeClass('active');
		$(this).addClass('active')
	})
	/*My Articles Tabs*/
	/*Bookmark Button Toggle*/
	$('.bookmark-checkbox input[type="checkbox"]').click(function(){
		if($(this).prop("checked") == true){
				$('.delete-bookmark,.cancel-bookmark').show();
		}
		else if($(this).prop("checked") == false){
			$('.delete-bookmark,.cancel-bookmark').hide();
		}
	});
	$('.cancel-bookmark').click(function(){
		$('.bookmark-checkbox input[type="checkbox"]').prop("checked",false);
		$('.delete-bookmark,.cancel-bookmark').hide();
	});	
	/*Bookmark Button Toggle*/
	/*Notification Read More*/
	$('.read-more').click(function(){		
		$(this).parent('.notification-post-comment').children('p').toggleClass('expand')
	})
	/*Notification Read More*/	
	/*New Article*/
	$('.btn-new-post').click(function(){
		$('#my-articles-content').hide();
		$('#new-article-content, #article-step-1').show();
	});
	$('#new-article-back, .cancel-article').click(function(){
		$('#new-article-content, #article-step-1, #article-step-2').hide();
		$('#my-articles-content').show();		
	});
	$('.proceed-article').click(function(){
		$('#article-step-1').hide();
		$('#article-step-2').show();	
	});
	/*New Article*/
	/*Signup Toggle*/
	$('.signup-link').click(function(){
		$('#celerity-login, #celerity-forgotpassword').hide();
		$('#celerity-signup').show();
	});
	$('.login-link').click(function(){
		$('#celerity-signup, #celerity-forgotpassword').hide();
		$('#celerity-login').show();
	});
	$('.fp-link').click(function(){
		$('#celerity-login,#celerity-signup').hide();
		$('#celerity-forgotpassword').show();
	});
	/*Signup Toggle*/
	/*Category Listing Dropdown Scroll*/
	$(window).on("load", function () {
		$(".category-select.nice-select .list").mCustomScrollbar({
			theme: "light"
		});
	});
	$(window).on("load", function () {
		$(".category-link-wrapper").mCustomScrollbar({
			theme: "light"
		});
	});
	/*Category Listing Dropdown Scroll*/
	/*Billing Plan Highlight*/
	$('.billing-plan-single').click(function(){		
		$('.billing-plan-single').removeClass('active');
		$(this).addClass('active');
	})
	/*Billing Plan Highlight*/
	if ($(window).width() > 992) {	
	$(window).on('scroll', function () {
		if ($(this).scrollTop() > 100) {
			$('.celerity-header').addClass('celerity-header-fixed');
			$('.quicknav-header-wrapper').addClass('quicknav-header-wrapper-fixed');
			$('.prognroll-bar').css({'display':'block'})
		} else {
			$('.celerity-header').removeClass('celerity-header-fixed');
			$('.quicknav-header-wrapper').removeClass('quicknav-header-wrapper-fixed');
			$('.prognroll-bar').css({'display':'none'})
		}
	});
	}	
	$('#dropdown-desktop').click(function () {
		$('.login-flyout').toggleClass('show');
	});
	if ($(window).width() > 992) {		
		$(document).click(function() {			
			if(	$('.login-flyout').hasClass('show')){
				$('.login-flyout').removeClass('show')
			}	
		});
		$("#dropdown-desktop").click(function(event) {			
				event.stopPropagation();
		});
		}
		$('.account-mobile').click(function(){
			$('.profile-tabs-menu.login-tabs-menu').slideToggle();
		})
});





