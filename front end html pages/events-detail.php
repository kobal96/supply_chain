<?php require_once('include/head.php') ?>
<?php require_once('include/header.php') ?>
<section class="page-section mt-30 mb-30">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-9">
        <a href="#/" class="md-back">Back</a>
        <div class="event-single event-detail">
          <div class="event-day events-detail-day">
            <div class="event-date">21</div>
            <div class="event-month">Jul, 20</div>
          </div>
          <div class="event-content-wrapper">
            <div class="event-content">              
              <div class="article-block-content">
                <p class="category-tag">Event</p>
                <h4>Title of the event will be t here and it won’t be longer than two lines</h4>
                <div class="event-detail-share">
                  <a href="#/" class="btn-celerity btn-small btn-blue text-uppercase register-event">Register</a><br/><br/>
                  <a href="#/" class="event-share"><img src="images/facebook-ad.svg" alt=""></a>
                  <a href="#/" class="event-share"><img src="images/twitter-ad.svg" alt=""></a>
                  <a href="#/" class="event-share"><img src="images/linkedin-ad.svg" alt=""></a>
                  <a href="#/" class="event-share"><img src="images/email-ad.svg" alt=""></a>
                  <a href="#/" class="event-share"><img src="images/link-ad.svg" alt=""></a>
                </div>
                <div class="event-description-detail">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam accumsan et dolor aliquam pellentesque. Aliquam elit nunc, lacinia quis nunc vitae, hendrerit aliquet risus. Suspendisse tincidunt, urna id faucibus porta, velit ex mollis orci, nec finibus lacus velit et lorem. Curabitur eros lectus, sollicitudin a turpis eu, placerat tempor quam. Donec a nisl pulvinar, vehicula leo vitae, tempor elit. Curabitur leo velit, mollis vitae urna eu, molestie imperdiet ligula. Pellentesque quis pellentesque nisi.</p>
                  <p>Etiam et volutpat lacus, sed semper libero. Nullam mi urna, eleifend eu tempor eget, aliquam nec lacus. Nunc eleifend turpis non odio aliquet fermentum. In vestibulum elit in magna facilisis, nec faucibus dui tincidunt. Nam ut sollicitudin justo. Nulla vitae nisl venenatis, porttitor dui eleifend, condimentum quam. Praesent consectetur nisi vel enim fermentum, nec tincidunt odio porttitor.</p>
                  <p>Nam scelerisque diam nec sapien efficitur blandit. Morbi facilisis enim ac arcu posuere pretium. In vitae convallis elit. Maecenas at magna ac metus suscipit feugiat. Etiam semper ac tortor hendrerit dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla mollis in quam non feugiat.</p>
                </div>                
              </div>              
            </div>
          </div>
        </div>
      </div>      
    </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>