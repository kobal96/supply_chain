	<?php require_once('include/head.php') ?>
	<?php require_once('include/header-login.php') ?>	
	<div class="categories-scroll-top">
		<a href="category-listing.php">Technology</a>
		<a href="category-listing.php">Automation</a>
		<a href="category-listing.php">Retail</a>
		<a href="category-listing.php">Procurement</a>
		<a href="category-listing.php">Prespective</a>
		<a href="category-listing.php">Automotive & Industrial</a>
		<a href="category-listing.php">Focus</a>
		<a href="category-listing.php">Gamechanger</a>
		<a href="category-listing.php">Show all</a>
	</div>
	<section class="page-section mb-60 mbmob-40">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-7">
					<div class="main-article-home">
						<a href="article-detail.php">
							<img src="images/hero-large.png" alt="" class="img-fluid main-article-home-banner">
							<div class="article-preview">
								<p class="category-tag">Pharma and Chemical</p>
								<h2 class="article-title-large">Shifting Dynamics - Vaccination Preparedness And Food Supply</h2>
								<p class="article-preview-text">A good supply chain is demand driven; right from provisioning the inputs, to support production, and to distribute the product to final buyer</p>
								<div class="article-author-info">
									<p>by <span class="author-name">Brett Howell</span></p>
									<p><span class="article-date">31 May 2020</span> <span class="reading-time">6 min read</span></p>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-5">
					<div class="home-spotlight">
						<h3 class="section-title-medium">Spotlight</h3>
						<div class="home-spotlight-list">
							<div class="home-spotlight-single">
								<a href="article-detail.php">
									<div class="hss-image">
										<img src="images/home-spotlight-single.png" alt="" class="img-fluid home-spotlight-single-img">
									</div>
									<div class="article-block-content">
										<p class="category-tag">Pharma and Chemical</p>
										<h4>A Generational Opportunity - Redesign of Supply Chains</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Brett Howell</span></p>
											<p><span class="article-date">31 May, 2020</span> <span class="reading-time">6 min read</span></p>
										</div>
									</div>
								</a>
							</div>
							<div class="home-spotlight-single">
								<a href="article-detail.php">
									<div class="hss-image">
										<img src="images/home-spotlight-single-2.png" alt="" class="img-fluid home-spotlight-single-img">
									</div>
									<div class="article-block-content">
										<p class="category-tag">Opinion</p>
										<h4>Businesses must prepare for a world trumped for trade</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Alfred Schneider</span></p>
											<p><span class="article-date">Feb 02, 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
							<div class="home-spotlight-single">
								<a href="article-detail.php">
									<div class="hss-image">
										<img src="images/home-spotlight-single-3.png" alt="" class="img-fluid home-spotlight-single-img">
									</div>
									<div class="article-block-content">
										<p class="category-tag">Food & Beverages</p>
										<h4>Eyeing SCM Consolidation Post-GST</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Sadie Jackson</span></p>
											<p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section mb-60 mbmob-40">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<a href="#/" class="home-banner-top"><img src="images/home-banner-1.png" alt="" class="img-fluid"></a>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 com-md-12 col-lg-9">
					<div class="row">
						<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
							<div class="hps-single">
								<a href="article-detail.php">
									<img src="images/perspective-1.png" alt="" class="img-fluid">
									<div class="hps-content">
										<p class="category-tag">Pharma and Chemical</p>
										<h4>A Generational Opportunity - Redesign of Supply Chains</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Joshua Mullins</span></p>
											<p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
							<div class="hps-single">
								<a href="article-detail.php">
									<img src="images/perspective-2.png" alt="" class="img-fluid">
									<div class="hps-content">
										<p class="category-tag">Technology</p>
										<h4>The Tech'tonic Shift</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Eliza Miller</span></p>
											<p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
							<div class="hps-single">
								<a href="article-detail.php">
									<img src="images/perspective-3.png" alt="" class="img-fluid">
									<div class="hps-content">
										<p class="category-tag">Procurement</p>
										<h4>Procurement Trends of Today, Goals of Tomorrow</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Raymond Bowers</span></p>
											<p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
							<div class="hps-single">
								<a href="article-detail.php">
									<img src="images/perspective-4.png" alt="" class="img-fluid">
									<div class="hps-content">
										<p class="category-tag">Fashion and lifestyle</p>
										<h4>Next Gen Automation is here</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Maurice Walsh</span></p>
											<p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
							<div class="hps-single">
								<a href="article-detail.php">
									<img src="images/perspective-5.png" alt="" class="img-fluid">
									<div class="hps-content">
										<p class="category-tag">Infrastructure</p>
										<h4>Landing Sustainable Infrastructure Prospects</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Ralph Watkins</span></p>
											<p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4">
							<div class="hps-single">
								<a href="article-detail.php">
									<img src="images/perspective-6.png" alt="" class="img-fluid">
									<div class="hps-content">
										<p class="category-tag">CONSUMER DURABLES, ELECTRONICS CONSUMER DURABLES, ELECTRONICS</p>
										<h4>Consumer Electronics - Delivering Service Satisfaction</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Joshua Mullins</span></p>
											<p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
						</div>
						<a href="#/" class="btn-celerity btn-large btn-blue text-uppercase m-auto">Load More</a>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-3">
					<div class="home-perspective">
						<h3 class="section-title-small">Perspective</h3>
						<div class="home-perspective-list">
							<div class="hps-single hps-sidebar">
								<a href="article-detail.php">
									<div class="hps-content">
										<p class="category-tag">Pharma and Chemical</p>
										<h4>A Generational Opportunity - Redesign of Supply Chains</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Joshua Mullins</span></p>
											<p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
							<div class="hps-single hps-sidebar">
								<a href="article-detail.php">
									<div class="hps-content">
										<p class="category-tag">Infrastructure</p>
										<h4>Next Gen Automation is here</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Raymond Bowers</span></p>
											<p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
							<a href="#/" class="view-more-small">View More</a>
						</div>
						<h3 class="section-title-small">Focus</h3>
						<div class="home-focus-list">
							<div class="hps-single hps-sidebar">
								<a href="article-detail.php">
									<div class="hps-content">
										<p class="category-tag">Pharma and Chemical</p>
										<h4>A Generational Opportunity - Redesign of Supply Chains</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Joshua Mullins</span></p>
											<p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
							<div class="hps-single hps-sidebar">
								<a href="article-detail.php">
									<div class="hps-content">
										<p class="category-tag">Pharma and Chemical</p>
										<h4>A Generational Opportunity - Redesign of Supply Chains</h4>
										<div class="article-author-info">
											<p>by <span class="author-name">Joshua Mullins</span></p>
											<p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
										</div>
									</div>
								</a>
							</div>
							<a href="#/" class="view-more-small">View More</a>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="section-separator mt-60 mb-60 mtmob-40 mbmob-40"></div>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section home-magazine-section mb-60 mbmob-40">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section-title-link">
						<h2 class="section-title-medium">Magazine Editions</h2>
						<a href="#/" class="view-more-small viewmore-desktop">View More</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-6 col-sm-3">
					<div class="home-magazine-single">
						<a href="magazine-detail.php">
							<img src="images/celerity-jan-feb.png" alt="" class="img-fluid">
							<h4>July-Aug, 2020</h4>
						</a>
					</div>
				</div>
				<div class="col-6 col-sm-3">
					<div class="home-magazine-single">
						<a href="magazine-detail.php">
							<img src="images/celerity-july-august.png" alt="" class="img-fluid">
							<h4>May - Jun, 2020</h4>
						</a>
					</div>
				</div>
				<div class="col-6 col-sm-3">
					<div class="home-magazine-single">
						<a href="magazine-detail.php">
							<img src="images/celerity-march-april-2020.png" alt="" class="img-fluid">
							<h4>Mar - Apr, 2020</h4>
						</a>
					</div>
				</div>
				<div class="col-6 col-sm-3">
					<div class="home-magazine-single">
						<a href="magazine-detail.php">
							<img src="images/may-june-2020.png" alt="" class="img-fluid">
							<h4>Jan - Feb, 2020</h4>
						</a>
					</div>
				</div>
				<div class="col-12 text-center">
				<a href="#/" class="view-more-small viewmore-mobile">View More</a>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<a href="#/" class="home-banner-bottom"><img src="images/home-banner-2.png" alt="" class="img-fluid"></a>
				</div>
				<div class="col-12">
					<div class="section-separator mt-60 mb-60 mtmob-40 mbmob-40"></div>
				</div>
			</div>
		</div>
	</section>
	<section class="page-section mb-60 mbmob-40">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-8">
					<div class="section-title-link">
						<h2 class="section-title-medium home-news-desktop">News</h2>						
						<a href="#/" class="view-more-small viewmore-desktop">See all</a>
					</div>
					<div class="news-list-home">
						<div class="nlh-single">
							<a href="article-detail.php">
								<div class="article-block-content">
									<p class="category-tag">News</p>
									<h4>Severe or fatal COVID-19 rare in children, UK study finds: Live - Al Jazeera English</h4>
									<div class="article-author-info">
										<p>by <span class="author-name">Zaheena Rasheed</span></p>
										<p><span class="article-date">31 May, 2020</span></p>
									</div>
								</div>
								<div class="nlh-image">
									<img src="images/news-home-1.png" alt="" class="nlh-img">
								</div>
							</a>
						</div>
						<div class="nlh-single">
							<a href="article-detail.php">
								<div class="article-block-content">
									<p class="category-tag">News</p>
									<h4>Before and after satellite images show widespread destruction from Hurricane Laura - CNN</h4>
									<div class="article-author-info">
										<p>by <span class="author-name">Joseph Wulfsohn</span></p>
										<p><span class="article-date">31 May, 2020</span></p>
									</div>
								</div>
								<div class="nlh-image">
									<img src="images/news-home-2.png" alt="" class="nlh-img">
								</div>
							</a>
						</div>
						<div class="nlh-single">
							<a href="article-detail.php">
								<div class="article-block-content">
									<p class="category-tag">News</p>
									<h4>Epic Games Sends Emails to Fortnite Players Blaming Apple for New Season's Unavailability - MacRumors</h4>
									<div class="article-author-info">
										<p>by <span class="author-name">Juli Clover</span></p>
										<p><span class="article-date">31 May, 2020</span></p>
									</div>
								</div>
								<div class="nlh-image">
									<img src="images/news-home-3.png" alt="" class="nlh-img">
								</div>
							</a>
						</div>
						<div class="nlh-single">
							<a href="article-detail.php">
								<div class="article-block-content">
									<p class="category-tag">News</p>
									<h4>Halo fitness band and app: Amazon's entry into the fitness space is ambitious, but odd - CNET</h4>
									<div class="article-author-info">
										<p>by <span class="author-name">Scott Stein</span></p>
										<p><span class="article-date">31 May, 2020</span></p>
									</div>
								</div>
								<div class="nlh-image">
									<img src="images/news-home-4.png" alt="" class="nlh-img">
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-4">
					<div class="section-title-link events-title">
						<h2 class="section-title-medium home-news-desktop">Events</h2>
						<h2 class="section-title-medium home-news-mobile">News & Events</h2>
						<a href="#/" class="view-more-small viewmore-desktop">See all</a>
					</div>
					<div class="events-list-home">
						<div class="event-single">
							<div class="event-day">
								<div class="event-date">21</div>
								<div class="event-month">Jul, 20</div>
							</div>
							<div class="event-content-wrapper">
								<div class="event-content">
									<a href="article-detail.php">
										<div class="article-block-content">
											<p class="category-tag">Event</p>
											<h4>Title of the event will be displayed here and it won’t be longer than two lines Title of the event will be displayed here and it won’t be longer than two lines</h4>									
										</div>
									</a>
								</div>
								<div class="event-content">
									<a href="article-detail.php">
										<div class="article-block-content">
											<p class="category-tag">Event</p>
											<h4>Title of the event will be displayed here and it won’t be longer than two lines Title of the event will be displayed here and it won’t be longer than two lines</h4>									
										</div>
										</a>
								</div>							
							</div>
						</div>
					</div>
					<a href="#/"><img src="images/events-banner.png" alt="" class="img-fluid events-banner-img"></a>
				</div>
			</div>
		</div>
	</section>
	<?php require_once('include/footer.php') ?>
	<?php require_once('include/footer-scripts.php') ?>
</body>
</html>