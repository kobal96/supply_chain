<?php require_once('include/head.php') ?>
<?php require_once('include/header.php') ?>
<section class="page-section mt-60 mtmob-0">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-6">
        <h1 class="page-title-large news-events-title">News and Events</h1>
      </div>
      <div class="col-12 col-sm-12 col-md-6">
        <div class="category-search">
          <form>
            <input type="text" class="category-search-text" placeholder="Search">
          </form>
        </div>
      </div>
      <div class="col-12">
        <div class="news-events-tabs">
          <a href="#/" id="news-link" class="active">News</a>
          <a href="#/" id="events-link">Events</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="category-filter-wrapper">
          <div class="category-sortby">
            <span class="filter-label">Sort By</span>
            <select name="" id="" class="niceselect">
              <option value="Recent">Recent</option>
              <option value="Most Viewed">Most Viewed</option>
              <option value="Most Commented">Most Commented</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-30 mb-60 mtmob-10" id="news-tab">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-1.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Chugging along the success track</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Joshua Mullins</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-2.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>The Driving Force </h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Eliza Miller</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-3.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Small Parts, Big Impact</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Raymond Bowers</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-1.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Chugging along the success track</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Joshua Mullins</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-2.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>The Driving Force </h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Eliza Miller</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-3.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Small Parts, Big Impact</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Raymond Bowers</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-1.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Chugging along the success track</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Joshua Mullins</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-2.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>The Driving Force </h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Eliza Miller</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-3.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Small Parts, Big Impact</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Raymond Bowers</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12">
        <div class="text-center">
          <a href="#/" class="btn-celerity btn-large btn-blue text-uppercase">Load More</a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-30 mb-60 mtmob-10" id="events-tab">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
        <div class="event-single events-tab-single">
          <div class="event-day">
            <div class="event-date">21</div>
            <div class="event-month">Jul, 20</div>
          </div>
          <div class="event-content-wrapper">
            <div class="event-content">
              <a href="#/">
                <div class="article-block-content">
                  <p class="category-tag">Event</p>
                  <h4>Title of the event will be displayed here and it won’t be longer than two lines Title of the event will be displayed here and it won’t be longer than two lines</h4>
                  <p class="event-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pulvinar blandit rutrum. Quisque hendrerit ultrices tortor ac porta. Donec id ante eleifend, condimentum felis non, eleifend enim. Aliquam fringilla sem id risus porta congue.</p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
        <div class="event-single events-tab-single">
          <div class="event-day">
            <div class="event-date">21</div>
            <div class="event-month">Jul, 20</div>
          </div>
          <div class="event-content-wrapper">
            <div class="event-content">
              <a href="#/">
                <div class="article-block-content">
                  <p class="category-tag">Event</p>
                  <h4>Title of the event will be displayed here and it won’t be longer than two lines Title of the event will be displayed here and it won’t be longer than two lines</h4>
                  <p class="event-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pulvinar blandit rutrum. Quisque hendrerit ultrices tortor ac porta. Donec id ante eleifend, condimentum felis non, eleifend enim. Aliquam fringilla sem id risus porta congue.</p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
        <div class="event-single events-tab-single">
          <div class="event-day">
            <div class="event-date">21</div>
            <div class="event-month">Jul, 20</div>
          </div>
          <div class="event-content-wrapper">
            <div class="event-content">
              <a href="#/">
                <div class="article-block-content">
                  <p class="category-tag">Event</p>
                  <h4>Title of the event will be displayed here and it won’t be longer than two lines Title of the event will be displayed here and it won’t be longer than two lines</h4>
                  <p class="event-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pulvinar blandit rutrum. Quisque hendrerit ultrices tortor ac porta. Donec id ante eleifend, condimentum felis non, eleifend enim. Aliquam fringilla sem id risus porta congue.</p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
        <div class="event-single events-tab-single">
          <div class="event-day">
            <div class="event-date">21</div>
            <div class="event-month">Jul, 20</div>
          </div>
          <div class="event-content-wrapper">
            <div class="event-content">
              <a href="#/">
                <div class="article-block-content">
                  <p class="category-tag">Event</p>
                  <h4>Title of the event will be displayed here and it won’t be longer than two lines Title of the event will be displayed here and it won’t be longer than two lines</h4>
                  <p class="event-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pulvinar blandit rutrum. Quisque hendrerit ultrices tortor ac porta. Donec id ante eleifend, condimentum felis non, eleifend enim. Aliquam fringilla sem id risus porta congue.</p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
        <div class="event-single events-tab-single">
          <div class="event-day">
            <div class="event-date">21</div>
            <div class="event-month">Jul, 20</div>
          </div>
          <div class="event-content-wrapper">
            <div class="event-content">
              <a href="#/">
                <div class="article-block-content">
                  <p class="category-tag">Event</p>
                  <h4>Title of the event will be displayed here and it won’t be longer than two lines Title of the event will be displayed here and it won’t be longer than two lines</h4>
                  <p class="event-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pulvinar blandit rutrum. Quisque hendrerit ultrices tortor ac porta. Donec id ante eleifend, condimentum felis non, eleifend enim. Aliquam fringilla sem id risus porta congue.</p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
        <div class="event-single events-tab-single">
          <div class="event-day">
            <div class="event-date">21</div>
            <div class="event-month">Jul, 20</div>
          </div>
          <div class="event-content-wrapper">
            <div class="event-content">
              <a href="#/">
                <div class="article-block-content">
                  <p class="category-tag">Event</p>
                  <h4>Title of the event will be displayed here and it won’t be longer than two lines Title of the event will be displayed here and it won’t be longer than two lines</h4>
                  <p class="event-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pulvinar blandit rutrum. Quisque hendrerit ultrices tortor ac porta. Donec id ante eleifend, condimentum felis non, eleifend enim. Aliquam fringilla sem id risus porta congue.</p>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12">
        <div class="text-center">
          <a href="#/" class="btn-celerity btn-large btn-blue text-uppercase">Load More</a>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>