<?php require_once('include/head.php') ?>
<?php require_once('include/header.php') ?>
<section class="page-section mt-60 mtmob-0 mttab-0">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-6 category-select-list-wrapper">        
        <div class="category-select-list">
          <select name="" id="" class="category-select niceselect">
            <option value="Automotive & Industrial">Automotive & Industrial</option>
            <option value="Technology">Technology</option>
            <option value="Commodity">Commodity</option>
            <option value="Procurement">Procurement</option>
            <option value="Retail">Retail</option>
            <option value="Pharma and Chemicals">Pharma and Chemicals</option>
            <option value="Industrial">Industrial</option>
            <option value="Food & Beverages FMCG">Food & Beverages FMCG</option>
            <option value="Infrastructure">Infrastructure</option>
          </select>
        </div>
      </div>
      <!-- Mobile view categories --> 
      <div class="col-12 col-sm-12 d-md-block col-md-12 d-lg-none d-xl-none nopadding-mobile">
        <div class="profile-tabs-wrapper profile-tabs-wrapper-mobile category-tabs-wrapper mt-30">
          <a href="#/" class="profile-link-mobile active automotive-link-mobile">Automotive & Industrial <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile technology-link-mobile">Technology <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile commodity-link-mobile">Commodity <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile procurement-link-mobile">Procurement <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile retail-link-mobile">Retail <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile pharma-link-mobile">Pharma and Chemicals <img src="images/profile-mobile-arrow.svg" alt=""></a>          
          <a href="#/" class="profile-link-mobile industrial-link-mobile">Industrial <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile food-link-mobile">Food & Beverages FMCG <img src="images/profile-mobile-arrow.svg" alt=""></a> 
          <a href="#/" class="profile-link-mobile infrastructure-link-mobile">Infrastructure <img src="images/profile-mobile-arrow.svg" alt=""></a>         
        </div>
        <div class="profile-tabs-menu">
          <a href="#/" class="close-submenu-mobile">
            <img src="images/down-menu-mobile.svg" alt="">
          </a>          
          <div class="profile-links-wrapper category-link-wrapper">
            <a href="#/" id="automotive" class="">Automotive & Industrial</a>
            <a href="#/" id="technology" class="">Technology</a>
            <a href="#/" id="commodity" class="active">Commodity</a>
            <a href="#/" id="procurement">Procurement</a>
            <a href="#/" id="retail">Retail</a> 
            <a href="#/" id="pharma">Pharma and Chemicals</a>
            <a href="#/" id="industrial">Industrial</a>
            <a href="#/" id="food">Food & Beverages FMCG</a> 
            <a href="#/" id="infrastructure">Infrastructure</a>          
          </div>
        </div>
      </div>
      <!-- Mobile view categories -->
      <div class="col-12 col-sm-12 col-md-6">
        <div class="category-search">
          <form>
            <input type="text" class="category-search-text" placeholder="Search">
          </form>
        </div>
      </div>
      <div class="col-12">
        <div class="category-select-seperator"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="category-filter-wrapper">
          <div class="category-sortby">
            <span class="filter-label">Sort By</span>
            <select name="" id="" class="niceselect">
              <option value="Recent">Recent</option>
              <option value="Most Viewed">Most Viewed</option>
              <option value="Most Commented">Most Commented</option>
            </select>
          </div>
          <div class="category-type">
            <span class="filter-label">Type</span>
            <select name="" id="" class="niceselect">
              <option value="Recent">Recent</option>
              <option value="Most Viewed">Most Viewed</option>
              <option value="Most Commented">Most Commented</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-30 mb-60 mtmob-10">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-1.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Chugging along the success track</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Joshua Mullins</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-2.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>The Driving Force </h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Eliza Miller</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-3.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Small Parts, Big Impact</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Raymond Bowers</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-1.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Chugging along the success track</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Joshua Mullins</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-2.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>The Driving Force </h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Eliza Miller</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-3.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Small Parts, Big Impact</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Raymond Bowers</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="category-listing-banner-full article-detail-banner-full text-center">
          <a href="#"><img src="images/home-banner-2.png" alt="" class="img-fluid m-auto d-none d-sm-none d-md-block"></a>
          <a href="#"><img src="images/home-banner-1.png" alt="" class="img-fluid m-auto d-block d-sm-block d-md-none"></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-1.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Chugging along the success track</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Joshua Mullins</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-2.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>The Driving Force </h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Eliza Miller</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-3.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Small Parts, Big Impact</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Raymond Bowers</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-1.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Chugging along the success track</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Joshua Mullins</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-2.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>The Driving Force </h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Eliza Miller</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-3.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Automotive & Industrial</p>
              <h4>Small Parts, Big Impact</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Raymond Bowers</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>