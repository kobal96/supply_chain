<?php require_once('include/head.php') ?>
<?php require_once('include/header.php') ?>
<section class="page-section magazine-detail-bg">  
  <div class="magazine-detail-bg-mobile"></div>
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-6">
        <a href="#/" class="md-back md-mobile"><img src="images/md-back.svg" alt=""> Back</a>
        <div class="magazine-detail-img">
          <img src="images/celerity-jan-feb.png" alt="" class="img-fluid">
        </div>        
      </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-6">
        <div class="magazine-detail-content">
            <a href="#/" class="md-back md-desktop"><img src="images/md-back.svg" alt=""> Back</a>
            <p class="category-tag">Magazine Edition</p>
            <p class="magazine-publish-date">July-Aug, 2020</p>
            <div class="magazine-detail-description">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pulvinar blandit rutrum. Quisque hendrerit ultrices tortor ac porta. Donec id ante eleifend, condimentum felis non, eleifend enim. Aliquam fringilla sem id risus porta congue. Sed suscipit nisl eu arcu condimentum, a posuere mi pulvinar. Maecenas sollicitudin dui erat, sit amet dapibus justo facilisis quis. Mauris quis massa metus.</p>
            </div> 
            <div class="magazine-detail-actions">
              <a href="#/" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase">View</a>
              <a href="#/" class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase">Download</a>
              <a href="#/"><img src="images/md-share.svg" alt=""></a>
            </div>           
        </div>
      </div>
    </div>
  </div>
</section>
<section class="page-section similar-magazines mt-60 mtmob-40 mbmob-40">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section-title-medium ">You might also like</h3>
      </div>
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/celerity-jan-feb.png" alt="" class="img-fluid">
            <h4>July-Aug, 2020</h4>
          </a>
        </div>
      </div>
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/celerity-july-august.png" alt="" class="img-fluid">
            <h4>May - Jun, 2020</h4>
          </a>
        </div>
      </div>
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/celerity-march-april-2020.png" alt="" class="img-fluid">
            <h4>Mar - Apr, 2020</h4>
          </a>
        </div>
      </div>
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/may-june-2020.png" alt="" class="img-fluid">
            <h4>Jan - Feb, 2020</h4>
          </a>
        </div>
      </div>           
    </div>        
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>