<!-- Header -->
<div class="container-fluid">
	<div class="row">
		<div class="col-12 pl-0 pr-0 nopadding-mobile">
			<div class="celerity-header">
				<div class="search-menu-wrapper">
					<a href="#" id="menu-toggle-link">
						<img src="images/menu.svg" alt="">
						<span>Explore</span>
					</a>
					<a href="#/" id="search-link-mobile"><img src="images/search-mobile.jpg" alt=""></a>
					<div class="search-wrapper">
						<form>
							<input type="text" class="nav-search" placeholder="Search">
						</form>
					</div>
				</div>
				<a href="index.php" class="celerity-logo"><img src="images/logo.png" alt=""></a>
				<div class="login-links">
					<a href="#/" class="btn-celerity btn-small btn-black">Celerity Awards</a>
					<a href="#/" class="btn-celerity btn-small btn-blue">Subscribe</a>
					<a href="login.php" class="btn-celerity btn-small btn-blue">Login</a>
				</div>
				<a href="#/" class="account-mobile"><img src="images/account-mobile.svg" alt=""></a>
			</div>

		</div>
	</div>
</div>
<!-- Navigation Menu -->
<div class="menu-overlay" id="menu-overlay"></div>
<div class="menu-block-wrapper" id="slider">
	<div class="menu-block">
		<div class="main-menu-block">
			<div class="menu-header-mobile">
				<a href="#" class="mobile-header-close">
					<img src="images/close-mobile-menu.svg" alt="">
				</a>
				<a href="index.php" class="mobile-header-logo">
					<img src="images/logo.png" alt="">
				</a>
				<a href="#/" class="mobile-header-account">
					<img src="images/account-mobile.svg" alt="">
				</a>
			</div>
			<div class="main-menu">
				<ul class="main-categories-list">
					<li class="main-category-dropdown">
						<a href="#/" class=" main-category-item"><span class="dot categories"></span>Categories <img src="images/right-arrow-menu.png" alt=""></a>
						<ul class="flyout-list">
							<li>
								<div class="sub-menu-block">
									<div class="submenu">
										<h3><img src="images/subcategory-back.svg" alt="" class="back-mobile"> Categories</h3>
										<ul class="sub-categories-list">
											<li><a href="category-listing.php" class="sub-category-item">Technology</a></li>
											<li><a href="category-listing.php" class="sub-category-item">Commodity</a></li>
											<li><a href="category-listing.php" class="sub-category-item">Procurement</a></li>
											<li><a href="category-listing.php" class="sub-category-item">Retail</a></li>
											<li><a href="category-listing.php" class="sub-category-item">Pharma and Chemicals</a></li>
											<li><a href="category-listing.php" class="sub-category-item">Industrial</a></li>
											<li><a href="category-listing.php" class="sub-category-item">Food & Beverages FMCG</a></li>
											<li><a href="category-listing.php" class="sub-category-item">Infrastructure</a></li>
											<li><a href="category-listing.php" class="sub-category-item">Colloquium</a></li>
											<li><a href="category-listing.php" class="sub-category-item">Electronics & Telecom</a></li>
											<li><a href="category-listing.php" class="sub-category-item">Fashion And Lifestyle</a></li>
											<li><a href="category-listing.php" class="sub-category-item">Leadership</a></li>
										</ul>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="main-category-dropdown">
						<a href="#/" class="main-category-item"><span class="dot magazine"></span>Magazine Editions <img src="images/right-arrow-menu.png" alt=""></a>
						<ul class="flyout-list">
							<li>
								<div class="sub-menu-block">
									<div class="submenu">
										<h3><img src="images/subcategory-back.svg" alt="" class="back-mobile"> Magazine Editions</h3>
										<ul class="sub-categories-list">
											<li><a href="magazine-detail.php" class="sub-category-item">July-August 2020</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">May - June 2020</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">March - April 2020</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">January - February 2020</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">November - December 2019</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">September - October 2019</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">July - August 2019</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">May - June 2019</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">March - April 2019</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">January - February 2019</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">November-December 2018</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">September-October 2018</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">July-August 2018</a></li>
											<li><a href="magazine-detail.php" class="sub-category-item">May-June 2018</a></li>
										</ul>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li><a href="news-events.php" class="main-category-item"><span class="dot news"></span>News & Events</a></li>
					<li><a href="#/" class="main-category-item"><span class="dot awards"></span>Awards</a></li>
					<li><a href="aboutus.php" class="main-category-item"><span class="dot about"></span> About us</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!-- Navigation Menu -->
<!-- Header -->
<div class="header-clear"></div>