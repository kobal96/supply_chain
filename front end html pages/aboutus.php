<?php require_once('include/head.php') ?>
<?php require_once('include/header.php') ?>
<section class="page-section mt-60 mtmob-40">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 offset-lg-1 col-lg-9">
        <div class="aboutus-intro">
          <h1>Celerity in business through supplychain</h1>
          <p class="about-intro-tagline">
            As the name suggests, Celerity aims to bring speed to all your supply chain thoughts and action in this time poor world. Through various platforms, we aim to promote how supply chain best practices bring speed in business and help businesses gain competitive advantage. The major objective is to highlight the importance of a well-run supply chain operation and bring supply chain from the back-end to the boardroom.
          </p>
        </div>      
      </div>
      <div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-5">
        <div class="vision-mission-wrapper">
          <h3>Our Vision </h3>
          <p>To persistently explore and uncover business needs pertaining to supply chain and logistics and enable business growth of our readers and subscribers by providing multiple platforms to share, ask and discover best practices.</p>
        </div>
      </div>  
      <div class="col-12 col-sm-12 col-md-12 col-lg-5">
          <div class="vision-mission-wrapper">
          <h3>Our Mission </h3>
          <p>To connect India’s professionals to share, learn, connect, exchange ideas, discover and network amongst their chosen domain, in this case, supply chain and logistics. Our services are designed to enable you to promote supply chain and logistics best practices through your own experiences in India and abroad. We aim to help you create a network of trusted relationships.</p>
        </div>
      </div>   
      <div class="col-12">
        <div class="vision-mission-seperator"></div>
      </div>
      <div class="col-12 col-sm-12 col-md-6">
        <div class="founders-wrapper">
          <img src="images/charulata-bansal.png" alt="" class="img-fluid">
          <div class="founder-content">
            <p class="founder-role">Our Founder</p>
            <h3>Charulata Bansal <a href="#/"><img src="images/linkedin-about.svg" alt=""></a></h3>
            <div class="founder-info">
              <p>A savvy marketer having worked across a variety of Industry verticals and companies, Charu’s last stint in a supply chain services company reinforced in her the need to get knowledge accessible to all through good content.</p>
              <p>When she is not disrupting at work, she disrupts her work by going for high altitude treks in the Himalayas.</p>
            </div>            
          </div>
        </div>
      </div> 
      <div class="col-12 col-sm-12 col-md-6">
        <div class="founders-wrapper">
          <img src="images/prerna-lodaya.png" alt="" class="img-fluid">
          <div class="founder-content">
            <p class="founder-role">Content Head</p>
            <h3>Prerna Lodaya <a href="#/"><img src="images/linkedin-about.svg" alt=""></a></h3>
            <div class="founder-info">
              <p>A cumulative experience of over 13 years in B2B journalism has been an enriching journey of continuous learning for me as a professional and an individual. I believe my biggest asset has been my networking skills, which helps and pushes me to drive and bring newer realms of content for the magazine.</p>
              <p>For me, Celerity has been more than just a profession as it was born with a  lot of passion and zeal to create a niche in the supply chain world.</p>
            </div>
          </div>
        </div>
      </div> 
      <div class="col-12">        
        <h3 class="our-advisors-title">Our Advisors </h3>
        <div class="advisors-wrapper">
          <div class="advisor-single">
            <img src="images/himanshu-mangloo.png" alt="">
            <h4>Himanshu Mangloo</h4>
            <p class="advisor-designation">Director-Supply Chain, J&J Pharma (Janssen) India</p>
          </div>
          <div class="advisor-single">
            <img src="images/rajat-sharma.png" alt="">
            <h4>Rajat Sharma</h4>
            <p class="advisor-designation">Head-SCM, Hamilton Housewares</p>
          </div>
          <div class="advisor-single">
            <img src="images/sanjay-desai.png" alt="">
            <h4>Sanjay Desai</h4>
            <p class="advisor-designation">Advisory Consulting</p>
          </div>
          <div class="advisor-single">
            <img src="images/umesh-madhyan.png" alt="">
            <h4>Umesh Madhyan</h4>
            <p class="advisor-designation">Associate Vice President-Logistics, Hindustan Coca-Cola Beverages</p>
          </div>
          <div class="advisor-single">
            <img src="images/vickram-srivastava.png" alt="">
            <h4>Vickram Srivastava</h4>
            <p class="advisor-designation">Head of Supply Chain Planning, Ipca Laboratories</p>
          </div>
        </div>
      </div>  
    </div>   
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>
</html>