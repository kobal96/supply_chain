<?php require_once('include/head.php') ?>
<?php require_once('include/header.php') ?>
<section class="page-section">  
  <div class="billing-bg"></div>
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 offset-lg-2 col-lg-8">
        <div class="billing-wrapper mt-60 mb-60 mtmob-40 mtmob-40">
          <div class="billing-title">
            <h1>Select a package</h1>
          </div>
          <div class="billing-plan-wrapper">
            <div class="billing-plan-single">
              <h3>₹500</h3>
              <p class="billing-duration">for 6 months</p>
            </div>
            <div class="billing-plan-single most-popular">
              <p class="most-popular-title">Most popular</p>
              <h3>₹850</h3>
              <p class="billing-duration">for 12 months</p>
            </div>
            <div class="billing-plan-single">
              <h3>₹1500</h3>
              <p class="billing-duration">for 24 months</p>
            </div>
          </div>
          <div class="billing-info-wrapper">
            <h3>Billing information</h3>
            <form class="billing-form">
            <div class="row">
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">First Name</label>
                  <input type="text" class="form-control" id="" placeholder="eg.John">                  
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">Last Name</label>
                  <input type="text" class="form-control" id="" placeholder="eg. Raskin">                  
                </div>
              </div>
              <div class="col-12 col-sm-12">
                <div class="form-group">
                  <label for="">Billing Address</label>
                 <textarea name="" id="" cols="30" rows="4" class="form-control" placeholder="eg. 50  & , Hazari Baugh,  Th Rd, Jvpd Scheme, Opp Uco Bank, Juhu"></textarea>               
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">Country</label>
                  <select name="" id="" class="niceselect form-group">
                    <option value="Australia">Australia</option> 
                    <option value="India">India</option>
                    <option value="United Kingdom">United Kingdom</option>
                    <option value="USA">USA</option>                                       
                  </select>                   
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">State</label>
                  <select name="" id="" class="niceselect form-group">
                    <option value="Maharashtra">Maharashtra</option>
                    <option value="Karnataka">Karnataka</option>
                    <option value="Tamil Nadu">Tamil Nadu</option>
                    <option value="Kerala">Kerala</option>                    
                  </select>     
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">City</label>
                  <select name="" id="" class="niceselect form-group">
                    <option value="Mumbai">Mumbai</option>
                    <option value="Bangalore">Bangalore</option>
                    <option value="Chennai">Chennai</option>
                    <option value="Kolkata">Kolkata</option>                    
                  </select>                   
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-6">
                <div class="form-group">
                  <label for="">ZIP / PIN</label>
                  <input type="text" class="form-control" id="" placeholder="Enter ZIP/ PIN Code">                  
                </div>
              </div>
            </div>  
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">I accept <a href="#/">Terms & Condition</a></label>
            </div>
            <button type="submit" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase">Proceed to payment</button>
            <button type="submit" class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase">Cancel</button>
            </form>
          </div>
        </div>
      </div>      
    </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>