<footer>
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-3">
					<a href="index.php"><img src="images/logo.png" alt="" class="img-fluid"></a>
				</div>
				<div class="col-12 col-sm-12 col-md-3">
					<div class="footer-section footer-address">					
						<p>1505, Darshan Heights,</p>
						<p>Opp Deepak Cinema, Off Tulsipipe Road,</p>
						<p>Elphinstone West, Mumbai</p>
						<p>400013 Maharashtra, India</p>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-3">
					<div class="footer-section get-in-touch">
						<h5 class="footer-title hidden-mobile">Get in touch</h5>
						<div class="contact-detail phone">
							<a href="tel:+917977105913">
								<img src="images/phone-call.svg" alt="">
								<span>+91 7977105913</span>
							</a>
						</div>
						<div class="contact-detail">
							<a href="mailto:tech@celerityin.com">
								<img src="images/mail.svg" alt="">
								<span>tech@celerityin.com</span>
							</a>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-3">
					<div class="footer-section newsletter">
						<h5 class="footer-title mb-10">Sign up for newsletter</h5>
						<p>Sign up for weekly emails on latest supplychain trends.</p>
						<form>
							<input type="text" id="newsletter-email" class="newsletter-email" placeholder="eg. john@gmail.com">
							<input type="submit" class="btn-celerity btn-small btn-blue" value="Sign Up">
						</form>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="footer-links">
						<a href="index.php">Home</a>
						<a href="news-events.php">News & Events</a>
						<a href="#/">Awards</a>
						<a href="aboutus.php">About us</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
				<div class="copyright-wrapper">
					<div class="copyright-text">
						<span>Copyright © Celerity India Marketing Services</span>
						<a href="#/" class="disclaimer">Disclaimer</a>
						<a href="#/" class="privacy">Privacy Policy</a>
					</div>
					<div class="social-footer">
						<a href="#/"><img src="images/facebook.svg" alt=""></a>
						<a href="#/"><img src="images/twitter.svg" alt=""></a>
						<a href="#/"><img src="images/linkedin.svg" alt=""></a>
					</div>
				</div>
				</div>
			</div>
		</div>
	</footer>