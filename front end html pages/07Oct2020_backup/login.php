<?php require_once('include/head.php') ?>
<?php require_once('include/header.php') ?>
<section class="page-section mt-60 mb-60 mtmob-40 mbmob-20">  
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="login-wrapper">
          <h1 class="section-title-medium">Login to your account</h1>
          <div class="social-login">
            <a href="#/"><img src="images/google-login.svg" alt=""> Continue with Google</a>
            <a href="#/"><img src="images/linkedin-login.svg" alt=""> Continue with Linkedin</a>
          </div>
          <p class="or"><span>or</span></p>
          <form class="billing-form login-form" id="celerity-login">
            <div class="form-group">
              <label for="">Email address</label>
              <input type="email" class="form-control" id="" placeholder="eg. john-clack@gmail.com">
            </div>
            <div class="form-group">
              <label for="">Password</label>
              <input type="password" class="form-control" id="" placeholder="6+ Characters">
            </div>            
            <div class="form-check text-left">
              <div>
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Remember me</label>
              </div>
              <div>
                <label class="form-check-label">
                  <a href="#/" class="fp-link">Forgot your password?</a>
                </label>                
              </div>
            </div>
            <div class="form-group">              
              <input type="submit" class="btn-celerity btn-large btn-blue btn-full text-uppercase" id=""value="Login">
            </div>
            <div class="form-group"> 
              <div class="create-account-link-wrapper">
                <p><span>Don’t have an account?</span> <a href="#/" class="signup-link">Create one</a></p>
              </div>              
            </div>
          </form>
          <form class="billing-form login-form" id="celerity-signup">
            <div class="form-group">
              <label for="">Email address</label>
              <input type="email" class="form-control" id="" placeholder="eg. john-clack@gmail.com">
            </div>
            <div class="form-group">
              <label for="">Password</label>
              <input type="password" class="form-control" id="" placeholder="eg. jo@cl@acH">
            </div>
            <div class="form-group">
              <label for="">Confirm Password</label>
              <input type="password" class="form-control" id="" placeholder="6+ Characters">
            </div>
            <div class="form-group">              
              <input type="submit" class="btn-celerity btn-large btn-blue btn-full text-uppercase" id=""value="Sign Up">
            </div>
            <div class="form-group"> 
              <div class="create-account-link-wrapper">
                <p><span>Already have an account?</span> <a href="#/" class="login-link">Log In</a></p>
              </div>              
            </div>
          </form>
          <form class="billing-form login-form" id="celerity-forgotpassword">
            <div class="form-group">
              <label for="">Email address</label>
              <input type="email" class="form-control" id="" placeholder="eg. john-clack@gmail.com">
            </div> 
            <div class="form-group">
              <label for="">New Password</label>
              <input type="password" class="form-control" id="" placeholder="eg. jo@cl@acH">
            </div>
            <div class="form-group">
              <label for="">Confirm Password</label>
              <input type="password" class="form-control" id="" placeholder="6+ Characters">
            </div>           
            <div class="form-group">              
              <input type="submit" class="btn-celerity btn-large btn-blue btn-full text-uppercase" id=""value="Reset Password">
            </div>
            <div class="form-group"> 
              <div class="create-account-link-wrapper">
                <p><a href="#/" class="login-link">Back to Log In</a></p>
              </div>              
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>