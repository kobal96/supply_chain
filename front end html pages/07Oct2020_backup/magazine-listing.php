<?php require_once('include/head.php') ?>
<?php require_once('include/header.php') ?>
<section class="page-section mt-60 mtmob-30">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-6">
        <h1 class="page-title-large magazine-title">Magazine Editions</h1>
      </div>
      <div class="col-12 col-sm-12 col-md-6">
        <div class="category-search magazine-search">
          <form>
            <input type="text" class="category-search-text" placeholder="Search">
          </form>
        </div>
      </div>
      <div class="col-12">
        <div class="magazine-title-seperator"></div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-60 mb-60 mtmob-40 mbmob-40">
  <div class="container">
    <div class="row">
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/celerity-jan-feb.png" alt="" class="img-fluid">
            <h4>July-Aug, 2020</h4>
          </a>
        </div>
      </div>
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/celerity-july-august.png" alt="" class="img-fluid">
            <h4>May - Jun, 2020</h4>
          </a>
        </div>
      </div>
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/celerity-march-april-2020.png" alt="" class="img-fluid">
            <h4>Mar - Apr, 2020</h4>
          </a>
        </div>
      </div>
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/may-june-2020.png" alt="" class="img-fluid">
            <h4>Jan - Feb, 2020</h4>
          </a>
        </div>
      </div>
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/celerity-jan-feb.png" alt="" class="img-fluid">
            <h4>July-Aug, 2020</h4>
          </a>
        </div>
      </div>
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/celerity-july-august.png" alt="" class="img-fluid">
            <h4>May - Jun, 2020</h4>
          </a>
        </div>
      </div>
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/celerity-march-april-2020.png" alt="" class="img-fluid">
            <h4>Mar - Apr, 2020</h4>
          </a>
        </div>
      </div>
      <div class="col-6 col-sm-3">
        <div class="home-magazine-single magazine-list-single">
          <a href="#/">
            <img src="images/may-june-2020.png" alt="" class="img-fluid">
            <h4>Jan - Feb, 2020</h4>
          </a>
        </div>
      </div>      
    </div>
    <div class="row">
      <div class="col-12">
        <div>
          <a href="#"><img src="images/home-banner-2.png" alt="" class="img-fluid m-auto d-none d-sm-none d-md-block"></a>
          <a href="#"><img src="images/home-banner-1.png" alt="" class="img-fluid m-auto d-block d-sm-block d-md-none"></a>
        </div>
      </div>
    </div>    
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>