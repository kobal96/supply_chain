<?php require_once('include/head.php') ?>
<?php require_once('include/header.php') ?>
<section class="page-section">
  <div class="container">
    <div class="row">
      <!-- Mobile Profile Links -->
      <div class="col-12 col-sm-12 d-md-block col-md-12 d-lg-none d-xl-none nopadding-mobile">
        <div class="profile-tabs-wrapper profile-tabs-wrapper-mobile mt-30">
          <a href="#/" class="profile-link-mobile active my-profile-link-mobile">My Profile <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile my-articles-link-mobile">My Articles <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile my-bookmarks-link-mobile">My Bookmarks <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile my-notifications-link-mobile">Notification <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="#/" class="profile-link-mobile my-settings-link-mobile">Settings <img src="images/profile-mobile-arrow.svg" alt=""></a>
          <a href="index.php">Logout</a>
        </div>
        <div class="profile-tabs-menu">
          <a href="#/" class="close-submenu-mobile">
            <img src="images/down-menu-mobile.svg" alt="">
          </a>
          <div class="profile-name">
            <img src="images/comments-avatar.png" alt="">
            <div>
              <p class="user-name">Hi, Samuel</p>
              <p class="user-email">samuel_32@gmail.com</p>
            </div>
          </div>
          <div class="profile-links-wrapper">
            <a href="#/" id="my-profile">My Profile</a>
            <a href="#/" id="my-articles">My Articles</a>
            <a href="#/" id="my-bookmarks">My Bookmarks</a>
            <a href="#/" id="my-notifications">Notifications</a>
            <a href="#/" id="my-settings">Settings</a>
            <span class="profile-logout-seperator"></span>
            <a href="index.php" id="profile-logout">Logout</a>
          </div>
        </div>
      </div>
      <!-- Mobile Profile Links -->
      <div class="d-none d-sm-none d-md-none d-lg-block col-lg-3 pr-0">
        <div class="profile-tabs-wrapper mt-30">
          <a href="#/" id="my-profile" class="active">My Profile</a>
          <a href="#/" id="my-articles">My Articles</a>
          <a href="#/" id="my-bookmarks">My Bookmarks</a>
          <a href="#/" id="my-notifications">Notification</a>
          <a href="#/" id="my-settings">Settings</a>
          <a href="index.php" id="profile-logout">Logout</a>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-9 profile-content-border">
        <div class="profile-content-tab" id="my-profile-content">
          <div class="profile-content-wrapper">
            <form class="billing-form">
              <h2 class="profile-section-title">Personal Info</h2>
              <div class="row mb-20">
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">First Name</label>
                    <input type="text" class="form-control" id="" placeholder="eg.John">
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">Last Name</label>
                    <input type="text" class="form-control" id="" placeholder="eg. Raskin">
                  </div>
                </div>
              </div>
              <h2 class="profile-section-title">Billing Info</h2>
              <div class="row mb-20">
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="">Billing Address</label>
                    <textarea name="" id="" cols="30" rows="4" class="form-control" placeholder="eg. 50  & , Hazari Baugh,  Th Rd, Jvpd Scheme, Opp Uco Bank, Juhu"></textarea>
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">Country</label>
                    <select name="" id="" class="niceselect form-group">
                      <option value="Australia">Australia</option>
                      <option value="India">India</option>
                      <option value="United Kingdom">United Kingdom</option>
                      <option value="USA">USA</option>
                    </select>
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">State</label>
                    <select name="" id="" class="niceselect form-group">
                      <option value="Maharashtra">Maharashtra</option>
                      <option value="Karnataka">Karnataka</option>
                      <option value="Tamil Nadu">Tamil Nadu</option>
                      <option value="Kerala">Kerala</option>
                    </select>
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">City</label>
                    <select name="" id="" class="niceselect form-group">
                      <option value="Mumbai">Mumbai</option>
                      <option value="Bangalore">Bangalore</option>
                      <option value="Chennai">Chennai</option>
                      <option value="Kolkata">Kolkata</option>
                    </select>
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">ZIP / PIN</label>
                    <input type="text" class="form-control" id="" placeholder="Enter ZIP/ PIN Code">
                  </div>
                </div>
              </div>
              <h2 class="profile-section-title">My Subscription</h2>
              <div class="row mb-20">
                <div class="col-12">
                  <div class="my-subscription-wrapper">
                    <div class="msw-text">
                      <p class="active-plan">Active Plan</p>
                      <p class="active-plan-amt">₹850 <span>/ for 12 months</span></p>
                    </div>
                    <a href="#/" class="renew-btn">Renew</a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <button type="submit" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase">Save</button>
                  <button type="submit" class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase">Cancel</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="profile-content-tab" id="my-articles-content">
          <h2 class="profile-section-title">My Articles</h2>
          <div class="articles-content-wrapper">
            <div class="articles-tab-wrapper">
              <div class="article-tab-links">
                <a href="#/" class="active" id="articles-all">All</a>
                <a href="#/" id="articles-published">Published</a>
                <a href="#/" id="articles-draft">Drafts</a>
              </div>
            </div>
            <div class="articles-list-wrapper" id="articles-all-content">
              <a href="#/" class="btn-celerity btn-small btn-blue btn-new-post"><img src="images/add-new-post.svg" alt=""> New Post</a>
              <div class="article-list">
                <div class="myarticle-single">
                  <a href="#/">
                    <div class="mas-image">
                      <img src="images/home-spotlight-single.png" alt="">
                    </div>
                    <div class="article-block-content">
                      <p class="category-tag">Pharma and Chemical All</p>
                      <h4>Shifting Dynamics - Vaccination Preparedness And Food Supply</h4>
                      <p class="mas-intro">A good supply chain is demand driven; right from provisioning the inputs, to support production, and to distribute the product to final buyer</p>
                      <div class="article-author-info">
                        <p>
                          <span class="author-name">Last Updated On</span> May 02, 2020</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div class="myarticle-single">
                  <a href="#/">
                    <div class="mas-image">
                      <img src="images/home-spotlight-single.png" alt="">
                    </div>
                    <div class="article-block-content">
                      <p class="category-tag">Supplychain</p>
                      <h4>A Generational Opportunity - Redesign of Supply Chains</h4>
                      <p class="mas-intro">A good supply chain is demand driven; right from provisioning the inputs, to support production, and to distribute the product to final buyer</p>
                      <div class="article-author-info">
                        <p>
                          <span class="author-name">Last Updated On</span> May 02, 2020</p>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <div class="articles-list-wrapper" id="articles-published-content">
              <div class="article-list">
                <div class="myarticle-single">
                  <a href="#/">
                    <div class="mas-image">
                      <img src="images/home-spotlight-single.png" alt="">
                    </div>
                    <div class="article-block-content">
                      <p class="category-tag">Pharma and Chemical Published</p>
                      <h4>Shifting Dynamics - Vaccination Preparedness And Food Supply</h4>
                      <p class="mas-intro">A good supply chain is demand driven; right from provisioning the inputs, to support production, and to distribute the product to final buyer</p>
                      <div class="article-author-info">
                        <p>
                          <span class="author-name">Last Updated On</span> May 02, 2020</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div class="myarticle-single">
                  <a href="#/">
                    <div class="mas-image">
                      <img src="images/home-spotlight-single.png" alt="">
                    </div>
                    <div class="article-block-content">
                      <p class="category-tag">Supplychain</p>
                      <h4>A Generational Opportunity - Redesign of Supply Chains</h4>
                      <p class="mas-intro">A good supply chain is demand driven; right from provisioning the inputs, to support production, and to distribute the product to final buyer</p>
                      <div class="article-author-info">
                        <p>
                          <span class="author-name">Last Updated On</span> May 02, 2020</p>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <div class="articles-list-wrapper" id="articles-draft-content">
              <a href="#/" class="btn-celerity btn-small btn-red btn-delete-post"><img src="images/delete-article.svg" alt=""> Delete</a>
              <a href="#/" class="btn-celerity btn-small btn-blue-inv btn-md-blue-inv btn-cancel-post">Cancel</a>
              <div class="article-list">
                <div class="myarticle-single">
                  <a href="#/">
                    <div class="draft-checkbox">
                      <input type="checkbox">
                    </div>
                    <div class="mas-image">
                      <img src="images/home-spotlight-single.png" alt="">
                    </div>
                    <div class="article-block-content">
                      <p class="category-tag">Pharma and Chemical Draft</p>
                      <h4>Shifting Dynamics - Vaccination Preparedness And Food Supply</h4>
                      <p class="mas-intro">A good supply chain is demand driven; right from provisioning the inputs, to support production, and to distribute the product to final buyer</p>
                      <div class="article-author-info">
                        <p>
                          <span class="author-name">Last Updated On</span> May 02, 2020</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div class="myarticle-single">
                  <a href="#/">
                    <div class="draft-checkbox">
                      <input type="checkbox">
                    </div>
                    <div class="mas-image">
                      <img src="images/home-spotlight-single.png" alt="">
                    </div>
                    <div class="article-block-content">
                      <p class="category-tag">Supplychain</p>
                      <h4>A Generational Opportunity - Redesign of Supply Chains</h4>
                      <p class="mas-intro">A good supply chain is demand driven; right from provisioning the inputs, to support production, and to distribute the product to final buyer</p>
                      <div class="article-author-info">
                        <p>
                          <span class="author-name">Last Updated On</span> May 02, 2020</p>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="profile-content-tab" id="new-article-content">
          <form class="billing-form article-form">
            <div id="article-step-1">
              <p class="category-tag article-step">Step 1 of 2</p>
              <h2 class="profile-section-title">
                <a href="#/" id="new-article-back"><img src="images/md-back.svg" alt=""></a>
                New Article
              </h2>
              <div class="row mb-20">
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="">Title</label>
                    <input type="text" class="form-control" id="" placeholder="eg. Vaccination Preparedness And Food Supply">
                  </div>
                </div>
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="">Select Category</label>
                    <select name="" id="" class="niceselect form-group">
                      <option value="">Select</option>
                      <option value="Pharma">Pharma</option>
                      <option value="Supply Chain">Supply Chain</option>
                    </select>
                  </div>
                </div>
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="">Tags</label>
                    <input type="text" class="form-control" id="" placeholder="Select Tags">
                  </div>
                </div>
                <div class="col-12 col-sm-12">
                  <div class="form-group">
                    <label for="">Cover Image (800px x 400px)</label>
                    <input type="file">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <button type="submit" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase proceed-article">Proceed</button>
                  <button type="submit" class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase cancel-article">Cancel</button>
                </div>
              </div>
            </div>
            <div id="article-step-2">
              <p class="category-tag article-step">Step 2 of 2</p>
              <h2 class="profile-section-title">
                <div class="article-compose-title">
                  <a href="#/" id="new-article-back"><img src="images/md-back.svg" alt=""></a>
                  <span>Title of the new article will be displayed here in this style </span>
                </div>
                <div class="article-compose-actions">
                  <button type="submit" class="btn-celerity btn-small btn-blue btn-md-blue text-uppercase">Send for Approval</button>
                  <button type="submit" class="btn-celerity btn-small btn-blue-inv btn-md-blue-inv text-uppercase cancel-article">Cancel</button>
                </div>
              </h2>
              <div class="row mt-40">
                <div class="col-12 col-sm-12">
                  <div class="form-group compose-article-content">
                    <img src="images/article-add.svg" alt="" class="article-add-icon">
                    <textarea name="" id="" cols="30" rows="10" class="compose-article" placeholder="Tell your story…"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="profile-content-tab" id="my-bookmarks-content">
          <h2 class="profile-section-title pst-border">My Bookmarks</h2>
          <div class="articles-list-wrapper bookmark-articles" id="articles-draft-content">
            <a href="#/" class="btn-celerity btn-small btn-red btn-delete-post delete-bookmark"><img src="images/delete-article.svg" alt=""> Delete</a>
            <a href="#/" class="btn-celerity btn-small btn-blue-inv btn-md-blue-inv btn-cancel-post cancel-bookmark">Cancel</a>
            <div class="article-list">
              <div class="myarticle-single">
                <a href="#/">
                  <div class="bookmark-checkbox">
                    <input type="checkbox">
                  </div>
                  <div class="mas-image">
                    <img src="images/home-spotlight-single.png" alt="">
                  </div>
                  <div class="article-block-content">
                    <p class="category-tag">Pharma and Chemical Draft</p>
                    <h4>Shifting Dynamics - Vaccination Preparedness And Food Supply</h4>
                    <p class="mas-intro">A good supply chain is demand driven; right from provisioning the inputs, to support production, and to distribute the product to final buyer</p>
                    <div class="article-author-info">
                      <p>
                        <span class="author-name">Last Updated On</span> May 02, 2020</p>
                    </div>
                  </div>
                </a>
              </div>
              <div class="myarticle-single">
                <a href="#/">
                  <div class="bookmark-checkbox">
                    <input type="checkbox">
                  </div>
                  <div class="mas-image">
                    <img src="images/home-spotlight-single.png" alt="">
                  </div>
                  <div class="article-block-content">
                    <p class="category-tag">Supplychain</p>
                    <h4>A Generational Opportunity - Redesign of Supply Chains</h4>
                    <p class="mas-intro">A good supply chain is demand driven; right from provisioning the inputs, to support production, and to distribute the product to final buyer</p>
                    <div class="article-author-info">
                      <p>
                        <span class="author-name">Last Updated On</span> May 02, 2020</p>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="profile-content-tab" id="my-notifications-content">
          <div class="row">
            <div class="col-12 col-sm-12 col-md-6">
              <h2 class="profile-section-title">Notifications</h2>
            </div>
            <div class="col-12 col-sm-12 col-md-6">
              <div class="category-search notifications-search">
                <form>
                  <input type="text" class="category-search-text" id="notifications-search" placeholder="Search">
                </form>
              </div>
            </div>
            <div class="col-12">
              <div class="magazine-title-seperator"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="category-filter-wrapper notifications-filter-wrapper">
                <div class="category-sortby">
                  <span class="filter-label">Sort By</span>
                  <select name="" id="" class="niceselect">
                    <option value="Recent">Recent</option>
                    <option value="Most Viewed">Most Viewed</option>
                    <option value="Most Commented">Most Commented</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="notification-list-wrapper">
                <div class="notification-single">
                  <div class="notification-icon">
                    <img src="images/notification-comment.svg" alt="">
                  </div>
                  <div class="notification-content">
                    <div class="notification-icon-mobile">
                      <img src="images/notification-comment.svg" alt="">
                    </div>
                    <div class="notification-name-date">
                      <p class="notification-name">Monty Hayton</p>
                      <p class="notification-date">Mar 13, 2020</p>
                    </div>
                    <div class="notification-post-name">
                      <p>comment on <span>Shifting Dynamics - Vaccination Preparedness And Food Supply</span></p>
                    </div>
                    <div class="notification-post-comment">
                      <p>"Thanks for the great work on this series.
                        A quick query on the ledger and world state definitions. Could you please describe more on Ledger contains the current world state of the network and a chain of describe more on Ledger contains the current world state of the network and a chain of. Thanks for the great work on this series.
                        A quick query on the ledger and world state definitions. Could you please describe more on Ledger contains the current world state of the network and a chain of describe more on Ledger contains the current world state of the network and a chain of"</p>
                      <a href="#/" class="read-more">Read more</a>
                    </div>
                    <div class="comment-single">
                      <div class="comment-like">
                        <a href="#/"><img src="images/comment-like.svg" alt=""> Like</a>
                        <a href="#/"><img src="images/comment-reply.svg" alt=""> Comment</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="notification-single">
                  <div class="notification-icon">
                    <img src="images/notification-article.svg" alt="">
                  </div>
                  <div class="notification-content">
                    <div class="notification-icon-mobile">
                      <img src="images/notification-article.svg" alt="">
                    </div>
                    <div class="notification-name-date">
                      <p class="notification-name">Article approved</p>
                      <p class="notification-date">Mar 13, 2020</p>
                    </div>
                    <div class="notification-post-comment">
                      <p>Your article on A Generational Opportunity - Redesign of Supply Chains is approved by celerity team and is live on the website. .</p>
                      <a href="#/" class="view-article">
                        Click here to view
                      </a>
                    </div>
                  </div>
                </div>
                <div class="notification-single">
                  <div class="notification-icon">
                    <img src="images/notification-comment.svg" alt="">
                  </div>
                  <div class="notification-content">
                    <div class="notification-icon-mobile">
                      <img src="images/notification-article.svg" alt="">
                    </div>
                    <div class="notification-name-date">
                      <p class="notification-name">Monty Hayton</p>
                      <p class="notification-date">Mar 13, 2020</p>
                    </div>
                    <div class="notification-post-name">
                      <p>comment on <span>Shifting Dynamics - Vaccination Preparedness And Food Supply</span></p>
                    </div>
                    <div class="notification-post-comment">
                      <p>"Thanks for the great work on this series.
                        A quick query on the ledger and world state definitions. Could you please describe more on Ledger contains the current world state of the network and a chain of describe more on Ledger contains the current world state of the network and a chain of. Thanks for the great work on this series.
                        A quick query on the ledger and world state definitions. Could you please describe more on Ledger contains the current world state of the network and a chain of describe more on Ledger contains the current world state of the network and a chain of"</p>
                      <a href="#/" class="read-more">Read more</a>
                    </div>
                    <div class="comment-single">
                      <div class="comment-like">
                        <a href="#/"><img src="images/comment-like.svg" alt=""> Like</a>
                        <a href="#/"><img src="images/comment-reply.svg" alt=""> Comment</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="profile-content-tab" id="my-settings-content">
          <div class="profile-content-wrapper">
            <form class="billing-form">
              <h2 class="profile-section-title">Settings</h2>
              <div class="row mb-20">
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" class="form-control" id="" placeholder="samuel_massey@gmail.com">
                  </div>
                </div>
              </div>
              <h2 class="profile-section-title">Change Password</h2>
              <div class="row mb-20">
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">Current Password</label>
                    <input type="text" class="form-control" id="" placeholder="Enter current password">
                  </div>
                </div>
              </div>
              <div class="row mb-20">
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">New Password</label>
                    <input type="text" class="form-control" id="" placeholder="Enter New password">
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                  <div class="form-group">
                    <label for="">Confirm Password</label>
                    <input type="text" class="form-control" id="" placeholder="Enter New password">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <button type="submit" class="btn-celerity btn-large btn-blue btn-md-blue text-uppercase">Save</button>
                  <button type="submit" class="btn-celerity btn-large btn-blue-inv btn-md-blue-inv text-uppercase">Cancel</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>