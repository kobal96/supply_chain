<?php require_once('include/head.php') ?>
<style>
@media (max-width:767px){
  #slider {
    top: -73px;  
  }
}
</style>
<div class="quicknav-header">
  <a href="#/">
    <p class="quicknav-article-title">
      <img src="images/back-header.svg" alt="">
      <span>Big Bang Transformation or Incremental Approach? Big Bang Transformation or Incremental Approach? Big Bang Transformation or Incremental Approach? Big Bang Transformation or Incremental Approach?</span>
    </p>
  </a>
  <div class="quicknav-share">
    <a href="#/"><img src="images/bookmark-header.svg" alt=""></a>
    <a href="#/"><img src="images/share-header.svg" alt=""></a>
  </div>
</div>
<div class="quicknav-header-clear"></div>
<?php require_once('include/header.php') ?>
<section class="page-section mt-60 mtmob-40">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5">
        <div class="article-detail-title">
          <p class="category-tag">Pharma and Chemical</p>
          <h1>Big Bang Transformation or Incremental Approach?</h1>
          <div class="article-author-info">
            <p>In the <span class="author-name">January - February 2020 Edition</span></p>
            <p>by Celerity Editorial Team</p>
          </div>
        </div>
        <div class="article-detail-share">
          <a href="#/"><img src="images/facebook-ad.svg" alt=""></a>
          <a href="#/"><img src="images/twitter-ad.svg" alt=""></a>
          <a href="#/"><img src="images/linkedin-ad.svg" alt=""></a>
          <a href="#/"><img src="images/email-ad.svg" alt=""></a>
          <a href="#/"><img src="images/link-ad.svg" alt=""></a>
        </div>
      </div>
      <div class="d-none d-lg-none d-xl-block col-xl-1"></div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <img src="images/article-detail-banner.png" alt="" class="img-fluid article-detail-banner">
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="article-detail-banner-full text-center">
          <a href="#"><img src="images/home-banner-2.png" alt="" class="img-fluid m-auto d-none d-sm-none d-md-block"></a>
          <a href="#"><img src="images/home-banner-1.png" alt="" class="img-fluid m-auto d-block d-sm-block d-md-none"></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="d-none d-sm-none d-md-block col-md-3">
        <p class="category-tag">In Spotlight</p>
        <ul class="spotlight">
          <li><a href="#/">A Generational Opportunity - Redesign of Supply Chains </a></li>
          <li><a href="#/">Procurement Trends of Today, Goals of Tomorrow</a></li>
          <li><a href="#/">Consumer Electronics - Delivering Service Satisfaction</a></li>
        </ul>
        <div class="article-detail-add-1">
          <img src="images/article-detail-add-1.png" alt="" class="img-fluid">
        </div>
      </div>
      <div class="d-sm-none d-md-block col-md-1"></div>
      <div class="col-12 col-sm-12 col-md-8">
        <div class="article-detail-introduction">
          <p>SPEED is what defines the growing proliferation of automation in supply chain in India and globally as well. The word, as smartly coined by Dheeraj Shah, Head – Supply Chain Management – Retail Business Group, TCS, aptly connotes the essence of what automation delivers to companies going for automation as it stands for S – Speed; P – Productivity; E – Efficiency; E – Excellence; and D – Differentiation. Having said that, experts have rightly pointed out that supply chain automation needs to be approached with focus, commitment to achieve scale, and with a long-term and enterprise-wide strategy if they want to get best RoI. This Cover Story delves into this very aspect and questions the very fundamental if there needs to be big bang transformation or an incremental approach for companies going for automation.</p>
        </div>
        <div class="article-detail-content">
          <p>The Global Logistics Automation Market is forecast to reach US$120.08 billion by 2026, according to a new report by Reports and Data. The logistics automation market is rising rapidly in the global market owing to the proliferation of the adoption rate of automation in the logistics chains globally. The convenience of lesser human effort and enhanced overall operational improvement is fueling up the market growth of logistics automation.</p>
          <p>McKinsey Global Institute estimates that the transportation-and-warehousing industry has the third-highest automation potential of any sector. Similarly, Capgemini Global Digital Supply Chain survey found that automation initiatives in procurement and supply chain functions deliver the highest returns as compared to any other function. Automation delivers an ROI in the supply chain of 18%, three percentage points more than for HR and four percentage points more than for IT. For other functions, including finance and accounting, research and development, and customer service, the difference was even greater.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-12 offset-md-2 col-md-10">
        <div class="article-detail-img-block">
          <div class="image-section">
            <img src="images/rupesh.png" alt="">
            <p class="img-caption"><span>Rupesh Narkar, Director</span>– Sales, Swisslog Logistics, Inc</p>
          </div>
          <div class="article-detail-content">
            <p>Towards this, Rupesh Narkar, Director – Sales, Swisslog Logistics, Inc., shares, “With the onset of warehouse space consolidation, Indian warehousing industry is witnessing rapid emergence of large warehousing spaces. Larger spaces are driving the need for efficient warehousing operations. With a smaller setup, operating efficiencies and costs were not a priority since the loss or impact of inefficiencies was bearable. As per the research by Knight and Frank, the future will see reduced needs to carry inventories thus substantially reducing number of small warehouses. With the progressing of this trend, warehouse operators are aiming on reaching higher capacities in terms of better space utilization, higher productivity in terms of maximum goods inbound/outbound and improved efficiency in terms of better resource utilization.”</p>
          </div>
        </div>
        <div class="article-detail-testimonial">
          <p>“The other factor gaining prominence is to gain a competitive edge in market. With the ever-increasing number of warehouse operators entering markets each day, customers today have a wide choice to shop the most economical warehousing and fulfillment vendor. For the warehouse operators, their existing and new customer are demanding faster fulfillment times, competitive rate contracts and error free operations. This is pushing them (mostly running manual operations) to rethinking their ability to extract full capacity and efficiency out of the current setup. This is where automation is helping in providing a great solution meeting all these needs,” </p>
          <p class="article-detail-testiomonial-name">
            - <span>Rupesh Narkar, Director</span> – Sales, Swisslog Logistics, Inc
          </p>
        </div>
        <div class="article-detail-img-block">
          <div class="image-section">
            <img src="images/rupesh.png" alt="">
            <p class="img-caption"><span>Dheeraj Shah, Head – Supply Chain Management</span>– Retail Business Group, TCS</p>
          </div>
          <div class="article-detail-content">
            <p>Dheeraj Shah, Head – Supply Chain Management – Retail Business Group, TCS, categorizes the drivers of automation in three parts:</p>
            <h3>1. The Need </h3>
            <p>As the customer expectations are getting tremendously more demanding on the supply chain, there is a need to accelerate the whole promise fulfilment proposition for supply chain. "Fast" is the new normal. So, the paradigm in which we are today is “What I want today is what I needed yesterday, however I am not late, but your supply chain is slow!” The other dimension is productivity and efficiency to drive costs down is extremely important with tremendous amount of pressures on balance sheets of organizations. </p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-12 offset-md-4 col-md-8">
        <div class="article-detail-content mt-60 mtmob-40">
          <p>Hence Supply Chain has to end its role of being an onlooker to active driver of a key "value" enhancer through cost reduction for the stakeholders and shareholders of the company.In some cases, the availability of workforce and the variations in learning curves, trade unions, expectations of accuracies and quality as well as dependency on casual and contract labor which makes managing supply chain very difficult is driving the growth of automation. Some of the industry functions in supply chain exhibit characteristics which need very high level of accuracies, quality and repetitiveness for e.g. Healthcare, Automotive, Hi-Tech. The Supply Chain in these industries, therefore, have mandated the need to embrace automation.</p>
          <h3>2. The Aspiration </h3>
          <p>Indian organizations aspire to be globally competitive and demonstrate that their operations, functions and supply chain processes are on par or even better than the global practices. While this might be a qualitative view, the quantitative dimension to this is that the aspirations of growths or efficiencies are exponentially higher and to realize those Supply Chains cannot leverage traditional approaches and hence need to embrace automation or even at times hyper automation. The other key factor is “Differentiation” through “first mover advantage” which organizations want to bring into their Supply Chain.</p>
          <h3>3. The Access and abundance </h3>
          <p>As we say, Supply Chain 4.0 is about harvesting abundance and leveraging the ecosystems. The advent of technology, research in automation and development automation solutions has sizeably increased. With this access and availability, it is easy for Supply Chains to adopt from the plethora of options at their disposal the best that suites their needs and objectives. In addition, in India there is now adequate availability of infrastructure including electric power etc. which makes the business case more palatable over what it was earlier.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-12 offset-md-2 col-md-10">
        <div class="article-detail-img-block">
          <div class="image-section">
            <img src="images/jitender.png" alt="">
            <p class="img-caption"><span>Jitender Lalit, Lead Commercial</span>– Logistics (Lifestyle Business), Raymond Ltd</p>
          </div>
          <div class="article-detail-content">
            <p>Seconding his views, Jitender Lalit, Lead Commercial - Logistics (Lifestyle Business), Raymond Ltd., adds, “Freight Management System (FMS) works closely on principles of Supply Chain 4.0 and has tremendous potential in automating the distribution in supply chain. It breaks the traditional ways in which distribution is currently done. According to him, use of FMS will bring visibility in distribution process, mitigate risks, and reallocate the existing manual freight bills checking resources to various other core tasks. This set of innovative digital technology that it offers could help companies reduce lost sales by 50 – 65% and cut distribution costs by 15 – 30%. Improved planning would make inventory reductions of 35 – 75% possible and supply chain administrative costs could be 80 – 120% lower.”</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-12 offset-md-4 col-md-8">
        <div class="article-detail-content mt-60 mtmob-40">
          <p>Ashok Kumar, Head-Intralogistics Solutions, Godrej & Boyce Mfg. Co. Ltd., informs that the choices available to the customer is a major factor that is influencing automation decisions. The market is highly competitive irrespective of the Industry, and hence the need to provide the client a superior product and delivery experience are proving to be very critical. This necessitates companies to look at various technologies, systems and methods to create a unique value proposition.</p>
          <img src="images/article-detail-banner-bottom.png" alt="" class="img-fluid article-inline-img">
          <h2>Indian Companie’s Receptiveness Towards Automation </h2>
          <p>Logistics companies are intrigued by the potential of automation but wary of the risks. Accordingly, they are investing conservatively. McKinsey research estimates investment in warehouse automation will grow the slowest in logistics, at about 3 – 5% per year to 2025. That’s about half the rate of logistics companies’ customers, such as retail and automotive (6 – 8%) and pharmaceuticals (8 – 10%). Offering insights from equipment manufacturers’ perspective, Mihir Contractor, Senior Vice President, Nilkamal Ltd., asserts that automation should be viewed as a return on investment and not as a cost. Multiple factors are considered while estimating the return on investment in an automation project. Typical savings arise from improving labour productivity, minimizing manual mistakes, & maximizing floor-space utilization.</p>
          <p>Vivek Sarbhai, Ex-Director Customer Service & Logistics, Mondelez Middle East & Africa: Technology enabled supply chain has helped a lot in preparing and withstanding the onslaught of the pandemic. In other words, it has made supply chain highly resilient. With minimal human involvement, we can manage information, physical flow of goods and cash supply chain. Imagine, this pandemic in 2010 or say 2015 – how much we, as a society, not just in India but globally would have struggled. In a way this pandemic opened our eyes that how much supply chain have become prepared already for an inevitable contact less economy of the future. Noteworthy amongst many tech enabled interventions that helped all of us are:</p>
          <ul>
            <li>Unprecedented scale of work from home and e-education</li>
            <li>Track & trace capability to trace patients and quarantine them</li>
            <li>Digital transfer of money directly into the account of people</li>
            <li>More enhanced usage of digital payments</li>
            <li>E-commerce supply chain for essentials</li>
            <li>Aarogya Setu App</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-12 offset-md-4 col-md-8">
        <div class="article-detail-tags article-tags">
          <a href="#/">Automation</a>
          <a href="#/">Tech-Enabled Supply Chain</a>
        </div>
        <div class="article-detail-share-bottom article-detail-share">
          <a href="#/"><img src="images/facebook-ad.svg" alt=""></a>
          <a href="#/"><img src="images/twitter-ad.svg" alt=""></a>
          <a href="#/"><img src="images/linkedin-ad.svg" alt=""></a>
          <a href="#/"><img src="images/email-ad.svg" alt=""></a>
          <a href="#/"><img src="images/link-ad.svg" alt=""></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="article-detail-newsletter-seperator"></div>
      </div>
      <div class="col-12 col-sm-12 col-sm-12 offset-lg-3 col-lg-9">
        <div class="article-detail-newsletter">
          <h4>Join our mailing list</h4>
          <div class="newsletter-form">
            <p>Sign up for weekly emails on latest supplychain trends.</p>
            <form>
              <input type="text" class="newsletter-email" placeholder="Your Email">
            </form>
            <input type="submit" value="Sign me up!" class="newsletter-submit">
          </div>
        </div>
      </div>
      <div class="col-12">
        <div class="article-detail-newsletter-seperator"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="article-detail-banner-full text-center">
          <a href="#"><img src="images/home-banner-2.png" alt="" class="img-fluid m-auto d-none d-sm-none d-md-block"></a>
          <a href="#"><img src="images/home-banner-1.png" alt="" class="img-fluid m-auto d-block d-sm-block d-md-none"></a>
        </div>
      </div>
    </div>
  </div>  
</section>
<section class="page-section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section-title-large related-article-section-title">
          More on Pharma and Chemical
        </h3>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-1.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Pharma and Chemical</p>
              <h4>A Generational Opportunity - Redesign of Supply Chains</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Joshua Mullins</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-2.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Technology</p>
              <h4>The Tech'tonic Shift</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Eliza Miller</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="images/article-detail-similar-3.png" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag">Procurement</p>
              <h4>Procurement Trends of Today, Goals of Tomorrow</h4>
              <div class="article-author-info">
                <p>by <span class="author-name">Raymond Bowers</span></p>
                <p><span class="article-date">31 May 2020</span> <span class="reading-time">12 min read</span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
      <div class="col-12">
        <div class="article-detail-newsletter-seperator"></div>
      </div>
    </div>
  </div>
</section>
<section class="page-section mt-60 mb-60 mtmob-40 mbmob-40">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 offset-md-12 offset-lg-2 col-lg-8">
        <div class="comments-section-wrapper">
          <div class="comments-input">
            <img src="images/comments-avatar.png" alt="">
            <textarea name="" id="" placeholder="Write your comment"></textarea>
            <input type="submit" value="Publish" class="btn-celerity btn-large btn-blue text-uppercase">            
          </div>
          <div class="comment-single">
            <img src="images/comments-avatar.png" alt="" class="comment-avatar">
            <div class="comment-single-content">
              <p class="comment-name">Lewis Watkins</p>
              <p class="comment-date">7 July, 2019</p>
              <p class="comment-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut risus leo, hendrerit eleifend tempus eget, mollis nec mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae quam turpis. Suspendisse eu dapibus nunc. Praesent venenatis, lacus ac maximus ullamcorper, sem odio aliquam mauris, quis sollicitudin lacus magna non sem. Nullam semper quam a tortor laoreet pretium. Etiam quam orci, accumsan ut consectetur vitae, lacinia quis lacus.</p>
              <div class="comment-like">
                <a href="#/"><img src="images/comment-like.svg" alt=""> Like</a>
                <a href="#/"><img src="images/comment-reply.svg" alt=""> Comment</a>
              </div>
            </div>
          </div>
          <div class="comment-single">
            <img src="images/comments-avatar.png" alt="" class="comment-avatar">
            <div class="comment-single-content">
              <p class="comment-name">Mae Oliver</p>
              <p class="comment-date">7 July, 2019</p>
              <p class="comment-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut risus leo, hendrerit eleifend tempus eget, mollis nec mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae quam turpis. Suspendisse eu dapibus nunc.</p>
              <div class="comment-like">
                <a href="#/"><img src="images/comment-like.svg" alt=""> Like</a>
                <a href="#/"><img src="images/comment-reply.svg" alt=""> Comment</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
<script>
  $(document).ready(function(){
    $("body").prognroll();
  });
</script>
</body>

</html>