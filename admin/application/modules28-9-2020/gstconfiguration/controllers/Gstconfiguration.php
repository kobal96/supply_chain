<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gstconfiguration extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('gstconfigurationmodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['gst_data'] = $this->common->getData("tbl_gst_configurations",'*',$condition,"id","desc");
		 /*echo "<pre>";
		 print_r($result);
		 exit;*/
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];
			$condition = "id ='".$id."' ";
			$result['gst_data'] = $this->common->getData("tbl_gst_configurations",'*',$condition);
			$gstconfiguration = $result['gst_data'][0];
		}
		$where = array('status'=>1, 'country_id'=>103);
		$result['origin_state_list'] = $this->common->getData("tbl_states",'id, state_name',$where);

		$where1 = array('status'=>1);
		$result['country_list'] = $this->common->getData("tbl_countries",'id, country_name',$where1);

		$where2 = array('status'=>1, 'country_id'=>$gstconfiguration['country']);
		$result['state_list'] = $this->common->getData("tbl_states",'id, state_name',$where2);

		$where3 = array('city_status'=>1, 'country_id'=>$gstconfiguration['country'], 'state_id'=>$gstconfiguration['state']);
		$result['city_list'] = $this->common->getData("tbl_city",'id, city_name',$where3);

		//echo "<pre>";print_r($result['city_list']);exit;

		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	function removeFile(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = " id = ".$this->input->post('id');
			$exisiting_data =$this->common->getData("tbl_magazine_articles",'*',$condition);
			if(!empty($this->input->post('id')) && $this->input->post('type') == "PDF"){
				if(is_array($exisiting_data) && !empty($exisiting_data[0]['article_pdf']) && file_exists(DOC_ROOT_FRONT."/images/article_pdf/".$exisiting_data[0]['article_pdf'])){
					unlink(DOC_ROOT_FRONT."/images/article_pdf/".$exisiting_data[0]['article_pdf']);
					$data['article_pdf'] = '';
				}
			}else{
				if(is_array($exisiting_data) && !empty($exisiting_data[0]['image']) && file_exists(DOC_ROOT_FRONT."/images/article_image/".$exisiting_data[0]['image'])){
					unlink(DOC_ROOT_FRONT."/images/article_image/".$exisiting_data[0]['image']);
					$data['image'] = '';
				}
			}

			$condition = "id = '".$this->input->post('id')."' ";
			$result = $this->common->updateData("tbl_magazine_articles",$data,$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}	
		}			
	}

	public function submitForm(){
		 //print_r($_POST);
		// print_r($_FILES);
		 //exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "title = '".$this->input->post('title')."' ";
			if($this->input->post('id') && $this->input->post('id') > 0){
				$condition .= " AND  id != ".$this->input->post('id')." ";
			}			
			$check_name = $this->common->getData("tbl_gst_configurations",'*',$condition);

			if(!empty($check_name[0]['id'])){
				echo json_encode(array("success"=>false, 'msg'=>'GST Name Already Present!'));
				exit;
			}

			$data['title'] = $this->input->post('title');
			$data['gstin'] = $this->input->post('gstin');
			$data['origin_state_id'] = $this->input->post('origin_state_id');
			$data['company_phone'] = $this->input->post('company_phone');
			$data['country'] = $this->input->post('country');
			$data['state'] = $this->input->post('state');
			$data['city'] = $this->input->post('city');
			$data['company_pincode'] = $this->input->post('company_pincode');
			$data['company_address'] = $this->input->post('company_address');
			$data['status'] = $this->input->post('package_status');
			$data['admin_status'] = $this->input->post('admin_status');
			$data['updated_at'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('supply_chain_admin')[0]['id'];
			if(!empty($this->input->post('id'))){
				$condition = "id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_gst_configurations",$data,$condition);
				if($result){

					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('supply_chain_admin')[0]['id'];
				$result = $this->common->insertData('tbl_gst_configurations',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/article_image/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		$config1['width'] = 1000;
		$config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/article_image/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
		}
		
}