
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Edition</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('edition') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-edition" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
									<input name="id" id="id" value="<?= (!empty($edition_data[0]['id'])?$edition_data[0]['id']:"") ?>" type="hidden" >
									
									<label for="">Basic Information</label><hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Title">Edition Name</label>
												<input type="text" class="form-control" id="title" name="title" value="<?= (!empty($edition_data[0]['title'])?$edition_data[0]['title']:"") ?>" placeholder="">	
											</div>
										</div>
										
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Volume">Volume</label>
												<input type="text" class="form-control" id="volume" name="volume" value="<?= (!empty($edition_data[0]['volume'])?$edition_data[0]['volume']:"") ?>" placeholder="">	
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
													<label for="issue_no">Issue Number</label>
													<input type="text" class="form-control" name="issue_no" id="issue_no" value="<?= (!empty($edition_data[0]['issue_no'])?$edition_data[0]['issue_no']:"") ?>" placeholder="">	
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="similar_magazine">Similar Mazagine</label>												
													<select class="newvideotags" name="similar_magazine[]" id="similar_magazine" multiple="multiple" required>	
														<?php $sel="";	
															foreach ($similar_magazine as $key => $value) {														
																if(!empty($similar_magazine)){														
																	$sel = (in_array($value['id'],$similar_magazine_data)?"selected":" ");															}?>	
																	<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>								
														<?php } ?>
													</select>						
											</div>
										</div>

										<div class="col-sm-12">
											<div class="form-group">
												<label for="magazine_description">Mazagine Description</label>
												<textarea class="form-control" id="magazine_description" name="magazine_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($edition_data[0]['magazine_description'])?$edition_data[0]['magazine_description']:"") ?></textarea>	
											</div>
										</div>
									</div>
									
									<label for="">Meta Information</label><hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($edition_data[0]['meta_title'])?$edition_data[0]['meta_title']:"") ?>"  name="meta_title" placeholder="">	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="slug">URL Slug</label>
												<input type="text" class="form-control" id="slug" value="<?= (!empty($edition_data[0]['slug'])?$edition_data[0]['slug']:"") ?>"  name="slug" placeholder="">	
											</div>
										</div>
									</div>


									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_keyword">Meta Keywords</label>
												<textarea class="form-control" id="meta_keyword" name="meta_keyword" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($edition_data[0]['meta_keyword'])?$edition_data[0]['meta_keyword']:"") ?></textarea>	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($edition_data[0]['meta_description'])?$edition_data[0]['meta_description']:"") ?></textarea>	
											</div>
										</div>
									</div>
											
											
									<label for="">Upload Image</label><hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="editionfile">Edition Image</label>
												<?php $req= '';
												if(empty($edition_data[0]['image'])){
													$req =''; 
												}else{?>
												<br>
													<img src="<?= FRONT_URL.'/images/edition_image/'.$edition_data[0]['image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
													<input name="pre_editionfile_name" id="pre_editionfile_name" value="<?= (!empty($edition_data[0]['image'])?$edition_data[0]['image']:"") ?>" type="hidden">
													<a href="#" class="btn btn-danger" onclick="removeFile('IMAGE','<?= $edition_data[0]['id']?>')">  X </a>

												<?php }?>
												
												<input type="file" class="form-control-file" id="editionfile" name="editionfile" <?= $req;?> >
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="editionpdffile">Edition PDF</label>
												<?php $req= '';
												if(empty($edition_data[0]['edition_pdf'])){
													$req =''; 
												}else{?>
												<br>
													<i class="fa fa-remove"></i>
													<a href="<?= FRONT_URL.'/images/edition_pdf/'.$edition_data[0]['edition_pdf']?>" target="black"><i  class="fas fa-file" style="font-size: 100px;"></i> </a>

													<input name="pre_editionpdffile_name" id="pre_editionpdffile_name" value="<?= (!empty($edition_data[0]['edition_pdf'])?$edition_data[0]['edition_pdf']:"") ?>" type="hidden">
													<a href="#" class="btn btn-danger" onclick="removeFile('PDF','<?= $edition_data[0]['id']?>')">  X </a>

												<?php }?>
												
												<input type="file" class="form-control-file" id="editionpdffile" name="editionpdffile" <?= $req;?> >
											</div>
										</div>
									</div><hr>	

									<div class="row">
										<div class="col-sm-6">
											<div class="form-check">
												<label>Publish Status</label><br>
												<div class="switch-field">
														<?php $published =  'checked';
															  $draft='';
														if(!empty($edition_data)){
															if($edition_data[0]['status']=='Published'){
																$published = "checked";
															}else{
																$draft = "checked";
															} 
														}?>
														
													<input type="radio" id="published" name="edition_status" value="Published" <?= $published?>/>
													<label for="published">Published</label>
													<input type="radio" id="draft" name="edition_status" value="Draft"  <?= $draft?>/>
													<label for="draft">Draft</label>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-check">
												<label>Approval Status</label><br>
												<div class="switch-field">
														<?php $approved = 'checked';
															 $pending= '';
														if(!empty($edition_data)){
															if($edition_data[0]['admin_status']=='Approved'){
																$approved = "checked";
															}else{
																$pending = "checked";
															} 
														}?>
													<input type="radio" id="approved" name="admin_status" value="Approved" <?= $approved?>/>
													<label for="approved">Approved</label>
													<input type="radio" id="pending" name="admin_status" value="Pending"  <?= $pending?>/>
													<label for="pending">Pending</label>
												</div>
											</div>
										</div>
									</div>					
											
											
												
									<div class="form-group">
										<button  type="submit" class="btn btn-black">Create</button>
										<a href="<?= base_url('edition')?>" class="btn btn-cancel btn-border">Cancel</a>
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
});


function removeFile(type,id){
var r = confirm("want to sure delete this file");
	if (r == true) {
		if(type && id){
			var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
			var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				$.ajax({
					url: "<?= base_url('edition/removeFile')?>",
					data:{type,id,[csrfName]: csrfHash },
					dataType: "json",
					type: "POST",
					success: function(response){
						if(response.success){
							swal("done", response.msg, {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							}).then(
							function() {
								location.reload();
							});
						}else{	
							swal("Failed", response.msg, {
								icon : "error",
								buttons: {        			
									confirm: {
										className : 'btn btn-danger'
									}
								},
							});
						}
					}
				});
		}else{
			alert("OOPS somethings went wrong")
		}
	}
}

// use for form submit
var vRules = 
{
	title:{required:true},
	volume:{required:true},
	issue_no:{required:true},
	slug:{required:true},
	meta_keyword:{required:true},
	meta_title:{required:true},
	meta_description:{required:true},
};
var vMessages = 
{
	title:{required:"Please Enter Edition Name."},
	Volume:{required:"Please Enter Volume."},
	issue_no:{required:"Please Enter Issue Number."},
	slug:{required:"Please Enter URL Slug."},
	meta_keyword:{required:"Please Enter Meta Keywords."},
	meta_title:{required:"Please Enter Meta Title."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-edition").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('edition/submitform')?>";
		$("#form-edition").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('edition')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>