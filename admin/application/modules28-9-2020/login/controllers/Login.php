<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation'); 
		$this->load->model('loginmodel','',TRUE);
		$this->load->model('common_model/common_model','common',TRUE);
		if($this->session->userdata('supply_chain_admin'))
			redirect(base_url().'home/index');  
	}

	public function index(){
		$this->load->view('head.php');
		$this->load->view('index.php');
		$this->load->view('footer.php');
	}

	public function form_validate(){
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if($this->form_validation->run()) {  
			//true  if validation property sectifice properly
			$username = $this->input->post('username');  
			$password = $this->input->post('password'); 
			$condition = "username = '".$username."' AND password = md5('".$password."')";
			$result = $this->common->getData('tbl_admins','*',$condition);
			if($result){
				$session_data = array('supply_chain_admin'=>$result);  
			   $this->session->set_userdata($session_data);  
			//    $admin->setEventType('Login');
				$data['ip'] = $_SERVER['REMOTE_ADDR'];
				$data['last_login'] = date('Y-m-d H:i:s');
				$condition = "id = '".$result[0]['id']."' ";
				$result = $this->common->updateData("tbl_admins",$data,$condition);

			   $this->session->set_flashdata('success_msg', 'login successfully');  
			   redirect(base_url().'home/index');  
			}else{
				// $this->session->unset_userdata('supply_chain_admin');
				$this->session->set_flashdata('error', 'Invalid Username and Password');  
                redirect(base_url().'login/index'); 
			}
		}else{  
			// if validation is not proper
			$this->index();  
		}  

	}

}


