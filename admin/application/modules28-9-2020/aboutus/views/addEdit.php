
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
								<h1 class="ff-page-title">About Us</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="#/" onclick="goBack()"  class="btn btn-light btn-border ff-page-title-btn">
										Cancel
								</a>
							</div>
						</div>
					</div>	
					<form class="" id="form-aboutus" method="post" enctype="multipart/form-data">
						<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
						<input name="about_us_id" id="about_us_id" value="<?= (!empty($about_us[0]['about_us_id'])?$about_us[0]['about_us_id']:"") ?>" type="hidden">				
						<div class="row">
							<div class="col-sm-12">
								<div class="card">
										<div class="card-body">
											<h3>Header</h3>
											<hr/>
											<div class="form-group">
											<label for="banner_image">Banner Image</label>
													<?php $req= '';
														if(empty($about_us[0]['banner_image'])){
															$req ='required';
														}else{?>
														<div>
															<img src="<?= FRONT_URL.'/images/aboutus/'.$about_us[0]['banner_image']?>" alt="..." class="rounded" height="150px" weight="150px">
														</div>
														<br>
															<input name="pre_banner_image" id="pre_banner_image" value="<?= (!empty($about_us[0]['banner_image'])?$about_us[0]['banner_image']:"") ?>" type="hidden">
														<?php }?>
														
														<input type="file" class="form-control-file" id="banner_image" name="banner_image" <?= $req;?> >
											</div>
											
											<div class="form-group">
												<label for="banner_heading">Banner Heading</label>
												<input type="text" class="form-control" id="banner_heading"  name="banner_heading" value="<?= (!empty($about_us[0]['banner_heading'])?$about_us[0]['banner_heading']:"") ?>" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="banner_subtext">Banner Subtext</label>
												<textarea class="form-control"  name="banner_subtext" id="banner_subtext" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($about_us[0]['banner_subtext'])?$about_us[0]['banner_subtext']:"") ?></textarea>	
											</div>
											<div class="clearfix">&nbsp;</div>
											<h3>Section 1</h3>
											<hr/>
											
											<div class="form-group">
													<label for="section1_image">Image</label>
													<?php $req= '';
														if(empty($about_us[0]['section1_image'])){
															$req ='required';
														}else{?>
														<div>
															<img src="<?= FRONT_URL.'/images/aboutus/'.$about_us[0]['section1_image']?>" alt="..." class="rounded" height="150px" weight="150px">
														</div>
														<br>
															<input name="pre_section1_image" id="pre_section1_image" value="<?= (!empty($about_us[0]['section1_image'])?$about_us[0]['section1_image']:"") ?>" type="hidden">
														<?php }?>
														
														<input type="file" class="form-control-file" id="section1_image" name="section1_image" <?= $req;?> >
											</div>
											<div class="form-group">
												<label for="section1_heading">Section 1 - Heading</label>
												<input type="text" class="form-control"  value="<?= (!empty($about_us[0]['section1_heading'])?$about_us[0]['section1_heading']:"") ?>" id="section1_heading" name="section1_heading" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="section1_paragraph">Section 1 - Paragraph</label>
												<textarea class="form-control" id="section1_paragraph" name="section1_paragraph" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($about_us[0]['section1_paragraph'])?$about_us[0]['section1_paragraph']:"") ?></textarea>	
											</div>
											<div class="clearfix">&nbsp;</div>
											<h3>Statistic Numbers</h3>
											<hr/>
											<div class="form-group">
													<label for="stat_one">Stat one</label>
													<input type="text" class="form-control mb-3"  value="<?= (!empty($about_us[0]['stat_one_no'])?$about_us[0]['stat_one_no']:"") ?>" name="stat_one_no"   id="stat_one_no" placeholder="5000+">	
													<input type="text" class="form-control" value="<?= (!empty($about_us[0]['stat_one_desc'])?$about_us[0]['stat_one_desc']:"") ?>"  name="stat_one_desc" id="stat_one_desc" placeholder="hours of footage">	
											</div>
											<div class="form-group">
													<label for="stat_two_no">Stat two</label>
													<input type="text" class="form-control mb-3" id="stat_two_no"  name="stat_two_no" value="<?= (!empty($about_us[0]['stat_two_no'])?$about_us[0]['stat_two_no']:"") ?>" placeholder="24 hours">	
													<input type="text"  value="<?= (!empty($about_us[0]['stat_two_desc'])?$about_us[0]['stat_two_desc']:"") ?>" class="form-control" id="stat_two_desc" name="stat_two_desc" placeholder="Turnaround service">	
											</div>
											<div class="form-group">
													<label for="stat_three_no">Stat three</label>
													<input type="text" class="form-control mb-3"  value="<?= (!empty($about_us[0]['stat_three_no'])?$about_us[0]['stat_three_no']:"") ?>" id="stat_three_no" name="stat_three_no" placeholder="25000">	
													<input type="text" class="form-control"  value="<?= (!empty($about_us[0]['stat_three_desc'])?$about_us[0]['stat_three_desc']:"") ?>" id="stat_three_desc"  name="stat_three_desc" placeholder="entries in our masters database">	
											</div>
											<div class="clearfix">&nbsp;</div>
											<h3>Section 2</h3>
											<hr/>
											<div class="form-group">
													<label for="section2_image">Image</label>
													<?php $req= '';
														if(empty($about_us[0]['section2_image'])){
															$req ='required';
														}else{?>
															<div>
															<img src="<?= FRONT_URL.'/images/aboutus/'.$about_us[0]['section2_image']?>" alt="..." class="rounded" height="150px" weight="150px">
														</div>
														<br>
															<input name="pre_section2_image" id="pre_section2_image" value="<?= (!empty($about_us[0]['section2_image'])?$about_us[0]['section2_image']:"") ?>" type="hidden">
														<?php }?>
														
														<input type="file" class="form-control-file" id="section2_image" name="section2_image" <?= $req;?> >
											</div>
											<div class="form-group">
												<label for="section2_heading">Section 2 - Heading</label>
												<input type="text" class="form-control" value="<?= (!empty($about_us[0]['section2_heading'])?$about_us[0]['section2_heading']:"") ?>" id="section2_heading" name="section2_heading" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="section2_paragraph">Section 2 - Paragraph</label>
												<textarea class="form-control" id="section2_paragraph"   name="section2_paragraph" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($about_us[0]['section2_paragraph'])?$about_us[0]['section2_paragraph']:"") ?></textarea>	
											</div>
											<div class="clearfix">&nbsp;</div>
											<h3>Meta Tags</h3>											
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title"   value="<?= (!empty($about_us[0]['meta_title'])?$about_us[0]['meta_title']:"") ?>"  name="meta_title" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description"  name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($about_us[0]['meta_description'])?$about_us[0]['meta_description']:"") ?></textarea>	
											</div>										
											<div class="form-group">
												<button class="btn btn-black" type="submit">Save</button>
												<!-- <button class="" onclick="goBack()">Cancel</button> -->
												<a href="#/" onclick="goBack()" class="btn btn-cancel btn-border">Cancel</a>
											</div>
												
										</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			
		</div>						
	</div>
</body>
</html>

<script>
$(document).ready(function(){
});

function goBack() {
		window.history.back()
	}
// use for form submit
var vRules = 
{
	banner_heading:{required:true},
	banner_subtext:{required:true},
	section1_heading:{required:true},
	section1_paragraph:{required:true},
	section2_heading:{required:true},
	section2_paragraph:{required:true},
	stat_one_no:{required:true},
	stat_one_desc:{required:true},
	stat_two_no:{required:true},
	stat_two_desc:{required:true},
	stat_three_no:{required:true},
	stat_three_desc:{required:true},
	meta_title:{required:true},
	meta_description:{required:true},

};
var vMessages = 
{
	banner_heading:{required:"Please Enter theme Name."},
	banner_subtext:{required:"Please Select theme."},
	section1_heading:{required:"Please Enter Section heading."},
	section1_paragraph:{required:"Please Enter Section paragraph."},
	section2_heading:{required:"Please Enter Section heading."},
	section2_paragraph:{required:"Please Enter Section paragraph."},
	stat_one_no:{required:"Plese Enter Stat one number"},
	stat_one_desc:{required:"Plese Enter Stat one description"},
	stat_two_no:{required:"Plese Enter Stat two number"},
	stat_two_desc:{required:"Plese Enter Stat two description"},
	stat_three_no:{required:"Plese Enter Stat Three number"},
	stat_three_desc:{required:"Plese Enter Stat Three description"},
	meta_title:{required:"Please Enter Meta Title."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-aboutus").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('aboutus/submitform')?>";
		$("#form-aboutus").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('aboutus')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});



</script>