
<body>
	<div class="wrapper">
    
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						
						<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">All Articles</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('article/addEdit')?>" class="btn btn-primary ff-page-title-btn ff-page-title-btn">
								Add Article
							</a>
						</div>
					</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="article-listing" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>#</th>
																<th>Title</th>	
																<th>Edition</th>
																<th>Author</th>
																<th>Status</th>
																<th>Featured</th>
																<th>Approval Status</th>																
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
															<th>#</th>
																<th>Title</th>	
																<th>Edition</th>
																<th>Author</th>
																<th>Status</th>
																<th>Featured</th>
																<th>Approval Status</th>																
																<th>Actions</th>												
															</tr>
														</tfoot>
														<tbody>
														<?php
														if(!empty($article_data) && isset($article_data))	{
															$cnt = 0;
															foreach ($article_data as $key => $value) {
																	$cnt= ++$cnt?>
																	<tr>														
																		<td>#<?=$cnt?></td>
																		<td><?= $value['title']?></td>
																		<td><?= $value['edition_name']?></td>

																		<td><?= $value['author']?></td>	
																				
																		<!-- <td><img src="<?= FRONT_URL.'/images/article_image/'.(!empty($value['image'])?$value['image']:"default-img.jpg")?>"class="img-thumbnail" alt="Cinque Terre" width="100" height="100"></td>																 -->
																		<td><?= $value['status']?></td>	
																		<td><?= $value['featured']?></td>	
																		<td><?= $value['admin_status']?></td>	

																		<td>
																			<div class="form-button-action">
																				<a href="<?= base_url('article/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-primary ff-page-title-btn  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<!-- <a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a> -->
																			</div>
																		</td>													
																	</tr>
															<?php }
														}?>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#article-listing').DataTable({
				"pageLength": 5,
				"ordering": false,
			});			
		});
	</script>
</body>
</html>
