
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
								<h1 class="ff-page-title">Contact Us</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
								<a href="#" class="btn btn-light btn-border ff-page-title-btn">
										Cancel
								</a>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
									<div class="card-body">
										<h3>Header</h3>
										<hr/>
										<form class="" id="form-contact-us" method="post" enctype="multipart/form-data">
										
										<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
										<input name="contact_id" id="contact_id" value="<?= (!empty($contactus_data[0]['contact_id'])?$contactus_data[0]['contact_id']:"") ?>" type="hidden">
							
										<div class="form-group">
												<label for="video_url">Header Video URL</label>
												<input type="text" class="form-control" id="video_url"  value="<?= (!empty($contactus_data[0]['video_url'])?$contactus_data[0]['video_url']:"") ?>" name="video_url" placeholder="https://vimeo.com/55900216">
										</div>
										<div class="form-group">
											<label for="heading">Heading</label>
											<input type="text" class="form-control" id="heading"  value="<?= (!empty($contactus_data[0]['heading'])?$contactus_data[0]['heading']:"") ?>"  name="heading" placeholder="Contact us">	
										</div>
										<div class="form-group">
											<label for="banner_heading">Banner Heading</label>
											<textarea class="form-control" id="banner_heading" name="banner_heading" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."> <?= (!empty($contactus_data[0]['banner_heading'])?$contactus_data[0]['banner_heading']:"") ?></textarea>	
										</div>																	
										<div class="form-group">
											<button class="btn btn-black">Save</button>
											<a href="<?= base_url('contactus')?>" class="btn btn-cancel btn-border">Cancel</a>
										</div>
										</form>
											
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
</body>
</html>

<script>
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	video_url:{required:true},
	banner_heading:{required:true},
	heading:{required:true}
};
var vMessages = 
{
	video_url:{required:"Please Enter Video Url."},
	banner_heading:{required:"Please Enter Banner Heading"},
	heading:{required:"Please Enter Heading."},
};

$("#form-contact-us").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('contactus/submitform')?>";
		$("#form-contact-us").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('contactus')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>