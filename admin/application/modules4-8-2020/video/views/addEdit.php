
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
								<h1 class="ff-page-title">New Master Video</h1>
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
								<a data-fancybox data-src="#bulk-upload" href="javascript:;" class="btn btn-primary ml-2 ff-page-title-btn">
									<i class="fas fa-plus"></i>
									Bulk Upload
								</a>
								<a href="#" class="btn btn-light btn-border ff-page-title-btn">
									Cancel
								</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<form action="">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label for="vimeourl">Vimeo URL</label>
													<input type="text" class="form-control" id="vimeourl" placeholder="Enter Vimeo Link">
												</div>
												<div class="form-group">
													<label for="videotitle">Title</label>
													<input type="text" class="form-control" id="videotitle" placeholder="eg. 1910-1918-WWI Era">
												</div>
												<div class="form-group">
													<label for="exampleFormControlFile1">Video Thumbnail</label>
													<input type="file" class="form-control-file" id="exampleFormControlFile1">
												</div>
												<div class="form-group">
													<label for="reelno">Reel No</label>
													<input type="text" class="form-control" id="reelno" placeholder="eg. 220657- 78">
												</div>
												<div class="form-group">
													<label for="themedesc">Short Decription</label>
													<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>
												</div>
												<div class="form-group">
													<label for="exampleFormControlSelect1">Year</label>
													<select class="form-control" id="exampleFormControlSelect1">
														<option>Select</option>
														<option>1919</option>
														<option>1920</option>
														<option>1921</option>
														<option>1922</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="exampleFormControlSelect1" style="font-size:18px !important;margin:0;">Add Country and Location</label>	
												</div>
											</div>											
											<div class="col-md-4">
												<div class="form-group">
													<label for="exampleFormControlSelect1">Country</label>
													<select class="form-control" id="exampleFormControlSelect1">
														<option>Select</option>
														<option>1919</option>
														<option>1920</option>
														<option>1921</option>
														<option>1922</option>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="exampleFormControlSelect1">Location</label>
													<input type="text" class="form-control" id="reelno" placeholder="Enter Location">
												</div>
											</div>											
										</div>
										<div id="addmore-location-select">
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<a href="#/" class="btn btn-primary btn-sm addmore-location">Add More</a>
												</div>
											</div>
										</div>
											
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label for="exampleFormControlSelect1">Sound</label>
													<select class="form-control" id="exampleFormControlSelect1">
														<option>Select</option>
														<option>1919</option>
														<option>1920</option>
														<option>1921</option>
														<option>1922</option>
													</select>
												</div>
												<div class="form-group">
													<label for="exampleFormControlSelect1">Color</label>
													<select class="form-control" id="exampleFormControlSelect1">
														<option>Select</option>
														<option>1919</option>
														<option>1920</option>
														<option>1921</option>
														<option>1922</option>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="videotitle">TC Begin</label>
													<input type="text" class="form-control" id="videotitle" placeholder="00:00:00">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="videotitle">TC End</label>
													<input type="text" class="form-control" id="videotitle" placeholder="00:00:00">
												</div>
											</div>
											<div class="col-md-12">
												<div class="form-group">
													<label for="videotitle">Duration</label>
													<input type="text" class="form-control" id="videotitle" placeholder="00:00:00">
												</div>
												<div class="form-group">
													<label for="themedesc">Description</label>
													<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>
												</div>
												<div class="form-group">
													<label for="newvideotags">Tags</label>
													<select class="newvideotags" name="newvideotags" multiple="multiple">
														<option value="Tag 1">Washington</option>
														<option value="Tag 2">Paris</option>
														<option value="Tag 3">London</option>
														<option value="Tag 4">Johannesberg</option>
														<option value="Tag 5">Jakarta</option>
													</select>
												</div>
												<div class="form-group">
													<label for="themename">Meta Title</label>
													<input type="text" class="form-control" id="themename" placeholder="Early 19th Century">
												</div>
												<div class="form-group">
													<label for="themedesc">Meta Description</label>
													<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>
												</div>										
												<div class="form-group">
													<label for="exampleFormControlSelect1" style="font-size:18px !important;margin:0;">Select Target Theme</label>												
												</div>
											</div>
										</div>
										<div class="row target-theme-row">
											<div class="col-sm-5">
												<div class="form-group">
													<label for="exampleFormControlSelect1">Master Theme</label>
													<select class="form-control" id="exampleFormControlSelect1">
														<option>Select</option>
														<option>1919</option>
														<option>1920</option>
														<option>1921</option>
														<option>1922</option>
													</select>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="form-group">
													<label for="exampleFormControlSelect1">Sub Theme</label>
													<select class="form-control" id="exampleFormControlSelect1">
														<option>Select</option>
														<option>1919</option>
														<option>1920</option>
														<option>1921</option>
														<option>1922</option>
													</select>
												</div>
											</div>											
										</div>
										<div id="addmore-video-select">
	
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<a href="#/" class="btn btn-primary btn-sm addmore-video-theme">Add More</a>
												</div>
											</div>
											<div class="col-sm-12">
												<div class="form-check">
													<label>Activate theme (show this theme on website)</label><br>
													<div class="switch-field">
														<input type="radio" id="radio-one" name="switch-one" value="yes" checked />
														<label for="radio-one">Yes</label>
														<input type="radio" id="radio-two" name="switch-one" value="no" />
														<label for="radio-two">No</label>
													</div>
												</div>
												<div class="form-group">
													<button class="btn btn-black">Finish</button>
													<button class="btn btn-cancel btn-border">Cancel</button>
												</div>
											</div>
										</div>
									</form>
								</div>							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	</div>
	<!-- Bulk Video Upload -->
	<div style="display: none;" id="bulk-upload" class="ff-popup">
		<div class="ff-popup-title"><span class="h2">Bulk Upload Videos</span></div>
		<div class="card">
			<div class="card-body">
				<p>STEP-1</p>
				<p>In order to upload, please organise that data in given excel format.</p>
				<a href="#/" class="btn btn-primary">Download File</a>
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				<p>STEP-2</p>
				<p>Once you have organised the data in the above given excel file.</p>
				<ul>
					<li>Click browse button to select the file.</li>
					<li>Then click on upload button to upload the file.</li>
				</ul>
				<div class="form-group" style="padding:0;margin:0 0 20px 0;">
					<label for="exampleFormControlFile1">Select File</label>
					<input type="file" class="form-control-file" id="exampleFormControlFile1">
				</div>
				<button class="btn btn-primary">Upload File</button>
			</div>
		</div>
	</div>
</body>

</html>