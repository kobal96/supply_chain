<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
	<div class="sidebar-wrapper scrollbar scrollbar-inner">
		<div class="sidebar-content">
			<ul class="nav nav-primary">
				
				<li class="nav-item <?php echo($this->uri->segment(1) == "dashboard" || $this->uri->segment(1) =="")?"active":"";?>">
					<a href="<?= base_url('dashboard')?>">
						<i class="fas fa-chart-line"></i>
						<p>Dashboard</p>
					</a>
				</li>
				<li class="nav-item <?php echo($this->uri->segment(1) == "article" || $this->uri->segment(1) =="")?"active":"";?> ">
					<a href="<?= base_url('article')?>">
						<i class="fas fa-newspaper"></i>
						<p>Articles</p>
					</a>
				</li>
				<?php  ?>
				<li class="nav-item <?php echo($this->uri->segment(1) == "spotlight" || $this->uri->segment(1) =="")?"active":"";?>">
					<a href="<?= base_url('enquiries')?>">
						<i class="fas fa-comment-alt"></i>
						<p>Home Spotlight</p>
					</a>
				</li>
				<li class="nav-item <?php echo($this->uri->segment(1) == "banner" || $this->uri->segment(1) =="")?"active":"";?>">
					<a href="<?= base_url('banner')?>">
						<i class="fas fa-comment-alt"></i>
						<p>Banner</p>
					</a>
				</li>
				<li class="nav-item  <?php echo(in_array($this->uri->segment(1),array("edition","aboutus","tag","article","meetteam")))?"active":"" ?>">
					<a data-toggle="collapse" href="#website">
						<i class="fas fa-window-maximize"></i>
						<p>Magazine</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="website">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("edition")))?"active":"" ?>">
								<a href="<?= base_url('edition')?>">
									<span class="sub-item">Edition</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("magazine_category")))?"active":"" ?>">
								<a href="<?= base_url('magazine_category')?>">
									<span class="sub-item">Magazine Category</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("tag")))?"active":"" ?>">
								<a href="<?= base_url('tag');?>">
									<span class="sub-item">Tags</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("article")))?"active":"" ?>">
								<a href="<?= base_url('article')?>">
									<span class="sub-item">Articles </span>
								</a>
							</li>
						
						</ul>
					</div>
				</li>

				<li class="nav-item  <?php echo(in_array($this->uri->segment(1),array("packages","promo")))?"active":"" ?>">
					<a data-toggle="collapse" href="#subscriptions">
						<i class="fas fa-window-maximize"></i>
						<p>Subscriptions</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="subscriptions">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("packages")))?"active":"" ?>">
								<a href="<?= base_url('packages')?>">
									<span class="sub-item">Packages</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("promo")))?"active":"" ?>">
								<a href="<?= base_url('promo')?>">
									<span class="sub-item">Promo</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
				
				
				
				<li class="nav-item <?php echo(in_array($this->uri->segment(1),array("users","subscriber")))?"active":"" ?>" >
					<a data-toggle="collapse" href="#users">
						<i class="fas fa-layer-group"></i>
						<p>Users</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="users">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("users")))?"active":"" ?>">
								<a href="<?= base_url('users');?>">
									<span class="sub-item">Users</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("subscriber")))?"active":"" ?>">
								<a href="<?= base_url('subscriber')?>">
									<span class="sub-item">Subscriber</span>
								</a>
							</li>
						
						</ul>
					</div>
				</li>

				<li class="nav-item <?php echo(in_array($this->uri->segment(1),array("user_data","session_data","device_data","geo_location","behavior")))?"active":"" ?>" >
					<a data-toggle="collapse" href="#reports">
						<i class="fas fa-layer-group"></i>
						<p>Reports</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="reports">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("user_data")))?"active":"" ?>">
								<a href="<?= base_url('user_data');?>">
									<span class="sub-item">User Data</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("session_data")))?"active":"" ?>">
								<a href="<?= base_url('session_data')?>">
									<span class="sub-item">Session Data</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("device_data")))?"active":"" ?>">
								<a href="<?= base_url('device_data')?>">
									<span class="sub-item">Device Data</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("geo_location")))?"active":"" ?>">
								<a href="<?= base_url('geo_location')?>">
									<span class="sub-item">Gro Location</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("behavior")))?"active":"" ?>">
								<a href="<?= base_url('behavior')?>">
									<span class="sub-item">Behavior</span>
								</a>
							</li>
						
						</ul>
					</div>
				</li>
				<br>
				<br>
				<br>
				<br>

				<li class="nav-item <?php echo($this->uri->segment(1) == "newandevent" || $this->uri->segment(1) =="")?"active":"";?>">
					<a href="<?=base_url('newandevent');?>">
					<i class="fas fa-newspaper"></i>
						<p>News & Events</p>
					</a>
				</li>
				<li class="nav-item <?php echo($this->uri->segment(1) == "profile" || $this->uri->segment(1) =="")?"active":"";?>">
				<a href="<?= base_url("profile")?>">
						<i class="fas fa-address-card"></i>
						<p>Profile</p>
					</a>
				</li>
				<li class="nav-item ">
					<a href="<?= base_url("home/logOut")?>">
						<i class="fas fa-sign-out-alt"></i>
						<p>Logout</p>
					</a>
				</li>

			</ul>
		</div>
	</div>
</div>
<!-- End Sidebar -->
