
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Package Detail</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('package') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<!-- <label for="">Basic Information</label> -->
								<hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Title">Title</label>
												<p><span><?= (!empty($package_data[0]['title'])?$package_data[0]['title']:"") ?></span></p>
													
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="Title">Duration</label>
												<p><span><?= (!empty($package_data[0]['duration'])?$package_data[0]['duration'].' Month':"") ?></span></p>
													
											</div>
										</div>
									
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="type">Price</label>
												<p><span><?= (!empty($package_data[0]['price'])?'Rs.'.$package_data[0]['price']:"") ?></span></p>
											</div>
										</div>
									</div>


									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="intrastate">Publish Status</label>
												<p><span><?= (!empty($package_data[0]['status'])?$package_data[0]['status']:"") ?></span></p>	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="IGST">Approval Status</label>
												<p><span><?= (!empty($package_data[0]['admin_status'])?$package_data[0]['admin_status']:"") ?></span></p>	
											</div>
											
										</div>
									</div>
									
									<hr>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>