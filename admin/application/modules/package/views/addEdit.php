
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Package</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('package') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-package" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
									<input name="id" id="id" value="<?= (!empty($package_data[0]['id'])?$package_data[0]['id']:"") ?>" type="hidden" >
									
									<label for="">Basic Information</label><hr>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="Title">Package Name</label>
												<input type="text" class="form-control" id="title" name="title" value="<?= (!empty($package_data[0]['title'])?$package_data[0]['title']:"") ?>" placeholder="Package Name">	
											</div>
										</div>
										
										<div class="col-sm-6">
										<input id="durationid" value="<?= (!empty($package_data[0]['duration'])?$package_data[0]['duration']:"") ?>" type="hidden" >
											<div class="form-group">
												<label for="duration">Duration</label>
												<select class="form-control form-control " id="duration" name="duration">
												<option value="" selected="selected" disabled>--- Select Duration ---</option>
												<option value="1">1 Month</option>
												<option value="2">2 Month</option>
												<option value="3">3 Month</option>
												<option value="4">4 Month</option>
												<option value="5">5 Month</option>
												<option value="6">6 Month</option>
												<option value="7">7 Month</option>
												<option value="8">8 Month</option>
												<option value="9">9 Month</option>
												<option value="10">10 Month</option>
												<option value="11">11 Month</option>
												<option value="12">12 Month</option>
												<option value="13">13 Month</option>
												<option value="14">14 Month</option>
												<option value="15">15 Month</option>
												<option value="16">16 Month</option>
												<option value="17">17 Month</option>
												<option value="18">18 Month</option>
												<option value="19">19 Month</option>
												<option value="20">20 Month</option>
												<option value="21">21 Month</option>
												<option value="22">22 Month</option>
												<option value="23">23 Month</option>
												<option value="24">24 Month</option>
												</select>
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="author">Price</label>
												<input type="text" class="form-control" id="price" name="price" value="<?= (!empty($package_data[0]['price'])?$package_data[0]['price']:"") ?>" placeholder="Price">	
											</div>
										</div>
									</div>
									<br>
									<br>
									<br>
									
									<label for="">GST Information</label><hr>
									<div class="row">
										<div class="col-sm-6">
										<input id="typeid" value="<?= (!empty($package_data[0]['gst_type'])?$package_data[0]['gst_type']:"") ?>" type="hidden" >
											<div class="form-group">
												<label for="type">Type</label>
												<select class="form-control form-control " id="type" name="type">
												<option value="" selected="selected">--- Select Type ---</option>
												<option value="Product">Product</option>
												<option value="Service">Service</option>
												</select>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="intrastate">Intra State %</label>
												<input type="text" class="form-control" id="intrastate" value="<?= (!empty($package_data[0]['gst_intra_state_per'])?$package_data[0]['gst_intra_state_per']:"") ?>"  name="intrastate" placeholder="Intra State %">	
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<label for="CGST">CGST %</label>
												<input type="text" class="form-control" id="CGST" value="<?= (!empty($package_data[0]['gst_cgst'])?$package_data[0]['gst_cgst']:"") ?>"  name="CGST" placeholder="CGST %">	
											</div>
										</div>

										<div class="col-sm-3">
											<div class="form-group">
												<label for="SGST">SGST %</label>
												<input type="text" class="form-control" id="SGST" value="<?= (!empty($package_data[0]['gst_sgst'])?$package_data[0]['gst_sgst']:"") ?>"  name="SGST" placeholder="SGST %">	
											</div>
										</div>

									</div>


									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="intrastate">Inter State %</label>
												<input type="text" class="form-control" id="interstate" value="<?= (!empty($package_data[0]['gst_inter_state_per'])?$package_data[0]['gst_inter_state_per']:"") ?>"  name="intrastate" placeholder="Inter State %">	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="IGST">IGST %</label>
												<input type="text" class="form-control" id="IGST" value="<?= (!empty($package_data[0]['gst_igst'])?$package_data[0]['gst_igst']:"") ?>"  name="IGST" placeholder="IGST %">	
											</div>
											
										</div>
									</div>
									
									<hr>	

									<div class="row">
										<div class="col-sm-6">
											<div class="form-check">
												<label>Publish Status</label><br>
												<div class="switch-field">
														<?php
														if(!empty($package_data)){
															if($package_data[0]['status']=='Published'){
																$published = "checked";
															}else{
																$draft = "checked";
															} 
														}else{
															$draft='checked';
														}

														?>
														
													<input type="radio" id="published" name="package_status" value="Published" <?= @$published?>/>
													<label for="published">Published</label>
													<input type="radio" id="draft" name="package_status" value="Draft"  <?= @$draft?>/>
													<label for="draft">Draft</label>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-check">
												<label>Approval Status</label><br>
												<div class="switch-field">
														<?php
														if(!empty($package_data)){
															if($package_data[0]['admin_status']=='Approved'){
																$approved = "checked";
															}else{
																$pending = "checked";
															} 
														}else{
															$pending= 'checked';
														}
														?>
													<input type="radio" id="approved" name="admin_status" value="Approved" <?= @$approved?>/>
													<label for="approved">Approved</label>
													<input type="radio" id="pending" name="admin_status" value="Pending"  <?= @$pending?>/>
													<label for="pending">Pending</label>
												</div>
											</div>
										</div>
										
									</div>					
											
											
												
									<div class="form-group">
										<button  type="submit" class="btn btn-black">Create</button>
										<a href="<?= base_url('article')?>" class="btn btn-cancel btn-border">Cancel</a>
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
	var duration = $("#durationid").val();
	$("#duration").val(duration);

	var type = $("#typeid").val();
	$("#type").val(type);

});


function removeFile(type,id){
var r = confirm("want to sure delete this file");
	if (r == true) {
		if(type && id){
			var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
			var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				$.ajax({
					url: "<?= base_url('package/removeFile')?>",
					data:{type,id,[csrfName]: csrfHash },
					dataType: "json",
					type: "POST",
					success: function(response){
						if(response.success){
							swal("done", response.msg, {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							}).then(
							function() {
								location.reload();
							});
						}else{	
							swal("Failed", response.msg, {
								icon : "error",
								buttons: {        			
									confirm: {
										className : 'btn btn-danger'
									}
								},
							});
						}
					}
				});
		}else{
			alert("OOPS somethings went wrong")
		}
	}
}

// use for form submit
var vRules = 
{
	title:{required:true},
	duration:{required:true},
	price:{required:true},
	type:{required:true},
	intrastate:{required:true},
	CGST:{required:true},
	SGST:{required:true},
	interstate:{required:true},
	IGST:{required:true},
};
var vMessages = 
{
	title:{required:"Please Enter Package Name."},
	duration:{required:"Please Select Duration."},
	price:{required:"Please Enter price."},
	type:{required:"Please Select Type ."},
	intrastate:{required:"Please Enter Intra State %."},
	CGST:{required:"Please Enter CGST %."},
	SGST:{required:"Please Select SGST %"},
	interstate:{required:"Please Enter Inter State %."},
	IGST:{required:"Please Enter IGST %."},

};

$("#form-package").validate({
	ignore:[],
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('package/submitform')?>";
		$("#form-package").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('package')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>