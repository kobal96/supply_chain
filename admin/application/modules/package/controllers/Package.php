<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Package extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('packagemodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['package_data'] = $this->common->getData("tbl_packages",'*',$condition,"id","desc");
		 /*echo "<pre>";
		 print_r($result);
		 exit;*/
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];
			$condition = "id ='".$id."' ";
			$result['package_data'] = $this->common->getData("tbl_packages",'*',$condition);
		}
		//echo "<pre>";print_r($result);exit;

		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	public function view(){

		$id = "";
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];
			$condition = "id ='".$id."' ";
			$result['package_data'] = $this->common->getData("tbl_packages",'*',$condition);
		}
		//echo "<pre>";print_r($result);exit;

		$this->load->view('main-header.php');
		$this->load->view('view.php',$result);
		$this->load->view('footer.php');
	}

	public function submitForm(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "title = '".$this->input->post('title')."' ";
			if($this->input->post('id') && $this->input->post('id') > 0){
				$condition .= " AND  id != ".$this->input->post('id')." ";
			}			
			$check_name = $this->common->getData("tbl_packages",'*',$condition);

			// print_r($check_name);
			// exit;
			if(!empty($check_name[0]['id'])){
				echo json_encode(array("success"=>false, 'msg'=>'Package Name Already Present!'));
				exit;
			}

			$data['title'] = $this->input->post('title');
			$data['duration'] = $this->input->post('duration');
			$data['price'] = $this->input->post('price');
			$data['gst_type'] = $this->input->post('type');
			$data['gst_intra_state_per'] = $this->input->post('intrastate');
			$data['gst_cgst'] = $this->input->post('CGST');
			$data['gst_sgst'] = $this->input->post('SGST');
			$data['gst_inter_state_per'] = $this->input->post('intrastate');
			$data['gst_igst'] = $this->input->post('IGST');
			$data['status'] = $this->input->post('package_status');
			$data['admin_status'] = $this->input->post('admin_status');
			$data['updated_at'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('supply_chain_admin')[0]['id'];
			if(!empty($this->input->post('id'))){
				$condition = "id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_packages",$data,$condition);
				if($result){

					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('supply_chain_admin')[0]['id'];
				$result = $this->common->insertData('tbl_packages',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
		
}
?>