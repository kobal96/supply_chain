
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">All Users</h1> 
							</div>							
						</div>
					</div>					
					<div class="row">
						<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>S.No</th>
																<th>Name</th>
																<th>Username</th>
																<th>Email</th>															
																<th style="width: 70px;">Status</th>	
																<!-- <th>Actions</th> -->															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>S.No</th>
																<th>Name</th>
																<th>Username</th>
																<th>Email</th>																
																<th style="width: 70px;">Status</th>	
																<!-- <th>Actions</th> -->															
															</tr>
														</tfoot>
														<tbody>
															<!-- <tr>																
																<td>Brett Hall</td>
																<td>bretthall345@gmail.com</td>																
																<td>+91-123-456-7890</td>																
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr> -->
												<?php
														if(isset($user_data) && !empty($user_data))	{
															$cnt = 0;
															foreach ($user_data as $key => $value) {
															$cnt= ++$cnt?>
															<tr>														
																<td><?=$cnt?></td>
																<td><?= $value['first_name'].' '.$value['last_name']?></td>
																<td><?= $value['username']?></td>
																<td><?= $value['email']?></td>
																<td style="width: 70px;">
																	<select name="status" id="status" data-status="<?= $value['id']?>">
																		<option value="Active" <?=($value['status']=='Active')?'selected':''?> >Active</option>
																		<option value="De-active" <?=($value['status']=='De-active')?'selected':''?> >De-active</option>
																	</select>
																		
																	</td>	

																<!-- <td>
																<div class="form-button-action">
																	<a href="<?= base_url('article/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-primary ff-page-title-btn  btn-edit" data-original-title="Edit">
																		<i class="fa fa-edit"></i>
																	</a>
																</div>
																</td> -->													
															</tr>
															<?php } } ?>

														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 10,
				"ordering": false,
			});			
		});

	$(document).on("change","#status",function () {
	var r = confirm("Are you sure status change for this user?");
	if (r == true) {
		var status = $(this).val();
		var userid = $(this).attr('data-status');
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
        var csrfHash = $('.txt_csrfname').val(); // CSRF hash
      $.ajax({
        url: "<?= base_url('users/changestatus')?>",
        type: "POST",
        data:{ status, userid, [csrfName]: csrfHash },
        dataType: "json",
        success: function(response){
        	
        	if(response.success){
				swal("done", response.msg, {
					icon : "success",
					buttons: {        			
						confirm: {
							className : 'btn btn-success'
						}
					},
				}).then(
				function() {
					$('#status').val(status);
					//location.reload();
				});
			}else{	
				swal("Failed", response.msg, {
					icon : "error",
					buttons: {        			
						confirm: {
							className : 'btn btn-danger'
						}
					},
				});
			}
          
        }
      });

      }else{
      	location.reload();
      }
          
    });
	</script>
      }
</body>
</html>
