
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Tag</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('tag') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-magazine_category" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="id" id="id" value="<?= (!empty($tag_data[0]['id'])?$tag_data[0]['id']:"") ?>" type="hidden">
									
									<label for="">Basic Information</label><hr>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="Title">Title </label>
												<input type="text" class="form-control" id="title" name="title" value="<?= (!empty($tag_data[0]['title'])?$tag_data[0]['title']:"") ?>" placeholder="">	
											</div>
										</div>
									</div>
									<hr>
							
									<div class="row">
										<div class="col-sm-6">
											<div class="form-check">
												<label>Publish Status</label><br>
												<div class="switch-field">
														<?php $published =  'checked';
															  $draft='';
														if(!empty($tag_data)){
															if($tag_data[0]['status']=='Published'){
																$published = "checked";
															}else{
																$draft = "checked";
															} 
														}?>
														
													<input type="radio" id="published" name="edition_status" value="Published" <?= $published?>/>
													<label for="published">Published</label>
													<input type="radio" id="draft" name="edition_status" value="Draft"  <?= $draft?>/>
													<label for="draft">Draft</label>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-check">
												<label>Approval Status</label><br>
												<div class="switch-field">
														<?php $approved = 'checked';
															 $pending= '';
														if(!empty($tag_data)){
															if($tag_data[0]['admin_status']=='Approved'){
																$approved = "checked";
															}else{
																$pending = "checked";
															} 
														}?>
													<input type="radio" id="approved" name="admin_status" value="Approved" <?= $approved?>/>
													<label for="approved">Approved</label>
													<input type="radio" id="pending" name="admin_status" value="Pending"  <?= $pending?>/>
													<label for="pending">Pending</label>
												</div>
											</div>
										</div>
									</div>					
													
									<div class="form-group">
										<button  type="submit" class="btn btn-black">Create</button>
										<a href="<?= base_url('magazine_category')?>" class="btn btn-cancel btn-border">Cancel</a>
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	title:{required:true},
	slug:{required:true},
	// meta_keyword:{required:true},
	// meta_title:{required:true},
	// meta_description:{required:true},
};
var vMessages = 
{
	title:{required:"Please Enter Edition Name."},
	// Volume:{required:"Please Enter Volume."},
	// issue_no:{required:"Please Enter Issue Number."},
	slug:{required:"Please Enter URL Slug."},
	// meta_keyword:{required:"Please Enter Meta Keywords."},
	// meta_title:{required:"Please Enter Meta Title."},
	// meta_description:{required:"Please Enter Meta Description."},

};

$("#form-magazine_category").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('tag/submitform')?>";
		$("#form-magazine_category").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('tag')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>