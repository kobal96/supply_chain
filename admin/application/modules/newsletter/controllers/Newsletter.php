<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Newsletter extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('newslettermodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
        $main_table = array("tbl_newsletter as a", array("a.*"));
        $join_tables =  array();
        $join_tables = array(
                array("left", "tbl_users as  u", "u.id = a.user_id", array("concat(u.first_name,' ',u.last_name) as user_name")),
                  );
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"",null); 
        $result['newsletter_data'] = $this->common->MySqlFetchRow($rs, "array");
		 /*echo "<pre>";
		 print_r($result);
		 exit;*/
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}
		
}
?>