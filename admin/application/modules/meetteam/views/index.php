
<body>
	<div class="wrapper">
    
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						
						<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">All Team Members</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('meetteam/addEdit')?>" class="btn btn-light btn-border ff-page-title-btn">
								Add Team Member
							</a>
						</div>
					</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>Member ID</th>
																<th>Member Image</th>	
																<th>Member title</th>
																<th>Member Description</th>															
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>Member ID</th>
																<th>Member Image</th>	
																<th>Member title</th>
																<th>Member Description</th>																
																<th>Actions</th>														
															</tr>
														</tfoot>
														<tbody>
														<?php
														// echo "<pre>";
														// print_r($theme_data);
														if(!empty($theme_data) && isset($theme_data))	{
															$cnt = 0;
															foreach ($theme_data as $key => $value) {
																	$cnt= ++$cnt?>
																	<tr>														
																		<td>#<?=$cnt?></td>
																		<td><img src="<?= FRONT_URL.'/images/team_member/'.$value['thumbnail_image']?>"class="img-thumbnail" alt="Cinque Terre" width="100" height="100"></td>																
																		<td><?= $value['member_heading']?></td>							
																		<td><?= $value['member_description']?></td>
																		<td>
																			<div class="form-button-action">
																				<a href="<?= base_url('meetteam/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['meetteam_id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<!-- <a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a> -->
																			</div>
																		</td>													
																	</tr>
															<?php }
														}else{?>
																	NO themes Added so far 
														<?php }?>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
