
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New banner</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('banner') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-banner" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden">
									<input name="id" id="id" value="<?= (!empty($banner_data[0]['id'])?$banner_data[0]['id']:"") ?>" type="hidden">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="themefile">Banner Image</label>
												<?php $req= '';
												if(empty($banner_data[0]['image'])){
													$req ='required';
												}else{?>
												<br>
													<img src="<?= FRONT_URL.'/images/banner_image/'.$banner_data[0]['image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
													<input name="pre_themefile_name" id="pre_themefile_name" value="<?= (!empty($banner_data[0]['image'])?$banner_data[0]['image']:"") ?>" type="hidden">
												<?php }?>
												
												<input type="file" class="form-control-file" id="themefile" name="themefile" <?= $req;?> >
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Title">Title</label>
												<input type="text" class="form-control" id="Title" name="Title" value="<?= (!empty($banner_data[0]['title'])?$banner_data[0]['title']:"") ?>" placeholder="Early 19th Century">	
											</div>
										</div>
									</div>
									

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
													<label for="sub_title">Sub Title</label>
													<input type="text" class="form-control" name="sub_title" id="sub_title" value="<?= (!empty($banner_data[0]['subtitle'])?$banner_data[0]['subtitle']:"") ?>" placeholder="Enter url (eg. https://vimeo.com/55900216)">	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Caption">Caption</label>
												<input type="text" class="form-control" id="caption" name="caption" value="<?= (!empty($banner_data[0]['caption'])?$banner_data[0]['caption']:"") ?>"   placeholder="Enter url (eg. https://vimeo.com/55900216)">	
											</div>
										</div>
									</div>
											
											

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="link">Link</label>
												<input type="text" class="form-control" id="link" name="link" value="<?= (!empty($banner_data[0]['link'])?$banner_data[0]['link']:"") ?>"  name="sub_title" placeholder="Enter url (eg. https://vimeo.com/55900216)">	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="type">Link Type</label>
												<select class="form-control form-control" id="type" name="type"> 
													<option value="">Select Link Type</option>
													<option value="">External</option>
													<option value="">Internal</option>
													
												</select>
											</div>
										</div>
									</div>				
											

									<div class="row">
										<div class="col-sm-6">
											<div class="form-check">
												<label>Publish Status</label><br>
												<div class="switch-field">
														<?php $published =  'checked';
															  $draft='';
														if(!empty($banner_data)){
															if($banner_data[0]['status']=='Published'){
																$published = "checked";
															}else{
																$draft = "checked";
															} 
														}?>
														
													<input type="radio" id="published" name="featured-theme" value="yes" <?= $published?>/>
													<label for="published">Published</label>
													<input type="radio" id="draft" name="featured-theme" value="no"  <?= $draft?>/>
													<label for="draft">Draft</label>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-check">
												<label>Approval Status</label><br>
												<div class="switch-field">
														<?php $approved = 'checked';
															 $pending= '';
														if(!empty($banner_data)){
															if($banner_data[0]['admin_status']=='Approved'){
																$approved = "checked";
															}else{
																$pending = "checked";
															} 
														}?>
													<input type="radio" id="approved" name="approved" value="Approved" <?= $approved?>/>
													<label for="activate-theme-three">Approved</label>
													<input type="radio" id="pending" name="pending" value="Pending"  <?= $pending?>/>
													<label for="activate-theme-four">Pending</label>
												</div>
											</div>
										</div>
									</div>					
											
											
												
									<div class="form-group">
										<button  type="submit" class="btn btn-black">Create</button>
										<a href="<?= base_url('theme')?>" class="btn btn-cancel btn-border">Cancel</a>
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	Title:{required:true},
	// theme_description:{required:true},
	sub_title:{required:true},
	meta_title:{required:true},
	meta_description:{required:true},

};
var vMessages = 
{
	Title:{required:"Please Enter theme Name."},
	// theme_description:{required:"Please Enter theme Description Name."},
	sub_title:{required:"Please Enter Main video."},
	meta_title:{required:"Please Enter Meta Title."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-banner").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('theme/submitform')?>";
		$("#form-banner").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				// $(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('theme')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>