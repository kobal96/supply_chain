<head>
<style>
	.typeahead { border: 2px solid #FFF;border-radius: 4px;padding: 8px 12px;max-width: 300px;min-width: 290px;background: rgba(66, 52, 52, 0.5);color: #FFF;}
	.tt-menu { width:300px; }
	ul.typeahead{margin:0px;padding:10px 0px;}
	ul.typeahead.dropdown-menu li a {padding: 10px !important;	border-bottom:#CCC 1px solid;color:#FFF;}
	ul.typeahead.dropdown-menu li:last-child a { border-bottom:0px !important; }
	.bgcolor {max-width: 550px;min-width: 290px;max-height:340px;background:url("world-contries.jpg") no-repeat center center;padding: 100px 10px 130px;border-radius:4px;text-align:center;margin:10px;}
	.demo-label {font-size:1.5em;color: #686868;font-weight: 500;color:#FFF;}
	.dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover {
		text-decoration: none;
		background-color: #1f3f41;
		outline: 0;
	}
	</style>	
	</head>
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Master Video</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">  
									<a data-fancybox data-src="#bulk-upload" href="javascript:;" class="btn btn-primary ml-2 ff-page-title-btn">
								<i class="fas fa-plus"></i> 
								Bulk Upload						
							</a>	                              
							<a href="#" class="btn btn-light btn-border ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-video" method="post" enctype="multipart/form-data">	
								<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>"  class="txt_csrfname" type="hidden">
									<input name="video_id" id="video_id" value="<?= (!empty($video_data[0]['video_id'])?$video_data[0]['video_id']:"") ?>" type="hidden">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<label for="vimeourl">Vimeo URL</label>
													<input type="text" class="form-control" id="vimeourl"  name="vimeourl" placeholder="Enter Vimeo Link">	
												</div>
												<div class="form-group">
													<label for="videotitle">Title</label>
													<input type="text" class="form-control" id="videotitle" name="videotitle" placeholder="eg. 1910-1918-WWI Era">	
												</div>
											
												<div class="form-group">
													<label for="videofile">Video Thumbnail</label>
													<?php $req= '';
													if(empty($theme_data[0]['thumbnail_image'])){
														$req ='required';
													}else{?>
													<br>
														<img src="<?= FRONT_URL.'/images/video/'.$theme_data[0]['thumbnail_image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
														<input name="pre_videofile_name" id="pre_videofile_name" value="<?= (!empty($theme_data[0]['thumbnail_image'])?$theme_data[0]['thumbnail_image']:"") ?>" type="hidden">
													<?php }?>
													
													<input type="file" class="form-control-file" id="videofile" name="videofile" <?= $req;?> >
												</div>
												<div class="form-group">
													<label for="reelno">Reel No</label>
													<input type="text" class="form-control" id="reelno"  name ="reelno" placeholder="eg. 220657- 78">	
												</div>
												<div class="form-group">
													<label for="short_description">Short Decription</label>
													<textarea class="form-control" name="short_description" id="short_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
												</div>	
												<div class="form-group">
													<label for="long_description">Long Decription</label>
													<textarea class="form-control" id="long_description" name="long_description"  rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>
												</div>																			
												<div class="form-group">
													<label for="year">Year</label>
													<select class="form-control" id="year" name="year" >
														<option>Select</option>
														<option>1919</option>
														<option>1920</option>
														<option>1921</option>
														<option>1922</option>
														<option>1923</option>
														
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="exampleFormControlSelect1" style="font-size:18px !important;margin:0;">Add Country and Location</label>	
												</div>
											</div>											
											<div class="col-md-4">
												<div class="form-group">
													<label for="exampleFormControlSelect0">Country</label>
													<select class="form-control" id="country0" name="country[]" >
													<option value="">Select Country</option>
														<?php $sel="";
														foreach ($countries as $key => $value) {
															$sel = ($value['country_id'] == $video_data[0]['country_id']?"selected":"");
															?>
															<option value="<?= $value['country_id']?>" <?= $sel?> ><?=$value['country_name']?></option>
														<?php } ?>
												</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label for="exampleFormControlSelect0">Location</label>
													<input type="text" class="form-control" id="location0"  autocomplete="off" onkeyup="getLocation(0)" name="location[]" value=""  placeholder="Enter Location">
												</div>
											</div>											
										</div>
										<div id="addmore-location-select">
	
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<a href="#/" class="btn btn-primary btn-sm addmore-location">Add More</a>
												</div>
											</div>
										</div>

										<div class="col-md-12">
											<div class="form-group">
												<label for="sound">Sound</label>
												<select class="form-control" id="sound" name="sound" >
													<option>Select Sound</option>
													<?php $sel="";
														foreach ($sounds as $key => $value) {
															$sel = ($value == $video_data[0]['sound_id']?"selected":"");
															?>
															<option value="<?= $value ?>" <?= $sel?> ><?=$value ?></option>
														<?php } ?>
												</select>
											</div>
											<div class="form-group">
												<label for="color">Color</label>
												<select class="form-control" id="color" name="color" >
													<option>Select Color</option>
													<?php $sel="";
														foreach ($colors as $key => $value) {
															$sel = ($value == $video_data[0]['sound_id']?"selected":"");
															?>
															<option value="<?= $value ?>" <?= $sel?> ><?=$value ?></option>
														<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="tc_begin">TC Begin</label>
												<input type="text" class="form-control" id="tc_begin" name="tc_begin" placeholder="00:00:00">	
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="tc_end">TC End</label>
												<input type="text" class="form-control" id="tc_end"  name ="tc_end" placeholder="00:00:00">	
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label for="duration">Duration</label>
												<input type="text" class="form-control" id="duration" name="duration" placeholder="00:00:00">	
											</div>
											<div class="form-group">
												<label for="video_description">Description</label>
												<textarea class="form-control" id="video_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
											</div>
											<div class="form-group">
												<label for="tags">Tags</label>
												<select class="newvideotags" name="tags[]" id="tags" multiple="multiple" required>
												<?php $sel="";
														foreach ($tags as $key => $value) {
															$sel = ($value['tag_id'] == $video_data[0]['tag_id']?"selected":"");
															?>
															<option value="<?= $value['tag_id']?>" <?= $sel?> ><?=$value['tag_name']?></option>
														<?php } ?>
												</select>
											</div>
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" name="meta_title" placeholder="Early 19th Century">	
											</div>
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
											</div>

											<div class="row target-theme-row">
											<div class="col-sm-5">
												<div class="form-group">
												<label for="theme_id">Select Target Theme</label>
												<select class="form-control " id="theme_id0" name="theme_id[]" onchange="getSubtheme(0)">
													<option value="" >Select Theme</option>
													<?php $sel="";
														foreach ($themes as $key => $value) {
															$sel = ($value['theme_id'] == $video_data[0]['theme_id']?"selected":"");
															?>
															<option value="<?= $value['theme_id']?>" <?= $sel?> ><?=$value['theme_name']?></option>
														<?php } ?>
												</select>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="form-group">
												<label for="sub_theme_id">Select Sub Theme</label>
												<select class="form-control newvideotags" id="sub_theme_id0" name="sub_theme_id[]"  multiple="multiple">
													<option>Select Sub Theme</option>
													
												</select>
												</div>
											</div>											
										</div>
										<div id="addmore-video-select">
	
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<a href="#/" class="btn btn-primary btn-sm addmore-video-theme">Add More</a>
												</div>
											</div>
											<div class="col-sm-12">
													<div class="switch-field">
														<?php $active = 'checked';
															 $in_active= '';
														if(!empty($theme_data)){
															if($theme_data[0]['status']=='Active'){
																$active = "checked";
															}else{
																$in_active = "checked";
															} 
														}?>
													<input type="radio" id="activate-theme-three" name="video-status" value="Active" <?= $active?>/>
													<label for="activate-theme-three">Yes</label>
													<input type="radio" id="activate-theme-four" name="video-status" value="In-active"  <?= $in_active?>/>
													<label for="activate-theme-four">No</label>
												</div>
												<div class="form-group">
													<button class="btn btn-black">Finish</button>
													<button class="btn btn-cancel btn-border">Cancel</button>
												</div>
											</div>
										</div>
										</div>		
									</div>
									</div>									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>							
</div>
<!-- 
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script> -->
<script>



$(document).ready(function(){
	$('.addmore-location').click(function () {
		var row_cnt = $('.addmore-location-select').length;
		row_cnt = row_cnt+1 ;
		$('#addmore-location-select').append(
			'<div class="row addmore-location-select">'+
				'<div class="col-md-4">'+
					'<div class="form-group">'+
					'<label for="exampleFormControlSelect'+row_cnt+'">Country</label>'+
					'<select class="form-control" id="country'+row_cnt+'" name="country[]" required  >'+
					'<option value="">Select Country</option>'+
						<?php $sel="";
						foreach ($countries as $key => $value) {
							if(!empty($video_data)){
								$sel = ($value['country_id'] == $video_data[0]['country_id']?"selected":"");
							}?>
						'<option value="<?= $value['country_id']?>" <?= $sel?> > <?=$value['country_name']?></option>'+
						<?php } ?>
					'</select>'+
					'</div>'+
				'</div>'+
				'<div class="col-md-4">'+
					'<div class="form-group">'+
						'<label for="exampleFormControlSelect'+row_cnt+'">Location</label>'+
						'<input type="text" class="form-control" id="location'+row_cnt+'"  onkeyup="getLocation('+row_cnt+')" name="location[]" placeholder="Enter Location">'+
                '</div>'+
					'</div>'+
					'<div class="col-sm-4">'+
						'<a href="#/"  class="btn btn-primary btn-xs remove-video-location">Remove</a>'+
					'</div>'+
				'</div>')
				
		});
		$('body').on('click', '.remove-video-location', function() {
			$(this).closest('.addmore-location-select').hide();       
		});


	$('.addmore-video-theme').click(function(){
		var row_cnt = '';
		row_cnt = $('.addmore-video-select').length;
		row_cnt = row_cnt+1 ;
		$('#addmore-video-select').append(
				'<div class="row addmore-theme">'+
				'<div class="col-sm-5">'+
					'<div class="form-group">'+
					'<label for="theme_id">Select Target Theme</label>'+
					'<select class="form-control " id="theme_id'+row_cnt+'" name="theme_id[]" onchange="getSubtheme('+row_cnt+')">'+
						'<option value="">Select Theme</option>'+
						<?php $sel="";
							foreach ($themes as $key => $value) {
								if(!empty($video_data) && isset($video_data)){
									$sel = ($value['theme_id'] == $video_data[0]['theme_id']?"selected":"");
								}
								?>
								'<option value="<?= $value['theme_id']?>" <?= $sel?> ><?=$value['theme_name']?></option>'+
							<?php } ?>
					'</select>'+
					'</div>'+
				'</div>'+

				'<div class="col-sm-5">'+
					'<div class="form-group">'+
					'<label for="sub_theme_id">Select Sub Theme</label>'+
					'<select class="form-control newvideotags" id="sub_theme_id'+row_cnt+'" name="sub_theme_id[]"  multiple="multiple">'+
						'<option>Select Sub Theme</option>'+
					'</select>'+
					'</div>'+
				'</div>'+
				'<div class="col-sm-2">' +
				'<a href="#/" class="btn btn-primary btn-xs remove-video-theme">Remove</a>' +
				'</div>' +
				'</div>')
				$('.newvideotags').select2();
				
	});
		$('body').on('click', '.remove-video-theme', function() {
			$(this).closest('.addmore-theme').hide();       
		});

});



	

function getLocation(id){
	var  location_name = $("#location"+id).val();
	var  country_id = $("#country"+id).val();
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	// if(location_name && country_id){
		// console.log(country_id);
		// $.ajax({
		// url: "<?= base_url('video/getLocation')?>",
		// type: 'POST',
		// dataType: 'json',
		// data:{id,location_name,country_id,[csrfName]: csrfHash },
		// 	success: function(result){
		// 		// $("#div1").html(result);
		// 		console.log($result);
		// 	}
		// });		

		$("#location"+id).typeahead({
			source: function (query, result) {
				// console.log(query);
                $.ajax({
                    url: "<?= base_url('video/getLocation')?>",
					data:{id,location_name,country_id,[csrfName]: csrfHash },
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
						// console.log(data);
						result($.map(data, function (item) {
							// console.log(item);
							return item;

                        }));
                    }
                });
            }
        });
		// $('#location0').tokenfield({
		// autocomplete: {
		// 	source: ['red','blue','green','yellow','violet','brown','purple','black','white'],
		// 	delay: 100
		// },
		// showAutocompleteOnFocus: true
		// })


	// }else{
	// 	alert("select Country First");
	// }
	
}
function getSubtheme(row_no){
	console.log(row_no);
	var theme_id = $("#theme_id"+row_no).val();
	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
    var csrfHash = $('.txt_csrfname').val(); // CSRF hash
	if(theme_id){
		$.ajax({
			url: "<?= base_url('video/getSubtheme')?>",
			data:{theme_id,[csrfName]: csrfHash },
			dataType: "json",
			type: "POST",
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#sub_theme_id"+row_no).html("<option value='' disabled> Select Sub Themes</option>"+res['option']);
						$("#sub_theme_id"+row_no).select2();
					}else{
						$("#sub_theme_id"+row_no).html("<option value='' disabled> Select Sub Themes</option>");
						$("#sub_theme_id"+row_no).select2();
					}
				}else{	
					$("#sub_theme_id"+row_no).html("<option value='' disabled> Select Sub Themes</option>");
						$("#sub_theme_id"+row_no).select2();
				}
			}
		});
	}
}




// use for form submit
var vRules = 
{
	vimeourl:{required:true},
	videotitle:{required:true},
	reelno:{required:true},
	short_description:{required:true},
	long_description:{required:true},
	'location[]':{required:true},
	'country[]':{required:true},
	year:{required:true},
	sound:{required:true},
	color :{required:true},
	tc_begin :{required:true},
	tc_end :{required:true},
	duration :{required:true},
	video_description :{required:true},
	tags:{required:true},
	theme_id:{required:true},
	meta_title:{required:true},
	meta_description:{required:true},

};
var vMessages = 
{
	vimeourl:{required:"Please Enter Vimeo URL."},
	videotitle:{required:"Please Enter Video Title."},
	reelno:{required:"Please Enter Reel No"},
	short_description:{required:"Please Enter Short Description"},
	long_description:{required:"Please Enter Long Description"},
	'location[]' :{required:"Please selection location"},
	'country[]' :{required:"Please selection location"},
	year:{required:"Select year"},
	sound:{required:"Select Sound"},
	color :{required:"Select color"},
	tc_begin :{required:"Please Enter TC Begin"},
	tc_end :{required:"Please Enter TC End"},
	duration :{required:"Please Enter Duration here"},
	video_description :{required:"Please Enter video Description"},
	tags :{required:"select tags"},
	theme_id :{required:"select Theme"},
	meta_title:{required:"Please Enter Meta Title."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-video").validate({
	// ignore:[],
	// rules: vRules,
	// messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('video/submitform')?>";
		$("#form-video").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn-primary").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('video')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

</script>