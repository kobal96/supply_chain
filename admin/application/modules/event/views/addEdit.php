<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/css/medium-editor-insert-plugin-frontend.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/css/medium-editor-insert-plugin.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/css/medium-editor.min.css" />

<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Event</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('event') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-article" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
									<input name="id" id="id" value="<?= (!empty($event_data[0]['id'])?$event_data[0]['id']:"") ?>" type="hidden" >

									<div class="basicinfo">

									<label for="">Basic Information</label><hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Title">Title</label>
												<input type="text" class="form-control" id="title" name="title" value="<?= (!empty($event_data[0]['title'])?$event_data[0]['title']:"") ?>" placeholder="">	
											</div>
										</div>
										
										<div class="col-sm-6">
											<div class="form-group">
												<label for="tag">Event Date</label>												
													<input type="date" name="event_date" id="event_date" value="<?= (!empty($event_data[0]['event_date'])?date('Y-m-d',strtotime($event_data[0]['event_date'])):"") ?>" >				
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="link">Register Link URL</label>
												<input type="text" class="form-control" id="link" name="link" value="<?= (!empty($event_data[0]['link'])?$event_data[0]['link']:"") ?>" placeholder="">	
											</div>
										</div>


									</div>
									
									<label for="">Meta Information</label><hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($event_data[0]['meta_title'])?$event_data[0]['meta_title']:"") ?>"  name="meta_title" placeholder="">	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="slug">URL Slug</label>
												<input type="text" class="form-control" id="slug" value="<?= (!empty($event_data[0]['slug'])?$event_data[0]['slug']:"") ?>"  name="slug" placeholder="">	
											</div>
										</div>
									</div>


									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_keyword">Meta Keywords</label>
												<textarea class="form-control" id="meta_keyword" name="meta_keyword" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($event_data[0]['meta_keyword'])?$event_data[0]['meta_keyword']:"") ?></textarea>	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($event_data[0]['meta_description'])?$event_data[0]['meta_description']:"") ?></textarea>	
											</div>
										</div>
									</div>
								
									<hr>	

									<div class="row">
										<div class="col-sm-6">
											<div class="form-check">
												<label>Publish Status</label><br>
												<div class="switch-field">
													<?php
													if(!empty($event_data)){
														if($event_data[0]['status']=='Published'){
															$published = "checked";
														}else{
															$draft = "checked";
														} 
													}else{
														$draft='checked';
													}

													?>
														
													<input type="radio" id="published" name="event_status" value="Published" <?= @$published?> >
													<label for="published">Published</label>
													<input type="radio" id="draft" name="event_status" value="Draft" <?= @$draft?> >
													<label for="draft">Draft</label>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-check">
												<label>Approval Status</label><br>
												<div class="switch-field">
														<?php
														if(!empty($event_data)){
															if($event_data[0]['admin_status']=='Approved'){
																$approved = "checked";
															}else{
																$pending = "checked";
															} 
														}else{
															$pending= 'checked';
														}
														?>
													<input type="radio" id="approved" name="admin_status" value="Approved" <?= @$approved?> >
													<label for="approved">Approved</label>
													<input type="radio" id="pending" name="admin_status" value="Pending" <?= @$pending?> >
													<label for="pending">Pending</label>
												</div>
											</div>
										</div>
									</div>

									</div>

									<div class="descript" style="display: none;">

									<label for="">Event Description</label><hr>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="event_description">Article Description</label>
												<textarea class="form-control event_description" id="event_description" name="event_description" rows="15" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($event_data[0]['description'])?$event_data[0]['description']:"") ?></textarea>	
											</div>
										</div>
									</div>

									</div>					
											
											
												
									<div class="form-group">
										<button type="button" id="next" class="btn btn-black">Save & Proceed</button>
										<button type="submit" id="save" class="btn btn-black" style="display: none;">Create</button>
										<a href="<?= base_url('event')?>" class="btn btn-cancel btn-border">Cancel</a>
										<button type="button" id="previous" class="btn btn-black" style="float: right;display: none;">Previous</button>
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<!-- <script src="//cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.12/handlebars.runtime.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sortable/0.9.13/jquery-sortable-min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery.ui.widget@1.10.3/jquery.ui.widget.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.iframe-transport/1.0.1/jquery.iframe-transport.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.28.0/js/jquery.fileupload.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/js/medium-editor.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/js/medium-editor-insert-plugin.min.js"></script>

<script>
        var editor = new MediumEditor('.event_description', {
            buttonLabels: 'fontawesome',
            toolbar: {
		        allowMultiParagraphSelection: true,
		        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist', 'orderedlist'],
		        diffLeft: 0,
		        diffTop: -10,
		        firstButtonClass: 'medium-editor-button-first',
		        lastButtonClass: 'medium-editor-button-last',
		        relativeContainer: null,
		        standardizeSelectionStart: false,
		        static: false,
		        align: 'center',
		        sticky: false,
		        updateOnEmptySelection: false
		    }
        });

        $(function () {
            $('.event_description').mediumInsert({
                editor: editor
            });
        });
    </script>

<script>
		
$(document).ready(function(){

    $('#previous').click(function(){
        current_fs = $('.descript');
        next_fs = $('.basicinfo');
        next_fs.show();
        current_fs.hide();
        $('#next').show();
        $('#save').hide();   
        $('#previous').hide();
    });

    $("#next").click(function(){
        var form = $("#form-article");
        form.validate({
            rules: {
                title:{required:true},
				event_date:{required:true},
				meta_title:{required:true},
				slug:{required:true},
				meta_keyword:{required:true},
				meta_description:{required:true},
            },
            messages: {
            	title:{required:"Please Enter Article Name."},
				event_date:{required:"Please Select Event Date."},
				meta_title:{required:"Please Enter Meta Title."},
				slug:{required:"Please Enter URL Slug."},
				meta_keyword:{required:"Please Enter Meta Keywords."},
				meta_description:{required:"Please Enter Meta Description."}, 
            }
            
        });
        if (form.valid() == true){
        	$('.descript').show();
            $('.basicinfo').hide();
            $('#next').hide();
            $('#save').show();   
            $('#previous').show();   
        }
    });


});


function removeFile(type,id){
var r = confirm("Are you sure you want to delete this image?");
	if (r == true) {
		if(type && id){
			var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
			var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				$.ajax({
					url: "<?= base_url('article/removeFile')?>",
					data:{type,id,[csrfName]: csrfHash },
					dataType: "json",
					type: "POST",
					success: function(response){
						if(response.success){
							swal("done", response.msg, {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							}).then(
							function() {
								location.reload();
							});
						}else{	
							swal("Failed", response.msg, {
								icon : "error",
								buttons: {        			
									confirm: {
										className : 'btn btn-danger'
									}
								},
							});
						}
					}
				});
		}else{
			alert("OOPS somethings went wrong")
		}
	}
}

// use for form submit
var vRules = 
{
	title:{required:true},
	event_date:{required:true},
	meta_title:{required:true},
	slug:{required:true},
	meta_keyword:{required:true},
	meta_description:{required:true},
};
var vMessages = 
{
	title:{required:"Please Enter Article Name."},
	event_date:{required:"Please Select Event Date."},
	meta_title:{required:"Please Enter Meta Title."},
	slug:{required:"Please Enter URL Slug."},
	meta_keyword:{required:"Please Enter Meta Keywords."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-article").validate({
	ignore:[],
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('event/submitform')?>";
		$("#form-article").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('event')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>