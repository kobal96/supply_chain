
<body>
	<div class="wrapper">
    
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						
						<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">All Articles</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('article/addEdit')?>" class="btn btn-primary ff-page-title-btn ff-page-title-btn">
								Add Article
							</a>
						</div>
					</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="article-listing" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>#</th>
																<th>Title</th>	
																<th>Edition</th>
																<th>Author</th>
																<th>Status</th>
																<th>Featured</th>
																<!-- <th>Approval Status</th>																 -->
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
															<th>#</th>
																<th>Title</th>	
																<th>Edition</th>
																<th>Author</th>
																<th>Status</th>
																<th>Featured</th>
																<!-- <th>Approval Status</th>																 -->
																<th>Actions</th>												
															</tr>
														</tfoot>
														<tbody>
														<?php
														if(!empty($article_data) && isset($article_data))	{
															$cnt = 0;
															foreach ($article_data as $key => $value) {
																	$cnt= ++$cnt?>
																	<tr>														
																		<td>#<?=$cnt?></td>
																		<td><?= $value['title']?></td>
																		<td><?= (!empty($value['edition_name'])?$value['edition_name']:"Online")?></td>

																		<td><?= $value['author_name']?></td>	
																				
																		<!-- <td><img src="<?= FRONT_URL.'/images/article_image/'.(!empty($value['image'])?$value['image']:"default-img.jpg")?>"class="img-thumbnail" alt="Cinque Terre" width="100" height="100"></td>	 -->
																		<!-- <td><?= $value['status']?></td>	 -->
																		
																		
																		<td>
																		<?php if($value['status'] == 'Draft' && $value['admin_status'] == 'Pending'){
																					echo "Draft";
																		 }else if($value['status'] == 'Draft' && $value['admin_status'] == 'Approved'){
																					echo "Draft  ";
																		 }elseif ($value['status'] == 'Published' && $value['admin_status'] == 'Pending') {
																					echo "For review ";
																		 }elseif ($value['status'] == 'Published' && $value['admin_status'] == 'Approved') {
																					echo	"Published ";
																		 }?>
																		 </td>
																		 <td><?= $value['featured']?></td>	
																		<!-- <td><?= $value['admin_status']?></td>		 -->

																		<td>
																			<div class="form-button-action">
																				<a href="<?= base_url('article/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-primary ff-page-title-btn  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="<?= base_url('article/view_comment?text='.rtrim(strtr(base64_encode("id=".$value['id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-link btn-primary" data-original-title="View Comments ">
																					<i class="fa fa-eye"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" onclick="delete_row(<?=$value['id']?>)" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
															<?php }
														}?>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#article-listing').DataTable({
				"pageLength": 10,
				"ordering": true,
				"aaSorting": [],
				columnDefs: [{
				orderable: false,
				targets: [0,1,2,3,6]
				}]
			});			
		});


		function delete_row(id){
			var csrfName = "<?= $this->security->get_csrf_token_name()?>"; // Value specified in $config['csrf_token_name']
			var csrfHash = "<?= $this->security->get_csrf_hash()?>"; // CSRF hash
			if(id ){
				var r = confirm("Are you sure?");
				if (r == true) {
					$.ajax({
						url: "<?= base_url('article/delete_row')?>",
						data:{id,[csrfName]: csrfHash },
						dataType: "json",
						type: "POST",
						success: function(response){
						
							if(response.success){
								swal("OK", response.msg, {
									icon : "success",
									buttons: {        			
										confirm: {
											className : 'btn btn-success'
										}
									},
								}).then(
								function() {
									location.reload();
								});
							}else{	
								swal("Failed", response.msg, {
									icon : "error",
									buttons: {        			
										confirm: {
											className : 'btn btn-danger'
										}
									},
								});
							}
						}
					});
				}
			}
		}
	</script>
</body>
</html>
