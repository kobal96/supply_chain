<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/css/medium-editor-insert-plugin-frontend.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/css/medium-editor-insert-plugin.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/css/medium-editor.min.css" />

<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Change Comment Status</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="#/"  onclick="window.history.go(-1); return false;" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-comment" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
									<input name="id" id="id" value="<?= (!empty($comment_data[0]['id'])?$comment_data[0]['id']:"") ?>" type="hidden" >

									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_title">Comments</label>
                                                <textarea class="form-control" name="comment" readonly    id="comment" cols="30" rows="5">
                                                     <?= (!empty ($comment_data[0]['comment'])?$comment_data[0]['comment']:"") ?>
                                                </textarea>
											</div>
										</div>
									</div>	

									<div class="row">
										<div class="col-sm-6">
											<div class="form-check">
												<label>Publish Status</label><br>
												<div class="switch-field">
														<?php $published =  '';
															  $draft='';
														if(!empty($comment_data)){
															if($comment_data[0]['status']=='Published'){
																$published = "checked";
															}else{
																$draft = "checked";
															} 
														}?>
														
													<input type="radio" id="published" name="comment_status" value="Published" <?= $published?>/>
													<label for="published">Published</label>
													<input type="radio" id="draft" name="comment_status" value="Draft"  <?= $draft?>/>
													<label for="draft">Draft</label>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-check">
												<label>Approval Status</label><br>
												<div class="switch-field">
														<?php $approved = '';
															 $pending= '';
														if(!empty($comment_data)){
															if($comment_data[0]['admin_status']=='Approved'){
																$approved = "checked";
															}else{
																$pending = "checked";
															} 
														}?>
													<input type="radio" id="approved" name="admin_status" value="Approved" <?= $approved?>/>
													<label for="approved">Approved</label>
													<input type="radio" id="pending" name="admin_status" value="Pending"  <?= $pending?>/>
													<label for="pending">Pending</label>
												</div>
											</div>
										</div>
									</div>

									
									<div class="form-group">
										<button type="submit" id="save" class="btn btn-black">Change Status</button>
										<a href="#/"  onclick="window.history.go(-1); return false;" class="btn btn-cancel btn-border">Cancel</a>
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<!-- <script src="//cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.12/handlebars.runtime.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sortable/0.9.13/jquery-sortable-min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery.ui.widget@1.10.3/jquery.ui.widget.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.iframe-transport/1.0.1/jquery.iframe-transport.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.28.0/js/jquery.fileupload.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/js/medium-editor.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/js/medium-editor-insert-plugin.min.js"></script>

<script>
        var editor = new MediumEditor('.article_description', {
            buttonLabels: 'fontawesome',
            toolbar: {
		        allowMultiParagraphSelection: true,
		        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist', 'orderedlist'],
		        diffLeft: 0,
		        diffTop: -10,
		        firstButtonClass: 'medium-editor-button-first',
		        lastButtonClass: 'medium-editor-button-last',
		        relativeContainer: null,
		        standardizeSelectionStart: false,
		        static: false,
		        align: 'center',
		        sticky: false,
		        updateOnEmptySelection: false
		    }
        });

        /*var editor = new MediumEditor('.article_description', {
          paste: {
            forcePlainText: false, // otherwise pasting doesn't work
          },
        });*/

        $(function () {
            $('.article_description').mediumInsert({
                editor: editor
            });
        });
    </script>
<script>

	$("input[type='radio']").on("change",function(){
            var radioValue = $("input[name='article_online']:checked").val();
            if(radioValue){
				if(radioValue == "NO"){
					$("#magazine_edition_div").show();
					$('#edition_id').prop('required', true);
				}else{
					$("#magazine_edition_div").hide();
					$("#edition_id").val('');
					$('#edition_id').prop('required', false);
				}
                // alert("Your are a - " + radioValue);
            }
		});
		
$(document).ready(function(){

});


// use for form submit
var vRules = 
{
	title:{required:true},
	author_id:{required:true},
	// edition_id:{required:true},
	"category[]":{required:true},
	"tag[]":{required:true},
	slug:{required:true},
	featured_month:{required:true},
	meta_keyword:{required:true},
	meta_title:{required:true},
	meta_description:{required:true},
};
var vMessages = 
{
	title:{required:"Please Enter Article Name."},
	author_id:{required:"Please select Author Name."},
	// edition_id:{required:"Please Select Edition."},
	"category[]":{required:"Please Select Categories ."},
	"tag[]":{required:"Please Select Tags ."},
	slug:{required:"Please Enter URL Slug."},
	featured_month:{required:"Please Select featured Month"},
	meta_keyword:{required:"Please Enter Meta Keywords."},
	meta_title:{required:"Please Enter Meta Title."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-comment").validate({
	// ignore:[],
	// rules: vRules,
	// messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('article/change_comment_status')?>";
		$("#form-comment").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
                        history.back()
						// window.location = "<?= base_url('article')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>