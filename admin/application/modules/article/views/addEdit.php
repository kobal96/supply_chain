<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/css/medium-editor-insert-plugin-frontend.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/css/medium-editor-insert-plugin.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/css/medium-editor.min.css" />

<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Article</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('article') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-article" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
									<input name="id" id="id" value="<?= (!empty($article_data[0]['id'])?$article_data[0]['id']:"") ?>" type="hidden" >

									<div class="basicinfo">

									<label for="">Basic Information</label><hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Title">Title</label>
												<input type="text" class="form-control" id="title" name="title" value="<?= (!empty($article_data[0]['title'])?$article_data[0]['title']:"") ?>" placeholder="">	
											</div>
										</div>
										
										<div class="col-sm-6">
											<div class="form-group">
												<label for="author">Author</label>
												<input type="text" class="form-control" id="author" name="author" value="<?= (!empty($article_data[0]['author'])?$article_data[0]['author']:"") ?>" placeholder="">	
												<!-- <select class="form-control form-control " id="author" name="author"> 
													<option value="">Select Author</option>
													<?php
													$sel="";
													foreach ($author as $key => $value) {
														if(!empty($article_data)){
															$sel = ($value['id'] == $article_data[0]['author']?"selected":"");
														}
														?>
														<option value="<?= $value['id']?>" <?= $sel?> ><?= $value['first_name'].' '.$value['last_name']?></option>
													<?php }	
													?>
												</select> -->
											</div>
										</div>

										

										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_title">ONLINE</label>
												<br>
												<label class="radio-inline">
													<input type="radio" name="article_online" value="YES" <?=((!empty($article_data[0]['article_online']) &&  $article_data[0]['article_online'] == 'YES'))?"checked":""?> required > Yes					
												</label>	
												<label class="radio-inline">
													<input type="radio"  name="article_online" value="NO"  <?=((!empty($article_data[0]['article_online']) &&  $article_data[0]['article_online'] == 'NO'))?"checked":""?>  required> No
												</label>	
											</div>
										</div>					

										<div class="col-sm-6" id="magazine_edition_div" style="display:none;">
											<div class="form-group">
												<label for="edition_id">Select Edition</label>
												<select class="form-control form-control " id="edition_id" name="edition_id" > 
												<option value="">Select Edition</option>
													<?php
													$sel="";
													foreach ($editions as $key => $value) {
														if(!empty($article_data)){
															$sel = ($value['id'] == $article_data[0]['edition_id']?"selected":"");
														}
														?>
														<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>
													<?php }	
													?>
												</select>
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="category">Category</label>												
													<select class="newvideotags" name="category[]" id="category" multiple="multiple" >	
														<?php $sel="";	
															foreach ($categories as $key => $value) {														
																if(!empty($categories)){		
																	if(!empty($article_category_data)){
																		$sel = (in_array($value['id'],$article_category_data)?"selected":" ");
																		}	
																	}?>												
																	<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>								
														<?php } ?>
													</select>						
											</div>
										</div>


										<div class="col-sm-6">
											<div class="form-group">
												<label for="tag">Tags</label>												
													<select class="newvideotags" name="tag[]" id="tag" multiple="multiple" >	
														<?php $sel="";	
															foreach ($tags as $key => $value) {														
																if(!empty($tags)){		
																	if(!empty($article_tag_data)){
																		$sel = (in_array($value['id'],$article_tag_data)?"selected":" ");
																		}	
																	}	?>											
																	<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>								
														<?php } ?>
													</select>						
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="featured_month">Featured for the month ?</label>
												<select class="form-control form-control" id="featured_month" name="featured_month"> 
												<option value="">Select Featured for the month</option>
												<option value="Yes"<?= (!empty($article_data[0]['month_featured']) && $article_data[0]['month_featured'] == 'Yes' ?"selected":"") ?>>Yes</option>
												<option value="No" <?php  (!empty($article_data[0]['month_featured']) && $article_data[0]['month_featured'] == 'No' ?"selected":"")?>>No</option>
													
												</select>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="article_date">Article Date</label>
												<input type="text" class="form-control" id="article_date" name="article_date" value="<?= (!empty($article_data[0]['article_date'])?date('d-m-Y',strtotime($article_data[0]['article_date'])):"") ?>" placeholder="Enter Article Date ">
											</div>
										</div>
									</div>
									<label for="">Upload Image</label><hr> 
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="articlefile">Article Image</label>
												<?php $req= '';
												if(empty($article_data[0]['image'])){
													$req ='required'; 
													?>
														<br><label for="articlefile" class="btn btn-primary">Upload Image </label>
												<?php }else{?>
												<br>
													<img src="<?= FRONT_URL.'/images/article_image/'.$article_data[0]['image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
													<input name="pre_articlefile_name" id="pre_articlefile_name" value="<?= (!empty($article_data[0]['image'])?$article_data[0]['image']:"") ?>" type="hidden">
													<a href="#" class="btn btn-danger del-celerity" onclick="removeFile('IMAGE','<?= $article_data[0]['id']?>')"></a>
													<br><label  for="articlefile" class="btn btn-primary">Replace Image </label>
												<?php }?>
												
												<input type="file" style="display:none" class="form-control-file" id="articlefile" name="articlefile" <?= $req;?> >
											</div>
										</div>

									</div>
									<hr>	
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="article_description">Article Description</label>
												<textarea class="form-control article_description" id="article_description" name="article_description" rows="15" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($article_data[0]['description'])?$article_data[0]['description']:"") ?></textarea>	
											</div>
										</div>
									</div>
									
									
									<label for="">Meta Information</label><hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($article_data[0]['meta_title'])?$article_data[0]['meta_title']:"") ?>"  name="meta_title" placeholder="">	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="slug">URL Slug</label>
												<input type="text" class="form-control" id="slug" value="<?= (!empty($article_data[0]['slug'])?str_replace('-', ' ',$article_data[0]['slug']):"") ?>"  name="slug" placeholder="">	
											</div>
										</div>
									</div>


									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_keyword">Meta Keywords</label>
												<textarea class="form-control" id="meta_keyword" name="meta_keyword" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($article_data[0]['meta_keyword'])?$article_data[0]['meta_keyword']:"") ?></textarea>	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($article_data[0]['meta_description'])?$article_data[0]['meta_description']:"") ?></textarea>	
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-check">
												<label>Publish Status</label><br>
												<div class="switch-field">
														<?php $published =  '';
															  $draft='';
														if(!empty($article_data)){
															if($article_data[0]['status']=='Published'){
																$published = "checked";
															}else{
																$draft = "checked";
															} 
														}else{
																$draft = "checked";
														}?>
														
													<input type="radio" id="published" name="article_status" value="Published" <?= $published?>/>
													<label for="published">Published</label>
													<input type="radio" id="draft" name="article_status" value="Draft"  <?= $draft?>/>
													<label for="draft">Draft</label>
												</div>
											</div>
										</div>

										<?php 
												if(!empty($article_data[0])){
													if($article_data[0]['created_by'] != $_SESSION['supply_chain_admin'][0]['id']){?>
														<div class="col-sm-6">
															<div class="form-check">
																<label>Approval Status</label><br>
																<div class="switch-field">
																		<?php $approved = '';
																			$pending= '';
																		if(!empty($article_data)){
																			if($article_data[0]['admin_status']=='Approved'){
																				$approved = "checked";
																			}else{
																				$pending = "checked";
																			} 
																		}else{
																			$pending = "checked";
																		}?>
																	<input type="radio" id="approved" name="admin_status" value="Approved" <?= $approved?>/>
																	<label for="approved">Approved</label>
																	<input type="radio" id="pending" name="admin_status" value="Pending"  <?= $pending?>/>
																	<label for="pending">Pending</label>
																</div>
															</div>
														</div>
													<?php }
												}
										?>				
										
									</div>

									</div>

									<!-- <div class="descript" style="display: none;">

									<label for="">Article Description</label><hr>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="article_description">Article Description</label>
												<textarea class="form-control article_description" id="article_description" name="article_description" rows="15" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($article_data[0]['description'])?$article_data[0]['description']:"") ?></textarea>	
											</div>
										</div>
									</div>

									</div>					 -->
											
											
												
									<div class="form-group">
										<!-- <button type="button" id="next" class="btn btn-black">Save & Proceed</button> -->
										<button type="submit" id="save" class="btn btn-black">Create</button>
										<a href="<?= base_url('article')?>" class="btn btn-cancel btn-border">Cancel</a>
										<!-- <button type="button" id="previous" class="btn btn-black" style="float: right;display: none;">Previous</button> -->
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<!-- <script src="//cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js"></script> -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.12/handlebars.runtime.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sortable/0.9.13/jquery-sortable-min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery.ui.widget@1.10.3/jquery.ui.widget.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.iframe-transport/1.0.1/jquery.iframe-transport.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.28.0/js/jquery.fileupload.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/js/medium-editor.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/medium-editor-insert-plugin/2.5.0/js/medium-editor-insert-plugin.min.js"></script>

<script>
 $(function () {
             $('#article_date').datepicker({
				dateFormat: 'dd-mm-yy'
			 });
         });
        var editor = new MediumEditor('.article_description', {
            buttonLabels: 'fontawesome',
            toolbar: {
		        allowMultiParagraphSelection: true,
		        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist', 'orderedlist'],
		        diffLeft: 0,
		        diffTop: -10,
		        firstButtonClass: 'medium-editor-button-first',
		        lastButtonClass: 'medium-editor-button-last',
		        relativeContainer: null,
		        standardizeSelectionStart: false,
		        static: false,
		        align: 'center',
		        sticky: false,
		        updateOnEmptySelection: false
		    }
        });

        /*var editor = new MediumEditor('.article_description', {
          paste: {
            forcePlainText: false, // otherwise pasting doesn't work
          },
        });*/

        $(function () {
            $('.article_description').mediumInsert({
                editor: editor
            });
        });
    </script>
<script>

	$("input[type='radio']").on("change",function(){
            var radioValue = $("input[name='article_online']:checked").val();
            if(radioValue){
				if(radioValue == "NO"){
					$("#magazine_edition_div").show();
					$('#edition_id').prop('required', true);
				}else{
					$("#magazine_edition_div").hide();
					$("#edition_id").val('');
					$('#edition_id').prop('required', false);
				}
                // alert("Your are a - " + radioValue);
            }
		});
		
$(document).ready(function(){
	<?php 
	if(!empty($article_data)){
		if($article_data[0]['article_online'] == 'NO'){?>
			$("#magazine_edition_div").show();
		<?php }
	}?>
    $('#previous').click(function(){
        current_fs = $('.descript');
        next_fs = $('.basicinfo');
        next_fs.show();
        current_fs.hide();
        $('#next').show();
        $('#save').hide();   
        $('#previous').hide();
    });

    $("#next").click(function(){
        var form = $("#form-article");
        form.validate({

			onfocusout: false, 
			invalidHandler: function(form, validator) 
			{ 
				var errors = validator.numberOfInvalids(); 
				if (errors) { validator.errorList[0].element.focus();
					}
			 },
			
            rules: {
                title:{required:true},
				author:{required:true},
				// edition_id:{required:true},
				slug:{required:true},
				featured_month:{required:true},
				meta_keyword:{required:true},
				meta_title:{required:true},
				meta_description:{required:true},
            },
            messages: {

            	title:{required:"Please Enter Article Name."},
				author:{required:"Please Enter Author Name."},
				// edition_id:{required:"Please Select Edition."},
				slug:{required:"Please Enter URL Slug."},
				featured_month:{required:"Please Select featured Month"},
				meta_keyword:{required:"Please Enter Meta Keywords."},
				meta_title:{required:"Please Enter Meta Title."},
				meta_description:{required:"Please Enter Meta Description."},
                
			},
			
			onfocusout: false,
			invalidHandler: function(form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {                    
					validator.errorList[0].element.focus();
				}
			}
            
        });
        if (form.valid() == true){
        	$('.descript').show();
            $('.basicinfo').hide();
            $('#next').hide();
            $('#save').show();   
            $('#previous').show();   
        }
    });


});


function removeFile(type,id){
var r = confirm("Are you sure?");
	if (r == true) {
		if(type && id){
			var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
			var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				$.ajax({
					url: "<?= base_url('article/removeFile')?>",
					data:{type,id,[csrfName]: csrfHash },
					dataType: "json",
					type: "POST",
					success: function(response){
						if(response.success){
							swal("done", response.msg, {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							}).then(
							function() {
								location.reload();
							});
						}else{	
							swal("Failed", response.msg, {
								icon : "error",
								buttons: {        			
									confirm: {
										className : 'btn btn-danger'
									}
								},
							});
						}
					}
				});
		}else{
			alert("OOPS somethings went wrong")
		}
	}
}

// use for form submit
var vRules = 
{
	title:{required:true},
	author:{required:true},
	// edition_id:{required:true},
	"category[]":{required:true},
	"tag[]":{required:true},
	slug:{required:true},
	featured_month:{required:true},
	meta_keyword:{required:true},
	meta_title:{required:true},
	meta_description:{required:true},
};
var vMessages = 
{
	title:{required:"Please Enter Article Name."},
	author:{required:"Please Enter Author Name."},
	// edition_id:{required:"Please Select Edition."},
	"category[]":{required:"Please Select Categories ."},
	"tag[]":{required:"Please Select Tags ."},
	slug:{required:"Please Enter URL Slug."},
	featured_month:{required:"Please Select featured Month"},
	meta_keyword:{required:"Please Enter Meta Keywords."},
	meta_title:{required:"Please Enter Meta Title."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-article").validate({
	ignore:[],
	rules: vRules,
	messages: vMessages,
	onfocusout: false,
	invalidHandler: function(form, validator) {
		var errors = validator.numberOfInvalids();
		if (errors) {                    
			validator.errorList[0].element.focus();
		}
	} ,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('article/submitform')?>";
		$("#form-article").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('article')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>