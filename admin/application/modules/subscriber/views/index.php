
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">Subscribers</h1> 
							</div>							
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>S.No</th>
																<th>Invoice No</th>
																<th>Username</th>
																<th>Email</th>															
																<th>Paid Date</th>															
																<th>Total</th>															
																<th>Payment Status</th>
																<!-- <th>Actions</th> -->															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>S.No</th>
																<th>Invoice No</th>
																<th>Username</th>
																<th>Email</th>															
																<th>Paid Date</th>															
																<th>Total</th>															
																<th>Payment Status</th>	
																<!-- <th>Actions</th> -->															
															</tr>
														</tfoot>
														<tbody>
												<?php
												if(isset($payment_data) && !empty($payment_data))	{
													$cnt = 0;
													foreach ($payment_data as $key => $value) {
													$cnt= ++$cnt?>
													<tr>														
														<td><?=$cnt?></td>
														<td><?= (!empty($value['invoice_no']))?$value['invoice_no']:'###';?></td>
														<td><?= $value['username']?></td>
														<td><?= $value['email']?></td>
														<td style="width: 125px;"><?= date('d-m-Y', strtotime($value['paid_date'])); ?></td>
														<td><?= $value['total']?></td>
														<td><?= $value['payment_status']?></td>	

														<!-- <td>
														<div class="form-button-action">
															<a href="<?= base_url('article/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-primary ff-page-title-btn  btn-edit" data-original-title="Edit">
																<i class="fa fa-edit"></i>
															</a>
														</div>
														</td> -->													
													</tr>
													<?php } } ?>

														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 10,
				"ordering": false,
			});			
		});
	</script>
</body>
</html>
