<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Edition extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('editionmodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['edition_data'] = $this->common->getData("tbl_magazine_editions",'*',$condition,"id","desc");
		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];
			$condition = "id ='".$id."' ";
			// $main_table = array("tbl_magazine_editions as tme", array("tme.*"));
			// $join_tables =  array();
			// $join_tables = array(
			// 					array("left", "tbl_similar_magazine as  tsm", "tsm.magazine_id = tme.id", array("tsm.magazine_id"))
			// 					);
		
			// $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tsm.magazine_id" => "DESC"),"",null); 
			//  // fetch query
			// $result['edition_data'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
			$result['edition_data'] = $this->common->getData("tbl_magazine_editions",'*',$condition);
			$condition = "magazine_id ='".$id."' ";	
			$magazine_similar_data = $this->common->getData("tbl_similar_magazine",'similar_magazine_id',$condition);		
			
			$similar_magazine_data = array();
			foreach ($magazine_similar_data as $key => $value) {
				array_push($similar_magazine_data,$value['similar_magazine_id']);
			}
			$result['similar_magazine_data'] = $similar_magazine_data;
		
		}	
		$condition = " admin_status ='Approved' ";
		$result['similar_magazine'] = $this->common->getData("tbl_magazine_editions",'id,title',$condition);

		
			// echo "<pre>";
			// print_r($result);
			// exit;
		//echo $id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

		
	function submitformtesting(){
		// codeigniter form validation 
		// $this->form_validation->set_rules('themename', 'Themename', 'trim|required|alpha');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');
		// $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|required');
		// $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|required');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');

		// if($this->form_validation->run()) {  
		// 	// true  if validation property sectifice properly
		// 	$themename = $this->input->post('themename');  
		// 	$mainvideo = $this->input->post('mainvideo'); 
		// 	$themename = $this->input->post('meta_title');  
		// 	$mainvideo = $this->input->post('meta_description'); 
		// 	$themename = $this->input->post('mainvideo');  
		// 	$mainvideo = $this->input->post('meta_title'); 
		// }else{  
		// 	// if validation is not proper
		// 	$this->index();  
		// } 
	}


	function removeFile(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = " id = ".$this->input->post('id');
			$exisiting_data =$this->common->getData("tbl_magazine_editions",'*',$condition);
			if(!empty($this->input->post('id')) && $this->input->post('type') == "PDF"){
				if(is_array($exisiting_data) && !empty($exisiting_data[0]['edition_pdf']) && file_exists(DOC_ROOT_FRONT."/images/edition_pdf/".$exisiting_data[0]['edition_pdf'])){
					unlink(DOC_ROOT_FRONT."/images/edition_pdf/".$exisiting_data[0]['edition_pdf']);
					$data['edition_pdf'] = '';
				}
			}else{
				if(is_array($exisiting_data) && !empty($exisiting_data[0]['image']) && file_exists(DOC_ROOT_FRONT."/images/edition_image/".$exisiting_data[0]['image'])){
					unlink(DOC_ROOT_FRONT."/images/edition_image/".$exisiting_data[0]['image']);
					$data['image'] = '';
				}
			}

			$condition = "id = '".$this->input->post('id')."' ";
			$result = $this->common->updateData("tbl_magazine_editions",$data,$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}	
		}			
	}

	public function submitForm(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "title = '".$this->input->post('title')."' ";
			if($this->input->post('id') && $this->input->post('id') > 0){
				$condition .= " AND  id != ".$this->input->post('id')." ";
			}			
			$check_name = $this->common->getData("tbl_magazine_editions",'*',$condition);

			// print_r($check_name);
			// exit;
			if(!empty($check_name[0]['id'])){
				echo json_encode(array("success"=>false, 'msg'=>'Edition Name Already Present!'));
				exit;
			}
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["editionfile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/edition_image/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['min_width']            = 1000;
                $config['min_height']           = 1000;
				// $config['allowed_types'] = '*';
				// $config['file_name']     = md5(uniqid("100_ID", true));
				$config['file_name']     = $_FILES["editionfile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("editionfile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);

				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('id'))){	
					$condition_image = " id = ".$this->input->post('id');
					$image =$this->common->getData("tbl_magazine_editions",'*',$condition_image);
					if(is_array($image) && !empty($image[0]['image']) && file_exists(DOC_ROOT_FRONT."/images/edition_image/".$image[0]['image']))
					{
						unlink(DOC_ROOT_FRONT."/images/edition_image/".$image[0]['image']);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_editionfile_name');
			}




			$magazine_pdf = "";
			if(isset($_FILES) && isset($_FILES["editionpdffile"]["name"])){
				 $config1 = array();
				$config1['upload_path'] = DOC_ROOT_FRONT."/images/edition_pdf/";
				// $config1['max_size']    = '2,000';
				$config1['allowed_types'] = 'pdf';
                // $config1['min_width']            = 1000;
                // $config1['min_height']           = 1000;
				// $config1['allowed_types'] = '*';
				// $config1['file_name']     = md5(uniqid("100_ID", true));
				$config1['file_name']     = $_FILES["editionpdffile"]["name"];
				$this->load->library('upload', $config1);
				if (!$this->upload->do_upload("editionpdffile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$magazine_pdf = $image_data['upload_data']['file_name']; 

				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('id'))){	
					$condition_image = " id = ".$this->input->post('id');
					$image =$this->common->getData("tbl_magazine_editions",'*',$condition_image);
					if(is_array($image) && !empty($image[0]['edition_pdf']) && file_exists(DOC_ROOT_FRONT."/images/edition_pdf/".$image[0]['edition_pdf']))
					{
						unlink(DOC_ROOT_FRONT."/images/edition_pdf/".$image[0]['edition_pdf']);
					}
				}
			}else{
				$magazine_pdf = $this->input->post('pre_editionpdffile_name');
			}

			$data['image'] = (!empty($thumnail_value)?$thumnail_value:"");
			$data['edition_pdf'] = (!empty($magazine_pdf)?$magazine_pdf:"");
			$data['title'] = $this->input->post('title');
			$data['magazine_description'] = $this->input->post('magazine_description');
			$data['volume'] = $this->input->post('volume');
			$data['issue_no'] = $this->input->post('issue_no');
			$data['issue_link'] = $this->input->post('issue_link');
			
			$data['slug'] = str_replace(' ', '-', strtolower(trim($this->input->post('slug'))));
			$data['meta_keyword'] = $this->input->post('meta_keyword');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['status'] = $this->input->post('edition_status');
			$data['admin_status'] = $this->input->post('admin_status');
			$data['updated_at'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('supply_chain_admin')[0]['id'];
			if(!empty($this->input->post('id'))){
				// print_r($data);
				$condition = "id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_magazine_editions",$data,$condition);
				if($result){
					$condition = "magazine_id = '".$this->input->post('id')."' ";
					$this->common->deleteRecord("tbl_similar_magazine",$condition);
					foreach ($_POST['similar_magazine'] as $key => $value) {
						$magazine_data = array();
						$magazine_data['magazine_id'] = $this->input->post('id');
						$magazine_data['similar_magazine_id'] = $value;
						$magazine_data['created_on'] = date("Y-m-d H:i:s");
						$magazine_data['created_at'] =  $this->session->userdata('supply_chain_admin')[0]['id'];
						$magazine_data['updated_on'] = date("Y-m-d H:i:s");
						$magazine_data['updated_at'] = $this->session->userdata('supply_chain_admin')[0]['id'];
						$this->common->insertData('tbl_similar_magazine',$magazine_data,'1');
					}
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('supply_chain_admin')[0]['id'];
				$result = $this->common->insertData('tbl_magazine_editions',$data,'1');
				if(!empty($result)){
					foreach ($_POST['similar_magazine'] as $key => $value) {
						$magazine_data = array();
						$magazine_data['magazine_id'] = $result;
						$magazine_data['similar_magazine_id'] = $value;
						$magazine_data['created_on'] = date("Y-m-d H:i:s");
						$magazine_data['created_at'] =  $this->session->userdata('supply_chain_admin')[0]['id'];
						$magazine_data['updated_on'] = date("Y-m-d H:i:s");
						$magazine_data['updated_at'] = $this->session->userdata('supply_chain_admin')[0]['id'];
						$this->common->insertData('tbl_similar_magazine',$magazine_data,'1');
					}
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/edition_image/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		$config1['width'] = 1000;
		$config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/edition_image/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
		}
}
?>