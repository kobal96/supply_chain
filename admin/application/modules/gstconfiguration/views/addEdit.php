
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Update GST Configuration</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('gstconfiguration') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
						<div class="card-body">
						<form class="" id="form-gst" method="post" enctype="multipart/form-data">
							
							<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
							<input name="id" id="id" value="<?= (!empty($gst_data[0]['id'])?$gst_data[0]['id']:"") ?>" type="hidden" >
							
							<label for="">Basic Information</label><hr>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="Title">Company Name</label>
										<input type="text" class="form-control" id="title" name="title" value="<?= (!empty($gst_data[0]['title'])?$gst_data[0]['title']:"") ?>" placeholder="Package Name">	
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<label for="gstin">GSTIN</label>
										<input type="text" class="form-control" id="gstin" name="gstin" value="<?= (!empty($gst_data[0]['gstin'])?$gst_data[0]['gstin']:"") ?>" placeholder="GSTIN">	
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="form-group">
										<label for="origin_state_id">Origin State</label>
										<select class="form-control form-control " id="origin_state_id" name="origin_state_id">
										<option value="" selected="selected" disabled>--- Select Origin State ---</option>
										<?php
										foreach ($origin_state_list as $key => $value) {
										 ?>

										 <option value="<?=$value['id'] ?>" <?= (isset($gst_data[0]['origin_state_id']) && $gst_data[0]['origin_state_id'] == $value['id']) ? "selected" :""; ?> ><?=$value['state_name'] ?></option>

										<?php 
											}
										 ?>
										
										</select>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<label for="company_phone">Company Phone</label>
										<input type="text" class="form-control" id="company_phone" name="company_phone" value="<?= (!empty($gst_data[0]['company_phone'])?$gst_data[0]['company_phone']:"") ?>" placeholder="Company Phone">	
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<label for="country">Country</label>
										<select class="form-control form-control " id="country" name="country">
										<option value="" selected="selected" disabled>--- Select Country ---</option>
										<?php
										foreach ($country_list as $key => $value) {
										 ?>

										 <option value="<?=$value['id'] ?>" <?= (isset($gst_data[0]['country']) && $gst_data[0]['country'] == $value['id']) ? "selected" :""; ?> ><?=$value['country_name'] ?></option>

										<?php } ?>
										</select>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<label for="state">State</label>
										<select class="form-control form-control " id="state" name="state">
										<option value="" selected="selected" disabled>--- Select State ---</option>
										<?php
										foreach ($state_list as $key => $value) {
										 ?>

										 <option value="<?=$value['id'] ?>" <?= (isset($gst_data[0]['state']) && $gst_data[0]['state'] == $value['id']) ? "selected" :""; ?> ><?=$value['state_name'] ?></option>

										<?php } ?>
										</select>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<label for="city">City</label>
										<select class="form-control form-control " id="city" name="city">
										<option value="" selected="selected" disabled>--- Select City ---</option>
										<?php
										foreach ($city_list as $key => $value) {
										 ?>

										 <option value="<?=$value['id'] ?>" <?= (isset($gst_data[0]['city']) && $gst_data[0]['city'] == $value['id']) ? "selected" :""; ?> ><?=$value['city_name'] ?></option>

										<?php } ?>
										
										</select>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<label for="company_pincode">Pincode</label>
										<input type="text" class="form-control" id="company_pincode" name="company_pincode" value="<?= (!empty($gst_data[0]['company_pincode'])?$gst_data[0]['company_pincode']:"") ?>" placeholder="Pincode">	
									</div>
								</div>
								
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="company_address">Company Address</label>
										<textarea class="form-control" id="company_address" name="company_address" rows="5" placeholder="Company Address"><?= (!empty($gst_data[0]['company_address'])?$gst_data[0]['company_address']:"") ?></textarea>	
									</div>
								</div>
								
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="articlefile">Existing Image</label>
										<?php $req= '';
										if(empty($gst_data[0]['company_logo'])){
											$req =''; 
										}else{?>
										<br>
											<img src="<?= FRONT_URL.'/images/gstconfig_image/'.$gst_data[0]['company_logo']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
											<input name="pre_articlefile_name" id="pre_articlefile_name" value="<?= (!empty($gst_data[0]['company_logo'])?$gst_data[0]['company_logo']:"") ?>" type="hidden">
											<!-- <a href="#" class="btn btn-danger" onclick="removeFile('IMAGE','<?= $gst_data[0]['id']?>')">  X </a> -->

										<?php }?>
										
										<!-- <input type="file" class="form-control-file" id="articlefile" name="articlefile" <?= $req;?> > -->
									</div>
								</div>

							</div>
							
							<hr>	

							<div class="row">
								<div class="col-sm-6">
									<div class="form-check">
										<label>Publish Status</label><br>
										<div class="switch-field">
												<?php
												if(!empty($gst_data)){
													if($gst_data[0]['status']=='Published'){
														$published = "checked";
													}else{
														$draft = "checked";
													} 
												}else{
													$draft='checked';
												}

												?>
												
											<input type="radio" id="published" name="package_status" value="Published" <?= @$published?>/>
											<label for="published">Published</label>
											<input type="radio" id="draft" name="package_status" value="Draft"  <?= @$draft?>/>
											<label for="draft">Draft</label>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-check">
										<label>Approval Status</label><br>
										<div class="switch-field">
												<?php
												if(!empty($gst_data)){
													if($gst_data[0]['admin_status']=='Approved'){
														$approved = "checked";
													}else{
														$pending = "checked";
													} 
												}else{
													$pending= 'checked';
												}
												?>
											<input type="radio" id="approved" name="admin_status" value="Approved" <?= @$approved?>/>
											<label for="approved">Approved</label>
											<input type="radio" id="pending" name="admin_status" value="Pending"  <?= @$pending?>/>
											<label for="pending">Pending</label>
										</div>
									</div>
								</div>
								
							</div>					
											
							<div class="form-group">
								<button  type="submit" class="btn btn-black">Create</button>
								<a href="<?= base_url('article')?>" class="btn btn-cancel btn-border">Cancel</a>
							</div>
						</form>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){

});


function removeFile(type,id){
var r = confirm("want to sure delete this file");
	if (r == true) {
		if(type && id){
			var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
			var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				$.ajax({
					url: "<?= base_url('package/removeFile')?>",
					data:{type,id,[csrfName]: csrfHash },
					dataType: "json",
					type: "POST",
					success: function(response){
						if(response.success){
							swal("done", response.msg, {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							}).then(
							function() {
								location.reload();
							});
						}else{	
							swal("Failed", response.msg, {
								icon : "error",
								buttons: {        			
									confirm: {
										className : 'btn btn-danger'
									}
								},
							});
						}
					}
				});
		}else{
			alert("OOPS somethings went wrong")
		}
	}
}

// use for form submit
var vRules = 
{
	title:{required:true},
	gstin:{required:true},
	origin_state_id:{required:true},
	company_phone:{required:true},
	country:{required:true},
	state:{required:true},
	city:{required:true},
	company_pincode:{required:true},
	company_address:{required:true},
};
var vMessages = 
{
	title:{required:"Please Enter Package Name."},
	gstin:{required:"Please Enter GSTIN."},
	origin_state_id:{required:"Please Select Origin State."},
	company_phone:{required:"Please Enter Company Phone."},
	country:{required:"Please Select Country ."},
	state:{required:"Please Select State ."},
	city:{required:"Please Select City ."},
	company_pincode:{required:"Please Enter Pincode."},
	company_address:{required:"Please Enter Company Address."},

};

$("#form-gst").validate({
	ignore:[],
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('gstconfiguration/submitform')?>";
		$("#form-gst").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('gstconfiguration')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>