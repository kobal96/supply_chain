<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller

{

	

	public function __construct(){

		// this is helper function  use to check whether the user is logged in or not file is written in helper folder inside application 

		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);

		parent::__construct();

	}



	public function index(){

		$this->load->view('main-header.php');

		$this->load->view('home/index.php');

		$this->load->view('footer.php');

	}

	

	public function logOut(){

		$this->session->unset_userdata('supply_chain_admin');

		redirect(base_url()); 



	}

	public function change_notification_status(){

		$data = array();
		$data['status'] = 'Read';
		$condition = "id = '".$this->input->post('notification_id')."' ";
		$result = $this->common->updateData("tbl_notifications",$data,$condition);
		if($result){
			echo json_encode(array('success'=>true, 'msg'=>'Status Change Successfully.'));
			exit;
		}
		else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
			exit;
		}
	}

}?>