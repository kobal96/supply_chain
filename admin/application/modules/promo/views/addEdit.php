
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title"><?= (!empty($promo_data[0]['id'])?"Update":"New") ?> Promo</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('promo') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-promo" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
									<input name="id" id="id" value="<?= (!empty($promo_data[0]['id'])?$promo_data[0]['id']:"") ?>" type="hidden" >
									
									<label for="">Basic Information</label><hr>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="Title">Promo Code</label>
												<input type="text" class="form-control" id="title" name="title" value="<?= (!empty($promo_data[0]['title'])?$promo_data[0]['title']:"") ?>" placeholder="Promo Code">	
											</div>
										</div>
										
										<div class="col-sm-6">
										<input id="discount_id" value="<?= (!empty($promo_data[0]['discount_type'])?$promo_data[0]['discount_type']:"") ?>" type="hidden" >
											<div class="form-group">
												<label for="discount_type">Discount Type</label>
												<select class="form-control form-control " id="discount_type" name="discount_type">
												<option value="" selected="selected" disabled>--- Select ---</option>
												<option value="percentage">% off</option>
												<option value="value">Value off</option>
												</select>
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="discount_value">Discount Value</label>
												<input type="text" class="form-control" id="discount_value" name="discount_value" value="<?= (!empty($promo_data[0]['discount_value'])?$promo_data[0]['discount_value']:"") ?>" placeholder="Discount Value">	
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="package_id">Package</label>
												<select class="form-control form-control " id="package_id" name="package_id">
												<option value="" selected="selected">--- Select Package ---</option>
												<?php
													if(!empty($package)){
													foreach ($package as $key => $value){
													?>
												<option value="<?= $value['id'] ?>" <?php if(isset($promo_data) && $promo_data[0]['package_id']==$value['id']){echo "selected";} ?> > <?= $value['title'] ?></option>
												<?php }	} ?>
												</select>
											</div>
										</div>
										
										<div class="col-sm-6">
											<div class="form-group">
												<label for="validity">Validity</label>
												<input type="text" class="form-control" id="validity" value="<?= (!empty($promo_data[0]['validity'])?date('m/d/Y', strtotime($promo_data[0]['validity'])):"") ?>"  name="validity" placeholder="Validity">	
											</div>
										</div>

									</div>
									
									<hr>	

									<div class="row">
										<div class="col-sm-6">
											<div class="form-check">
												<label>Publish Status</label><br>
												<div class="switch-field">
														<?php
														if(!empty($promo_data)){
															if($promo_data[0]['status']=='Published'){
																$published = "checked";
															}else{
																$draft = "checked";
															} 
														}else{
															$draft='checked';
														}

														?>
														
													<input type="radio" id="published" name="package_status" value="Published" <?= @$published?>/>
													<label for="published">Published</label>
													<input type="radio" id="draft" name="package_status" value="Draft"  <?= @$draft?>/>
													<label for="draft">Draft</label>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-check">
												<label>Approval Status</label><br>
												<div class="switch-field">
														<?php
														if(!empty($promo_data)){
															if($promo_data[0]['admin_status']=='Approved'){
																$approved = "checked";
															}else{
																$pending = "checked";
															} 
														}else{
															$pending= 'checked';
														}
														?>
													<input type="radio" id="approved" name="admin_status" value="Approved" <?= @$approved?>/>
													<label for="approved">Approved</label>
													<input type="radio" id="pending" name="admin_status" value="Pending"  <?= @$pending?>/>
													<label for="pending">Pending</label>
												</div>
											</div>
										</div>
										
									</div>					
											
											
												
									<div class="form-group">
										<button  type="submit" class="btn btn-black">Create</button>
										<a href="<?= base_url('promo')?>" class="btn btn-cancel btn-border">Cancel</a>
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
	$( function() {
	    $( "#validity" ).datepicker();
	  });

$(document).ready(function(){
	var discount_id = $("#discount_id").val();
	$("#discount_type").val(discount_id);

});


function removeFile(type,id){
var r = confirm("want to sure delete this file");
	if (r == true) {
		if(type && id){
			var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
			var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				$.ajax({
					url: "<?= base_url('package/removeFile')?>",
					data:{type,id,[csrfName]: csrfHash },
					dataType: "json",
					type: "POST",
					success: function(response){
						if(response.success){
							swal("done", response.msg, {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							}).then(
							function() {
								location.reload();
							});
						}else{	
							swal("Failed", response.msg, {
								icon : "error",
								buttons: {        			
									confirm: {
										className : 'btn btn-danger'
									}
								},
							});
						}
					}
				});
		}else{
			alert("OOPS somethings went wrong")
		}
	}
}

// use for form submit
var vRules = 
{
	title:{required:true},
	discount_type:{required:true},
	discount_value:{required:true},
	package_id:{required:true},
	validity:{required:true},
	
};
var vMessages = 
{
	title:{required:"Please Enter Package Name."},
	discount_type:{required:"Please Select Discount Type."},
	discount_value:{required:"Please Enter Discount Value."},
	package_id:{required:"Please Select Package ."},
	validity:{required:"Please Enter Validity."},

};

$("#form-promo").validate({
	ignore:[],
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('promo/submitform')?>";
		$("#form-promo").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('promo')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>