
<body>
	<div class="wrapper">
    
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						
						<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Promo</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('promo/addEdit')?>" class="btn btn-primary ff-page-title-btn ff-page-title-btn">
								Add Promo
							</a>
						</div>
					</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="article-listing" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>#</th>
																<th>Title</th>	
																<th>Package</th>
																<th>Discount Type</th>
																<th>Discount Value</th>
																<th>Validity</th>
																<th>Status</th>
																<th>Approval Status</th>																
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
															<th>#</th>
																<th>Title</th>	
																<th>Package</th>
																<th>Discount Type</th>
																<th>Discount Value</th>
																<th>Validity</th>
																<th>Status</th>
																<th>Approval Status</th>																
																<th>Actions</th>												
															</tr>
														</tfoot>
														<tbody>
														<?php
														if(!empty($promo_data) && isset($promo_data))	{
															$cnt = 0;
															foreach ($promo_data as $key => $value) {
																	$cnt= ++$cnt?>
																	<tr>														
																		<td>#<?=$cnt?></td>
																		<td><?= $value['title']?></td>
																		<td><?= $value['package']?></td>
																		<td><?= $value['discount_type']?></td>
																		<td><?= $value['discount_value']?></td>
																		<td><?= $value['validity']?></td>
																		<td><?= $value['status']?></td>
																		<td><?= $value['admin_status']?></td>	

																		<td>
																			<div class="form-button-action">
																				<a href="<?= base_url('promo/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-primary ff-page-title-btn  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				
																			</div>
																		</td>													
																	</tr>
															<?php }
														}?>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#article-listing').DataTable({
				"pageLength": 5,
				"ordering": false,
			});			
		});
	</script>
</body>
</html>
