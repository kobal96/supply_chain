<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Promo extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('promomodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$main_table = array("tbl_promos as tpo", array("tpo.*"));
		$join_tables =  array();
		$join_tables = array(
							array("left", "tbl_packages as  tpa", "tpa.id = tpo.package_id", array("tpa.title as package"))
							);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tpo.id" => "DESC"),"",null); 
		$result['promo_data'] = $this->common->MySqlFetchRow($rs, "array");
		//echo "<pre>";print_r($result);exit;

		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){
		$id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];
			$condition = "id ='".$id."' ";
			$result['promo_data'] = $this->common->getData("tbl_promos",'*',$condition);
		}
		$condition = "1=1";
		$result['package'] = $this->common->getData("tbl_packages",'id,title',$condition);
		//echo "<pre>";print_r($result['package']);exit;

		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	function removeFile(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = " id = ".$this->input->post('id');
			$exisiting_data =$this->common->getData("tbl_magazine_articles",'*',$condition);
			if(!empty($this->input->post('id')) && $this->input->post('type') == "PDF"){
				if(is_array($exisiting_data) && !empty($exisiting_data[0]['article_pdf']) && file_exists(DOC_ROOT_FRONT."/images/article_pdf/".$exisiting_data[0]['article_pdf'])){
					unlink(DOC_ROOT_FRONT."/images/article_pdf/".$exisiting_data[0]['article_pdf']);
					$data['article_pdf'] = '';
				}
			}else{
				if(is_array($exisiting_data) && !empty($exisiting_data[0]['image']) && file_exists(DOC_ROOT_FRONT."/images/article_image/".$exisiting_data[0]['image'])){
					unlink(DOC_ROOT_FRONT."/images/article_image/".$exisiting_data[0]['image']);
					$data['image'] = '';
				}
			}

			$condition = "id = '".$this->input->post('id')."' ";
			$result = $this->common->updateData("tbl_magazine_articles",$data,$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}	
		}			
	}

	public function submitForm(){
		 //print_r($_POST);
		// print_r($_FILES);
		 //exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "title = '".$this->input->post('title')."' ";
			if($this->input->post('id') && $this->input->post('id') > 0){
				$condition .= " AND  id != ".$this->input->post('id')." ";
			}			
			$check_name = $this->common->getData("tbl_promos",'*',$condition);

			if(!empty($check_name[0]['id'])){
				echo json_encode(array("success"=>false, 'msg'=>'Promo Code Already Present!'));
				exit;
			}


			$data['title'] = $this->input->post('title');
			$data['discount_type'] = $this->input->post('discount_type');
			$data['discount_value'] = $this->input->post('discount_value');
			$data['package_id'] = $this->input->post('package_id');
			$data['validity'] = date('Y-m-d', strtotime($this->input->post('validity')));
			$data['status'] = $this->input->post('package_status');
			$data['admin_status'] = $this->input->post('admin_status');
			$data['updated_at'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('supply_chain_admin')[0]['id'];
			if(!empty($this->input->post('id'))){
				$condition = "id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_promos",$data,$condition);
				if($result){

					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('supply_chain_admin')[0]['id'];
				$result = $this->common->insertData('tbl_promos',$data,'1');
				if(!empty($result)){
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}

		
}
?>