<section class="page-section mt-30 mb-30">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12">  
        <div class="mt-60 mb-60">
          <div class="payment-status-wrapper">
            <div>
              <img src="<?= base_url('images/404.svg')?>" class="mb-40" alt="">
              <h2 class="page-title-large mb-40">Opps! Page not found</h2>
              <p>We’re sorry,  the page that you’re looking for doesn’t exist. You may have mistype the address or the page may have moved.</p>
              <a href="<?= base_url()?>" class="btn-celerity btn-large btn-blue">Back to Homepage</a>
            </div>
          </div>
        </div>       
      </div>      
    </div>
  </div>
</section>