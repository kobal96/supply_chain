<div class="container heading-how">
	<div class="row">
		<div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-5">
				<h3 data-aos="fade-up"><?= $sections[0]['content_heading']?></h3>
				<!-- <p>Global Indians want meaningful interactions from their dating experience, and Sirf Coffee is helping them find love offline. We're a unique approach to modern dating that takes the spotlight off technology and shines it on people. We promise you a time-honored, offline dating experience with a like-minded match we've hand-picked for you.</p>
				<p>Sirf Coffee began in 2009 when Sunil Hiranandani (who was single at the time) found that dating avenues available to the global Indian were inadequate. He's proof that we work—Sunil and his wife are Sirf Coffee's original success story!</p>
				<p>Ten years later, we've endured the myriad dating apps and algorithms to find hundreds of hand-picked matches for our members around the world. Sirf Coffee has expanded into an international service with members in 18 cities and offices in Mumbai, London, and Dubai.</p> -->
				<p data-aos="fade-up"><?= $sections[0]['content_description']?></p>
		</div>
		<div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-7 img-how-we-work">
			<img src="<?= base_url('uploads/content_images/'.$sections[0]['content_image'].'')?>" alt="Sirf Coffee" title="Sirf Coffee" />
		</div>	
	</div>	
	<div class="row">
		<div class="col-12 col-sm-12 col-md-12 col-lg-11 col-xl-11">						
			<div class="sec-testimonial box-testimonial abt-testimonial">
				<div class="row">
					<div class="col-12 col-sm-3">
						<!--<img src="assets/images/whoweare-testimonial.jpg" alt="" class="img-fluid">-->
						<img src="<?= base_url('uploads/content_images/'.$sections[1]['content_image'].'')?>" />
					</div>
					<div class="col-12 col-sm-9">
						<!-- <h6 data-aos="fade-up"><?= $sections[1]['content_heading'] ?></h6> -->
						<div class="abt-testimonial-content">
							<p data-aos="fade-up"><?= $sections[1]['content_description'] ?> </p>
							<h5 data-aos="fade-up"><?= $sections[1]['content_heading'] ?></h5>
						</div>
					</div>
				</div>
			</div>
		</div>					
	</div>
</div>
</header>
<article> 
<section class="page-whowe">
			<div class="container">
				<h3 data-aos="fade-up"><?=$sections[2]['tagline_text']?>
					<span><?= $sections[3]['tagline_text']?></span>
				</h3>
				<div class="row thum-row">
					<?php foreach ($sections[4] as $key => $value) {?>
						<div class="col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3" onclick="getpopup(`<?= $value['grid_heading']?>`,`<?=$value['link_title']?>`,`<?=$value['grid_image']?>`,`<?= $value['grid_description']?>`)">
							<div class="thum-service">
								<img src="<?= base_url('uploads/grid/grid_images/'.$value['grid_image'].'')?>" alt="Sirf Coffee" title="Sirf Coffee" />
							</div>
							<h4 data-aos="fade-up"><span class="team-name"><?= $value['grid_heading']?></span></h4>
							<p data-aos="fade-up"><?= $value['link_title']?></p>
						</div>
					<?php }?>
					
					</div>
			</div>
	</section>
	<section class="sec-year">
			<div class="container">
				<h3 data-aos="fade-up"><?= $sections[5]['tagline_text']?></h3>
			
				<div class="row">
					<div class="col-12">
						<div  class="whowearetimeline">
							<?php foreach ($sections[6] as $key => $value) {?>
								<div class="whowearetimeline-single">
								<div class="wwa-text-placeholder-top">

								</div>
								<div class="wwa-year">
									<p><?= $value['grid_heading']?></p>
								</div>
								<div>
								<?php 
									if(($key % 2) == '0'){?>
										<img src="assets/images/timeline-odd.svg" alt="" style="width:100%">
									<?php }else{ ?>
										<img src="assets/images/timeline-even.svg" alt="" style="width:100%">
									<?php } ?> 
								</div>
								<div class="wwa-text">
									<p><?= $value['grid_description'] ?></p>
								</div>
								<div class="wwa-text-placeholder-bottom">

								</div>
							</div>
							<?php } ?>
						</div>
						<div class="whowearetimeline-mobile">
							<ul>
							<?php foreach ($sections[6] as $key => $value) {?>
								<li>
									<div class="tc-mobile">
										<img src="assets/images/timeline-circle-mobile.svg" alt="">
										<p><?= $value['grid_heading']?></p>
									</div>
									<div class="tc-mobile-text">
										<p><?= $value['grid_description'] ?></p>
									</div>
								</li>
							<?php }?>
							</ul>  
						</div>	
					</div>
				</div>				
			</div>
	</section>			
</article>
<span id="popupdata"></span>
<script>
function getpopup(heading,title,image,description){
	// console.log(heading);
	// console.log(title);
	// console.log(image);
	var popupdata = '<div class="modal fade bd-example-modal-lg popup-team" id="bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">'+
		'<div class="modal-dialog modal-lg modal-dialog-centered">'+
			'<div class="modal-content">'+
				'<div class="modal-content">'+
					'<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>'+
					'<div class="row">'+
						/* '<div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 box-popup">'+
							'<div class="popup-thum">'+
						'<img src="<?= base_url('uploads/grid/grid_images/')?>'+image+'" alt="Sirf Coffee" title="Sirf Coffee" class="popup-thumb-img"/>'+
					'</div>'+							
						'</div>'+ */
						'<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 box-popup">'+
							'<div class="popup-cont">'+
								'<h3>'+heading+'</h3>'+
								'<h6>'+title+'</h6>'+
								'<p>'+description+'</p>'+
							'</div>'+						
						'</div>'+
					'</div>'+												
				'</div>'+
			'</div>'+
		'</div>'+
	'</div>';

	$("#popupdata").html(popupdata);
$("#bd-example-modal-lg").modal('show');
}
document.title="Who we are";
</script>