
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						
						<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">All Magazine Category</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('magazine_category/addEdit')?>" class="btn btn-primary ff-page-title-btn ff-page-title-btn">
								Add Magazine Category
							</a>
						</div>
					</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="edition-listing" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>#</th>
																<th>Title</th>	
																<th>Url Slug</th>
																<!-- <th>Image</th> -->
																<th>Status</th>																
																<th>Approval Status</th>																
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>#</th>
																<th>Title</th>	
																<th>Url Slug</th>
																<!-- <th>Image</th> -->
																<th>Status</th>																
																<th>Approval Status</th>																
																<th>Actions</th>													
															</tr>
														</tfoot>
														<tbody>
														<?php
														if(!empty($magazine_category_data) && isset($magazine_category_data))	{
															$cnt = 0;
															foreach ($magazine_category_data as $key => $value) {
																	$cnt= ++$cnt?>
																	<tr>														
																		<td>#<?=$cnt?></td>
																		<td><?= $value['title']?></td>																
																		<td><?= $value['slug']?></td>	
																		<td><?= $value['status']?></td>	
																		<td><?= $value['admin_status']?></td>	

																		<td>
																			<div class="form-button-action">
																				<a href="<?= base_url('magazine_category/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-primary ff-page-title-btn  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" onclick="delete_row(<?=$value['id']?>)" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
															<?php }
														}?>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#edition-listing').DataTable({
				"pageLength": 10,
				// "ordering": true,
			});			
		});

		function delete_row(id){
			var csrfName = "<?= $this->security->get_csrf_token_name()?>"; // Value specified in $config['csrf_token_name']
			var csrfHash = "<?= $this->security->get_csrf_hash()?>"; // CSRF hash
			if(id ){
				var r = confirm("Are you sure?");
				if (r == true) {
					$.ajax({
						url: "<?= base_url('magazine_category/delete_row')?>",
						data:{id,[csrfName]: csrfHash },
						dataType: "json",
						type: "POST",
						success: function(response){
						
							if(response.success){
								swal("OK", response.msg, {
									icon : "success",
									buttons: {        			
										confirm: {
											className : 'btn btn-success'
										}
									},
								}).then(
								function() {
									location.reload();
								});
							}else{	
								swal("Failed", response.msg, {
									icon : "error",
									buttons: {        			
										confirm: {
											className : 'btn btn-danger'
										}
									},
								});
							}
						}
					});
				}
			}
		}
	</script>
</body>
</html>
