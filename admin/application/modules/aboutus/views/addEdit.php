<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<!-- About us content -->
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">About Us</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('article') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-aboutus" method="post" enctype="multipart/form-data">								
								<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">	

								<input name="about_us_id" id="about_us_id" value="<?= (!empty($about_us[0]['about_us_id'])?$about_us[0]['about_us_id']:"") ?>" type="hidden" >
									<div class="basicinfo">
									
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="Title">Header Title</label>
												<input type="text" name="title" class="form-control" value="<?= (!empty($about_us[0]['title'])?$about_us[0]['title']:"") ?>">	
											</div>										
										</div>
										<div class="col-sm-12">
											<div class="form-group">
												<label for="header_content">Header Tagline Content</label>
												<textarea class="form-control" id="header_content" name="header_content" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($about_us[0]['header_content'])?$about_us[0]['header_content']:"") ?></textarea>
											</div>										
										</div>
										<div class="col-sm-6">
											<div class="card-header pl-0">
												<h4 class="card-title">Our Vision</h4>
											</div>											
											<div class="form-group">
												<label for="Title">Section Title</label>
												<input type="text" name="section1_heading" class="form-control" value="<?= (!empty($about_us[0]['section1_heading'])?$about_us[0]['section1_heading']:"") ?>">
											</div>
											<div class="form-group">
												<label for="Title">Section Content</label>
												<textarea class="form-control" name="section1_paragraph" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($about_us[0]['section1_paragraph'])?$about_us[0]['section1_paragraph']:"") ?></textarea>
											</div>											
										</div>
										<div class="col-sm-6">	
											<div class="card-header pl-0">
												<h4 class="card-title">Our Mission</h4>
											</div>										
											<div class="form-group">
												<label for="Title">Section Title</label>
												<input type="text" name="section2_heading" class="form-control" value="<?= (!empty($about_us[0]['section2_heading'])?$about_us[0]['section2_heading']:"") ?>">
											</div>
											<div class="form-group">
												<label for="Title">Section Content</label>
												<textarea class="form-control" name="section2_paragraph" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($about_us[0]['section2_paragraph'])?$about_us[0]['section2_paragraph']:"") ?></textarea>
											</div>										
										</div>
										<div class="col-sm-6">
											<div class="card-header pl-0">
												<h4 class="card-title">Our Founder</h4>
											</div>											
											<div class="form-group">
												<label for="Title">Founder Name</label>
												<input type="text" name="founder_name" class="form-control" value="<?= (!empty($about_us[0]['founder_name'])?$about_us[0]['founder_name']:"") ?>" >
											</div>
											<div class="form-group">
												<label for="Title">Founder Image</label>
												<?php
												if(!empty($about_us[0]['founder_image'])){
												?>
												<div>
													<img src="<?= FRONT_URL.'/images/aboutus/'.$about_us[0]['founder_image']?>" alt="..." class="rounded" height="150px" weight="150px">
												</div>
												<br>
												<input name="pre_founder_image" id="pre_founder_image" value="<?= (!empty($about_us[0]['founder_image'])?$about_us[0]['founder_image']:"") ?>" type="hidden">
												<?php } ?>
												<input type="file" name="founder_image" class="form-control" >
											</div>
											<div class="form-group">
												<label for="Title">LinkedIn Profile Link</label>
												<input type="text" name="founder_linkedin" class="form-control" value="<?= (!empty($about_us[0]['founder_linkedin'])?$about_us[0]['founder_linkedin']:"") ?>" >
											</div>
											<div class="form-group">
												<label for="Title">Founder Description</label>
												<textarea class="form-control" name="founder_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($about_us[0]['founder_description'])?$about_us[0]['founder_description']:"") ?></textarea>
											</div>											
										</div>
										<div class="col-sm-6">	
											<div class="card-header pl-0">
												<h4 class="card-title">Content Head</h4>
											</div>
											<div class="form-group">
												<label for="Title">Content Head Name</label>
												<input type="text" name="head_name" class="form-control" value="<?= (!empty($about_us[0]['head_name'])?$about_us[0]['head_name']:"") ?>" >
											</div>
											<div class="form-group">
												<label for="Title">Content Head Image</label>
												<?php
												if(!empty($about_us[0]['head_image'])){
												?>
												<div>
													<img src="<?= FRONT_URL.'/images/aboutus/'.$about_us[0]['head_image']?>" alt="..." class="rounded" height="150px" weight="150px">
												</div>
												<br>
												<input name="pre_head_image" id="pre_head_image" value="<?= (!empty($about_us[0]['head_image'])?$about_us[0]['head_image']:"") ?>" type="hidden">
												<?php } ?>
												<input type="file" name="head_image" class="form-control"  >
											</div>
											<div class="form-group">
												<label for="Title">LinkedIn Profile Link</label>
												<input type="text" name="head_linkedin" class="form-control" value="<?= (!empty($about_us[0]['head_linkedin'])?$about_us[0]['head_linkedin']:"") ?>" >
											</div>
											<div class="form-group">
												<label for="Title">Content Head Description</label>
												<textarea class="form-control" name="head_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($about_us[0]['head_description'])?$about_us[0]['head_description']:"") ?></textarea>
											</div>										
										</div>
										<div class="col-sm-12">	
											<div class="card-header pl-0">
												<h4 class="card-title">Our Advisors</h4>
											</div>																		
										</div>										
									</div>	

										<?php
										if (!empty($about_us[0]['about_us_id'])) {
										 	$condition = "about_us_id ='".$about_us[0]['about_us_id']."' ";
		                                    $dvisors = $this->common->getData("tbl_about_advisors",'*',$condition);
		                                    if (!empty($dvisors)) {
		                                    foreach ($dvisors as $key => $value) { ?>
		                                <div class="row">
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Image</label>
												<?php
												if(!empty($value['advisor_image'])){
												?>
												<div>
													<img src="<?= FRONT_URL.'/images/aboutus/'.$value['advisor_image']?>" alt="..." class="rounded" height="50px" weight="50px">
												</div>
												<br>
												<input name="pre_advisor_image[]" id="pre_founder_image" value="<?= (!empty($value['advisor_image'])?$value['advisor_image']:"") ?>" type="hidden">
												<?php } ?>
												<input type="file" name="advisor_image[]" class="form-control">
											</div>											
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="Title">Advisor Name</label>
												<input type="text" name="advisor_name[]" class="form-control" value="<?=$value['advisor_name'] ?>">
											</div>											
										</div>
										
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Designation</label>
												<input type="text" name="advisor_designation[]" class="form-control" value="<?=$value['advisor_designation'] ?>">
											</div>
										</div>
									</div>

		                                    	
		                            <?php }
										 } 
										}else{ ?>
									<div class="row">
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Image</label>
												<input type="file" name="advisor_image[]" class="form-control">
											</div>											
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="Title">Advisor Name</label>
												<input type="text" name="advisor_name[]" class="form-control">
											</div>											
										</div>
										
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Designation</label>
												<input type="text" name="advisor_designation[]" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Image</label>
												<input type="file" name="advisor_image[]" class="form-control">
											</div>											
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="Title">Advisor Name</label>
												<input type="text" name="advisor_name[]" class="form-control">
											</div>											
										</div>
										
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Designation</label>
												<input type="text" name="advisor_designation[]" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Image</label>
												<input type="file" name="advisor_image[]" class="form-control">
											</div>											
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="Title">Advisor Name</label>
												<input type="text" name="advisor_name[]" class="form-control">
											</div>											
										</div>
										
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Designation</label>
												<input type="text" name="advisor_designation[]" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Image</label>
												<input type="file" name="advisor_image[]" class="form-control">
											</div>											
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="Title">Advisor Name</label>
												<input type="text" name="advisor_name[]" class="form-control">
											</div>											
										</div>
										
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Designation</label>
												<input type="text" name="advisor_designation[]" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Image</label>
												<input type="file" name="advisor_image[]" class="form-control">
											</div>											
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="Title">Advisor Name</label>
												<input type="text" name="advisor_name[]" class="form-control">
											</div>											
										</div>
										
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Designation</label>
												<input type="text" name="advisor_designation[]" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Image</label>
												<input type="file" name="advisor_image[]" class="form-control">
											</div>											
										</div>
										<div class="col-sm-4">
											<div class="form-group">
												<label for="Title">Advisor Name</label>
												<input type="text" name="advisor_name[]" class="form-control">
											</div>											
										</div>
										
										<div class="col-sm-4">											
											<div class="form-group">
												<label for="Title">Advisor Designation</label>
												<input type="text" name="advisor_designation[]" class="form-control">
											</div>
										</div>
									</div>

									<?php } ?>
									
									<div class="row">
										<div class="col-12">
											<div class="form-group">
												<a href="#/" class="btn btn-primary btn-sm addmore-advisors-link">Add More</a>
											</div>
										</div>
									</div>																			
									<div id="addmore-advisors">
										
									</div>	
									
									<div class="form-group">
										<button type="submit" id="next" class="btn btn-black">Save</button>
										<a href="<?= base_url('article')?>" class="btn btn-cancel btn-border">Cancel</a>										
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- About us content -->
		</div>
	</div>
</div>

<script type="text/javascript">
	// use for form submit
	var vRules = 
	{
		title:{required:true},
		header_content:{required:true},
		section1_heading:{required:true},
		section1_paragraph:{required:true},
		section2_heading:{required:true},
		section2_paragraph:{required:true},
		founder_name:{required:true},
		head_name:{required:true},
	};
	var vMessages = 
	{
		title:{required:"Please Enter Title."},
		header_content:{required:"Please Enter Header Content."},
		section1_heading:{required:"Please Enter Section Title."},
		section1_paragraph:{required:"Please Enter Section Content."},
		section2_heading:{required:"Please Enter Section Title."},
		section2_paragraph:{required:"Please Enter Section Content."},
		founder_name:{required:"Please Enter Founder Name."},
		head_name:{required:"Please Enter Content Head Name."},

	};

	$("#form-aboutus").validate({
		ignore:[],
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{	
			var act = "<?= base_url('aboutus/submitform')?>";
			$("#form-aboutus").ajaxSubmit({
				url: act, 
				type: 'post',
				dataType: 'json',
				cache: false,
				clearForm: false,
				async:false,
				beforeSubmit : function(arr, $form, options){
					$(".btn btn-black").hide();
				},
				success: function(response) 
				{
					if(response.success){
						swal("done", response.msg, {
							icon : "success",
							buttons: {        			
								confirm: {
									className : 'btn btn-success'
								}
							},
						}).then(
						function() {
							window.location = "<?= base_url('aboutus')?>";
							// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
						});
					}else{	
						swal("Failed", response.msg, {
							icon : "error",
							buttons: {        			
								confirm: {
									className : 'btn btn-danger'
								}
							},
						});
					}
				}
			});
		}
	});

</script>
			
	<script>
			    	/*Addmore advisors*/
	$('.addmore-advisors-link').click(function(){
		$('#addmore-advisors').append(
			"<div class='row addmore-advisor-content'>"+
			"<div class='col-sm-4'>"+										
				"<div class='form-group'>"+
					"<label for='Title'>Advisor Image</label>"+
					"<input type='file' name='advisor_image[]' class='form-control'>"+
				"</div>			"+								
			"</div>"+
			"<div class='col-sm-4'>"+
				"<div class='form-group'>"+
					"<label for='Title'>Advisor Name</label>"+
					"<input type='text' name='advisor_name[]' class='form-control'>"+
				"</div>"+											
			"</div>"+			
			"<div class='col-sm-4'>"+											
				"<div class='form-group'>"+
					"<label for='Title'>Advisor Designation</label>"+
					"<input type='text'  name='advisor_designation[]' class='form-control'>"+
				"</div>"+
				"<a href='#/' class='remove-advisor btn btn-primary btn-sm'>Remove</a>"+
			"</div>"+
			"</div>"
		);
	});
	$('body').on('click', '.remove-advisor', function() {
			$(this).closest('.addmore-advisor-content').remove();
		});
	/*Addmore advisors*/
	</script>