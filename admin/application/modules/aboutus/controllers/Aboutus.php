<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class aboutus extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('aboutusmodel','',TRUE);
	}

	public function index(){
		$result = array();
		$condition = "1=1";
		$result['about_us'] = $this->common->getData("tbl_about_us",'*',$condition);
		// echo "<pre>";
		// print_r($result);
		// exit;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$tag_id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$tag_id = $url_prams['id'];
			$condition = " tag_id ='".$tag_id."' ";
			$result['tag_data'] = $this->common->getData("tbl_tags",'*',$condition);
		}
		
		// echo "<pre>";
		// print_r($result);
		// exit;
		//echo $user_id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}

	function submitformtesting(){
		// codeigniter form validation 
		// $this->form_validation->set_rules('themename', 'Themename', 'trim|required|alpha');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');
		// $this->form_validation->set_rules('meta_title', 'Meta title', 'trim|required');
		// $this->form_validation->set_rules('meta_description', 'Meta description', 'trim|required');
		// $this->form_validation->set_rules('mainvideo', 'Main video', 'trim|required');

		// if($this->form_validation->run()) {  
		// 	// true  if validation property sectifice properly
		// 	$themename = $this->input->post('themename');  
		// 	$mainvideo = $this->input->post('mainvideo'); 
		// 	$themename = $this->input->post('meta_title');  
		// 	$mainvideo = $this->input->post('meta_description'); 
		// 	$themename = $this->input->post('mainvideo');  
		// 	$mainvideo = $this->input->post('meta_title'); 
		// }else{  
		// 	// if validation is not proper
		// 	$this->index();  
		// } 
	}


	function submitForm(){
		// echo "<pre>";	
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$data['title'] = $this->input->post('title');
			$data['header_content'] = $this->input->post('header_content');
			$data['section1_heading'] = $this->input->post('section1_heading');
			$data['section1_paragraph'] = $this->input->post('section1_paragraph');
			$data['section2_heading'] = $this->input->post('section2_heading');
			$data['section2_paragraph'] = $this->input->post('section2_paragraph');
			$data['founder_name'] = $this->input->post('founder_name');
			$data['founder_linkedin'] = $this->input->post('founder_linkedin');
			$data['founder_description'] = $this->input->post('founder_description');
			$data['head_name'] = $this->input->post('head_name');
			$data['head_linkedin'] = $this->input->post('head_linkedin');
			$data['head_description'] = $this->input->post('head_description');

			$founder_image = "";
			if(isset($_FILES) && isset($_FILES["founder_image"]["name"])){
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				if (!$this->upload->do_upload("founder_image")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$founder_image = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('about_us_id'))){	
					$condition_image = " about_us_id = ".$this->input->post('about_us_id');
					$image =$this->common->getData("tbl_about_us",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->founder_image) && file_exists(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->founder_image))
					{
						unlink(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->founder_image);
					}
				}
			}else{
				$founder_image = $this->input->post('pre_founder_image');
			}


			$head_image = "";
			if(isset($_FILES) && isset($_FILES["head_image"]["name"])){
				$this->load->library('upload');
				$this->upload->initialize($this->set_upload_options());
				if (!$this->upload->do_upload("head_image")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$head_image = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);
				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('about_us_id'))){	
					$condition_image = " about_us_id = ".$this->input->post('about_us_id');
					$image =$this->common->getData("tbl_about_us",'*',$condition_image);
					if(is_array($image) && !empty($image[0]->head_image) && file_exists(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->head_image))
					{
						unlink(DOC_ROOT_FRONT."/images/aboutus/".$image[0]->main_image);
					}
				}
			}else{
				$head_image = $this->input->post('pre_head_image');
			}

			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('supply_chain_admin')[0]['id'];
			$data['founder_image'] = $founder_image;
			$data['head_image'] = $head_image;

			if(!empty($this->input->post('about_us_id'))){
				// print_r($data);
				$condition = "about_us_id = '".$this->input->post('about_us_id')."' ";
				$result = $this->common->updateData("tbl_about_us",$data,$condition);
				if($result){

				    $condition = "about_us_id = '".$this->input->post('about_us_id')."' ";
					$this->common->deleteRecord("tbl_about_advisors",$condition);

					if(isset($_FILES) && isset($_FILES["advisor_image"]["name"])){
					$this->load->library('upload');
					    $dataInfo = array();
					    $files = $_FILES;
					    $cpt = count($_FILES['advisor_image']['name']);
					    for($i=0; $i<$cpt; $i++)
					    {           
					        $_FILES['advisor_image']['name']= $files['advisor_image']['name'][$i];
					        $_FILES['advisor_image']['type']= $files['advisor_image']['type'][$i];
					        $_FILES['advisor_image']['tmp_name']= $files['advisor_image']['tmp_name'][$i];
					        $_FILES['advisor_image']['error']= $files['advisor_image']['error'][$i];
					        $_FILES['advisor_image']['size']= $files['advisor_image']['size'][$i];    

					        $this->upload->initialize($this->set_upload_options());
					        $this->upload->do_upload('advisor_image');
					        $dataInfo[] = $this->upload->data();
					    }
					if(!empty($_POST['advisor_name']) && isset($_POST['advisor_name'])){
						foreach ($_POST['advisor_name'] as $key => $value) {
							$advisor_data = array();
							$advisor_data['about_us_id'] = $this->input->post('about_us_id');
							$advisor_data['advisor_name'] = $value;
							$advisor_data['advisor_designation'] = $_POST['advisor_designation'][$key];
							$advisor_data['advisor_image'] = $dataInfo[$key]['file_name'];
							$this->common->insertData('tbl_about_advisors',$advisor_data,'1');
							
						}
					}
				}else{

					if(!empty($_POST['advisor_name']) && isset($_POST['advisor_name'])){
						foreach ($_POST['advisor_name'] as $key => $value) {
							$advisor_data = array();
							$advisor_data['about_us_id'] = $this->input->post('about_us_id');
							$advisor_data['advisor_name'] = $value;
							$advisor_data['advisor_designation'] = $_POST['advisor_designation'][$key];
							$advisor_data['advisor_image'] = $_POST['pre_advisor_image'][$key];
							$this->common->insertData('tbl_about_advisors',$advisor_data,'1');
							
						}
					}

				}
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] = $this->session->userdata('supply_chain_admin')[0]['id'];
				$result = $this->common->insertData('tbl_about_us',$data,'1');
				if(!empty($result)){
					 $this->load->library('upload');
					    $dataInfo = array();
					    $files = $_FILES;
					    $cpt = count($_FILES['advisor_image']['name']);
					    for($i=0; $i<$cpt; $i++)
					    {           
					        $_FILES['advisor_image']['name']= $files['advisor_image']['name'][$i];
					        $_FILES['advisor_image']['type']= $files['advisor_image']['type'][$i];
					        $_FILES['advisor_image']['tmp_name']= $files['advisor_image']['tmp_name'][$i];
					        $_FILES['advisor_image']['error']= $files['advisor_image']['error'][$i];
					        $_FILES['advisor_image']['size']= $files['advisor_image']['size'][$i];    

					        $this->upload->initialize($this->set_upload_options());
					        $this->upload->do_upload('advisor_image');
					        $dataInfo[] = $this->upload->data();
					    }
					if(!empty($_POST['advisor_name']) && isset($_POST['advisor_name'])){
						foreach ($_POST['advisor_name'] as $key => $value) {
							$advisor_data = array();
							$advisor_data['about_us_id'] = $result;
							$advisor_data['advisor_name'] = $value;
							$advisor_data['advisor_designation'] = $_POST['advisor_designation'][$key];
							$advisor_data['advisor_image'] = $dataInfo[$key]['file_name'];
							$this->common->insertData('tbl_about_advisors',$advisor_data,'1');
						}
					}

					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}


	function set_upload_options($file_name = ""){   
		$config = array();
		if(!empty($file_name) &&  $file_name != ""){
			$config['file_name']   = $file_name.time();
		}
		$config['upload_path'] = DOC_ROOT_FRONT."/images/aboutus/";
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'],0777, true);		
		}
		$config['allowed_types'] = 'gif|jpg|png|jpeg|svg|gif|mp4';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;
		// $config['min_width']            = 1000;
		// $config['min_height']           = 1000;
		return $config;
	}
	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/aboutus/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		// $config1['width'] = 1000;
		// $config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/aboutus/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
	} 
}
?>