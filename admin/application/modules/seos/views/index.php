
<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
								<h1 class="ff-page-title">Seo</h1> 
							</div>

						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('seos/addEdit')?>" class="btn btn-primary ff-page-title-btn ff-page-title-btn">
								Add Seo
							</a>
						</div>

						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>#</th>
																<th>Tag</th>
																<th>Title</th>
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>#</th>
																<th>Tag</th>
																<th>Title</th>
																<th>Actions</th>															
															</tr>
														</tfoot>
														<tbody>
															
												<?php
												if(isset($seo_data) && !empty($seo_data))	{
													$cnt = 0;
													foreach ($seo_data as $key => $value) {
													$cnt= ++$cnt?>
													<tr>														
														<td>#<?=$cnt?></td>
														<td><?= $value['tag']?></td>
														<td><?= $value['meta_title']?></td>
														<td>
														<div class="form-button-action">
															<a href="<?= base_url('seos/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-primary ff-page-title-btn  btn-edit" data-original-title="Edit">
																<i class="fa fa-edit"></i>
															</a>
														</div>
														</td>													
													</tr>
													<?php } } ?>

														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 10,
				"ordering": false,
			});			
		});
	</script>
</body>
</html>
