
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Seo</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('seos') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-seo" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
									<input name="id" id="id" value="<?= (!empty($seo_data[0]['id'])?$seo_data[0]['id']:"") ?>" type="hidden" >
									
									<label for="">Information</label><hr>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="tag">Tag Name</label>
												<input type="text" class="form-control" id="tag" name="tag" value="<?= (!empty($seo_data[0]['tag'])?$seo_data[0]['tag']:"") ?>" placeholder="Tag Name">	
											</div>
										</div>

									</div>
									<br>
									<br>
									
									<label for="">Meta Information</label><hr>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" name="meta_title" value="<?= (!empty($seo_data[0]['meta_title'])?$seo_data[0]['meta_title']:"") ?>" placeholder="Meta Title">	
											</div>
										</div>

									</div>


									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_keyword">Meta Keywords</label>
												<textarea class="form-control" id="meta_keyword" name="meta_keyword" rows="5" placeholder="Meta Keywords"><?= (!empty($seo_data[0]['meta_keyword'])?$seo_data[0]['meta_keyword']:"") ?></textarea>	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="Meta Description"><?= (!empty($seo_data[0]['meta_description'])?$seo_data[0]['meta_description']:"") ?></textarea>	
											</div>
										</div>
									</div>
									<br>
									<br>
									<hr>		
												
									<div class="form-group">
										<button  type="submit" class="btn btn-black">Create</button>
										<a href="<?= base_url('seos')?>" class="btn btn-cancel btn-border">Cancel</a>
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
});


// use for form submit
var vRules = 
{
	tag:{required:true},
	meta_title:{required:true},
	meta_keyword:{required:true},
	meta_description:{required:true},
};
var vMessages = 
{
	tag:{required:"Please Enter Tag Name."},
	meta_title:{required:"Please Enter Meta Title."},
	meta_keyword:{required:"Please Enter Meta Keywords."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-seo").validate({
	ignore:[],
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('seos/submitform')?>";
		$("#form-seo").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('seos')?>";
						
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>