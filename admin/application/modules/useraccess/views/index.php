
<body>
	<div class="wrapper">
    
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						
						<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">CMS Users</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?= base_url('useraccess/addEdit')?>" class="btn btn-primary ff-page-title-btn ff-page-title-btn">
								Add CMS Users
							</a>
						</div>
					</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="article-listing" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>#</th>
																<th>First name</th>	
																<th>Last Name</th>
																<th>Email</th>
																<th>Username</th>
																<th>Status</th>
																<th>Last Active</th>																
																<!-- <th>Actions</th> -->															
															</tr>
														</thead>
														<tfoot>
															<tr>
															<th>#</th>
																<th>First name</th>	
																<th>Last Name</th>
																<th>Email</th>
																<th>Username</th>
																<th>Status</th>
																<th>Last Active</th>																
																<!-- <th>Actions</th> -->												
															</tr>
														</tfoot>
														<tbody>
														<?php
														if(!empty($user_data) && isset($user_data))	{
															$cnt = 0;
															foreach ($user_data as $key => $value) {
																$date1 = date('Y-m-d', strtotime($value['last_login']));
																$date2 = date('Y-m-d');
																$diff = abs(strtotime($date2) - strtotime($date1));
																$years = floor($diff / (365*60*60*24));
																if ($value['last_login']!=null) {
																if ($years>0) {
																	$last_login= $years. ' years ago';
																}
																}else{
																	$last_login= 'Never';
																}
																
																	$cnt= ++$cnt?>
																	<tr>														
																		<td>#<?=$cnt?></td>
																		<td><?= $value['first_name']?></td>
																		<td><?= $value['last_name']?></td>
																		<td><?= $value['email']?></td>
																		<td><?= $value['username']?></td>
																		<td><?= $value['status']?></td>
																		<td><?= $last_login?></td>

																		<!-- <td>
																			<div class="form-button-action">
																				<a href="<?= base_url('useraccess/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['id']), '+/', '-_'), '=').'')?>" data-toggle="tooltip" title="" class="btn btn-primary ff-page-title-btn  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				
																			</div>
																		</td> -->													
																	</tr>
															<?php }
														}?>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#article-listing').DataTable({
				"pageLength": 5,
				"ordering": false,
			});			
		});
	</script>
</body>
</html>
