
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Create CMS User</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('useraccess') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-article" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
									<input name="id" id="id" value="<?= (!empty($article_data[0]['id'])?$article_data[0]['id']:"") ?>" type="hidden" >

									<label for="">Basic Information</label><hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="first_name">First Name</label>
												<input type="text" class="form-control" id="first_name" name="first_name" value="<?= (!empty($article_data[0]['first_name'])?$article_data[0]['first_name']:"") ?>" placeholder="First Name">	
											</div>
										</div>
										
										<div class="col-sm-6">
											<div class="form-group">
												<label for="last_name">Last Name</label>
												<input type="text" class="form-control" id="last_name" name="last_name" value="<?= (!empty($article_data[0]['last_name'])?$article_data[0]['last_name']:"") ?>" placeholder="Last Name">	
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="email">Email</label>
												<input type="text" class="form-control" id="email" name="email" value="<?= (!empty($article_data[0]['email'])?$article_data[0]['email']:"") ?>" placeholder="Email Address">	
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="email">Username</label>
												<input type="text" class="form-control" id="username" name="username" value="<?= (!empty($article_data[0]['username'])?$article_data[0]['username']:"") ?>" placeholder="Username">	
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="email">Password</label>
												<input type="text" class="form-control" id="password" name="password" value="<?= (!empty($article_data[0]['password'])?$article_data[0]['password']:"") ?>" placeholder="Password">	
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="email">Confirm Password</label>
												<input type="text" class="form-control" id="password_confirmation" name="password_confirmation" value="<?= (!empty($article_data[0]['password_confirmation'])?$article_data[0]['password_confirmation']:"") ?>" placeholder="Confirm Password">	
											</div>
										</div>


									</div>
									<br>
									<br>
									
									<label for="">Access Control</label><hr>
			<div class="row">
				<div class="cl-box-title"> Modules </div>
			</div>
			<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label for="">Banner</label>	
				</div>
			</div>

			<div class="col-sm-8">
				<div class="cl-permission">
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[33][]" class="permission-create" value="0" data-parsley-multiple="create">
                Create
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[33][]" class="permission-read" value="1" data-parsley-multiple="read">
                Read
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[33][]" class="permission-update" value="2" data-parsley-multiple="update">
                Update
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[33][]" class="permission-delete" value="3" data-parsley-multiple="delete">
                Delete
            </a>
            </div>
					
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					<label for="">Seo</label>	
				</div>
			</div>

			<div class="col-sm-8">

			<div class="cl-permission">
		            <a href="javascript:void(0)" class="btn btn-default ">
		                <input type="checkbox" name="checkdata[9][]" id="create-seo" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516">
		                Create
		            </a>
		            <a href="javascript:void(0)" class="btn btn-default ">
		                <input type="checkbox" name="checkdata[9][]" id="read-seo" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
		                Read
		            </a>
		            <a href="javascript:void(0)" class="btn btn-default ">
		                <input type="checkbox" name="checkdata[9][]" id="update-seo" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
		                Update
		            </a>
		            <a href="javascript:void(0)" class="btn btn-default ">
		                <input type="checkbox" name="checkdata[9][]" id="delete-seo" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
		                Delete
		            </a>
		    </div>
					
			</div>

			<!-- <div class="col-sm-4">
				<div class="form-group">
					<label for="">Poll</label>	
				</div>
			</div>

			<div class="col-sm-8">

			<div class="cl-permission">
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[10][]" id="create-poll" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516">
                Create
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[10][]" id="read-poll" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
                Read
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[10][]" id="update-poll" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
                Update
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[10][]" id="delete-poll" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
                Delete
            </a>
            </div>
					
			</div> -->

			<div class="col-sm-4">
				<div class="form-group">
					<label for="">Magazine</label>	
				</div>
			</div>

			<div class="col-sm-8">

			<div class="cl-permission">
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[11][]" id="create-magazine" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516">
                Create
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[11][]" id="read-magazine" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
                Read
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[11][]" id="update-magazine" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
                Update
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[11][]" id="delete-magazine" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
                Delete
            </a>
            </div>
					
		</div>

		<!-- <div class="col-sm-4">
			<div class="form-group">
				<label for="">Blog</label>	
			</div>
		</div>

		<div class="col-sm-8">

		<div class="cl-permission">
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[18][]" id="create-blog" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516">
	                Create
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[18][]" id="read-blog" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
	                Read
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[18][]" id="update-blog" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
	                Update
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[18][]" id="delete-blog" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
	                Delete
	            </a>
	    </div>
				
		</div> -->

		<div class="col-sm-4">
			<div class="form-group">
				<label for="">Package</label>	
			</div>
		</div>

		<div class="col-sm-8">

		<div class="cl-permission">
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[21][]" id="create-package" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516">
	                Create
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[21][]" id="read-package" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
	                Read
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[21][]" id="update-package" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
	                Update
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[21][]" id="delete-package" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
	                Delete
	            </a>
	    </div>
				
		</div>

		<div class="col-sm-4">
			<div class="form-group">
				<label for="">Promo</label>	
			</div>
		</div>

		<div class="col-sm-8">

		<div class="cl-permission">
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[22][]" id="create-promo" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516">
	                Create
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[22][]" id="read-promo" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
	                Read
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[22][]" id="update-promo" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
	                Update
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[22][]" id="delete-promo" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
	                Delete
	            </a>
	    </div>
				
		</div>

		<!-- <div class="col-sm-4">
			<div class="form-group">
				<label for="">Forum</label>	
			</div>
		</div>

		<div class="col-sm-8">

		<div class="cl-permission">
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[23][]" id="create-forum" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516">
	                Create
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[23][]" id="read-forum" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
	                Read
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[23][]" id="update-forum" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
	                Update
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[23][]" id="delete-forum" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
	                Delete
	            </a>
	    </div>
				
		</div> -->

		<div class="col-sm-4">
			<div class="form-group">
				<label for="">Users</label>	
			</div>
		</div>

		<div class="col-sm-8">

		<div class="cl-permission">
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[26][]" id="create-users" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516">
	                Create
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[26][]" id="read-users" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
	                Read
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[26][]" id="update-users" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
	                Update
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[26][]" id="delete-users" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
	                Delete
	            </a>
	    </div>
				
		</div>

		<div class="col-sm-4">
			<div class="form-group">
				<label for="">Subscriptions</label>	
			</div>
		</div>

		<div class="col-sm-8">

		<div class="cl-permission">
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[31][]" id="create-subscriptions" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516">
	                Create
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[31][]" id="read-subscriptions" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
	                Read
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[31][]" id="update-subscriptions" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
	                Update
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[31][]" id="delete-subscriptions" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
	                Delete
	            </a>
	    </div>
				
		</div>

		<div class="col-sm-4">
			<div class="form-group">
				<label for="">GST Configuration</label>	
			</div>
		</div>

		<div class="col-sm-8">

		<div class="cl-permission">
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[32][]" id="create-gst-configuration" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516" disabled>
	                Create
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[32][]" id="read-gst-configuration" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
	                Read
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[32][]" id="update-gst-configuration" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
	                Update
	            </a>
	            <a href="javascript:void(0)" class="btn btn-default ">
	                <input type="checkbox" name="checkdata[32][]" id="delete-gst-configuration" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
	                Delete
	            </a>
	    </div>
				
		</div>
		</div>





		<div class="example-box-wrapper">
        <div class="cl-box-wrapper">                                   
                              
                
        <div class="cl-box-title">
            Configurations
        </div>
    <div class="row">
    <div class="col-sm-4">
			<div class="form-group">
				<label for="">CMS Users</label>	
			</div>
		</div>                                                        

    <div class="col-sm-8">
    <div class="cl-permission">
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[1][]" id="create-admin" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516">
                Create
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[1][]" id="read-admin" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
                Read
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[1][]" id="update-admin" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
                Update
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[1][]" id="delete-admin" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
                Delete
            </a>
    </div>
    </div>
    </div>

    <div class="cl-box-title">
        Reports
    </div>
                                    
    
    
    <div class="row">
    <div class="col-sm-4">
		<div class="form-group">
			<label for="">Google Reports</label>	
		</div>
	</div>
	<div class="col-sm-8">
    <div class="cl-permission">
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[2][]" id="create-google-reports" class="permission-create" value="0" data-parsley-multiple="create" data-parsley-id="6516">
                Create
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[2][]" id="read-google-reports" class="permission-read" value="1" data-parsley-multiple="read" data-parsley-id="1083">
                Read
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[2][]" id="update-google-reports" class="permission-update" value="2" data-parsley-multiple="update" data-parsley-id="7352">
                Update
            </a>
            <a href="javascript:void(0)" class="btn btn-default ">
                <input type="checkbox" name="checkdata[2][]" id="delete-google-reports" class="permission-delete" value="3" data-parsley-multiple="delete" data-parsley-id="1100">
                Delete
            </a>
    </div>
</div>
</div>

</div>                            

</div>
	<hr>	

		<div class="row">
			<div class="col-sm-6">
				<div class="form-check">
					<label>User Status</label><br>
					<div class="switch-field">
							<?php
							if(!empty($article_data)){
								if($article_data[0]['status']=='Active'){
									$published = "checked";
								}else{
									$draft = "checked";
								} 
							}else{
								$draft='checked';
							}
							?>
							
						<input type="radio" id="published" name="article_status" value="Active" <?= @$published?>/>
						<label for="published">Active</label>
						<input type="radio" id="draft" name="article_status" value="Inactive"  <?= @$draft?>/>
						<label for="draft">Inactive</label>
					</div>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="form-check">
					<label>Approval Status</label><br>
					<div class="switch-field">
							<?php
							if(!empty($package_data)){
								if($package_data[0]['admin_status']=='Approved'){
									$approved = "checked";
								}else{
									$pending = "checked";
								} 
							}else{
								$pending= 'checked';
							}
							?>
						<input type="radio" id="approved" name="admin_status" value="Approved" <?= @$approved?>/>
						<label for="approved">Approved</label>
						<input type="radio" id="pending" name="admin_status" value="Pending"  <?= @$pending?>/>
						<label for="pending">Pending</label>
					</div>
				</div>
			</div>

		</div>	
												
							<div class="form-group">
								<button type="submit" class="btn btn-black">Create</button>
								<a href="<?= base_url('useraccess')?>" class="btn btn-cancel btn-border">Cancel</a>
							</div>
							</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){

});


function removeFile(type,id){
var r = confirm("want to sure delete this file");
	if (r == true) {
		if(type && id){
			var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
			var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				$.ajax({
					url: "<?= base_url('article/removeFile')?>",
					data:{type,id,[csrfName]: csrfHash },
					dataType: "json",
					type: "POST",
					success: function(response){
						if(response.success){
							swal("done", response.msg, {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							}).then(
							function() {
								location.reload();
							});
						}else{	
							swal("Failed", response.msg, {
								icon : "error",
								buttons: {        			
									confirm: {
										className : 'btn btn-danger'
									}
								},
							});
						}
					}
				});
		}else{
			alert("OOPS somethings went wrong")
		}
	}
}

// use for form submit
var vRules = 
{
	first_name:{required:true},
	last_name:{required:true},
	email:{required:true},
	username:{required:true},
	password:{required:true},
	password_confirmation:{required:true},
};
var vMessages = 
{
	first_name:{required:"Please Enter First Name."},
	last_name:{required:"Please Enter Last Name."},
	email:{required:"Please Enter Email."},
	username:{required:"Please Enter Username."},
	password:{required:"Please Enter Password."},
	password_confirmation:{required:"Please Enter password_confirmation."},

};

$("#form-article").validate({
	ignore:[],
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('useraccess/submitform')?>";
		$("#form-article").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('useraccess')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>