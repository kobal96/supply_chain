<body>
	<div class="wrapper">
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="row">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-10 col-lg-10">
									<h1 class="ff-page-title">Welcome Admin,</h1>
									<p>A few items for your <strong>approval</strong></p>
							</div>	

							<div class="col-12 col-sm-12 col-md-2 col-lg-2">  
								<!-- <select class="form-control form-control" id="defaultSelect">
									<option>Monthly</option>
									<option>Weekly</option>
									<option>Daily</option>
									<option>Yearly</option>
								</select> -->	
							</div>						
						</div>
					</div>					
					</div>
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="row">
													<div class="col-sm-12">	
														<ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
															<li class="nav-item">
																<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Articles</a>
															</li>
															<li class="nav-item">
																<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Comments</a>
															</li>															
														</ul>
														<div class="tab-content mt-2 mb-3" id="pills-tabContent">
															<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
																<!--<h2><strong>Articles</strong></h2>-->
																<!--<hr/>-->
																<div class="table-responsive">
																	<table id="add-row" class="display table table-striped table-hover" >
																		<thead>
																			<tr>			
																				<th>Title</th>
																				<th>Edition</th>	
																				<th>Author</th>
																				<th>Status</th>	
																				<th>Updated</th>	
																				<th>Actions</th>
																			</tr>
																		</thead>
																		<tfoot>
																			<tr>
																				<th>Title</th>
																				<th>Edition</th>	
																				<th>Author</th>
																				<th>Status</th>	
																				<th>Updated</th>	
																				<th>Actions</th>
																			</tr>
																		</tfoot>
																		<tbody>
																			<tr>
																				<td>Procurement Today and Tomorrow</td>
																				<td>July - August 2020</td>
																				<td>Celerity Editorial Team</td>
																				<td>For Review</td>
																				<td>02 Oct 2020</td>
																				<td>
																					<div class="form-button-action">
																						<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																							<i class="fa fa-edit"></i>
																						</a>
																						<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																							<i class="fa fa-times"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>Procurement Today and Tomorrow</td>
																				<td>July - August 2020</td>
																				<td>Celerity Editorial Team</td>
																				<td>For Review</td>
																				<td>02 Oct 2020</td>
																				<td>
																					<div class="form-button-action">
																						<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																							<i class="fa fa-edit"></i>
																						</a>
																						<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																							<i class="fa fa-times"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>Procurement Today and Tomorrow</td>
																				<td>July - August 2020</td>
																				<td>Celerity Editorial Team</td>
																				<td>For Review</td>
																				<td>02 Oct 2020</td>
																				<td>
																					<div class="form-button-action">
																						<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																							<i class="fa fa-edit"></i>
																						</a>
																						<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																							<i class="fa fa-times"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>Procurement Today and Tomorrow</td>
																				<td>July - August 2020</td>
																				<td>Celerity Editorial Team</td>
																				<td>For Review</td>
																				<td>02 Oct 2020</td>
																				<td>
																					<div class="form-button-action">
																						<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																							<i class="fa fa-edit"></i>
																						</a>
																						<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																							<i class="fa fa-times"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>Procurement Today and Tomorrow</td>
																				<td>July - August 2020</td>
																				<td>Celerity Editorial Team</td>
																				<td>For Review</td>
																				<td>02 Oct 2020</td>
																				<td>
																					<div class="form-button-action">
																						<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																							<i class="fa fa-edit"></i>
																						</a>
																						<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																							<i class="fa fa-times"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																		</tbody>
																	</table>
															</div>
															</div>
															<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
																<div class="table-responsive">
																	<table id="add-row" class="display table table-striped table-hover" >
																		<thead>
																			<tr>			
																				<th>Comment</th>						
																				<th>Author</th>
																				<th>Status</th>	
																				<th>Updated</th>	
																				<th>Actions</th>
																			</tr>
																		</thead>
																		<tfoot>
																			<tr>
																				<th>Comment</th>
																				<th>Author</th>
																				<th>Status</th>	
																				<th>Updated</th>	
																				<th>Actions</th>
																			</tr>
																		</tfoot>
																		<tbody>
																			<tr>
																				<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</td>
																				<td>Celerity Editorial Team</td>
																				<td>For Review</td>
																				<td>02 Oct 2020</td>
																				<td>
																					<div class="form-button-action">
																						<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																							<i class="fa fa-edit"></i>
																						</a>
																						<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																							<i class="fa fa-times"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</td>
																				<td>Celerity Editorial Team</td>
																				<td>For Review</td>
																				<td>02 Oct 2020</td>
																				<td>
																					<div class="form-button-action">
																						<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																							<i class="fa fa-edit"></i>
																						</a>
																						<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																							<i class="fa fa-times"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</td>
																				<td>Celerity Editorial Team</td>
																				<td>For Review</td>
																				<td>02 Oct 2020</td>
																				<td>
																					<div class="form-button-action">
																						<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																							<i class="fa fa-edit"></i>
																						</a>
																						<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																							<i class="fa fa-times"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</td>
																				<td>Celerity Editorial Team</td>
																				<td>For Review</td>
																				<td>02 Oct 2020</td>
																				<td>
																					<div class="form-button-action">
																						<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																							<i class="fa fa-edit"></i>
																						</a>
																						<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																							<i class="fa fa-times"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																			<tr>
																				<td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</td>
																				<td>Celerity Editorial Team</td>
																				<td>For Review</td>
																				<td>02 Oct 2020</td>
																				<td>
																					<div class="form-button-action">
																						<a href="enquiry-reply.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Reply">
																							<i class="fa fa-edit"></i>
																						</a>
																						<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																							<i class="fa fa-times"></i>
																						</a>
																					</div>
																				</td>
																			</tr>
																		</tbody>
																	</table>
															</div>
															</div>															
														</div>
														
														</div>
													</div>
												</div>

												<div class="row mt-5">
													<div class="col-sm-12">
														<div class="row ff-title-block">
															<div class="col-12 col-sm-12 col-md-7 col-lg-9">
																<h2>
																<strong>Total Visits</strong>: 
																<span style="color:#00807D;">227</span>
																<strong> &nbsp;&nbsp;&nbsp;Today's Visits</strong>:
																<span style="color:#00807D;">27</span></h2>
															</div>
															<div class="col-12 col-sm-12 col-md-5 col-lg-3">
																<select class="form-control form-control" id="defaultSelect">
																	<option>12 Aug - 17 Aug, 2020</option>
																	<option>12 Aug - 17 Aug, 2020</option>
																	<option>12 Aug - 17 Aug, 2020</option>
																</select>
															</div>
														</div>
														<hr/>
														<div class="row">
															<div class="col-12">
																Total Visits Graph will come here													
															</div>
														</div>	
														
													</div>

													<div class="col-sm-6">
														<div class="row ff-title-block mt-5">
															<div class="col-12 col-sm-12 col-md-7 col-lg-7">
																<h2><strong>Device Type</strong></h2>
															</div>															
														</div>
														<hr/>
														<div class="row">
															<div class="col-12">
																Device Type Graph will come here													
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="row ff-title-block  mt-5">
															<div class="col-12 col-sm-12 col-md-7 col-lg-7">
																<h2><strong>User Locations</strong></h2>
															</div>															
														</div>
														<hr/>
														<div class="row">
															<div class="col-12">
																User Location Graph will come here
															</div>
														</div>
													</div>

												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function() {
			$('#add-row').DataTable({
				"pageLength": 5,
			});
		});
	</script>

</body>

</html>

