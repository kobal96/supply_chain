<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/medium-editor/latest/css/medium-editor.min.css" type="text/css" charset="utf-8">
	
    <link rel="stylesheet" href="<?php echo base_url("assets/helpers/medium-demo.css");?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/helpers/medium-insert.css"); ?>">
    <style>
    .btn-primary, .btn-primary:hover {
        background-color: #000000;
        border-color: #000000;
        color: #fff;
    }
    </style>
    <style>
    .medium-insert-images figure figcaption,
    .mediumInsert figure figcaption,
    .medium-insert-embeds figure figcaption,
    .mediumInsert-embeds figure figcaption {
        font-size: 12px;
        line-height: 1.2em;
    }
    .medium-insert-images-slideshow figure {
        width: 100%;
    }
    .medium-insert-images-slideshow figure img {
        margin: 0;
    }
    .medium-insert-images.medium-insert-images-grid.small-grid figure {
        width: 12.5%;
    }
    @media (max-width: 750px) {
        .medium-insert-images.medium-insert-images-grid.small-grid figure {
            width: 25%;
        }
    }
    @media (max-width: 450px) {
        .medium-insert-images.medium-insert-images-grid.small-grid figure {
            width: 50%;
        }
    }
    </style>
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">New Article</h1> 
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('article') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div>
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form class="" id="form-article" method="post" enctype="multipart/form-data">
									
									<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
									<input name="id" id="id" value="<?= (!empty($article_data[0]['id'])?$article_data[0]['id']:"") ?>" type="hidden" >
									
									<label for="">Basic Information</label><hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="Title">Title</label>
												<input type="text" class="form-control" id="title" name="title" value="<?= (!empty($article_data[0]['title'])?$article_data[0]['title']:"") ?>" placeholder="">	
											</div>
										</div>
										
										<div class="col-sm-6">
											<div class="form-group">
												<label for="author">Author</label>
												<input type="text" class="form-control" id="author" name="author" value="<?= (!empty($article_data[0]['author'])?$article_data[0]['author']:"") ?>" placeholder="">	
											</div>
										</div>


										<div class="col-sm-6">
											<div class="form-group">
												<label for="edition_id">Select Edition</label>
												<select class="form-control form-control " id="edition_id" name="edition_id"> 
												<option value="">Select Edition</option>
													<?php
													$sel="";
													foreach ($editions as $key => $value) {
														$sel = ($value['id'] == $article_data[0]['edition_id']?"selected":"");
														?>
														<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>
													<?php }	
													?>
												</select>
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="category">Category</label>												
													<select class="newvideotags" name="category[]" id="category" multiple="multiple" required>	
														<?php $sel="";	
															foreach ($categories as $key => $value) {														
																if(!empty($categories)){														
																	$sel = (in_array($value['id'],$article_category_data)?"selected":" ");															}?>	
																	<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>								
														<?php } ?>
													</select>						
											</div>
										</div>


										<div class="col-sm-6">
											<div class="form-group">
												<label for="tag">Tags</label>												
													<select class="newvideotags" name="tag[]" id="tag" multiple="multiple" required>	
														<?php $sel="";	
															foreach ($tags as $key => $value) {														
																if(!empty($tags)){														
																	$sel = (in_array($value['id'],$article_tag_data)?"selected":" ");															}?>	
																	<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>								
														<?php } ?>
													</select>						
											</div>
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<label for="featured_month">Featured for the month ?</label>
												<select class="form-control form-control" id="featured_month" name="featured_month"> 
												<option value="">Select Featured for the month</option>
												<option value="Yes"<?= (!empty($article_data[0]['month_featured']) && $article_data[0]['month_featured'] == 'Yes' ?"selected":"") ?>>Yes</option>
												<option value="No" <?php  (!empty($article_data[0]['month_featured']) && $article_data[0]['month_featured'] == 'No' ?"selected":"")?>>No</option>
													
												</select>
											</div>
										</div>

										<div class="col-sm-12">
											<div class="form-group">
												<label for="article_description">Article Description</label>
												<textarea class="form-control my-text-editor" style="min-height: 200px; "id="article_description" name="article_description" rows="15" ><?= (!empty($article_data[0]['description'])?$article_data[0]['description']:"") ?></textarea>	
											</div>
										</div>
									
									</div>
									
									<label for="">Meta Information</label><hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_title">Meta Title</label>
												<input type="text" class="form-control" id="meta_title" value="<?= (!empty($article_data[0]['meta_title'])?$article_data[0]['meta_title']:"") ?>"  name="meta_title" placeholder="">	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="slug">URL Slug</label>
												<input type="text" class="form-control" id="slug" value="<?= (!empty($article_data[0]['slug'])?$article_data[0]['slug']:"") ?>"  name="slug" placeholder="">	
											</div>
										</div>
									</div>


									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_keyword">Meta Keywords</label>
												<textarea class="form-control" id="meta_keyword" name="meta_keyword" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($article_data[0]['meta_keyword'])?$article_data[0]['meta_keyword']:"") ?></textarea>	
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="meta_description">Meta Description</label>
												<textarea class="form-control" id="meta_description" name="meta_description" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."><?= (!empty($article_data[0]['meta_description'])?$article_data[0]['meta_description']:"") ?></textarea>	
											</div>
										</div>
									</div>
											
											
									<label for="">Upload Image</label><hr>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="articlefile">Article Image</label>
												<?php $req= '';
												if(empty($article_data[0]['image'])){
													$req =''; 
												}else{?>
												<br>
													<img src="<?= FRONT_URL.'/images/article_image/'.$article_data[0]['image']?>"class="img-thumbnail" alt="Cinque Terre" width="250" height="300">
													<input name="pre_articlefile_name" id="pre_articlefile_name" value="<?= (!empty($article_data[0]['image'])?$article_data[0]['image']:"") ?>" type="hidden">
													<a href="#" class="btn btn-danger" onclick="removeFile('IMAGE','<?= $article_data[0]['id']?>')">  X </a>

												<?php }?>
												
												<input type="file" class="form-control-file" id="articlefile" name="articlefile" <?= $req;?> >
											</div>
										</div>

									</div>
									<hr>	

									<div class="row">
										<div class="col-sm-6">
											<div class="form-check">
												<label>Publish Status</label><br>
												<div class="switch-field">
														<?php $published =  '';
															  $draft='checked';
														if(!empty($article_data)){
															if($article_data[0]['status']=='Published'){
																$published = "checked";
															}else{
																$draft = "checked";
															} 
														}?>
														
													<input type="radio" id="published" name="article_status" value="Published" <?= $published?>/>
													<label for="published">Published</label>
													<input type="radio" id="draft" name="article_status" value="Draft"  <?= $draft?>/>
													<label for="draft">Draft</label>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-check">
												<label>Approval Status</label><br>
												<div class="switch-field">
														<?php $approved = '';
															 $pending= 'checked';
														if(!empty($article_data)){
															if($article_data[0]['admin_status']=='Approved'){
																$approved = "checked";
															}else{
																$pending = "checked";
															} 
														}?>
													<input type="radio" id="approved" name="admin_status" value="Approved" <?= $approved?>/>
													<label for="approved">Approved</label>
													<input type="radio" id="pending" name="admin_status" value="Pending"  <?= $pending?>/>
													<label for="pending">Pending</label>
												</div>
											</div>
										</div>
									</div>					
											
											
												
									<div class="form-group">
										<button  type="submit" class="btn btn-black">Create</button>
										<a href="<?= base_url('article')?>" class="btn btn-cancel btn-border">Cancel</a>
									</div>
									</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){
});


function removeFile(type,id){
var r = confirm("want to sure delete this file");
	if (r == true) {
		if(type && id){
			var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
			var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				$.ajax({
					url: "<?= base_url('article/removeFile')?>",
					data:{type,id,[csrfName]: csrfHash },
					dataType: "json",
					type: "POST",
					success: function(response){
						if(response.success){
							swal("done", response.msg, {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							}).then(
							function() {
								location.reload();
							});
						}else{	
							swal("Failed", response.msg, {
								icon : "error",
								buttons: {        			
									confirm: {
										className : 'btn btn-danger'
									}
								},
							});
						}
					}
				});
		}else{
			alert("OOPS somethings went wrong")
		}
	}
}

// use for form submit
var vRules = 
{
	title:{required:true},
	author:{required:true},
	edition_id:{required:true},
	"category[]":{required:true},
	"tag[]":{required:true},
	slug:{required:true},
	featured_month:{required:true},
	meta_keyword:{required:true},
	meta_title:{required:true},
	meta_description:{required:true},
};
var vMessages = 
{
	title:{required:"Please Enter Article Name."},
	author:{required:"Please Enter Author Name."},
	edition_id:{required:"Please Select Edition."},
	"category[]":{required:"Please Select Categories ."},
	"tag[]":{required:"Please Select Tags ."},
	slug:{required:"Please Enter URL Slug."},
	featured_month:{required:"Please Select featured Month"},
	meta_keyword:{required:"Please Enter Meta Keywords."},
	meta_title:{required:"Please Enter Meta Title."},
	meta_description:{required:"Please Enter Meta Description."},

};

$("#form-article").validate({
	ignore:[],
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('article/submitform')?>";
		$("#form-article").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('article')?>";
						// $this->router->fetch_class().'/'.$this->router->fetch_method() ?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});


</script>
<script src="<?php echo base_url()?>assets/helpers/jquery.ui.widget.js")></script>
        <script src="<?php echo base_url()?>assets/helpers/jquery.iframe-transport.js")></script>
        <script src="<?php echo base_url()?>assets/helpers/jquery.fileupload.js")></script>
        <script src="https://cdn.jsdelivr.net/medium-editor/latest/js/medium-editor.min.js"></script>
        <script src="<?php echo base_url()?>assets/helpers/runtime.js")></script>
        <script src="<?php echo base_url()?>assets/helpers/sortable.js")></script>
        <script src="<?php echo base_url()?>assets/helpers/cycle.js")></script>
        <script src="<?php echo base_url()?>assets/helpers/cycle.center.js")></script>
        <script src="<?php echo base_url()?>assets/helpers/medium-insert.js")></script>
        <script src="<?php echo base_url()?>assets/helpers/autolist.js")></script>
        <script type="text/javascript">
        var autolist = new AutoList();
        var editor = new MediumEditor('.my-text-editor',{
            extensions: {
                'autolist': autolist
            },
            placeholder: {
                /* This example includes the default options for placeholder,
                if nothing is passed this is what it used */
                text: 'Article description body',
                hideOnClick: true
            },
            toolbar: {
                buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'unorderedlist','orderedlist']
            }
        });

        $('.my-text-editor').mediumInsert({
            editor: editor,
            addons: {
                images: {
                    uploadScript: null,
                    deleteScript: null,
                    autoGrid: 0,
                    captionPlaceholder: 'Type caption for image',
                    styles: {
                        slideshow: {
                            label: '',
                            added: function ($el) {
                                $el
                                .data('cycle-center-vert', true)
                                .cycle({
                                    slides: 'figure'
                                });
                            },
                            removed: function ($el) {
                                $el.cycle('destroy');
                            }
                        },grid: {
                    label: ''
                },
                    },
                    actions: null
                },
                embeds: false
            }
        });
	
		</script>