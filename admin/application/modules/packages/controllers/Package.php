<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Package extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('packagemodel','',TRUE);
	}

	public function index(){
		// $result = array();
		// $condition = "1=1";
		// $main_table = array("tbl_magazine_articles as tma", array("tma.*"));
		// $join_tables =  array();
		// $join_tables = array(
		// 					array("left", "tbl_magazine_editions as  tme", "tme.id = tma.edition_id", array("tma.title as edition_name"))
		// 					);
	
		// $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tma.id" => "DESC"),"",null); 
		//  // fetch query
		// $result['article_data'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
		// // $result['article_data'] = $this->common->getData("tbl_magazine_articles",'*',$condition,"id","desc");
		// echo "<pre>";
		// print_r($result);
		// exit;
		$condition = "1=1";
		$result['package_data'] = $this->common->getData("tbl_packages",'*',$condition);
		echo "<pre>";
		print_r($result['package_data']);
		exit;
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function addEdit(){

		$id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];
			$condition = "id ='".$id."' ";
			$result['article_data'] = $this->common->getData("tbl_magazine_articles",'*',$condition);
			$condition = "article_id ='".$id."' ";	
			$article_category = $this->common->getData("tbl_magazine_article_category",'category_id',$condition);
			$article_tag = $this->common->getData("tbl_magazine_article_tag",'tag_id',$condition);

			$article_category_data = array();
			foreach ($article_category as $key => $value) {
				array_push($article_category_data,$value['category_id']);
			}
			$result['article_category_data'] = $article_category_data;

			$article_tag_data = array();
			foreach ($article_tag as $key => $value) {
				array_push($article_tag_data,$value['tag_id']);
			}
			$result['article_tag_data'] = $article_tag_data;
		
		}	
		$condition = " admin_status ='Approved' ";
		$result['editions'] = $this->common->getData("tbl_magazine_editions",'id,title',$condition);
		$result['tags'] = $this->common->getData("tbl_magazine_tags",'id,title',$condition);
		$result['categories'] = $this->common->getData("tbl_magazine_categories",'id,title',$condition);


		
			// echo "<pre>";
			// print_r($result);
			// exit;
		//echo $id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}



	function removeFile(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = " id = ".$this->input->post('id');
			$exisiting_data =$this->common->getData("tbl_magazine_articles",'*',$condition);
			if(!empty($this->input->post('id')) && $this->input->post('type') == "PDF"){
				if(is_array($exisiting_data) && !empty($exisiting_data[0]['article_pdf']) && file_exists(DOC_ROOT_FRONT."/images/article_pdf/".$exisiting_data[0]['article_pdf'])){
					unlink(DOC_ROOT_FRONT."/images/article_pdf/".$exisiting_data[0]['article_pdf']);
					$data['article_pdf'] = '';
				}
			}else{
				if(is_array($exisiting_data) && !empty($exisiting_data[0]['image']) && file_exists(DOC_ROOT_FRONT."/images/article_image/".$exisiting_data[0]['image'])){
					unlink(DOC_ROOT_FRONT."/images/article_image/".$exisiting_data[0]['image']);
					$data['image'] = '';
				}
			}

			$condition = "id = '".$this->input->post('id')."' ";
			$result = $this->common->updateData("tbl_magazine_articles",$data,$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}	
		}			
	}

	public function submitForm(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = "title = '".$this->input->post('title')."' ";
			if($this->input->post('id') && $this->input->post('id') > 0){
				$condition .= " AND  id != ".$this->input->post('id')." ";
			}			
			$check_name = $this->common->getData("tbl_magazine_articles",'*',$condition);

			// print_r($check_name);
			// exit;
			if(!empty($check_name[0]['id'])){
				echo json_encode(array("success"=>false, 'msg'=>'Article Name Already Present!'));
				exit;
			}
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["articlefile"]["name"])){
				 $config = array();
				$config['upload_path'] = DOC_ROOT_FRONT."/images/article_image/";
				$config['max_size']    = '0';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['min_width']            = 1000;
                $config['min_height']           = 1000;
				// $config['allowed_types'] = '*';
				// $config['file_name']     = md5(uniqid("100_ID", true));
				$config['file_name']     = $_FILES["articlefile"]["name"];
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("articlefile")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}else{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name']; 
					$this->setCompresseredImage($image_data['upload_data']['file_name']);

				}	
				/* Unlink previous category image */
				if(!empty($this->input->post('id'))){	
					$condition_image = " id = ".$this->input->post('id');
					$image =$this->common->getData("tbl_magazine_articles",'*',$condition_image);
					if(is_array($image) && !empty($image[0]['image']) && file_exists(DOC_ROOT_FRONT."/images/article_image/".$image[0]['image']))
					{
						unlink(DOC_ROOT_FRONT."/images/article_image/".$image[0]['image']);
					}
				}
			}else{
				$thumnail_value = $this->input->post('pre_articlefile_name');
			}

			$data['image'] = (!empty($thumnail_value)?$thumnail_value:"");
			$data['title'] = $this->input->post('title');
			$data['description'] = $this->input->post('description');
			$data['author'] = $this->input->post('author');
			$data['edition_id'] = $this->input->post('edition_id');
			$data['month_featured'] = $this->input->post('featured_month');
			$data['description'] = $this->input->post('article_description');
			$data['slug'] = $this->input->post('slug');
			$data['meta_keyword'] = $this->input->post('meta_keyword');
			$data['meta_title'] = $this->input->post('meta_title');
			$data['meta_description'] = $this->input->post('meta_description');
			$data['status'] = $this->input->post('article_status');
			$data['admin_status'] = $this->input->post('admin_status');
			$data['updated_at'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $this->session->userdata('supply_chain_admin')[0]['id'];
			if(!empty($this->input->post('id'))){
				$condition = "id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_magazine_articles",$data,$condition);
				if($result){

					$condition = "article_id = '".$this->input->post('id')."' ";
					$this->common->deleteRecord("tbl_magazine_article_category",$condition);
					$this->common->deleteRecord("tbl_magazine_article_tag",$condition);
					if(!empty($_POST['category']) && isset($_POST['category'])){
						foreach ($_POST['category'] as $key => $value) {
							$article_data_category = array();
							$article_data_category['article_id'] = $this->input->post('id');
							$article_data_category['category_id'] = $value;
							$this->common->insertData('tbl_magazine_article_category',$article_data_category,'1');
							// echo "inside category  update";
							// exit;
						}
					}

					if(!empty($_POST['tag']) && isset($_POST['tag'])){
						foreach ($_POST['tag'] as $key => $value) {
							$article_data_tag = array();
							$article_data_tag['article_id'] = $this->input->post('id');
							$article_data_tag['tag_id'] = $value;
							$this->common->insertData('tbl_magazine_article_tag',$article_data_tag,'1');
						}
					}
					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");
				$data['created_by'] =  $this->session->userdata('supply_chain_admin')[0]['id'];
				$result = $this->common->insertData('tbl_magazine_articles',$data,'1');
				if(!empty($result)){
					if(!empty($_POST['category']) && isset($_POST['category'])){
						foreach ($_POST['category'] as $key => $value) {
							$article_data_category = array();
							$article_data_category['article_id'] = $result;
							$article_data_category['category_id'] = $value;
							$this->common->insertData('tbl_magazine_article_category',$article_data_category,'1');
						}
					}

					if(!empty($_POST['tag']) && isset($_POST['tag'])){
						foreach ($_POST['tag'] as $key => $value) {
							$article_data_tag = array();
							$article_data_tag['article_id'] = $result;
							$article_data_tag['tag_id'] = $value;
							$this->common->insertData('tbl_magazine_article_tag',$article_data_tag,'1');
						}
					}
					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
	public function setCompresseredImage($image_name){
		$config1 = array();
		$this->load->library('image_lib');
		$config1['image_library'] = 'gd2';
		$config1['source_image'] = DOC_ROOT_FRONT."/images/article_image/".$image_name;
		$config1['maintain_ratio'] = TRUE;
		$config1['quality'] = '100%';
		$config1['width'] = 1000;
		$config1['height'] = 1000;
		$config1['new_image'] = DOC_ROOT_FRONT."/images/article_image/".$image_name;
		$this->image_lib->initialize($config1);
		$this->image_lib->resize();
		$this->image_lib->clear();
		}
		
}
?>