
<div class="wrapper">
	<div class="main-panel">
		<div class="container">
			<div class="page-inner">
				<div class="col-12">
					<div class="row ff-title-block">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<h1 class="ff-page-title">Spotlight</h1> 
						</div>
						<!-- <div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
							<a href="<?=base_url('spotlight') ?>" class="btn btn-primary ff-page-title-btn">
								Cancel
							</a>
						</div> -->
					</div>
				</div>					
				<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
							<form class="" id="form-article" method="post" enctype="multipart/form-data">		
								<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">
								<!-- <input name="id" id="id" value="<?= (!empty($article_data[0]['id'])?$article_data[0]['id']:"") ?>" type="hidden" > -->
								<input name="id" id="id" value="" type="hidden" >
								
								<label for="">Featured</label><hr>
								<div class="row">
									<input type="hidden" name="type[]" value="Featured">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="edition_id">Select Category</label>
											<select class="form-control selectclass" id="edition_id" name="edition_id[]"> 
											<option value="">--- Select Category ---</option>
												<?php
												$sel="";
												foreach ($categories as $key => $value) {
													$sel = ($value['id'] == $spotlight[0]['category_id']?"selected":"");
													?>
													<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>
												<?php }	
												?>
											</select>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="form-group">
											<label for="article_id">Select Article</label>
											<?php
											$condition = "tme.category_id ='".$spotlight[0]['category_id']."' ";
		                                    $main_table = array("tbl_magazine_articles as tma", array("tma.*"));
											$join_tables =  array();
											$join_tables = array(
																array("", "tbl_magazine_article_category as tme", "tme.article_id = tma.id", array())
																);
										
											$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tma.id" => "DESC"),"",null);
											$article_data = $this->common->MySqlFetchRow($rs, "array"); 
											 ?>
											<select class="form-control selectclass" id="article_id" name="article_id[]"> 
											<option value="">--- Select Article ---</option>
											<?php
												$sel="";
												foreach ($article_data as $key => $value) {
													$sel = ($value['id'] == $spotlight[0]['article_id']?"selected":"");
													?>
													<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>
												<?php }	
												?>
												
											</select>
										</div>
									</div>

									<!-- <div class="col-sm-6">
										<div class="form-group">
											<label for="category">Category</label>												
												<select class="newvideotags" name="category[]" id="category" multiple="multiple" required>	
													<?php $sel="";	
														foreach ($categories as $key => $value) {														
															if(!empty($categories)){														
																$sel = (in_array($value['id'],$article_category_data)?"selected":" ");															}?>	
																<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>								
													<?php } ?>
												</select>						
										</div>
									</div> -->

									<!-- <div class="col-sm-6">
										<div class="form-group">
											<label for="tag">Tags</label>												
												<select class="newvideotags" name="tag[]" id="tag" multiple="multiple" required>	
													<?php $sel="";	
														foreach ($tags as $key => $value) {														
															if(!empty($tags)){														
																$sel = (in_array($value['id'],$article_tag_data)?"selected":" ");															}?>	
																<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>								
													<?php } ?>
												</select>						
										</div>
									</div> -->

								</div>
								<br>
								<br>
								
								<label for="">Spotlight List</label><hr>
								<div class="row">
									<div class="col-sm-12">
									<label for="">Article No.1</label>
								    </div>
									
									<input type="hidden" name="type[]" value="Spotlight">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="edition_id1">Select Category</label>
											<select class="form-control selectclass edition_id1" data-catid="article_id1" id="edition_id1" name="edition_id[]"> 
											<option value="">--- Select Category ---</option>
												<?php
												$sel="";
												foreach ($categories as $key => $value) {
													$sel = ($value['id'] == $spotlight[1]['category_id']?"selected":"");
													?>
													<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>
												<?php }	
												?>
											</select>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="form-group">
											<label for="article_id1">Select Article</label>
											<?php
		                                    $condition = "tme.category_id ='".$spotlight[1]['category_id']."' ";
		                                    $main_table = array("tbl_magazine_articles as tma", array("tma.*"));
											$join_tables =  array();
											$join_tables = array(
																array("", "tbl_magazine_article_category as tme", "tme.article_id = tma.id", array())
																);
										
											$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tma.id" => "DESC"),"",null);
											$article_data1 = $this->common->MySqlFetchRow($rs, "array"); 
											 ?>
											<select class="form-control selectclass" id="article_id1" name="article_id[]"> 
											<option value="">--- Select Article ---</option>
											<?php
												$sel="";
												foreach ($article_data1 as $key => $value) {
													$sel = ($value['id'] == $spotlight[1]['article_id']?"selected":"");
													?>
													<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>
												<?php }	
												?>
												
											</select>
										</div>
									</div>
									
								</div>

								<div class="row">
									<div class="col-sm-12">
									<label for="">Article No.2</label>
								    </div>
									<input type="hidden" name="type[]" value="Spotlight">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="edition_id2">Select Category</label>
											<select class="form-control selectclass edition_id1" data-catid="article_id2" id="edition_id2" name="edition_id[]"> 
											<option value="">--- Select Category ---</option>
												<?php
												$sel="";
												foreach ($categories as $key => $value) {
													$sel = ($value['id'] == $spotlight[2]['category_id']?"selected":"");
													?>
													<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>
												<?php }	
												?>
											</select>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="form-group">
											<label for="article_id2">Select Article</label>
											<?php
		                                    $condition = "tme.category_id ='".$spotlight[2]['category_id']."' ";
		                                    $main_table = array("tbl_magazine_articles as tma", array("tma.*"));
											$join_tables =  array();
											$join_tables = array(
																array("", "tbl_magazine_article_category as tme", "tme.article_id = tma.id", array())
																);
										
											$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tma.id" => "DESC"),"",null);
											$article_data2 = $this->common->MySqlFetchRow($rs, "array"); 
											 ?>
											<select class="form-control selectclass" id="article_id2" name="article_id[]"> 
											<option value="">--- Select Article ---</option>
											<?php
												$sel="";
												foreach ($article_data2 as $key => $value) {
													$sel = ($value['id'] == $spotlight[2]['article_id']?"selected":"");
													?>
													<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>
												<?php }	
												?>
												
											</select>
										</div>
									</div>
									
								</div>

								<div class="row">
									<div class="col-sm-12">
									<label for="">Article No.3</label>
								    </div>
									<input type="hidden" name="type[]" value="Spotlight">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="edition_id3">Select Category</label>
											<select class="form-control selectclass edition_id1" data-catid="article_id3" id="edition_id3" name="edition_id[]"> 
											<option value="">--- Select Category ---</option>
												<?php
												$sel="";
												foreach ($categories as $key => $value) {
													$sel = ($value['id'] == $spotlight[3]['category_id']?"selected":"");
													?>
													<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>
												<?php }	
												?>
											</select>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="form-group">
											<label for="article_id3">Select Article</label>
											<?php
											 $condition = "tme.category_id ='".$spotlight[3]['category_id']."' ";
		                                    $main_table = array("tbl_magazine_articles as tma", array("tma.*"));
											$join_tables =  array();
											$join_tables = array(
																array("", "tbl_magazine_article_category as tme", "tme.article_id = tma.id", array())
																);
										
											$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tma.id" => "DESC"),"",null);
											$article_data3 = $this->common->MySqlFetchRow($rs, "array"); 
											 ?>
											<select class="form-control selectclass" id="article_id3" name="article_id[]"> 
											<option value="">--- Select Article ---</option>
											<?php
												$sel="";
												foreach ($article_data3 as $key => $value) {
													$sel = ($value['id'] == $spotlight[3]['article_id']?"selected":"");
													?>
													<option value="<?= $value['id']?>" <?= $sel?> ><?=$value['title']?></option>
												<?php }	
												?>
												
											</select>
										</div>
									</div>
									
								</div>
										
								
								<hr>					
								
								<div class="form-group">
									<button  type="submit" class="btn btn-black">Save</button>
									<a href="<?= base_url('spotlight')?>" class="btn btn-cancel btn-border">Cancel</a>
								</div>
								</form>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>						
</div>

<script>
$(document).ready(function(){

});


function removeFile(type,id){
var r = confirm("want to sure delete this file");
	if (r == true) {
		if(type && id){
			var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
			var csrfHash = $('.txt_csrfname').val(); // CSRF hash
				$.ajax({
					url: "<?= base_url('article/removeFile')?>",
					data:{type,id,[csrfName]: csrfHash },
					dataType: "json",
					type: "POST",
					success: function(response){
						if(response.success){
							swal("done", response.msg, {
								icon : "success",
								buttons: {        			
									confirm: {
										className : 'btn btn-success'
									}
								},
							}).then(
							function() {
								location.reload();
							});
						}else{	
							swal("Failed", response.msg, {
								icon : "error",
								buttons: {        			
									confirm: {
										className : 'btn btn-danger'
									}
								},
							});
						}
					}
				});
		}else{
			alert("OOPS somethings went wrong")
		}
	}
}

// use for form submit
var vRules = 
{
	edition_id:{required:true},
	article_id:{required:true},
	edition_id1:{required:true},
	article_id1:{required:true},
};
var vMessages = 
{
	edition_id:{required:"Please Select Edition."},
	article_id:{required:"Please Select Article."},
	edition_id1:{required:"Please Select Edition."},
	article_id1:{required:"Please Select Article."},
};

$("#form-article").validate({
	ignore:[],
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{	
		var act = "<?= base_url('spotlight/submitform')?>";
		$("#form-article").ajaxSubmit({
			url: act, 
			type: 'post',
			dataType: 'json',
			cache: false,
			clearForm: false,
			async:false,
			beforeSubmit : function(arr, $form, options){
				$(".btn btn-black").hide();
			},
			success: function(response) 
			{
				if(response.success){
					swal("done", response.msg, {
						icon : "success",
						buttons: {        			
							confirm: {
								className : 'btn btn-success'
							}
						},
					}).then(
					function() {
						window.location = "<?= base_url('spotlight')?>";
					});
				}else{	
					swal("Failed", response.msg, {
						icon : "error",
						buttons: {        			
							confirm: {
								className : 'btn btn-danger'
							}
						},
					});
				}
			}
		});
	}
});

$(document).on('change', '#edition_id', function() {
  	var d = $(this).val();

  	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('spotlight/getarticle')?>",
			data:{main: d,[csrfName]: csrfHash },
			dataType: "json",
			type: "POST",
			success: function(data){
				//data=JSON.parse(data);
				var select = "";
                    if (data.length == 0) {
                        select += '<option value="">There is no Article</option>';
                    }
                    else {
                        select += '<option value="">--- Select Article ---</option>';
                        $(data).each(function (index, j) {
                                select += '<option value="' + j.id + '">' + j.title + '</option>';
                            });
                    }

                    $("#article_id").html(select);
				
			}
		});
});

$(document).on('change', '.edition_id1', function() {
  	var d = $(this).val();
  	var catid=$(this).attr("data-catid");
  	var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
	var csrfHash = $('.txt_csrfname').val(); // CSRF hash
		$.ajax({
			url: "<?= base_url('spotlight/getarticle')?>",
			data:{main: d,[csrfName]: csrfHash },
			dataType: "json",
			type: "POST",
			success: function(data){
				//data=JSON.parse(data);
				var select = "";
                    if (data.length == 0) {
                        select += '<option value="">There is no Article</option>';
                    }
                    else {
                        select += '<option value="">--- Select Article ---</option>';
                        $(data).each(function (index, j) {
                                select += '<option value="' + j.id + '">' + j.title + '</option>';
                            });
                    }

                    //$("#article_id1").html(select);
                    $("#"+catid).html(select);
				
			}
		});
});


</script>