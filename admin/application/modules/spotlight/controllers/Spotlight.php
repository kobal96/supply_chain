<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Spotlight extends MX_Controller
{
	
	public function __construct(){
		parent::__construct();
		checklogin();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('spotlightmodel','',TRUE);
	}

	public function index123(){
		$result = array();
		$condition = "1=1";
		$main_table = array("tbl_magazine_articles as tma", array("tma.*"));
		$join_tables =  array();
		$join_tables = array(
							array("left", "tbl_magazine_editions as  tme", "tme.id = tma.edition_id", array("tma.title as edition_name"))
							);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tma.id" => "DESC"),"",null);
		$result['article_data'] = $this->common->MySqlFetchRow($rs, "array");
		
		echo "<pre>";print_r($result);exit;
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function index(){
		$id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];
			$condition = "id ='".$id."' ";
			$result['article_data'] = $this->common->getData("tbl_magazine_articles",'*',$condition);
			$condition = "article_id ='".$id."' ";	
			$article_category = $this->common->getData("tbl_magazine_article_category",'category_id',$condition);
			$article_tag = $this->common->getData("tbl_magazine_article_tag",'tag_id',$condition);

			$article_category_data = array();
			foreach ($article_category as $key => $value) {
				array_push($article_category_data,$value['category_id']);
			}
			$result['article_category_data'] = $article_category_data;

			$article_tag_data = array();
			foreach ($article_tag as $key => $value) {
				array_push($article_tag_data,$value['tag_id']);
			}
			$result['article_tag_data'] = $article_tag_data;
		
		}	
		$condition = " admin_status ='Approved' ";
		$result['editions'] = $this->common->getData("tbl_magazine_editions",'id,title',$condition);
		$result['tags'] = $this->common->getData("tbl_magazine_tags",'id,title',$condition);
		$result['categories'] = $this->common->getData("tbl_magazine_categories",'id,title',$condition,"id","desc");

		$condition = "status ='1' ";
		$result['spotlight'] = $this->common->getData("tbl_spotlight",'*',$condition);

		$condition = "1=1";
		$result['article_data'] = $this->common->getData("tbl_magazine_articles",'*',$condition);

		//echo "<pre>"; print_r($result['categories']); exit;
		
		$this->load->view('main-header.php');
		$this->load->view('index.php',$result);
		$this->load->view('footer.php');
	}

	public function getarticle(){
		$category = $this->input->post('main');

		/*$condition = "edition_id ='".$edition."' ";
		$article_data = $this->common->getData("tbl_magazine_articles",'id,title',$condition);*/

		$condition = "tme.category_id ='".$category."' ";
        $main_table = array("tbl_magazine_articles as tma", array("tma.*"));
		$join_tables =  array();
		$join_tables = array(
							array("", "tbl_magazine_article_category as tme", "tme.article_id = tma.id", array())
							);
	
		$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tma.id" => "DESC"),"",null);
		$article_data = $this->common->MySqlFetchRow($rs, "array");


		echo json_encode($article_data);	
	}

	public function addEdit(){

		$id = "";
		//print_r($_GET);
		$result = array();
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$id = $url_prams['id'];
			$condition = "id ='".$id."' ";
			$result['article_data'] = $this->common->getData("tbl_magazine_articles",'*',$condition);
			$condition = "article_id ='".$id."' ";	
			$article_category = $this->common->getData("tbl_magazine_article_category",'category_id',$condition);
			$article_tag = $this->common->getData("tbl_magazine_article_tag",'tag_id',$condition);

			$article_category_data = array();
			foreach ($article_category as $key => $value) {
				array_push($article_category_data,$value['category_id']);
			}
			$result['article_category_data'] = $article_category_data;

			$article_tag_data = array();
			foreach ($article_tag as $key => $value) {
				array_push($article_tag_data,$value['tag_id']);
			}
			$result['article_tag_data'] = $article_tag_data;
		
		}	
		$condition = " admin_status ='Approved' ";
		$result['editions'] = $this->common->getData("tbl_magazine_editions",'id,title',$condition);
		$result['tags'] = $this->common->getData("tbl_magazine_tags",'id,title',$condition);
		$result['categories'] = $this->common->getData("tbl_magazine_categories",'id,title',$condition);


		
			// echo "<pre>";
			// print_r($result);
			// exit;
		//echo $id;
		$this->load->view('main-header.php');
		$this->load->view('addEdit.php',$result);
		$this->load->view('footer.php');
	}



	function removeFile(){
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$condition = " id = ".$this->input->post('id');
			$exisiting_data =$this->common->getData("tbl_magazine_articles",'*',$condition);
			if(!empty($this->input->post('id')) && $this->input->post('type') == "PDF"){
				if(is_array($exisiting_data) && !empty($exisiting_data[0]['article_pdf']) && file_exists(DOC_ROOT_FRONT."/images/article_pdf/".$exisiting_data[0]['article_pdf'])){
					unlink(DOC_ROOT_FRONT."/images/article_pdf/".$exisiting_data[0]['article_pdf']);
					$data['article_pdf'] = '';
				}
			}else{
				if(is_array($exisiting_data) && !empty($exisiting_data[0]['image']) && file_exists(DOC_ROOT_FRONT."/images/article_image/".$exisiting_data[0]['image'])){
					unlink(DOC_ROOT_FRONT."/images/article_image/".$exisiting_data[0]['image']);
					$data['image'] = '';
				}
			}

			$condition = "id = '".$this->input->post('id')."' ";
			$result = $this->common->updateData("tbl_magazine_articles",$data,$condition);
			if($result){
				echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
				exit;
			}else{
				echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
				exit;
			}	
		}			
	}

	public function submitForm(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data = array();
			$type        = $this->input->post('type');
			$edition_id  = $this->input->post('edition_id');
			$article_id  = $this->input->post('article_id');

			if(!empty($this->input->post('id'))){
				$condition = "id = '".$this->input->post('id')."' ";
				$result = $this->common->updateData("tbl_spotlight",$data,$condition);
				if($result){

					echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
					exit;
				}
				else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while updating data.'));
					exit;
				}
			}else{
				$condition = "status = '1' ";
		        $this->common->deleteRecord("tbl_spotlight",$condition);

				foreach ($type as $key => $value) {
				if ($value=='Featured') {
					$data['type'] = $value;
					$data['category_id'] = $edition_id[$key];
				    $data['article_id'] = $article_id[$key];
				}else{
					$data['type'] = $value;
					$data['category_id'] = $edition_id[$key];
				    $data['article_id'] = $article_id[$key];
				}
			$result = $this->common->insertData('tbl_spotlight',$data,'1');
			}
				
				if(!empty($result)){

					echo json_encode(array('success'=>true, 'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array('success'=>false, 'msg'=>'Problem while adding data.'));
					exit;
				}
			}
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Problem While Add/Edit Data.'));
			exit;
		}
	}
	
		
}
?>