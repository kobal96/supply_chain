<?php require_once('head.php')?>
<?php 

	$this->load->model('common_model/common_model','common',TRUE);
	// $this->load->model('home/home','',TRUE);
	$condition = "sender_id = 1 AND tn.status = 'Unread' ";
	$main_table = array("tbl_notifications as tn", array("tn.notification_type,tn.article_id,tn.comment_id,tn.id as notification_id"));
	$join_tables =  array();
	$join_tables = array(
						array("left", "tbl_magazine_articles as  ta", "ta.id = tn.article_id", array("ta.title as article_title,ta.id as article_id")),
						array("left", "tbl_users as  tu", "tu.id = tn.user_id", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
						array("left", "tbl_magazine_comments as  tmc", "tmc.id = tn.comment_id", array("tmc.id as commented_id")),
						array("left", "tbl_magazine_articles as  ta1", "ta1.id = tmc.article_id", array('ta1.title as commented_article_title,ta1.id as commented_article_id'))
					);

	$rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("tn.id" => "DESC"),"",null); 
		// fetch query
	$notification_admin = $this->common->MySqlFetchRow($rs, "array"); // fetch result
	
	// echo "<pre>";
	// print_r($notification_admin);
	// exit;
?>

<input name="<?= $this->security->get_csrf_token_name()?>" value="<?= $this->security->get_csrf_hash()?>" type="hidden" class="txt_csrfname">

<!-- Wrapper start -- end in footer.php -->
<div class="wrapper">

    <div class="main-header">

        <!-- Logo Header -->

        <div class="logo-header" data-background-color="blue">

            <a href="<?= base_url('dashboard')?>" class="logo">

                <img
                    src="<?= base_url() ?>assets/images/logo.jpg"
                    alt="navbar brand"
                    class="navbar-brand">

            </a>

            <button
                class="navbar-toggler sidenav-toggler ml-auto"
                type="button"
                data-toggle="collapse"
                data-target="collapse"
                aria-expanded="false"
                aria-label="Toggle navigation">

                <span class="navbar-toggler-icon">

                    <i class="icon-menu"></i>

                </span>

            </button>

            <button class="topbar-toggler more">
                <i class="icon-options-vertical"></i>
            </button>

            <div class="nav-toggle">

                <button class="btn btn-toggle toggle-sidebar">

                    <i class="icon-menu"></i>

                </button>

            </div>

        </div>

        <!-- End Logo Header -->

        <!-- Navbar Header -->

        <nav
            class="navbar navbar-header navbar-expand-lg"
            data-background-color="blue2">

            <div class="container-fluid">

                <!-- <div class="collapse" id="search-nav"> <form class="navbar-left navbar-form
                nav-search mr-md-3"> <div class="input-group"> <div class="input-group-prepend">
                <button type="submit" class="btn btn-search pr-1"> <i class="fa fa-search
                search-icon"></i> </button> </div> <input type="text" placeholder="Search ..."
                class="form-control"> </div> </form> </div> -->

                <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">

                    <li class="nav-item toggle-nav-search hidden-caret">

                        <a
                            class="nav-link"
                            data-toggle="collapse"
                            href="#search-nav"
                            role="button"
                            aria-expanded="false"
                            aria-controls="search-nav">

                            <i class="fa fa-search"></i>

                        </a>

                    </li>

                    <li class="nav-item dropdown hidden-caret">

                        <a
                            class="nav-link dropdown-toggle"
                            href="#"
                            id="notifDropdown"
                            role="button"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">

                            <i class="fa fa-bell"></i>
							<?php 
							if(sizeof($notification_admin) != 0){?>
								<span class="notification"><?= sizeof($notification_admin)?></span>
							<?php } ?>

                        </a>

                        <ul
                            class="dropdown-menu notif-box animated fadeIn"
                            aria-labelledby="notifDropdown">

                            <li>
							

                                <div class="dropdown-title">You have <?= sizeof($notification_admin)?> new notification</div>

                            </li>

                            <li>

                                <div class="notif-scroll scrollbar-outer">

                                    <div class="notif-center">



									<?php 
										foreach ($notification_admin as $key => $value) {
											
											if($value['notification_type'] == 'Payment'){?>
											<a href="javascript:void(0)" onclick="change_status(`<?= base_url('subscriber')?>`,`<?= $value['notification_id']?>`)">
												<div class="notif-icon notif-primary">
													<i class="fa fa-user-plus"></i>
												</div>
												<div class="notif-content">
													<span class="block">
														<?= $value['author_name'].' Has subscribed the package'?>
													</span>
													<!-- <span class="time">5 minutes ago</span> -->
												</div>
											</a>
										<?php }else if ($value['notification_type'] == 'Articles') {?>
											<a href="javascript:void(0)" onclick="change_status(`<?= base_url('article/AddEdit?text='.rtrim(strtr(base64_encode("id=".$value['article_id']), '+/', '-_'), '=').'')?>`,`<?= $value['notification_id']?>`)">
												<div class="notif-icon notif-primary">
													<i class="fa fa-user-plus"></i>
												</div>
												<div class="notif-content">
													<span class="block">
														<?= $value['author_name'].' Added a Article <b>'.$value['article_title'].'</b>'?>
													</span>
													<!-- <span class="time">5 minutes ago</span> -->
												</div>
											</a>
										<?php }else if ($value['notification_type'] == 'Comment') {?>
											<a href="javascript:void(0)" onclick="change_status(`<?= base_url('article/comment_status?text='.rtrim(strtr(base64_encode("id=".$value['comment_id']), '+/', '-_'), '=').'')?>`,`<?= $value['notification_id']?>`)">
												<div class="notif-icon notif-primary">
													<i class="fa fa-user-plus"></i>
												</div>
												<div class="notif-content">
													<span class="block">
														<?= $value['author_name'].' Has Commented on Article <b>'.$value['commented_article_title'].'</b>' ?>
													</span>
													<!-- <span class="time">5 minutes ago</span> -->
												</div>
											</a>
										<?php } 
										
										}
									
									?>		
                                        

                                     

                                   

                                    </div>

                                </div>

                            </li>

                            <li>

                                <a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i>
                                </a>

                            </li>

                        </ul>

                    </li>

                    <?php $profile_image = (!empty($this->session->userdata('supply_chain_admin')[0]['profile_photo']))? FRONT_URL.'/images/profile_image/'.$this->session->userdata('supply_chain_admin')[0]['profile_photo']:"https://via.placeholder.com/50"?>

                    <li class="nav-item dropdown hidden-caret">

                        <a
                            class="dropdown-toggle profile-pic"
                            data-toggle="dropdown"
                            href="#"
                            aria-expanded="false">

                            <div class="avatar-sm">

                                <img src="<?= $profile_image?>" alt="..." class="avatar-img rounded-circle">

                            </div>

                        </a>

                        <ul class="dropdown-menu dropdown-user animated fadeIn">

                            <div class="dropdown-user-scroll scrollbar-outer">

                                <li>

                                    <div class="user-box">

                                        <div class="avatar-lg"><img src="<?= $profile_image?>" alt="image profile" class="avatar-img rounded"></div>

                                        <div class="u-text">

                                            <h4><?=  $this->session->userdata('supply_chain_admin')[0]['username']?></h4>

                                            <!-- <p class="text-muted"><?=
                                            $this->session->userdata('supply_chain_admin')[0]['email']?></p><a href="<?=
                                            base_url('profile')?>" class="btn btn-xs btn-secondary btn-sm">View Profile</a>
                                            -->

                                        </div>

                                    </div>

                                </li>

                                <li>

                                    <div class="dropdown-divider"></div>

                                    <!-- <a class="dropdown-item" href="<?= base_url('profile')?>">My Profile</a>
                                    -->

                                    <!-- <a class="dropdown-item" href="#">My Balance</a> -->

                                    <!-- <a class="dropdown-item" href="#">Inbox</a> -->

                                    <!-- <div class="dropdown-divider"></div> -->

                                    <!-- <a class="dropdown-item" href="<?= base_url('profile')?>">Account
                                    Setting</a> -->

                                    <!-- <div class="dropdown-divider"></div> -->

                                    <a class="dropdown-item" href="<?= base_url("home/logOut")?>">Logout</a>

                                </li>

                            </div>

                        </ul>

                    </li>

                </ul>

            </div>

        </nav>

        <!-- End Navbar -->

    </div>

    <?php require_once('sidebar.php')?>


	<script>
	function change_status(url,notification_id){
		// console.log(url);
		// console.log(notification_id);
		if(url && notification_id){
			var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
			var csrfHash = $('.txt_csrfname').val(); // CSRF hash
			$.ajax({
			url: "<?= base_url('home/change_notification_status')?>",
			type: "POST",
			data:{ notification_id, url, [csrfName]: csrfHash },
			dataType: "json",
			success: function(response){
				if(response.success){
					window.location.href = url;
				}else{ 
				swal("Failed", response.msg, {
					icon : "error",
					buttons: {              
					confirm: {
						className : 'btn btn-danger'
					}
					},
				});
				}
				
			}
			}); 
		}

	}
	
	</script>