<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
	<div class="sidebar-wrapper scrollbar scrollbar-inner">
		<div class="sidebar-content">
			<ul class="nav nav-primary">
				
				<li class="nav-item <?php echo($this->uri->segment(1) == "dashboard" || $this->uri->segment(1) =="")?"active":"";?>">
					<a href="<?= base_url('dashboard')?>">
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-dashboard.svg'?>"/>
						<p>Dashboard</p>
					</a>
				</li>
				<li class="nav-item <?php echo($this->uri->segment(1) == "article")?"active":"";?> ">
					<a href="<?= base_url('article')?>">
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-articles.svg'?>"/>
						<p>Articles</p>
					</a>
				</li>
				<?php  ?>
				<!-- <li class="nav-item <?php echo($this->uri->segment(1) == "spotlight")?"active":"";?>">
					<a href="<?= base_url('enquiries')?>">
						<i class="fas fa-comment-alt"></i>
						<p>Home Spotlight</p>
					</a>
				</li> -->
				<li class="nav-item <?php echo($this->uri->segment(1) == "edition")?"active":"";?>">
					<a href="<?= base_url('edition')?>">
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-magazine.svg'?>"/>
						<p>Magazine</p>
					</a>
				</li>
				<li class="nav-item  <?php echo(in_array($this->uri->segment(1),array("package","promo")))?"active":"" ?>">
					<a data-toggle="collapse" href="#subscriptions">
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-subscribe.svg'?>"/>
						<p>Subscriptions</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php echo(in_array($this->uri->segment(1),array("package","promo")))?"show":"" ?>" id="subscriptions">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("package")))?"active":"" ?>">
								<a href="<?= base_url('package')?>">
									<span class="sub-item">Package</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("promo")))?"active":"" ?>">
								<a href="<?= base_url('promo')?>">
									<span class="sub-item">Promo</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item <?php echo($this->uri->segment(1) == "spotlight")?"active":"";?>">
					<a href="<?= base_url('spotlight')?>">
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-spotlight.svg'?>"/>
						<p>Spotlight</p>
					</a>
				</li>
				
				
				<li class="nav-item <?php echo(in_array($this->uri->segment(1),array("users","subscriber","newsletter")))?"active":"" ?>" >
					<a data-toggle="collapse" href="#users">
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-users.svg'?>"/>
						<p>Users</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php echo(in_array($this->uri->segment(1),array("users","subscriber","newsletter")))?"show":"" ?> " id="users">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("users")))?"active":"" ?>">
								<a href="<?= base_url('users');?>">
									<span class="sub-item">Users</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("subscriber")))?"active":"" ?>">
								<a href="<?= base_url('subscriber')?>">
									<span class="sub-item">Subscriber</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("newsletter")))?"active":"" ?>">
								<a href="<?= base_url('newsletter')?>">
									<span class="sub-item">Newsletter</span>
								</a>
							</li>
						
						</ul>
					</div>
				</li>

				<li class="nav-item <?php echo(in_array($this->uri->segment(1),array("user_data","session_data","device_data","geo_location","behavior")))?"active":"" ?>" >
					<a data-toggle="collapse" href="#reports">
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-reports.svg'?>"/>
						<p>Reports</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php echo(in_array($this->uri->segment(1),array("user_data","session_data","device_data","geo_location","behavior")))?"show":"" ?>" id="reports">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("user_data")))?"active":"" ?>">
								<!-- <a href="<?= base_url('user_data');?>"> -->
								<a href="#/">
								<span class="sub-item">User Data</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("session_data")))?"active":"" ?>">
								<!-- <a href="<?= base_url('session_data')?>"> -->
								<a href="#/">
									<span class="sub-item">Session Data</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("device_data")))?"active":"" ?>">
								<!-- <a href="<?= base_url('device_data')?>"> -->
								<a href="#/">
									<span class="sub-item">Device Data</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("geo_location")))?"active":"" ?>">
								<!-- <a href="<?= base_url('geo_location')?>"> -->
								<a href="#/">
									<span class="sub-item">Geo Location</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("behavior")))?"active":"" ?>">
								<!-- <a href="<?= base_url('behavior')?>"> -->
								<a href="#/">
									<span class="sub-item">Behavior</span>
								</a>
							</li>
						
						</ul>
					</div>
				</li>
				<li class="nav-item  <?php echo($this->uri->segment(1) == "event")?"active":"";?>">
					<a href="<?=base_url('event');?>">	
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-news.svg'?>"/>
						<p> Events</p>
					</a>
				</li>
				<li class="nav-item <?php echo(in_array($this->uri->segment(1),array("aboutus","terms_conditions","privacy_policy")))?"active":"" ?>" >
					<a data-toggle="collapse" href="#pages">
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-pages.svg'?>"/>
						<p>Website Pages</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php echo(in_array($this->uri->segment(1),array("aboutus","terms_conditions","privacy_policy")))?"show":"" ?> " id="pages">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("aboutus")))?"active":"" ?>">
								<a href="<?= base_url('aboutus')?>">
									<span class="sub-item">About Us</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("terms_conditions")))?"active":"" ?>">
								<a href="<?= base_url('terms_conditions')?>">
									<span class="sub-item">Terms & Conditions</span>
								</a>
							</li><li class="<?php echo(in_array($this->uri->segment(1),array("privacy_policy")))?"active":"" ?>">
								<a href="<?= base_url('privacy_policy')?>">
									<span class="sub-item">Privacy Policy</span>
								</a>
							</li>
						
						</ul>
					</div>
				</li>
				<li class="nav-item divider-item <?php echo(in_array($this->uri->segment(1),array("magazine_category","masterpolls","tag","seos")))?"active":"" ?>" >
					<a data-toggle="collapse" href="#masters">
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-master.svg'?>"/>
						<p>Master</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php echo(in_array($this->uri->segment(1),array("magazine_category","tag","masterpolls","seos")))?"show":"" ?> " id="masters">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("magazine_category")))?"active":"" ?>">
								<a href="<?= base_url('magazine_category')?>">
									<span class="sub-item">Category</span>
								</a>
							</li>
						
							<li class="<?php echo(in_array($this->uri->segment(1),array("tag")))?"active":"" ?>">
								<a href="<?= base_url('tag')?>">
									<span class="sub-item">Tags</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("masterpolls")))?"active":"" ?>">
								<a href="#/">
									<span class="sub-item">Polls</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("seos")))?"active":"" ?>">
								<a href="<?= base_url('seos')?>">
									<span class="sub-item">Seo</span>
								</a>
							</li>
							
						
						</ul>
					</div>
				</li>
				<!-- <li class="nav-item <?php echo(in_array($this->uri->segment(1),array("cmsusers","myprofile","adminlog")))?"active":"" ?>" >
					<a data-toggle="collapse" href="#settings">
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-settings.svg'?>"/>
						<p>Settings</p>
						<span class="caret"></span>
					</a>
					<div class="collapse <?php echo(in_array($this->uri->segment(1),array("cmsusers","myprofile","adminlog")))?"active":"" ?> " id="settings">
						<ul class="nav nav-collapse">
							<li class="<?php echo(in_array($this->uri->segment(1),array("myprofile")))?"active":"" ?>">
								<a href="#/">
									<span class="sub-item">My Profile</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("cmsusers")))?"active":"" ?>">
								<a href="#/">
									<span class="sub-item">CMS Users</span>
								</a>
							</li>
							<li class="<?php echo(in_array($this->uri->segment(1),array("adminlog")))?"active":"" ?>">
								<a href="#/">
									<span class="sub-item">Admin Log</span>
								</a>
							</li>
							
						
						</ul>
					</div>
				</li> -->
				<li class="nav-item <?php echo($this->uri->segment(1) == "logOut")?"active":"";?>">
					<a href="<?= base_url('home/logOut')?>">
						<img src="<?= FRONT_URL.'/admin/assets/images/menu-logout.svg'?>"/>
						<p>Logout</p>
					</a>
				</li>

			</ul>
		</div>
	</div>
</div>
<!-- End Sidebar -->
