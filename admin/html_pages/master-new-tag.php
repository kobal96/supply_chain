
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
        <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
                    <div class="col-12">
                        <div class="row ff-title-block">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <h1 class="ff-page-title">New Tag</h1> 
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
                                <a href="#" class="btn btn-light btn-border ff-page-title-btn">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </div>					
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="">                                        
																				<div class="form-group">
																					<label for="themename">Tag Name</label>
																					<input type="text" class="form-control" id="themename" placeholder="Early 19th Century">	
																				</div>
																				<div class="form-group">
																					<label for="themedesc">Theme Decription</label>
																					<textarea class="form-control" id="themedesc" rows="5" placeholder="We offer a 24-48 hour turnaround service for masters we hold in our London office. If we need to duplicate the material from Washington, the delays are similar to those you would incur if you went straight to the source."></textarea>	
																				</div>																				
																				<div class="form-check">
																					<label>Activate Category (show this category on blog)</label><br>
																					<div class="switch-field">
																						<input type="radio" id="radio-one" name="switch-one" value="yes" checked/>
																						<label for="radio-one">Yes</label>
																						<input type="radio" id="radio-two" name="switch-one" value="no" />
																						<label for="radio-two">No</label>
																					</div>
																				</div>	
																				<div class="form-group">
																					<button class="btn btn-black">Save</button>
																					<button class="btn btn-cancel btn-border">Cancel</button>
																				</div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
</body>
</html>
