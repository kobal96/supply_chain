
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
    <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">My Profile</h1> 
							</div>	
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
									<a href="all-users.php" class="btn btn-light btn-border ff-page-title-btn">
											Back
									</a>
							</div>						
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
													<li class="nav-item">
														<a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="true">Profile</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" id="pills-security-tab" data-toggle="pill" href="#pills-security" role="tab" aria-controls="pills-security" aria-selected="false">Security</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" id="pills-team-tab" data-toggle="pill" href="#pills-team" role="tab" aria-controls="pills-team" aria-selected="false">Team Members</a>
													</li>												
												</ul>
												<div class="tab-content mt-2 mb-3" id="pills-tabContent">
													<div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
														<div class="row mt-3">
															<div class="col-md-12">
																<div>
																	<span class="h2">Personal Info</span>
																</div>																
																<div class="form-group">																
																	<div class="avatar avatar-xl">
																		<img src="https://via.placeholder.com/50" alt="..." class="avatar-img rounded-circle">
																	</div>	
																</div>
															</div>
															<div class="col-md-4">																
																<div class="form-group">
																	<label for="username">Name</label>
																	<input type="text" class="form-control" id="username" placeholder="Jimmy Spencer">	
																</div>
																<div class="form-group">
																	<label for="email">Email</label>
																	<input type="email" class="form-control" id="email" placeholder="jimmy-spenser@hotmail.com">	
																</div>																
															</div>
															<div class="col-md-4">
																<div class="form-group">
																	<label for="gender">Gender</label>
																	<input type="text" class="form-control" id="gender" placeholder="Male">	
																</div>
																<div class="form-group">
																	<label for="dob">Phone</label>
																	<input type="text" class="form-control" id="dob" placeholder="+91- 123-456-7890">	
																</div>		
															</div>
															<div class="col-md-4">
																<div class="form-group">
																	<label for="dob">Date of Birth</label>
																	<input type="text" class="form-control" id="dob" placeholder="04 March,1981 ">	
																</div>																
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																		<button class="btn btn-black">Save</button>
																		<button class="btn btn-cancel btn-border">Cancel</button>
																	</div>
															</div>																	
														</div>														
													</div>
													<div class="tab-pane fade" id="pills-security" role="tabpanel" aria-labelledby="pills-security-tab">
														<div class="row mt-3">
															<div class="col-md-12">
																<div>
																	<span class="h2">Security</span>
																</div>	
															</div>
															<div class="col-md-4">																
																<div class="form-group">
																	<label for="currentpass">Current Password</label>
																	<input type="password" class="form-control" id="currentpass" placeholder="Enter current password">	
																</div>
																<div class="form-group">
																	<label for="newpass">New Password</label>
																	<input type="password" class="form-control" id="newpass" placeholder="Enter new password">	
																</div>																
															</div>															
															<div class="col-sm-12">
																<div class="form-group">
																		<button class="btn btn-black">Save</button>
																		<button class="btn btn-cancel btn-border">Cancel</button>
																	</div>
															</div>																	
														</div>
													</div>
													<div class="tab-pane fade" id="pills-team" role="tabpanel" aria-labelledby="pills-team-tab">
														<div class="row mt-3">
															<div class="col-md-12">
																<div>
																	<span class="h2">Collaborators</span>
																</div>	
															</div>
															</div>
															<div class="row mt-3">
																<div class="col-sm-6 col-md-6">
																	<div class="card card-round">
																		<div class="card-body ">
																			<div class="row">
																				<div class="col-12 col-sm-7">
																					<h4>Adrian Tucker</h4>
																					<p>adrian_tucker@footagefarm.com</p>
																				</div>
																				<div class="col-12 col-sm-3">
																						<div class="form-group" style="padding:0;">
																							<label for="role">Role</label><br/>																						
																							<select name="statusselect" id="statusselect">
																								<option value="">Editor</option>
																								<option value="">Owner</option>
																								<option value="">Viewer</option>
																							</select> 
																						</div>
																				</div>
																				<div class="col-12 col-sm-2">
																					<a href="#/" class="delete-team-member">
																						<i class="fas fa-trash-alt fa-lg"></i>
																					</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6 col-md-6">
																	<div class="card card-round">
																		<div class="card-body ">
																			<div class="row">
																				<div class="col-12 col-sm-7">
																					<h4>Adrian Tucker</h4>
																					<p>adrian_tucker@footagefarm.com</p>
																				</div>
																				<div class="col-12 col-sm-3">
																						<div class="form-group" style="padding:0;">
																							<label for="role">Role</label><br/>																						
																							<select name="statusselect" id="statusselect">
																								<option value="">Editor</option>
																								<option value="">Owner</option>
																								<option value="">Viewer</option>
																							</select> 
																						</div>
																				</div>
																				<div class="col-12 col-sm-2">
																					<a href="#/" class="delete-team-member">
																						<i class="fas fa-trash-alt fa-lg"></i>
																					</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6 col-md-6">
																	<div class="card card-round">
																		<div class="card-body ">
																			<div class="row">
																				<div class="col-12 col-sm-7">
																					<h4>Adrian Tucker</h4>
																					<p>adrian_tucker@footagefarm.com</p>
																				</div>
																				<div class="col-12 col-sm-3">
																						<div class="form-group" style="padding:0;">
																							<label for="role">Role</label><br/>																						
																							<select name="statusselect" id="statusselect">
																								<option value="">Editor</option>
																								<option value="">Owner</option>
																								<option value="">Viewer</option>
																							</select> 
																						</div>
																				</div>
																				<div class="col-12 col-sm-2">
																					<a href="#/" class="delete-team-member">
																						<i class="fas fa-trash-alt fa-lg"></i>
																					</a>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>	
																<div class="col-sm-12">																	
																	<a data-fancybox="" data-src="#add-team-member" href="javascript:;" class="btn btn-primary ml-2">
																			<i class="fas fa-plus"></i> 
																			Add Team Member					
																	</a>																	
																</div>														
																<div class="col-sm-12">
																	<div class="form-group">
																		<button class="btn btn-black">Save</button>
																		<button class="btn btn-cancel btn-border">Cancel</button>
																	</div>
																</div>																	
														</div>
													</div>													
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>			
		</div>						
	</div>
	<!-- Add Team Member -->
	<div style="display: none;" id="add-team-member" class="ff-popup">
		<div class="ff-popup-title"><span class="h2">Invite Collaborator</span></div>		
		<div class="card">
			<div class="card-body">
				<div class="form-group">
					<label for="selectrole">Select Role</label>
					<select class="form-control" id="selectrole">
						<option>Select</option>
						<option value="">Editor</option>
						<option value="">Owner</option>
						<option value="">Viewer</option>
					</select>
				</div>
				<div class="form-group">
					<label for="collabemail">Email</label>
					<input type="email" class="form-control" id="collabemail" placeholder="Enter email id">
				</div>
				<div class="form-group">
					<button class="btn btn-black">Save</button>
					<button class="btn btn-cancel btn-border">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Add Team Member -->
	<?php require_once('include/footer.php')?>	
	<script >
		$(document).ready(function() {
			
			$('#user-projects').DataTable({
				"pageLength": 5,
			});	
			
			$('#user-enquiry').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
