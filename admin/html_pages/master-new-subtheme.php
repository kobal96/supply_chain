
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
        <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
                    <div class="col-12">
                        <div class="row ff-title-block">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                <h1 class="ff-page-title">New Sub-Theme</h1> 
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
                                <a href="#" class="btn btn-light btn-border ff-page-title-btn">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </div>					
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <form action="">
                                        <div class="form-group">
                                            <label for="exampleFormControlFile1">Sub-Theme Thumbnail Image</label>
                                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
																				</div>
																				<div class="form-group">
																					<label for="themename">Sub-Theme Name</label>
																					<input type="text" class="form-control" id="themename" placeholder="Early 19th Century">	
																				</div>
																				<div class="form-group">
																					<label for="themedesc">Sub-Theme Decription (Optional)</label>
																					<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
																				</div>
																				<div class="form-group">
																					<label for="mainvideo">Sub-Theme Main video (on all the banners)</label>
																					<input type="text" class="form-control" id="mainvideo" placeholder="Enter url (eg. https://vimeo.com/55900216)">	
																				</div>
																				<div class="form-group">
																					<label for="themename">Meta Title</label>
																					<input type="text" class="form-control" id="themename" placeholder="Early 19th Century">	
																				</div>
																				<div class="form-group">
																					<label for="themedesc">Meta Description</label>
																					<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. A surprise military strike by the Imperial Japanese Navy Air Service upon the United States against the naval base at Pearl Harbor."></textarea>	
																				</div>																				
																				<div class="form-check">
																					<label>Activate theme (show this theme on website)</label><br>
																					<div class="switch-field">
																						<input type="radio" id="radio-one" name="switch-one" value="yes" checked/>
																						<label for="radio-one">Yes</label>
																						<input type="radio" id="radio-two" name="switch-one" value="no" />
																						<label for="radio-two">No</label>
																					</div>
																				</div>	
																				<div class="form-group">
																					<button class="btn btn-black">Create</button>
																					<button class="btn btn-cancel btn-border">Cancel</button>
																				</div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
</body>
</html>
