<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
        <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">New FAQ</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
									<a href="#/" class="btn btn-light btn-border ff-page-title-btn">
											Cancel
									</a>
							</div>
						</div>
					</div>					
					<div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-body">
								<form action="">                                        
										<div class="form-group">
											<label for="themename">Question</label>
											<textarea class="form-control" id="themedesc" rows="2" placeholder="eg. How long does it take to obtain material from you?"></textarea>	
										</div>										
										<div class="form-group">
											<label for="themedesc">Answer</label>
											<textarea class="form-control" id="themedesc" rows="5" placeholder="eg. We offer a 24-48 hour turnaround service for masters we hold in our London office. If we need to duplicate the material from Washington, the delays are similar to those you would incur if you went straight to the source."></textarea>	
										</div>																				
										<div class="form-check">
											<label>Activate FAQ</label><br>
											<div class="switch-field">
												<input type="radio" id="radio-three" name="switch-two" value="yes" checked/>
												<label for="radio-three">Yes</label>
												<input type="radio" id="radio-four" name="switch-two" value="no" />
												<label for="radio-four">No</label>
											</div>
										</div>	
										<div class="form-group">
											<button class="btn btn-black">Save</button>
											<button class="btn btn-cancel btn-border">Cancel</button>
										</div>
								</form>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
</body>
</html>
