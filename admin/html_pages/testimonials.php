
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
        <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">Testimonials</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">  
									<a href="add-testimonial.php" class="btn btn-primary ml-2 ff-page-title-btn">
											<i class="fas fa-plus"></i> 
											New Testimonial					
									</a>	
							</div>
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>
																<th>Name</th>
																<th>Designation/Company</th>																
																<th>Testimonial Text</th>
																<th>Created On</th>
																<th>Updated On</th>
																<th>Status</th>	
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>Name</th>
																<th>Designation/Company</th>																
																<th>Testimonial Text</th>
																<th>Created On</th>
																<th>Updated On</th>
																<th>Status</th>	
																<th>Actions</th>															
															</tr>
														</tfoot>
														<tbody>
															<tr>																
																<td>Jerry Wolfe</td>
																<td>CFO, Framer</td>
																<td>Lorem ipsum dolor sit amet, co…</td>
																<td>12 Aug, 20</td>
																<td>12 Aug, 20</td>		
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															<tr>
																<td>Warren Bass</td>
																<td>SVP and GM, Core Dr…</td>
																<td>Lorem ipsum dolor sit amet, co…</td>
																<td>10 Aug, 20</td>
																<td>10 Aug, 20</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Lucile Ford</td>
																<td>Former CFO, NIKE</td>
																<td>Lorem ipsum dolor sit amet, co…</td>
																<td>02 Jul, 20</td>
																<td>02 Jul, 20</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Ann Schmidt</td>
																<td>Board Member and For…</td>
																<td>Lorem ipsum dolor sit amet, co…</td>
																<td>02 Jul, 20</td>
																<td>02 Jul, 20</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Elnora Chavez</td>
																<td>Former CFO, Priceline</td>
																<td>Lorem ipsum dolor sit amet, co…</td>
																<td>21 Mar, 20</td>
																<td>21 Mar, 20</td>
																<td>Inactive</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>																
																<td>Jerry Wolfe</td>
																<td>CFO, Framer</td>
																<td>Lorem ipsum dolor sit amet, co…</td>
																<td>12 Aug, 20</td>
																<td>12 Aug, 20</td>		
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>													
															</tr>
															<tr>
																<td>Warren Bass</td>
																<td>SVP and GM, Core Dr…</td>
																<td>Lorem ipsum dolor sit amet, co…</td>
																<td>10 Aug, 20</td>
																<td>10 Aug, 20</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Lucile Ford</td>
																<td>Former CFO, NIKE</td>
																<td>Lorem ipsum dolor sit amet, co…</td>
																<td>02 Jul, 20</td>
																<td>02 Jul, 20</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Ann Schmidt</td>
																<td>Board Member and For…</td>
																<td>Lorem ipsum dolor sit amet, co…</td>
																<td>02 Jul, 20</td>
																<td>02 Jul, 20</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Elnora Chavez</td>
																<td>Former CFO, Priceline</td>
																<td>Lorem ipsum dolor sit amet, co…</td>
																<td>21 Mar, 20</td>
																<td>21 Mar, 20</td>
																<td>Inactive</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
