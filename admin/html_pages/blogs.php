
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
        <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">Blogs</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">  
									<a href="add-blog.php" class="btn btn-primary ml-2 ff-page-title-btn">
											<i class="fas fa-plus"></i> 
											New Blog					
									</a>	
							</div>
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>																
																<th style="width:60px"></th>																
																<th>Title</th>																
																<th>Created at</th>	
																<th>Published on</th>	
																<th>Status</th>		
																<th>Actions</th>												
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th></th>																
																<th>Title</th>																
																<th>Created at</th>	
																<th>Published on</th>	
																<th>Status</th>		
																<th>Actions</th>																	
															</tr>
														</tfoot>
														<tbody>
															<tr>																
																<td><img src="assets/images/blog-thumb.jpg" alt="" class="blog-list-thumb"></td>
																<td>How Recursion Works — explained with flowcharts and a video	</td>
																<td>12 Mar, 2020</td>
																<td>12 Mar, 2020</td>
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>																											
															</tr>
															<tr>																
																<td><img src="assets/images/blog-thumb.jpg" alt="" class="blog-list-thumb"></td>
																<td>10 examples of emerging technologies that are revolutionizing the….</td>
																<td>24 Mar, 2020</td>
																<td>27 Mar, 2020</td>
																<td>Draft</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>																											
															</tr>
															<tr>																
																<td><img src="assets/images/blog-thumb.jpg" alt="" class="blog-list-thumb"></td>
																<td>5 ways for publishers to cut through the noise with social video</td>
																<td>30 Mar, 2020</td>
																<td>01 Mar, 2020</td>
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>																											
															</tr>
															<tr>																
																<td><img src="assets/images/blog-thumb.jpg" alt="" class="blog-list-thumb"></td>
																<td>B2B marketing strategies for connecting with people at work with video…</td>
																<td>01 Apr, 2020</td>
																<td>01 Apr, 2020</td>
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>																											
															</tr>
															<tr>																
																<td><img src="assets/images/blog-thumb.jpg" alt="" class="blog-list-thumb"></td>
																<td>Why video is crucial to get your tech company off the ground</td>
																<td>07 Apr, 2020</td>
																<td>11 Apr, 2020</td>
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>																											
															</tr>
															<tr>																
																<td><img src="assets/images/blog-thumb.jpg" alt="" class="blog-list-thumb"></td>
																<td>5 video scripts you can use to quickly promote and recap every event</td>
																<td>11 Apr, 2020</td>
																<td>11 Apr, 2020</td>
																<td>Draft</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>																											
															</tr>
																														
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
