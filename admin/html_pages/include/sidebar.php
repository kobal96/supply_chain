<!-- Sidebar -->
<div class="sidebar sidebar-style-2">
	<div class="sidebar-wrapper scrollbar scrollbar-inner">
		<div class="sidebar-content">
			<ul class="nav nav-primary">
				<li class="nav-item active">
					<a href="all-videos.php" aria-expanded="false">
						<i class="fas fa-plus"></i>
						<p>Add Videos</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="index.php">
						<i class="fas fa-chart-line"></i>
						<p>Dashboard</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="all-videos.php">
						<i class="fas fa-video"></i>
						<p>All Videos</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="all-enquiries.php">
						<i class="fas fa-comment-alt"></i>
						<p>All Enquiries</p>
					</a>
				</li>
				<li class="nav-item">
					<a data-toggle="collapse" href="#website">
						<i class="fas fa-window-maximize"></i>
						<p>Website Pages</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="website">
						<ul class="nav nav-collapse">
							<li>
								<a href="blogs.php">
									<span class="sub-item">Blog</span>
								</a>
							</li>
							<li>
								<a href="about-us.php">
									<span class="sub-item">About Us</span>
								</a>
							</li>
							<li>
								<a href="faqs.php">
									<span class="sub-item">FAQs</span>
								</a>
							</li>
							<li>
								<a href="contact-us.php">
									<span class="sub-item">Contact Us</span>
								</a>
							</li>
							<li>
								<a href="#/">
									<span class="sub-item">Homepage Block</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item">
					<a href="testimonials.php">
						<i class="fas fa-comment-dots"></i>
						<p>Testimonials</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="all-users.php">
						<i class="fas fa-users"></i>
						<p>All Users</p>
					</a>
				</li>
				<li class="nav-item">
					<a data-toggle="collapse" href="#master">
						<i class="fas fa-layer-group"></i>
						<p>Master</p>
						<span class="caret"></span>
					</a>
					<div class="collapse" id="master">
						<ul class="nav nav-collapse">
							<li>
								<a href="master-new-tag.php">
									<span class="sub-item">Tags</span>
								</a>
							</li>
							<li>
								<a href="master-new-theme.php">
									<span class="sub-item">Theme Master</span>
								</a>
							</li>
							<li>
								<a href="master-new-subtheme.php">
									<span class="sub-item">Sub Theme Master</span>
								</a>
							</li>
							<li>
								<a href="master-new-video.php">
									<span class="sub-item">Video Master</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="nav-item">
					<a href="admin-profile.php">
						<i class="fas fa-address-card"></i>
						<p>Profile</p>
					</a>
				</li>
				<li class="nav-item">
					<a href="login.php">
						<i class="fas fa-sign-out-alt"></i>
						<p>Logout</p>
					</a>
				</li>

			</ul>
		</div>
	</div>
</div>
<!-- End Sidebar -->