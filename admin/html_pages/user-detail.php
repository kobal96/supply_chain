
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
    <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">Jimmy Spencer</h1> 
							</div>	
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">                                
									<a href="all-users.php" class="btn btn-light btn-border ff-page-title-btn">
											Back
									</a>
							</div>						
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
													<li class="nav-item">
														<a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="true">Profile</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" id="pills-projects-tab" data-toggle="pill" href="#pills-projects" role="tab" aria-controls="pills-projects" aria-selected="false">Projects</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" id="pills-enquiry-tab" data-toggle="pill" href="#pills-enquiry" role="tab" aria-controls="pills-enquiry" aria-selected="false">Enquiry</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" id="pills-favourites-tab" data-toggle="pill" href="#pills-favourites" role="tab" aria-controls="pills-contact" aria-selected="false">Favourites</a>
													</li>
												</ul>
												<div class="tab-content mt-2 mb-3" id="pills-tabContent">
													<div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
														<div class="row mt-3">
															<div class="col-md-12">
																<div>
																	<span class="h2">Personal Info</span>
																</div>																
																<div class="form-group">																
																	<div class="avatar avatar-xl">
																		<img src="https://via.placeholder.com/50" alt="..." class="avatar-img rounded-circle">
																	</div>	
																</div>
															</div>
															<div class="col-md-4">																
																<div class="form-group">
																	<label for="username">Name</label>
																	<input type="text" class="form-control" id="username" placeholder="Jimmy Spencer">	
																</div>
																<div class="form-group">
																	<label for="email">Email</label>
																	<input type="email" class="form-control" id="email" placeholder="jimmy-spenser@hotmail.com">	
																</div>																
															</div>
															<div class="col-md-4">
																<div class="form-group">
																	<label for="gender">Gender</label>
																	<input type="text" class="form-control" id="gender" placeholder="Male">	
																</div>
																<div class="form-group">
																	<label for="dob">Phone</label>
																	<input type="text" class="form-control" id="dob" placeholder="+91- 123-456-7890">	
																</div>		
															</div>
															<div class="col-md-4">
																<div class="form-group">
																	<label for="dob">Date of Birth</label>
																	<input type="text" class="form-control" id="dob" placeholder="04 March,1981 ">	
																</div>																
															</div>																	
														</div>														
													</div>
													<div class="tab-pane fade" id="pills-projects" role="tabpanel" aria-labelledby="pills-projects-tab">
														<div class="table-responsive">
															<table id="user-projects" class="display table table-striped table-hover" >
																<thead>
																	<tr>
																		<th>Name</th>
																		<th>No. of Videos</th>		
																		<th>Created On</th>
																		<th>Updated On</th>
																		<th>Status</th>	
																		<th>Actions</th>															
																	</tr>
																</thead>
																<tfoot>
																	<tr>
																		<th>Name</th>
																		<th>No. of Videos</th>	
																		<th>Created On</th>
																		<th>Updated On</th>
																		<th>Status</th>	
																		<th>Actions</th>														
																	</tr>
																</tfoot>
																<tbody>
																	<tr>																
																		<td>Name of project will be displayed here, should not…</td>
																		<td>81</td>
																		<td>12 Aug, 20</td>
																		<td>12 Aug, 20</td>
																		<td>Active</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	<tr>																
																		<td>Name of project will be displayed here, should not…</td>
																		<td>81</td>
																		<td>12 Aug, 20</td>
																		<td>12 Aug, 20</td>
																		<td>Active</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	<tr>																
																		<td>Name of project will be displayed here, should not…</td>
																		<td>81</td>
																		<td>12 Aug, 20</td>
																		<td>12 Aug, 20</td>
																		<td>Active</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	<tr>																
																		<td>Name of project will be displayed here, should not…</td>
																		<td>81</td>
																		<td>12 Aug, 20</td>
																		<td>12 Aug, 20</td>
																		<td>Active</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	<tr>																
																		<td>Name of project will be displayed here, should not…</td>
																		<td>81</td>
																		<td>12 Aug, 20</td>
																		<td>12 Aug, 20</td>
																		<td>Active</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	<tr>																
																		<td>Name of project will be displayed here, should not…</td>
																		<td>81</td>
																		<td>12 Aug, 20</td>
																		<td>12 Aug, 20</td>
																		<td>Active</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>																	
																</tbody>
															</table>
														</div>
													</div>
													<div class="tab-pane fade" id="pills-enquiry" role="tabpanel" aria-labelledby="pills-enquiry-tab">
													<div class="table-responsive">
															<table id="user-enquiry" class="display table table-striped table-hover" >
																<thead>
																	<tr>
																		<th>Inquiry ID</th>
																		<th>Name of person</th>		
																		<th>Email</th>
																		<th>No. Videos</th>
																		<th>Inquiry Date</th>	
																		<th>Actions</th>															
																	</tr>
																</thead>
																<tfoot>
																	<tr>
																		<th>Inquiry ID</th>
																		<th>Name of person</th>		
																		<th>Email</th>
																		<th>No. Videos</th>
																		<th>Inquiry Date</th>	
																		<th>Actions</th>															
																	</tr>
																</tfoot>
																<tbody>
																	<tr>																
																		<td>#2121</td>
																		<td>Pauline Caldwell</td>
																		<td>pauline_Caldwell@gmail.com</td>
																		<td>12</td>
																		<td>12 Aug, 20</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="user-detail-enquiry.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="View Details">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	<tr>																
																		<td>#2121</td>
																		<td>Pauline Caldwell</td>
																		<td>pauline_Caldwell@gmail.com</td>
																		<td>12</td>
																		<td>12 Aug, 20</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="View Details">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	<tr>																
																		<td>#2121</td>
																		<td>Pauline Caldwell</td>
																		<td>pauline_Caldwell@gmail.com</td>
																		<td>12</td>
																		<td>12 Aug, 20</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="user-detail-enquiry.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="View Details">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	<tr>																
																		<td>#2121</td>
																		<td>Pauline Caldwell</td>
																		<td>pauline_Caldwell@gmail.com</td>
																		<td>12</td>
																		<td>12 Aug, 20</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="user-detail-enquiry.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="View Details">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																	<tr>																
																		<td>#2121</td>
																		<td>Pauline Caldwell</td>
																		<td>pauline_Caldwell@gmail.com</td>
																		<td>12</td>
																		<td>12 Aug, 20</td>	
																		<td>
																			<div class="form-button-action">
																				<a href="user-detail-enquiry.php" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="View Details">
																					<i class="fa fa-edit"></i>
																				</a>
																				<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																					<i class="fa fa-times"></i>
																				</a>
																			</div>
																		</td>													
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<div class="tab-pane fade" id="pills-favourites" role="tabpanel" aria-labelledby="pills-favourites-tab">
														<div class="row row-projects">
															<div class="col-sm-6 col-lg-3">
																<div class="card project-card">
																	<a href="#/">
																		<div class="p-2">
																			<img class="card-img-top rounded" src="https://via.placeholder.com/150x100" alt="Product 1">
																		</div>
																		<div class="card-body pt-2">
																			<h4 class="mb-1 fw-bold">Name of project will be displayed here, should …</h4>
																			<p class="text-muted small mb-2">Reel No: 583458854285 </p>	
																		</div>
																	</a>																	
																</div>
															</div>
															<div class="col-sm-6 col-lg-3">
																<div class="card project-card">
																	<a href="#/">
																		<div class="p-2">
																			<img class="card-img-top rounded" src="https://via.placeholder.com/150x100" alt="Product 1">
																		</div>
																		<div class="card-body pt-2">
																			<h4 class="mb-1 fw-bold">Name of project will be displayed here, should …</h4>
																			<p class="text-muted small mb-2">Reel No: 583458854285 </p>	
																		</div>
																	</a>																	
																</div>
															</div>
															<div class="col-sm-6 col-lg-3">
																<div class="card project-card">
																	<a href="#/">
																		<div class="p-2">
																			<img class="card-img-top rounded" src="https://via.placeholder.com/150x100" alt="Product 1">
																		</div>
																		<div class="card-body pt-2">
																			<h4 class="mb-1 fw-bold">Name of project will be displayed here, should …</h4>
																			<p class="text-muted small mb-2">Reel No: 583458854285 </p>	
																		</div>
																	</a>																	
																</div>
															</div>
															<div class="col-sm-6 col-lg-3">
																<div class="card project-card">
																	<a href="#/">
																		<div class="p-2">
																			<img class="card-img-top rounded" src="https://via.placeholder.com/150x100" alt="Product 1">
																		</div>
																		<div class="card-body pt-2">
																			<h4 class="mb-1 fw-bold">Name of project will be displayed here, should …</h4>
																			<p class="text-muted small mb-2">Reel No: 583458854285 </p>	
																		</div>
																	</a>																	
																</div>
															</div>
															<div class="col-sm-6 col-lg-3">
																<div class="card project-card">
																	<a href="#/">
																		<div class="p-2">
																			<img class="card-img-top rounded" src="https://via.placeholder.com/150x100" alt="Product 1">
																		</div>
																		<div class="card-body pt-2">
																			<h4 class="mb-1 fw-bold">Name of project will be displayed here, should …</h4>
																			<p class="text-muted small mb-2">Reel No: 583458854285 </p>	
																		</div>
																	</a>																	
																</div>
															</div>
															<div class="col-sm-6 col-lg-3">
																<div class="card project-card">
																	<a href="#/">
																		<div class="p-2">
																			<img class="card-img-top rounded" src="https://via.placeholder.com/150x100" alt="Product 1">
																		</div>
																		<div class="card-body pt-2">
																			<h4 class="mb-1 fw-bold">Name of project will be displayed here, should …</h4>
																			<p class="text-muted small mb-2">Reel No: 583458854285 </p>	
																		</div>
																	</a>																	
																</div>
															</div>
															<div class="col-sm-6 col-lg-3">
																<div class="card project-card">
																	<a href="#/">
																		<div class="p-2">
																			<img class="card-img-top rounded" src="https://via.placeholder.com/150x100" alt="Product 1">
																		</div>
																		<div class="card-body pt-2">
																			<h4 class="mb-1 fw-bold">Name of project will be displayed here, should …</h4>
																			<p class="text-muted small mb-2">Reel No: 583458854285 </p>	
																		</div>
																	</a>																	
																</div>
															</div>
															<div class="col-sm-6 col-lg-3">
																<div class="card project-card">
																	<a href="#/">
																		<div class="p-2">
																			<img class="card-img-top rounded" src="https://via.placeholder.com/150x100" alt="Product 1">
																		</div>
																		<div class="card-body pt-2">
																			<h4 class="mb-1 fw-bold">Name of project will be displayed here, should …</h4>
																			<p class="text-muted small mb-2">Reel No: 583458854285 </p>	
																		</div>
																	</a>																	
																</div>
															</div>
																
														</div>
													</div>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>	
	<script >
		$(document).ready(function() {
			
			$('#user-projects').DataTable({
				"pageLength": 5,
			});	
			
			$('#user-enquiry').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
