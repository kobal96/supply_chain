
<?php require_once('include/head.php')?>
<body>
	<div class="wrapper">
        <?php require_once('include/main-header.php')?>
		<?php require_once('include/sidebar.php')?>
		<div class="main-panel">
			<div class="container">
				<div class="page-inner">
					<div class="col-12">
						<div class="row ff-title-block">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
									<h1 class="ff-page-title">FAQs</h1> 
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">  
									<a href="add-faq.php" class="btn btn-primary ml-2 ff-page-title-btn">
											<i class="fas fa-plus"></i> 
											New FAQ					
									</a>	
							</div>
						</div>
					</div>					
					<div class="row">
							<div class="col-sm-12">
									<div class="card">
											<div class="card-body">
												<div class="table-responsive">
													<table id="add-row" class="display table table-striped table-hover" >
														<thead>
															<tr>																
																<th>Question</th>																
																<th>Answer</th>																
																<th>Status</th>	
																<th>Actions</th>															
															</tr>
														</thead>
														<tfoot>
															<tr>
																<th>Question</th>																
																<th>Answer</th>																
																<th>Status</th>	
																<th>Actions</th>																	
															</tr>
														</tfoot>
														<tbody>
															<tr>																
																<td>What is public domain material?</td>
																<td>This is footage that is not in copyright and is not subject to the usual licensing constrictions and prices…</td>
																<td>Active</td>	
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>																											
															</tr>
															<tr>
																<td>What is public domain material? Where does it come from?</td>
																<td>Our footage is largely acquired from Themes in the United States (The National Archives, The Library…</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>What type of subjects?</td>
																<td>The material spans nearly 100 years (late 19th century and throughout most of the 20th) and covers all w…</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Can I reuse the material?</td>
																<td>Yes. We charge a one-time buy-out fee. Once you have bought the physical tape, it is yours to use and re…</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>How is it priced?</td>
																<td>For easy pricing, our pricing structure parallels the costs at the National Archives in Washington in that…</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>I already use NARA database. Why do I need Footage Farm?</td>
																<td>Although the National Archives in Washington DC do have a website, only a fraction of their…</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>
															<tr>
																<td>Do you have a catalogue?</td>
																<td>No. In order to keep our costs low, we ask you to tell us about the type of material you are looking for by…</td>
																<td>Active</td>
																<td>
																	<div class="form-button-action">
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-primary  btn-edit" data-original-title="Edit">
																			<i class="fa fa-edit"></i>
																		</a>
																		<a href="#/" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="fa fa-times"></i>
																		</a>
																	</div>
																</td>
															</tr>															
														</tbody>
													</table>
												</div>
											</div>
									</div>
							</div>
					</div>
				</div>
			</div>
			
		</div>						
	</div>
	<?php require_once('include/footer.php')?>
	<script >
		$(document).ready(function() {
			// Add Row
			$('#add-row').DataTable({
				"pageLength": 5,
			});			
		});
	</script>
</body>
</html>
