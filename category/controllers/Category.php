<?php
class Category  extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_model/common_model','common',TRUE);
		$this->load->model('categorymodel','',TRUE);
		parent::__construct();
		
    }

    function _remap($method_name){
		if(!method_exists($this, $method_name)){
			$this->index();
		}else{
			$this->{$method_name}();
		}
    }
    
    
    public function index(){

        $result = array();
         // get all category 
         $condition = " status = 'Published' && admin_status ='Approved' ";
         $result['all_categories'] = $this->common->getData("tbl_magazine_categories",'*',$condition);

        if($this->uri->segment(2)){
            $condition = " status = 'Published' && admin_status ='Approved' AND slug = '".$this->uri->segment(2)."'  ";
            $result['category_id'] = $this->common->getData("tbl_magazine_categories",'id',$condition);
          
            $result['meta_description'] = "meta description";
            $result['meta_keywords'] = "meta keywords";
            $result['meta_title'] = "meta title";
               // echo "<pre>";
            // print_r($result);
            // exit;

            $this->load->view('header',$result);
            $this->load->view('index',$result);
            $this->load->view('footer');
        }else{
            
            $result['meta_description'] = "meta description";
            $result['meta_keywords'] = "meta keywords";
            $result['meta_title'] = "meta title";
         
            $this->load->view('header',$result);
            $this->load->view('index',$result);
            $this->load->view('footer');
        }
    }

    function ArticleListing(){
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

            $condition = '1=1';
            if(!empty($_POST['search_value']) && isset($_POST['search_value'])){
                $condition .=" && a.title like '%".$_POST['search_value']."%' " ;
            }

            if(!empty($_POST['category_id']) && isset($_POST['category_id'])){
                $condition .=" && category_id = ".$_POST['category_id']." " ;
            }

            $condition .=" && a.status = 'Published' && a.admin_status ='Approved'  ";
            $main_table = array("tbl_magazine_article_category as ac", array());
            $join_tables =  array();
            $join_tables = array(
                    array("", "tbl_magazine_articles as  a", "ac.article_id = a.id", array("a.*")),
                    array("left", "tbl_users as  tu", "tu.id = a.author_id", array("concat(tu.first_name,' ',tu.last_name) as author_name")),
                    array("", "tbl_magazine_categories as  c", "c.id = ac.category_id", array("c.title as category_name")),
                      );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, array("a.id" => "DESC"),"a.id",null); 
            $result['article_data_listing'] = $this->common->MySqlFetchRow($rs, "array");

      
            
            $category_html = $this->load->view('category-listing-block',$result,true);
            if(!empty($category_html)){
                echo json_encode(array('success'=>true, 'msg'=>'category fetch Successfully.' , "html"=>$category_html));
                exit;
            }else{
                echo json_encode(array('success'=>false, 'msg'=>'Problem while fetching category data.'));
                exit;
            }
        }
    }
}

?>