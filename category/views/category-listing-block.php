
<?php
if($article_data_listing){
    foreach ($article_data_listing as $key => $value) { ?>
       <div class="col-12 col-sm-6 col-md-4">
        <div class="hps-single">
          <a href="#/">
            <img src="<?= base_url('images/article_image/'.$value['image'])?>" alt="" class="img-fluid">
            <div class="hps-content">
              <p class="category-tag"><?= $value['category_name']?></p>
              <h4><?= $value['title']?></h4>
              <div class="article-author-info">
                <p>by <span class="author-name"><?=(!empty($value['author_name'])?$value['author_name']:$value['author'])?></span></p>
                <p><span class="article-date"><?= date('d-M,Y',strtotime($value['updated_at']))?></span> <span class="reading-time"></span></p>
              </div>
            </div>
          </a>
        </div>
      </div>
    <?php  }
    }else{?>
       <h4>No Article Found for this Category ....sorry!! </h4>
    <?php }?>


